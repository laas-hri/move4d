//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_MDL_TYPES_H
#define MOVE4D_MDL_TYPES_H

#include <utility>

#include <memory>
#include <string>
#include <vector>

#include "move4d/common.h"

namespace rl
{
namespace sg
{
class Model;
}
namespace mdl
{
class Dynamic;
}
} // namespace rl

namespace move4d
{

/**
 * @brief namespace containing code mostly related to kinematics, dynamics and collisions. It
 * manages the representation of environment, robots and humans.
 */
namespace mdl
{

class Scene;
using ScenePtr = std::shared_ptr<Scene>;
class State;
class WorldState;
class SceneObject;
class Chain;
using ChainPtr = std::shared_ptr<Chain>;
class EndEffector;
using EndEffectorPtr = std::shared_ptr<EndEffector>;

using Model = rl::sg::Model;
using Kinematic = rl::mdl::Dynamic;
using StatePtr = std::shared_ptr<State>;
using SceneObjectPtr = std::shared_ptr<SceneObject>;
class Movable;
class Manipulable;

struct SceneBody {
    SceneBody() = delete;
    SceneBody(const SceneBody &) = default;
    SceneBody(SceneBody &&) = default;
    SceneBody &operator=(const SceneBody &) = default;
    SceneBody &operator=(SceneBody &&) = default;
    virtual ~SceneBody() = default;

    SceneBody(Identifier id, size_t index) : sceneObj(std::move(id)), bodyIndex(index) {}

    Identifier sceneObj;
    size_t bodyIndex;
    bool operator==(const SceneBody &other) const
    {
        return sceneObj == other.sceneObj && bodyIndex == other.bodyIndex;
    }
    bool operator<(const SceneBody &other) const
    {
        return (sceneObj < other.sceneObj) ||
               (sceneObj == other.sceneObj && bodyIndex < other.bodyIndex);
    }
};

class CollisionManager;
using CollisionManagerPtr = std::shared_ptr<CollisionManager>;
class CollisionWhiteList;
using CollisionWhiteListPtr = std::shared_ptr<CollisionWhiteList>;

class CollisionPair
{
    SceneBody body1, body2;

  public:
    explicit CollisionPair(SceneBody a, SceneBody b) : body1(std::move(a)), body2(std::move(b))
    {
        if (body1.sceneObj < body2.sceneObj ||
            (body1.sceneObj == body2.sceneObj && body1.bodyIndex <= body2.bodyIndex)) {
            // ok
        } else {
            std::swap(body1, body2);
        }
        Ensures(body1.bodyIndex < body2.bodyIndex || body1.sceneObj < body2.sceneObj);
    }
    bool operator==(const CollisionPair &other) const
    {
        return (body1 == other.body1) && (body2 == other.body2);
    }
    bool operator<(const CollisionPair &other) const
    {
        return (body1 < other.body1) || (body1 == other.body1 && body2 < other.body2);
    }
    SceneBody &first() { return body1; }
    SceneBody &second() { return body2; }
    const SceneBody &first() const { return body1; }
    const SceneBody &second() const { return body2; }
};

class StateType
{
  public:
    /**
     * the various quantities a state value can represents: force, position and derivates of the
     * position
     */
    enum Type { Torque = 0, Position, Speed, Acceleration, Jerk, Snap };

    static const std::vector<Type> &types();

    static const std::vector<std::string> &strings();
};

} // namespace mdl

} // namespace move4d
#endif

//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//

#include "move4d-soqt/Application.h"

#include "move4d/mdl/CollisionManager.h"
#include "move4d/mdl/Scene.h"

#include "move4d-qt/MainWindow.h"
#include "move4d-soqt/ViewerSoQt.h"
#include "move4d-soqt/ViewerThreadQt.h"

#include "move4d/3rdParty/prettyprint/prettyprint.hpp"
#include "move4d/file/find_file.h"
#include "move4d/file/m4df/M4DFParser.h"
#include "move4d/file/model/URDFImporter.h"
#include "move4d/file/sem/SRDFImporter.h"
#include "move4d/file/world/SDFImporter.h"
#include "move4d/plan/ompl/converters/PlannerFactory.h"
#include "move4d/utils/filesystem_path.h"
#include "move4d/utils/random.h"

#include <Inventor/Qt/SoQt.h>
#include <rl/sg/so/Scene.h>

#include "move4d/utils/filesystem.h"
#include <QBoxLayout>
#include <QMainWindow>
#include <QSplashScreen>
#include <QWidget>
#include <boost/algorithm/string.hpp>
#include <utility>

namespace move4d
{
namespace SoQt
{

Application::Application(const std::string &collisionEngine, std::function<void()> runnerFn)
{
    init(collisionEngine, std::move(runnerFn));
}

Application::Application(boost::program_options::variables_map cli_options,
                         std::function<void()> runnerFn)
    : m_cli_options(std::move(cli_options))
{
    init(m_cli_options["engine"].as<std::string>(), std::move(runnerFn));
}

// needs to be define here due to unique_ptrs
Application::~Application() {}

bool Application::checkCollisionEngine(const std::string &collision_engine) const
{
    auto types = move4d::mdl::CollisionSceneFactory::instance().getTypes();
    auto search = std::find(types.begin(), types.end(), collision_engine);
    if (search == types.end()) {
        std::cerr << "choose a collision engine among the available types:\n\t" << types << "\n";
        return false;
    }
    return true;
}

bool Application::checkPlanner(const std::string &planner) const
{
    auto types = move4d::plan::ompl::convert::PlannerFactory::instance().getTypes();
    auto search = std::find(types.begin(), types.end(), planner);
    if (search == types.end()) {
        std::cerr << "available planner types are:\n\t " << types << "\n";
        return false;
    }
    return true;
}

void Application::init(const std::string &collisionEngine,
                       std::function<void()> runnerFn) // protected
{
    move4d::log::initializeLogger();

    if ((m_cli_options.count("engine") &&
         !checkCollisionEngine(m_cli_options["engine"].as<std::string>())) ||
        (m_cli_options.count("planner") &&
         !checkPlanner(m_cli_options["planner"].as<std::string>()))) {
        move4d::Stop("");
        return;
    }
    if (m_cli_options.count("seed")) {
        move4d::random::instance().seed(m_cli_options["seed"].as<unsigned long int>());
    }
    if (m_cli_options.count("view-collisions") || m_cli_options.count("vc")) {
        setLoadCollisionsInView(true);
    }

    m_mainWidget = ::SoQt::init("move4d", "Move4dSoQt");

    // TODO: path to splash
    // QPixmap splashImg("path_to/splash.png");
    m_splashScreen = std::make_shared<QSplashScreen>(/*splashImg*/);
    m_splashScreen->show();
    m_splashScreen->showMessage("move4d is loading");

    m_mainWindow.reset(new move4d::qt::MainWindow(*this));

    m_mainWindow->setCentralWidget(m_mainWidget);
    m_viewer = std::make_shared<view::ThreadedViewer>();
    view::ViewerInstance::instance().set(m_viewer);
    auto collisionScene = mdl::CollisionSceneFactory::instance().create(collisionEngine);
    m_scene = std::make_shared<mdl::Scene>(collisionScene);
    m_collisionManager = std::make_shared<mdl::CollisionManager>(m_scene, collisionScene);

    if (m_cli_options.count("file-paths")) {
        auto &cli_paths = m_cli_options["file-paths"].as<std::vector<std::string>>();
        for (const auto &paths_str : cli_paths) {
            std::vector<std::string> split_paths;
            boost::split_iterator<std::string::const_iterator> begin = boost::make_split_iterator(
                paths_str, boost::first_finder(std::string(":"), boost::is_equal()));
            for (auto it = begin; it != boost::split_iterator<std::string::const_iterator>();
                 ++it) {
                file::filePath().push_back(
                    std::filesystem::absolute(boost::copy_range<std::string>(*it)));
            }
        }
    }
    m_m4df_definition =
        std::make_unique<file::m4df::M4DFDefinition>(m_scene, m_viewer->getViewer());
    if (m_cli_options.count("file")) {
        for (auto &f : m_cli_options["file"].as<std::vector<std::string>>()) {
            loadFile(f);
        }
    }

    m_scene->resetCollisionWhiteList();

    // NOLINTNEXTLINE(hicpp-use-auto,modernize-use-auto)
    gsl::owner<QVBoxLayout *> layout = new QVBoxLayout();
    layout->addWidget(m_viewer->getViewer());
    m_mainWidget->setLayout(layout);

    m_viewer->getViewer()->show();
    // m_mainWindow->resize(800, 600);

    m_runnerFunction = std::move(runnerFn);
}

bool Application::getLoadCollisionsInView() const { return m_loadCollisionsInView; }

void Application::setLoadCollisionsInView(bool loadCollisionsInView)
{
    m_loadCollisionsInView = loadCollisionsInView;
}

boost::program_options::options_description Application::defaultCliDescritpions()
{
    namespace po = boost::program_options;
    po::options_description desc("Allowed options");
    // clang-format off
    desc.add_options()("help,h", "produce help message and exit")
            ("file-paths,P", po::value<std::vector<std::string>>(),"colon-separated list(s) of additional paths to look for ressources")
            ("file,f", po::value<std::vector<std::string>>()->default_value({}), "load a file")
            ("view-collisions","")
            ("vc", "display collision bodies in the viewer")
            ("engine,c", po::value<std::string>()->default_value(move4d::mdl::CollisionSceneFactory::instance().getTypes().at(0)), "collision engine to use")
            ("planner,p", po::value<std::string>()->default_value("RRTConnect"), "planning algorithm to use")
            ("path", po::value<std::string>(), "play a path instead of planning")
            ("time-limit,t",po::value<float>(), "planning algorithm time limit")
            ("seed", po::value<unsigned long int>(), "seed for the random number generator, "
                                                     "defaults to a random number from your computer random number device");
    return desc;
}

const mdl::CollisionManagerPtr &Application::getCollisionManager() const
{
    return m_collisionManager;
}

const view::ThreadedViewerPtr &Application::getViewer() const { return m_viewer; }

const mdl::ScenePtr &Application::getScene() const { return m_scene; }

void Application::start()
{
    if (move4d::isStopping())
        return;
    ::SoQt::show(m_mainWindow.get());
    m_mainWindow->setWindowState(Qt::WindowMaximized);
    m_splashScreen->close();
    m_splashScreen.reset();
    m_runnerThread = std::thread(&Application::run, this);
    ::SoQt::mainLoop();
    m_runnerThread.join();
    ::SoQt::done();
}

void Application::stop() { ::SoQt::exitMainLoop(); }

void Application::setRunFn(std::function<void()> fn) { m_runnerFunction = std::move(fn); }

void Application::unsetRunFn() { m_runnerFunction = std::function<void()>(); }

void Application::run()
{
    if (m_runnerFunction) {
        m_runnerFunction();
    }
}
void Application::loadFile(const std::string &path)
{
    if (m_splashScreen) {
        m_splashScreen->showMessage(std::string("loading file " + path).c_str());
    }
    if (checkExtension(path, "urdf", false)) {
        file::model::URDFImporter urdfImporter;
        urdfImporter.load(path, m_scene.get(), "");
        urdfImporter.loadView(path, m_viewer->getViewer(), "", {}, getLoadCollisionsInView());
    } else if (checkExtension(path, "sdf", false)) {
        file::world::SDFImporter sdfImporter;
        sdfImporter.load(path, m_scene.get());
        sdfImporter.loadView(path, m_viewer->getViewer());
    } else if (checkExtension(path, "srdf", false)) {
        file::sem::SRDFImporter srdfImporter;
        srdfImporter.loadFile(path, m_scene);
    } else if (checkExtension(path, "m4df", false)) {
        file::m4df::M4DFParser parser(m_scene, m_viewer->getViewer());
        *m_m4df_definition += *parser.loadFile(path);
    } else {
        throw std::invalid_argument("unsupported/wrong file type for file " + path);
    }
}

} // namespace SoQt
} // namespace move4d

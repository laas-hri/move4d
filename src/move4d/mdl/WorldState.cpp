//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//

#include "move4d/mdl/WorldState.h"
#include "move4d/mdl/Scene.h"
#include "move4d/mdl/SceneObject.h"
#include "move4d/mdl/State.h"

namespace move4d
{

namespace mdl
{

WorldState::WorldState(const Scene *scene) : m_scene(scene)
{
    for (const auto &obj : *scene) {
        // creates a copy of the state
        m_states.emplace(obj.first, State(obj.second.get()));
    }
}

WorldState::WorldState(const WorldState &other) : m_scene(other.m_scene)
{
    m_states = other.m_states;
}

WorldState &WorldState::operator=(const WorldState &other)
{
    this->m_scene = other.m_scene;
    m_states = other.m_states;
    return *this;
}

bool WorldState::operator==(const WorldState &other) const
{
    for (auto &it : m_states) {
        auto search = other.m_states.find(it.first);
        if (search == other.m_states.end()) {
            return false;
        }
        if (!(it.second == search->second)) {
            return false;
        }
    }
    return true;
}

const State &WorldState::getState(const std::string &id) const { return m_states.at(id); }
State &move4d::mdl::WorldState::getState(const std::string &id) { return m_states.at(id); }

/**
 * the sum of squared distances between every state of this and other
 */
move4d::Real WorldState::distanceSquared(const WorldState &other, StateType::Type type) const
{
    assert(m_states.size() == other.m_states.size());
    Real d{0};
    for (auto &s : m_states) {
        auto search = other.m_states.find(s.first);
        assert(search != other.m_states.end());
        if (search != other.m_states.end()) {
            d += s.second.distanceSquared(search->second, type);
        }
    }
    return d;
}

/**
 * return the distance between the two worldstates
 */
move4d::Real WorldState::distance(const WorldState &other, StateType::Type type) const
{
    return ::std::sqrt(this->distanceSquared(other, type));
}

const State &WorldState::operator[](const move4d::Identifier &id) const
{
    auto search = m_states.find(id);
    if (search != m_states.end()) {
        return search->second;
    }
    // TODO throw?
    assert(false);
}

State &WorldState::operator[](const Identifier &id)
{
    auto search = m_states.find(id);
    if (search != m_states.end()) {
        return search->second;
    }
    // TODO throw?
    assert(false);
}

} // namespace mdl

} // namespace move4d

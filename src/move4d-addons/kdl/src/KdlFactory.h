//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef _ADDONS_KDL_SRC_KDLFACTORY_H
#define _ADDONS_KDL_SRC_KDLFACTORY_H

#include "move4d/Factory.h"
#include "move4d/mdl/Types.h"
#include "move4d/utils/containers/LRUCache.h"

namespace KDL
{
class Chain;
class Segment;
} // namespace KDL

namespace rl
{
namespace mdl
{
class Kinematic;
} // namespace mdl
} // namespace rl

namespace move4d
{
namespace mdl
{
class Chain;
} // namespace mdl

namespace kin
{
namespace kdl
{

class KdlFactory
    : public move4d::Factory<KdlFactory, std::shared_ptr<KDL::Chain>(move4d::mdl::SceneObject &)>
{
    MOVE4D_STATIC_LOGGER;

  public:
    std::shared_ptr<KDL::Chain> createChain(const mdl::Chain &chain);
    KDL::Segment createSegment(rl::mdl::Kinematic *kin, size_t joint_index);

  private:
    // TODO(Jules): magic variable
    utils::containers::LRUCache<std::shared_ptr<::rl::mdl::Kinematic>, std::shared_ptr<KDL::Chain>>
        _cache{128};
};

} // namespace kdl
} // namespace kin
} // namespace move4d

#endif // _ADDONS_KDL_SRC_KDLFACTORY_H

//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_UTILS_CONTAINERS_LRUCACHE_H
#define MOVE4D_UTILS_CONTAINERS_LRUCACHE_H
#include <assert.h>
#include <list>
#include <unordered_map>

namespace move4d
{
namespace utils
{
namespace containers
{

template <class KEY_T, class VAL_T> class LRUCache
{
  private:
    std::list<std::pair<KEY_T, VAL_T>> _item_list;
    std::unordered_map<KEY_T, decltype(_item_list.begin())> _item_map;
    size_t _cache_size;

  private:
    void clean(void)
    {
        while (_item_map.size() > _cache_size) {
            auto last_it = _item_list.end();
            last_it--;
            _item_map.erase(last_it->first);
            _item_list.pop_back();
        }
    };

  public:
    LRUCache() : _cache_size(16){}; // TODO(Jules): magic variable
    LRUCache(int cache_size) : _cache_size(cache_size){};

    void put(const KEY_T &key, const VAL_T &val)
    {
        auto it = _item_map.find(key);
        if (it != _item_map.end()) {
            _item_list.erase(it->second);
            _item_map.erase(it);
        }
        _item_list.push_front(make_pair(key, val));
        _item_map.insert(make_pair(key, _item_list.begin()));
        clean();
    };
    bool exist(const KEY_T &key) { return (_item_map.count(key) > 0); };
    VAL_T get(const KEY_T &key)
    {
        assert(exist(key));
        auto it = _item_map.find(key);
        _item_list.splice(_item_list.begin(), _item_list, it->second);
        return it->second->second;
    };

    void resize(size_t cache_size)
    {
        _cache_size = cache_size;
        clean();
    }
};

} // namespace containers
} // namespace utils
} // namespace move4d
#endif // MOVE4D_UTILS_CONTAINERS_LRUCACHE_H

//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#include "move4d/plan/ompl/StateSpace.h"
#include "move4d/plan/ClosedLoopSearchSpace.h"
#include "move4d/plan/KinematicGraphSearchSpace.h"
#include "move4d/plan/RobotSpace.h"
#include "move4d/plan/SO3Space.h"
#include "move4d/plan/SearchSpace.h"
#include "move4d/plan/ompl/converters/StateSpaceFactory.h"
#include "move4d/utils/io/print_to_stream.hpp"
#include "move4d/utils/random.h"

#include <ompl/base/spaces/DubinsStateSpace.h>
#include <ompl/base/spaces/RealVectorStateSpace.h>
#include <ompl/base/spaces/ReedsSheppStateSpace.h>
#include <ompl/base/spaces/SO2StateSpace.h>
#include <ompl/base/spaces/SO3StateSpace.h>

namespace move4d
{
namespace plan
{
namespace ompl
{

INIT_MOVE4D_STATIC_LOGGER(StateSpace, "move4d.plan.ompl.statespace");

StateSpace::StateSpace(const SearchSpace &space, mdl::WorldStatePtr initWs)
    : m_initWorldState(std::move(initWs)), m_variableCount(space.getDimension()),
      m_stateValuesSize(m_variableCount * sizeof(move4d::Real))
{
    addSubspaces(space);
}

unsigned int StateSpace::getDimension() const
{
    unsigned int d{0};
    for (auto &s : m_subspaces) {
        d += s->getStateSpace()->getDimension();
    }
    return d;
}

double StateSpace::getMaximumExtent() const
{
    double e{0.0};
    for (auto &s : m_subspaces) {
        e += s->getStateSpace()->getMaximumExtent();
    }
    return e;
}

double StateSpace::getMeasure() const
{
    double m{1.};
    for (auto &s : m_subspaces) {
        m *= s->getStateSpace()->getMeasure();
    }
    return m;
}

void StateSpace::enforceBounds(::ompl::base::State *state) const
{
    size_t current_first_dof = 0, dummy = 0;
    for (auto ss : m_subspaces) {
        auto ss1 = convertStateForSubspace(ss, state->as<StateType>(), current_first_dof, dummy);
        ss->getStateSpace()->enforceBounds(ss1);
        current_first_dof =
            copyValuesFromSubState(state->as<StateType>(), ss, ss1, current_first_dof);
        freeSubspaceState(ss, ss1);
    }
    assert(current_first_dof == getVariableCount());
}

bool StateSpace::satisfiesBounds(const ::ompl::base::State *state) const
{
    size_t current_first_dof = 0;
    for (auto ss : m_subspaces) {
        auto ss1 = convertStateForSubspace(ss, state->as<StateType>(), current_first_dof,
                                           current_first_dof);
        if (!ss->getStateSpace()->satisfiesBounds(ss1)) {
            M4D_TRACE("substate " << ss->getSearchSpace()->getName() << " is out of bounds:\n"
                                  << PRINT_TO_STREAM(ss->getStateSpace()->printState(ss1, oss)));
            return false;
        }
        freeSubspaceState(ss, ss1);
    }
    return true;
}

void StateSpace::copyState(::ompl::base::State *destination,
                           const ::ompl::base::State *source) const
{
    memcpy(destination->as<StateType>()->values, source->as<StateType>()->values,
           m_stateValuesSize);
    destination->as<StateType>()->distance = source->as<StateType>()->distance;
    destination->as<StateType>()->flags = source->as<StateType>()->flags;
}

double StateSpace::distance(const ::ompl::base::State *state1,
                            const ::ompl::base::State *state2) const
{
    size_t current_first_dof = 0, dummy = 0;
    double dist = 0;
    for (auto ss : m_subspaces) {
        auto ss1 = convertStateForSubspace(ss, state1->as<StateType>(), current_first_dof, dummy);
        auto ss2 = convertStateForSubspace(ss, state2->as<StateType>(), current_first_dof,
                                           current_first_dof);
        dist += ss->getStateSpace()->distance(ss1, ss2);
        freeSubspaceState(ss, ss1);
        freeSubspaceState(ss, ss2);
    }
    assert(current_first_dof == getVariableCount());
    return dist;
}

bool StateSpace::equalStates(const ::ompl::base::State *state1,
                             const ::ompl::base::State *state2) const
{
    Expects(dynamic_cast<const StateType *>(state1));
    Expects(dynamic_cast<const StateType *>(state2));
    size_t current_first_dof = 0;
    size_t dummy = 0;
    for (auto ss : m_subspaces) {
        // copy the data
        auto ss1 = convertStateForSubspace(ss, state1->as<StateType>(), current_first_dof, dummy);
        auto ss2 = convertStateForSubspace(ss, state1->as<StateType>(), current_first_dof,
                                           current_first_dof);

        if (!ss->getStateSpace()->equalStates(ss1, ss2)) {
            return false;
        }

        freeSubspaceState(ss, ss1);
        freeSubspaceState(ss, ss2);
    }
    assert(current_first_dof == getVariableCount());
    return true;
}

void StateSpace::interpolate(const ::ompl::base::State *from, const ::ompl::base::State *to,
                             double t, ::ompl::base::State *state) const
{
    Expects(dynamic_cast<const StateType *>(state));
    size_t current_first_dof = 0, dummy;
    std::vector<::ompl::base::State *> states;
    state->as<StateType>()->flags = 0;
    for (auto ss : m_subspaces) {
        // copy the data
        auto ss1 = convertStateForSubspace(ss, from->as<StateType>(), current_first_dof, dummy);
        auto ss2 = convertStateForSubspace(ss, to->as<StateType>(), current_first_dof, dummy);
        // do not copy the data
        states.emplace_back(createStateForSubspace(ss, state->as<StateType>(), current_first_dof,
                                                   current_first_dof));

        // TODO(Jules): Cache the Reeds&Shepp path when used (and other maybe)
        // see ompl::ReedsSheppStateSpace::interpolate variants
        ss->getStateSpace()->interpolate(ss1, ss2, t, states.back());
        freeSubspaceState(ss, ss1);
        freeSubspaceState(ss, ss2);
    }
    assert(current_first_dof == getVariableCount());
    // get back the data where needed
    copyValuesFromSubStates(state->as<StateType>(), states);
    // free the temporary states
    for (size_t i = 0; i < m_subspaces.size(); ++i) {
        freeSubspaceState(m_subspaces[i], states[i]);
    }
}

void StateSpace::printState(const ::ompl::base::State *state_, std::ostream &out) const
{
    auto state = state_->as<StateType>();
    size_t current_first_dof = 0;
    out << "move4d::plan::ompl::StateSpace::StateType of dimension " << getDimension() << "[\n";
    for (auto ss : m_subspaces) {
        auto ss1 = convertStateForSubspace(ss, state->as<StateType>(), current_first_dof,
                                           current_first_dof);
        ss->getStateSpace()->printState(ss1, out);
        freeSubspaceState(ss, ss1);
    }
    assert(current_first_dof == getVariableCount());
    out << "]\n";
}

double *StateSpace::getValueAddressAtIndex(::ompl::base::State *state, unsigned int index) const
{
    return (index < m_variableCount ? &state->as<StateType>()->values[index] : nullptr);
}

void StateSpace::copyToReals(std::vector<double> &reals, const ::ompl::base::State *source) const
{
    reals = {source->as<StateType>()->values, source->as<StateType>()->values + m_variableCount};
    Ensures(reals.size() == m_variableCount);
    Ensures(source->as<StateType>()->values[0] == reals[0]);
}

void StateSpace::copyFromReals(::ompl::base::State *destination,
                               const std::vector<double> &reals) const
{
    Expects(reals.size() == m_variableCount);
    std::memcpy(destination->as<StateType>()->values, &reals[0], reals.size());
    Ensures(destination->as<StateType>()->values[0] == reals[0]);
}

void StateSpace::sanityChecks(double zero, double eps, unsigned int flags) const
{
    auto s1 = allocState()->as<StateType>();
    auto s2 = allocState()->as<StateType>();
    auto sampler = allocStateSampler();
    sampler->sampleUniform(s1);
    sampler->sampleUniform(s2);
    std::vector<Real> reals, reals2;
    // check copyFromReals / copyToReals
    copyToReals(reals, s1);
    copyFromReals(s2, reals);
    copyToReals(reals2, s2);
    if (!equalStates(s1, s2))
        throw ::ompl::Exception(
            "copying a state via copyToReals - copyFromReals must create an equal state");

    // check convert for subspaces and copy from subspaces
    size_t current_first_dof = 0;
    std::vector<::ompl::base::State *> sub_states;
    for (size_t i = 0; i < m_subspaces.size(); ++i) {
        sub_states.push_back(
            convertStateForSubspace(m_subspaces[i], s1, current_first_dof, current_first_dof));
    }
    copyState(s2, s1);                       // backup s1 into s2
    sampler->sampleUniform(s1);              // change s1
    copyValuesFromSubStates(s1, sub_states); // put back subspace values to s1
    if (!equalStates(s1, s2))                // check s1 == backed up s1
        throw ::ompl::Exception("copying a state via convertStateForSubspace - "
                                "copyValuesFromSubState must create an equal state");

    freeState(s1);
    freeState(s2);

    ::ompl::base::StateSpace::sanityChecks(zero, eps, flags);
}

void StateSpace::sanityChecks() const { ::ompl::base::StateSpace::sanityChecks(); }

std::weak_ptr<StateSpaceWrapper> StateSpace::getWrapper() const { return m_wrapper; }

void StateSpace::setWrapper(std::weak_ptr<StateSpaceWrapper> wrapper)
{
    m_wrapper = std::move(wrapper);
}

const std::shared_ptr<const mdl::WorldState> &StateSpace::getInitWorldState() const
{
    return m_initWorldState;
}

void StateSpace::setInitWorldState(std::shared_ptr<const mdl::WorldState> initWorldState)
{
    m_initWorldState = std::move(initWorldState);
}

::ompl::base::StateSamplerPtr StateSpace::allocDefaultStateSampler() const
{
    return std::make_shared<DefaultStateSampler>(this);
}

void StateSpace::allocStateMembers(StateSpace::StateType *state) const
{
    state->values = new double[m_variableCount];
}

::ompl::base::State *StateSpace::allocState() const
{
    auto state = new StateType();
    allocStateMembers(state);
    return state;
}

void StateSpace::freeState(::ompl::base::State *state) const
{
    delete[] state->as<StateType>()->values;
    delete state->as<StateType>();
}

const std::vector<StateSpaceWrapperPtr> &StateSpace::getSubspaces() const { return m_subspaces; }

gsl::owner<::ompl::base::State *>
StateSpace::createStateForSubspace(const StateSpaceWrapperPtr &subspace,
                                   const StateSpace::StateType *state, size_t first_dof,
                                   size_t &next_dof) const
{
    Expects(subspace->getSearchSpace()->getType() != SearchSpace::Type::Compound);
    ::ompl::base::State *new_state{};
    switch (subspace->getSearchSpace()->getType()) {
    case SearchSpace::Type::KinematicGroup:
    case SearchSpace::Type::RealVector:
        new_state = new ::ompl::base::RealVectorStateSpace::StateType;
        new_state->as<::ompl::base::RealVectorStateSpace::StateType>()->values =
            &state->values[first_dof];
        next_dof = first_dof + subspace->getStateSpace()->getDimension();
        return new_state;
    case SearchSpace::Type::SO3:
        next_dof = first_dof + 4;
        return subspace->getStateSpace()->allocState();
    case SearchSpace::Type::SO2:
        next_dof = first_dof + 1;
        return subspace->getStateSpace()->allocState();
    case SearchSpace::Type::Dubins:
    case SearchSpace::Type::ReedsShepp:
        next_dof = first_dof + 3;
        return subspace->getStateSpace()->allocState();
    default:
        throw std::invalid_argument(M4D_FILE_LINE_FUNC +
                                    "there shouldn't be that kind of search space here!");
    }
}

gsl::owner<::ompl::base::State *>
StateSpace::convertStateForSubspace(const StateSpaceWrapperPtr &subspace,
                                    const StateSpace::StateType *state, size_t first_dof,
                                    size_t &next_dof) const
{
    Expects(subspace->getSearchSpace()->getType() != SearchSpace::Type::Compound);
    ::ompl::base::State *new_state{};
    switch (subspace->getSearchSpace()->getType()) {
    case SearchSpace::Type::KinematicGroup:
    case SearchSpace::Type::RealVector:
        new_state = new ::ompl::base::RealVectorStateSpace::StateType;
        new_state->as<::ompl::base::RealVectorStateSpace::StateType>()->values =
            &state->values[first_dof];
        next_dof = first_dof + subspace->getStateSpace()->getDimension();
        return new_state;
    case SearchSpace::Type::SO3: {
        auto tmp_state =
            subspace->getStateSpace()->allocState()->as<::ompl::base::SO3StateSpace::StateType>();
        tmp_state->x = state->values[first_dof + 0];
        tmp_state->y = state->values[first_dof + 1];
        tmp_state->z = state->values[first_dof + 2];
        tmp_state->w = state->values[first_dof + 3];
        next_dof = first_dof + 4;
        return tmp_state;
    }
    case SearchSpace::Type::SO2: {
        auto tmp_state =
            subspace->getStateSpace()->allocState()->as<::ompl::base::SO2StateSpace::StateType>();
        tmp_state->value = state->values[first_dof];
        next_dof = first_dof + 1;
        return tmp_state;
    }
    case SearchSpace::Type::Dubins: {
        auto tmp_state = subspace->getStateSpace()
                             ->allocState()
                             ->as<::ompl::base::DubinsStateSpace::StateType>();
        tmp_state->setX(state->values[first_dof + 0]);
        tmp_state->setY(state->values[first_dof + 1]);
        tmp_state->setYaw(state->values[first_dof + 2]);
        next_dof = first_dof + 3;
        return tmp_state;
    }
    case SearchSpace::Type::ReedsShepp: {
        auto tmp_state = subspace->getStateSpace()
                             ->allocState()
                             ->as<::ompl::base::ReedsSheppStateSpace::StateType>();
        tmp_state->setX(state->values[first_dof + 0]);
        tmp_state->setY(state->values[first_dof + 1]);
        tmp_state->setYaw(state->values[first_dof + 2]);
        next_dof = first_dof + 3;
        return tmp_state;
    }
    default:
        throw std::invalid_argument(M4D_FILE_LINE_FUNC +
                                    "there shouldn't be that kind of search space here!");
    }
}

void StateSpace::copyValuesFromSubStates(const StateType *state,
                                         const std::vector<::ompl::base::State *> &states) const
{
    Expects(states.size() == m_subspaces.size());
    size_t current_first_dof = 0;
    for (size_t i = 0; i < states.size(); ++i) {
        current_first_dof =
            copyValuesFromSubState(state, m_subspaces[i], states[i], current_first_dof);
    }
    assert(current_first_dof == getVariableCount());
}
size_t StateSpace::copyValuesFromSubState(const StateType *state,
                                          const StateSpaceWrapperPtr &subspace,
                                          const ::ompl::base::State *substate,
                                          size_t first_dof) const
{
    Expects(subspace->getSearchSpace()->getType() == SearchSpace::Type::RealVector ||
            subspace->getSearchSpace()->getType() == SearchSpace::Type::KinematicGroup ||
            subspace->getSearchSpace()->getType() == SearchSpace::Type::SO3 ||
            subspace->getSearchSpace()->getType() == SearchSpace::Type::SO2 ||
            subspace->getSearchSpace()->getType() == SearchSpace::Type::Dubins ||
            subspace->getSearchSpace()->getType() == SearchSpace::Type::ReedsShepp);

    switch (subspace->getSearchSpace()->getType()) {
    case SearchSpace::Type::KinematicGroup:
    case SearchSpace::Type::RealVector:
        // check that the values are actually on the heap at the same location
        assert(&state->values[first_dof] ==
               substate->as<::ompl::base::RealVectorStateSpace::StateType>()->values);
        return first_dof + subspace->getStateSpace()->getDimension();
    case SearchSpace::Type::SO3: {
        auto tmp_state = substate->as<::ompl::base::SO3StateSpace::StateType>();
        state->values[first_dof + 0] = tmp_state->x;
        state->values[first_dof + 1] = tmp_state->y;
        state->values[first_dof + 2] = tmp_state->z;
        state->values[first_dof + 3] = tmp_state->w;
        return first_dof + 4;
    }
    case SearchSpace::Type::SO2: {
        state->values[first_dof] = substate->as<::ompl::base::SO2StateSpace::StateType>()->value;
        return first_dof + 1;
    }
    case SearchSpace::Type::Dubins: {
        auto tmp_state = substate->as<::ompl::base::DubinsStateSpace::StateType>();
        state->values[first_dof + 0] = tmp_state->getX();
        state->values[first_dof + 1] = tmp_state->getY();
        state->values[first_dof + 2] = tmp_state->getYaw();
        return first_dof + 3;
    }
    case SearchSpace::Type::ReedsShepp: {
        auto tmp_state = substate->as<::ompl::base::ReedsSheppStateSpace::StateType>();
        state->values[first_dof + 0] = tmp_state->getX();
        state->values[first_dof + 1] = tmp_state->getY();
        state->values[first_dof + 2] = tmp_state->getYaw();
        return first_dof + 3;
    }
    default:
        throw std::invalid_argument(M4D_FILE_LINE_FUNC +
                                    "there shouldn't be that kind of search space here!");
    }
}

void StateSpace::freeSubspaceState(const StateSpaceWrapperPtr &subspace,
                                   ::ompl::base::State *state) const
{
    if (subspace->getSearchSpace()->getType() == SearchSpace::Type::RealVector ||
        subspace->getSearchSpace()->getType() == SearchSpace::Type::KinematicGroup) {
        delete state->as<::ompl::base::RealVectorStateSpace::StateType>();
        // it doesn't own its member array double * values wich points to the member of our state
        // (see convertStateForSubspace)
    } else {
        subspace->getStateSpace()->freeState(state);
    }
}

// recursive
void StateSpace::addSubspaces(const SearchSpace &space)
{
    M4D_DEBUG("add subspace " << std::quoted(space.getName()) << " of type "
                              << SearchSpace::typeString(space.getType()));
    move4d::plan::ompl::convert::SearchSpaceFactory factory;
    switch (space.getType()) {
    case SearchSpace::Type::Compound:
        for (const auto &ss : dynamic_cast<const CompoundSearchSpace &>(space).getSubspaces()) {
            addSubspaces(ss);
        }
        break;
    case SearchSpace::Type::ClosedLoop: {
        auto &s = dynamic_cast<const ClosedLoopSearchSpace &>(space);
        if (s.getPre()) {
            addSubspaces(*s.getPre());
        }
        if (s.getFkChain()) {
            addSubspaces(*s.getFkChain());
        }
        for (const auto &ss : s.getIkChains()) {
            addSubspaces(*ss);
        }
        if (s.getPost()) {
            addSubspaces(*s.getPost());
        }
        break;
    }
    case SearchSpace::Type::KinematicGraph: {
        auto &s = dynamic_cast<const KinematicGraphSearchSpace &>(space);
        for (const auto &x : s) {
            if (x.type != KinematicGraphSearchSpace::Edge::Fixed) {
                addSubspaces(*x.space);
            }
        }
        break;
    }
    case SearchSpace::Type::SE3: {
        auto ss = dynamic_cast<const SE3Space &>(space).split();
        addSubspaces(*ss.first);
        addSubspaces(*ss.second);
        break;
    }
    case SearchSpace::Type::SE2: {
        auto ss = dynamic_cast<const SE2Space &>(space).split();
        addSubspaces(*ss.first);
        addSubspaces(*ss.second);
        break;
    }
    case SearchSpace::Type::KinematicGroup: {
        if (dynamic_cast<const KinematicSpaceWrapper *>(&space)) {
            addSubspaces(space.as<KinematicSpaceWrapper>()->getSpace());
        } else {
            m_subspaces.push_back(factory.create(space));
        }
        break;
    }
    case SearchSpace::Type::SO3:
    case SearchSpace::Type::SO2:
    case SearchSpace::Type::Dubins:
    case SearchSpace::Type::ReedsShepp:
    case SearchSpace::Type::RealVector:
        m_subspaces.push_back(factory.create(space));
        break;
    default:
        throw std::invalid_argument(M4D_FILE_LINE_FUNC +
                                    "there shouldn't be that kind of search space here!");
        break;
    }
}

DefaultStateSampler::DefaultStateSampler(const StateSpace *space)
    : ::ompl::base::StateSampler(space)
{
    for (const auto &ss : space->getSubspaces()) {
        subsamplers_.emplace_back(ss->getStateSpace()->allocDefaultStateSampler());
    }
}

void DefaultStateSampler::sampleUniform(::ompl::base::State *state)
{
    Expects(dynamic_cast<const StateSpace::StateType *>(state));
    auto space = space_->as<StateSpace>();
    const auto &ss = space->getSubspaces();
    size_t current_first_dof = 0;
    std::vector<::ompl::base::State *> states;
    for (size_t i = 0; i < ss.size(); ++i) {
        states.push_back(space->createStateForSubspace(ss[i], state->as<StateSpace::StateType>(),
                                                       current_first_dof, current_first_dof));

        subsamplers_[i]->sampleUniform(states.back());
        assert(ss[i]->getStateSpace()->satisfiesBounds(states.back()));
    }
    assert(current_first_dof == space->getVariableCount());
    space->copyValuesFromSubStates(state->as<StateSpace::StateType>(), states);
    for (size_t i = 0; i < ss.size(); ++i) {
        space->freeSubspaceState(space->getSubspaces()[i], states[i]);
    }
}

void DefaultStateSampler::sampleUniformNear(::ompl::base::State *state,
                                            const ::ompl::base::State *near, double distance)
{
    Expects(dynamic_cast<const StateSpace::StateType *>(state));
    auto space = space_->as<StateSpace>();
    const auto &ss = space->getSubspaces();
    size_t current_first_dof = 0, dummy = 0;
    std::vector<::ompl::base::State *> states, nears;
    for (size_t i = 0; i < ss.size(); ++i) {
        states.push_back(space->createStateForSubspace(ss[i], state->as<StateSpace::StateType>(),
                                                       current_first_dof, dummy));
        nears.push_back(space->convertStateForSubspace(ss[i], near->as<StateSpace::StateType>(),
                                                       current_first_dof, current_first_dof));

        subsamplers_[i]->sampleUniformNear(states.back(), nears.back(), distance);
    }
    assert(current_first_dof == space->getVariableCount());
    space->copyValuesFromSubStates(state->as<StateSpace::StateType>(), states);
    for (size_t i = 0; i < ss.size(); ++i) {
        space->freeSubspaceState(space->getSubspaces()[i], states[i]);
        space->freeSubspaceState(space->getSubspaces()[i], nears[i]);
    }
}

void DefaultStateSampler::sampleGaussian(::ompl::base::State *state,
                                         const ::ompl::base::State *mean, double stdDev)
{
    Expects(dynamic_cast<const StateSpace::StateType *>(state));
    auto space = space_->as<StateSpace>();
    const auto &ss = space->getSubspaces();
    size_t current_first_dof = 0;
    std::vector<::ompl::base::State *> states, means;
    size_t dummy;
    for (size_t i = 0; i < ss.size(); ++i) {
        states.push_back(space->createStateForSubspace(ss[i], state->as<StateSpace::StateType>(),
                                                       current_first_dof, dummy));
        means.push_back(space->convertStateForSubspace(ss[i], mean->as<StateSpace::StateType>(),
                                                       current_first_dof, current_first_dof));

        subsamplers_[i]->sampleGaussian(states.back(), means.back(), stdDev);
    }
    assert(current_first_dof == space->getVariableCount());
    space->copyValuesFromSubStates(state->as<StateSpace::StateType>(), states);
    for (size_t i = 0; i < ss.size(); ++i) {
        space->freeSubspaceState(space->getSubspaces()[i], states[i]);
        space->freeSubspaceState(space->getSubspaces()[i], means[i]);
    }
}

} // namespace ompl
} // namespace plan
} // namespace move4d

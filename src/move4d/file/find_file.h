#ifndef MOVE4D_FILE_FIND_FILE_H
#define MOVE4D_FILE_FIND_FILE_H

#include "move4d/common.h"
#include <functional>
#include <string>
#include <vector>

namespace move4d
{
namespace file
{

class FileError : public move4d::Exception
{
  public:
    FileError(const std::string &arg) : Exception(arg) {}
};

class FileNotFound : public FileError
{
  public:
    FileNotFound(const std::string &path);
};

/**
 * @brief findFile search for the given file in various places
 * @param file_name file name or path portion
 * @return
 *
 * First, try the findFileCallback specified with setFindFileCallback.
 * Then, if the path contains "://", i.e. a protocol header, abort.
 * Then, if file_name is absolute, check the existence of the file and return file_name or throw
 * FileNotFound.
 *
 * If file_name if relative, first try in the current directory, and if it is not here, try in each
 * element of filePath
 *
 * If the file is not found, throw FileNotFound
 * May throw FileError if an element of filePath is not an absolute path
 *
 */
extern std::string findFile(const std::string &file_name,
                            const std::vector<std::string> &additional_paths = {});

/**
 * @brief setFindFileCallback set a function to use when searching for a file
 * @param cb
 *
 * e.g. the function is called with what is found in the <uri> element of a m4df file.
 *
 */
extern void setFindFileCallback(std::function<std::string(const std::string &)> cb);
extern std::function<std::string(const std::string &)> findFileCallback();

/**
 * @brief filePath
 * @return a non-const reference to the vector of paths
 *
 * You must ensure that path elements are absolute paths
 */
std::vector<std::string> &filePath();

} // namespace file

} // namespace move4d

#endif // MOVE4D_FILE_FIND_FILE_H

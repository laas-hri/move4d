#include "move4d/file/m4df/M4DFDefinition.h"
#include "move4d/plan/KinematicGraphSearchSpace.h"
#include "move4d/plan/SearchSpace.h"

namespace move4d
{
namespace file
{
namespace m4df
{

M4DFDefinition::M4DFDefinition(mdl::ScenePtr scene, view::Viewer *viewer)
    : _scene(std::move(scene)), _viewer(viewer)
{
}

M4DFDefinition::~M4DFDefinition() {}

ptr_wrapper<plan::SearchSpace> M4DFDefinition::searchSpace(const std::string &name) const
{
    auto search = _search_spaces.find(name);
    if (search != _search_spaces.end()) {
        return search->second;
    } else {
        return {};
    }
}

void M4DFDefinition::addSearchSpace(const std::string &name,
                                    std::unique_ptr<plan::SearchSpace> &&space)
{
    _search_spaces[name] = std::move(space);
}

const std::map<std::string, ptr_wrapper<plan::KinematicGraphSearchSpaceBuilder>> &
M4DFDefinition::getKinematicGraphBuilders() const
{
    return _kinematic_graph_builders;
}

M4DFDefinition &M4DFDefinition::operator+=(const M4DFDefinition &other)
{
    if (_scene && other._scene && _scene != other._scene) {
        throw Exception("cannot add M4DFDefintion that are defined for different scenes");
    }
    if (!_scene) {
        _scene = other._scene;
    }
    if (!_viewer) {
        _viewer = other._viewer;
    }
    _search_spaces.insert(other._search_spaces.begin(), other._search_spaces.end());
    _kinematic_graph_builders.insert(other._kinematic_graph_builders.begin(),
                                     other._kinematic_graph_builders.end());
    return *this;
}

} // namespace m4df
} // namespace file
} // namespace move4d

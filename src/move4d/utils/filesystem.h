#ifndef MOVE4D_SRC_UTILS_FILESYSTEM_H
#define MOVE4D_SRC_UTILS_FILESYSTEM_H

#if __has_include(<filesystem>)
#include <filesystem>
#elif __has_include(<experimental/filesystem>)
#include <experimental/filesystem>
namespace std
{
namespace filesystem = experimental::filesystem;
}
#else
#error need std c++ filesystem library (or experimental/filesystem)
#endif

#endif // MOVE4D_SRC_UTILS_FILESYSTEM_H

//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//

#include <iostream>
#include <string>

#include "move4d-soqt/Application.h"
#include "move4d/file/world/SDFImporter.h"
#include "move4d/mdl/Scene.h"

#include <Inventor/Qt/SoQt.h>
#include <Inventor/Qt/viewers/SoQtExaminerViewer.h>
#include <Inventor/nodes/SoLightModel.h>
#include <QWidget>
#include <QtCore/Qt>

#include <Inventor/annex/FXViz/nodes/SoShadowDirectionalLight.h>
#include <QVBoxLayout>
#include <move4d-soqt/ViewerSoQt.h>
#include <move4d-soqt/ViewerThreadQt.h>
#include <move4d/math/Vector.h>
#include <move4d/mdl/SceneObject.h>
#include <random>
#include <rl/plan/SimpleModel.h>
#include <rl/sg/Scene.h>
#include <thread>

class MainApplication
{
  public:
    MainApplication(int argc, char *argv[]);
    move4d::mdl::Scene *createScene(const std::string &filename);
    void shoot();
    rl::sg::Scene *getSceneGraph() const;
    move4d::view::ViewerSoQt *m_viewer;
    move4d::view::ViewerThreadQtPtr m_viewerIF;

  protected:
    move4d::SoQt::Application *m_app;
    rl::sg::so::Scene *m_sceneView;
    move4d::mdl::ScenePtr m_scene;
    rl::sg::Scene *m_sceneGraph;
};

int main(int argc, char *argv[])
{

    MainApplication app(argc, argv);
    //    auto scene = app.createScene(argv[1]);
    //
    //
    //
    //    // set the viewer to display the scene
    //    auto viewer = new SoQtExaminerViewer(widget, nullptr, true, SoQtFullViewer::BUILD_POPUP);
    //    app.m_viewer = viewer;
    //    viewer->setSceneGraph(app.getSceneGraph()->root);
    //    viewer->setTransparencyType(SoGLRenderAction::SORTED_OBJECT_BLEND);
    //    viewer->show();
    //    viewer->setBackgroundColor(SbColor(1.,1.,1.));
    //    viewer->setHeadlight(false);
    //
    //    widget->setWindowTitle("move4dViewDemo");

    // SoQt::show(widget);
    return 0;
}

MainApplication::MainApplication(int argc, char *argv[])
{
    std::cout << "LoadSDF MainApplication ctor" << std::endl;
    m_app = new move4d::SoQt::Application("pqp", std::bind(&MainApplication::shoot, this));
    m_scene = m_app->getScene();
    m_sceneGraph = nullptr;

    // the scene for visualisation
    m_viewer = m_app->getViewer()->getViewer();
    //  for the interface (async)
    m_viewerIF = m_app->getViewer();

    move4d::file::world::SDFImporter importer;
    std::cout << "will load sdf " << argv[1] << std::endl;
    // load URDF for collision
    importer.load(argv[1], m_scene.get());
    // load URDF for visualisation
    importer.loadView(argv[1], m_viewer);
    m_viewer->updateModels();

    m_app->start();
}

void MainApplication::shoot()
{
    auto robot = m_scene->begin()->second;
    auto mdl = robot->model();
    move4d::math::Vector rand(mdl->getDof());
    std::cout << mdl->getDof() << std::endl;
    std::uniform_real_distribution<double> distrib(0., 1.);
    std::random_device rd;
    std::mt19937 engine(rd());
    for (int j = 0; j < mdl->getDof() * 2 + 1; j++) {
        for (uint i = 0; i < 6; i++) {
            rand(i) = 0.5;
        }
        for (uint i = 6; i < mdl->getDof(); ++i) {
            rand(i) = distrib(engine);
            ;
        }
        if (j % 2 != 0) {
            rand(j / 2) = 0.5;
        }
        move4d::math::Vector pos = mdl->generatePositionUniform(rand);
        for (long i = 0; i < pos.size(); ++i) {
            pos[i] = (pos[i] != std::numeric_limits<move4d::Real>::infinity() ? pos[i] : 0);
        }
        move4d::mdl::State state(robot.get());
        state(move4d::mdl::StateType::Position) = pos;
        robot->applyState(state);
        m_viewerIF->update(*m_scene->getCurrentWorldState());
        // m_viewer->update(*m_scene->getCurrentWorldState());
        sleep(3);
    }
}

rl::sg::Scene *MainApplication::getSceneGraph() const { return m_sceneGraph; }

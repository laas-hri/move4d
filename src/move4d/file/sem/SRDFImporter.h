//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_FILE_SEM_SRDFIMPORTER_H
#define MOVE4D_FILE_SEM_SRDFIMPORTER_H

#include "move4d/mdl/Types.h"

class TiXmlElement;
namespace move4d
{
namespace file
{
namespace sem
{

class SRDFImporter
{
    MOVE4D_STATIC_LOGGER;

  public:
    SRDFImporter();
    virtual ~SRDFImporter();

  public:
    bool loadFile(const std::string &filepath, const mdl::ScenePtr &scene);
    bool loadString(const std::string &xml_string, const mdl::ScenePtr &scene);
    bool loadXml(TiXmlElement *robot_xml, const mdl::ScenePtr &scene);

  private:
    struct Impl;
    std::unique_ptr<Impl> p_impl;
};

} // namespace sem
} // namespace file
} // namespace move4d

#endif // MOVE4D_FILE_SEM_SRDFIMPORTER_H

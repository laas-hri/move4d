//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#include "tests/plan/FixtureKinematicGraph.h"

#include "move4d/mdl/Movable.h"
#include "move4d/plan/KinematicGraphSearchSpace.h"
#include "move4d/plan/SO3Space.h"

namespace move4d
{
namespace tests
{

FixtureKinematicGraph::FixtureKinematicGraph()
{
    object = lwr_ff_base.get()->as<mdl::Movable>();
    part_base = std::make_shared<plan::SE2Space>(
        object, 0, move4d::plan::SE2Space::SE2Type::Holonomic, "base_holo");
    kin_base = std::make_shared<plan::KinematicSpaceWrapper>(part_base->cloneSearchSpace(), object,
                                                             "root", "lwr_base_link");
    kin_base->setPoseBounds(_scene->getSpaceBounds().first, _scene->getSpaceBounds().second);

    arm_part = std::make_shared<move4d::plan::RobotKinematicGroupSpace>(object, arm_chain, "ikfast",
                                                                        "arm_ikfast");
    arm_part->setPoseBounds(_scene->getSpaceBounds().first, _scene->getSpaceBounds().second);
    kin_graph =
        move4d::plan::KinematicGraphSearchSpaceBuilder()
            .withName("free_base_fixed_gripper")
            .addInverseChain(*arm_part, "arm_base", "gripper")
            .addFixedTransform(move4d::plan::KinematicGraphSearchSpaceBuilder::Fixed, "gripper",
                               math::Transform(math::Translation(0, 0, 2.)))
            .addForwardChain(*kin_base,
                             /*base_vertex=*/move4d::plan::KinematicGraphSearchSpaceBuilder::Fixed,
                             "arm_base")
            .build();
}

} // namespace tests
} // namespace move4d

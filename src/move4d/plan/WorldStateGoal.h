//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_PLAN_WORLDSTATEGOAL_H
#define MOVE4D_PLAN_WORLDSTATEGOAL_H

#include "move4d/common.h"
#include "move4d/plan/Goal.h"
#include <memory>

namespace move4d
{
namespace mdl
{
class WorldState;
}
} // namespace move4d

namespace move4d
{

namespace plan
{

/**
 * The planner must reach a worldstate where some degrees of freedom are specified
 */
class WorldStateGoal : public Goal
{
  public:
    WorldStateGoal(std::shared_ptr<const move4d::mdl::WorldState> ws,
                   move4d::Real distance_threshold = -1.);

    inline const std::shared_ptr<const mdl::WorldState> &getGoalWorldState() const;

    void setGoalWorldState(std::shared_ptr<const move4d::mdl::WorldState> &value);

    /**
     * heuristic distance to reach the goal
     */
    virtual move4d::Real distance(const move4d::mdl::WorldState &ws) const override;

    /**
     * is the goal reached?
     */
    virtual bool check(const move4d::mdl::WorldState &ws) const override;

  private:
    std::shared_ptr<const move4d::mdl::WorldState> m_goalWorldState;
    move4d::Real m_distance_threshold;

  protected:
    virtual void impl_compute(result_ref result, const_argument_ref argument) const override;
};
inline const std::shared_ptr<const move4d::mdl::WorldState> &
WorldStateGoal::getGoalWorldState() const
{
    return m_goalWorldState;
}

} // namespace plan

} // namespace move4d
#endif

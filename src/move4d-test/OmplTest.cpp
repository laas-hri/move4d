//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#include "move4d/mdl/CollisionManager.h"
#include "move4d/mdl/CollisionWhiteList.h"
#include "move4d/mdl/DegreeOfFreedomInfo.h"
#include "move4d/mdl/Movable.h"
#include "move4d/mdl/Scene.h"
#include "move4d/mdl/SceneObject.h"
#include "move4d/mdl/Types.h"
#include "move4d/mdl/WorldState.h"

#include "move4d/plan/RobotSpace.h"
#include "move4d/plan/WorldStateGoal.h"
#include "move4d/plan/rl/Planner.h"
#include "move4d/plan/rl/UniformSampler.h"

#include "move4d/file/model/URDFImporter.h"

#include "move4d-soqt/ViewerSoQt.h"
#include "move4d-soqt/ViewerThreadQt.h"
#include <Inventor/Qt/SoQt.h>
#include <Inventor/Qt/viewers/SoQtExaminerViewer.h>

#include <rl/math/Vector.h>
#include <rl/mdl/Body.h>
#include <rl/mdl/Joint.h>
#include <rl/plan/SimpleModel.h>
#include <rl/sg/so/Scene.h>

#include <QApplication>
#include <QMainWindow>
#include <QVBoxLayout>

#include <chrono>
#include <functional>
#include <thread>

#include <ompl/base/ConstrainedSpaceInformation.h>
#include <ompl/base/Constraint.h>
#include <ompl/base/ProblemDefinition.h>
#include <ompl/base/ScopedState.h>
#include <ompl/base/SpaceInformation.h>
#include <ompl/base/spaces/SE3StateSpace.h>
#include <ompl/base/spaces/constraint/ProjectedStateSpace.h>
#include <ompl/geometric/PathGeometric.h>
#include <ompl/geometric/planners/prm/PRM.h>
#include <ompl/geometric/planners/rrt/RRTConnect.h>

#include <gsl/gsl>

using namespace move4d;

class Application
{
  public:
    Application(int argc, char *argv[]);

    void run();

    static bool isStateValid(const ompl::base::State *);

    //    static void convert(ompl::base::CompoundStateSpace &space, const ompl::base::CompoundState
    //    &x);
    static void putIn(ompl::base::ConstrainedStateSpace &space,
                      const ompl::base::ConstrainedStateSpace::StateType &x, mdl::State &state);

  protected:
    static Application *s_instance;
    void f();

    move4d::mdl::Scene *m_scene;
    move4d::mdl::SceneObjectPtr m_robot;
    rl::sg::Scene *m_sceneView;
    QWidget *m_mainWidget;
    view::ViewerSoQt *m_viewer;
    view::ViewerThreadQt *m_viewerIF;
    std::shared_ptr<ompl::base::ConstrainedStateSpace> m_space;

    size_t m_run_nb{1};
    bool m_showPath{false};

    Real m_z_desired{1.};
    Real m_z_tolerance{.01};
    float m_time_limit{1.};
};

class MyConstraint : public ompl::base::Constraint
{

  public:
    MyConstraint(move4d::mdl::SceneObjectPtr robot, Real z_desired)
        : ompl::base::Constraint(robot->getNumberOfDof(), 1), robot(robot), z_desired(z_desired)
    {
    }

    virtual void function(const Eigen::Ref<const Eigen::VectorXd> &x,
                          Eigen::Ref<Eigen::VectorXd> out) const override;
    virtual void jacobian(const Eigen::Ref<const Eigen::VectorXd> &x,
                          Eigen::Ref<Eigen::MatrixXd> out) const override;

    move4d::mdl::SceneObjectPtr robot;

    Real z_desired;
};

void MyConstraint::function(const Eigen::Ref<const Eigen::VectorXd> &x,
                            Eigen::Ref<Eigen::VectorXd> out) const
{
    Expects(x.size() == robot->getNumberOfDof(mdl::StateType::Position));
    auto q = *robot->getState();
    q(mdl::StateType::Position) = x;
    robot->applyState(q);
    auto kin = robot->model()->mdl;
    auto endeff = kin->getFrame(kin->getFrames() - 1);
    //    out[0]=std::abs(math::Quaternion(endeff->x.transform().linear()).z()-z_desired);
    out[0] = std::abs(endeff->x.transform().translation().z() - z_desired);
    //    out[0]=endeff->x.transform().translation().block(0,0,1,2).norm() - 0.7;
}

void MyConstraint::jacobian(const Eigen::Ref<const Eigen::VectorXd> &x,
                            Eigen::Ref<Eigen::MatrixXd> out) const
{
    auto q = robot->getState();
    (*q)(mdl::StateType::Position) = x;
    robot->applyState(*q);
    auto kin = robot->model()->mdl;
    auto endeff = kin->getBody(kin->getBodies() - 1);

    kin->calculateJacobian();
    out = kin->getJacobian().block(2, 0, 1, robot->getNumberOfDof());
}

Application *Application::s_instance = nullptr;

Application::Application(int argc, char *argv[])
{
    Application::s_instance = this;
    QWidget *mainwindow = SoQt::init(argv[0]);

    std::string engine{"undef"};
    std::vector<std::string> files;
    for (int a = 0; a < argc; ++a) {
        std::cout << "\"" << argv[a] << "\"" << std::endl;
    }
    for (int a = 0; a < argc; ++a) {
        if (strcmp(argv[a], "-n") == 0) {
            m_run_nb = std::atoi(argv[++a]);
        } else if (strcmp(argv[a], "-e") == 0 || strcmp(argv[a], "--engine") == 0) {
            engine = argv[++a];
        } else if (strcmp(argv[a], "-f") == 0) {
            files.push_back(argv[++a]);
        } else if (strcmp(argv[a], "-v") == 0) {
            m_showPath = true;
        } else if (strcmp(argv[a], "-z") == 0) {
            m_z_desired = std::atof(argv[++a]);
        } else if (strcmp(argv[a], "-ztol") == 0) {
            m_z_tolerance = std::atof(argv[++a]);
        } else if (strcmp(argv[a], "-t") == 0) {
            m_time_limit = std::atof(argv[++a]);
        }
    }

    // the main scene for computation
    m_scene = new mdl::Scene(engine);

    // the scene for visualisation
    m_viewer = new view::ViewerSoQt();
    m_sceneView = view::ViewerSoQt::createScene();
    m_viewer->setScene(m_sceneView);
    //  for the interface (async)
    m_viewerIF = new view::ViewerThreadQt();
    if (!view::ViewerThreadQt::connect(m_viewerIF, m_viewer)) {
        throw std::runtime_error("failed to connect viewer interface to asynchronous viewer");
    }

    auto *layout = new QVBoxLayout();
    layout->addWidget(m_viewer);
    mainwindow->setLayout(layout);

    file::model::URDFImporter importer;
    for (auto f : files) {
        // load URDF for collision
        auto r = importer.load(f, m_scene, f);
        // load URDF for visualisation
        importer.loadView(f, m_viewer, f);
        r->updateModels();
        if (!m_robot) {
            m_robot = r;
        }
    }
    m_viewer->setScene(m_sceneView);

    m_scene->resetCollisionWhiteList();
    for (auto r : *m_scene) {
        m_scene->getCollisionWhitelist()->removeSelfColliding(*r.second,
                                                              *m_scene->getCollisionManager());
    }
    std::cout << "whitelist\n" << *m_scene->getCollisionWhitelist() << std::endl;

    m_viewer->show();

    mainwindow->resize(800, 600);
    // mainwindow.show();
    SoQt::show(mainwindow);
}
void Application::run()
{
    std::thread the_thread(&Application::f, this);
    SoQt::mainLoop();
    the_thread.join();
}

bool Application::isStateValid(const ompl::base::State *x)
{
    Expects(s_instance);
    mdl::State q = *s_instance->m_robot->getState();
    auto so = x->as<ompl::base::ConstrainedStateSpace::StateType>();
    putIn(*s_instance->m_space, *so, q);
    s_instance->m_robot->applyState(q);
    return !s_instance->m_scene->getCollisionManager()->isColliding();
}

void Application::putIn(ompl::base::ConstrainedStateSpace &,
                        const ompl::base::ConstrainedStateSpace::StateType &x, mdl::State &state)
{
    std::vector<double> reals;
    // space.copyToReals(reals,&x);
    // putIn(*x.components[0]->as<ompl::base::SE3StateSpace::StateType>(),state);
    state(mdl::StateType::Position) = x;
}

void Application::f()
{
    using namespace std;

    typedef ompl::base::ConstrainedStateSpace::StateType StateType;

    // for constrained planning, we can use only vector space (no SO3 or so...)
    auto robot = m_robot;
    // create the ambient space (robot space)
    auto subspace(std::make_shared<ompl::base::RealVectorStateSpace>(robot->getNumberOfDof()));
    ompl::base::RealVectorBounds subbounds(subspace->getDimension());
    for (size_t i = 0; i < subspace->getDimension(); ++i) {
        subbounds.low[i] = robot->getDegreeOfFreedomInfo(i, mdl::StateType::Position).getMinimum();
        subbounds.high[i] = robot->getDegreeOfFreedomInfo(i, mdl::StateType::Position).getMaximum();
    }
    subspace->setBounds(subbounds);

    auto constraint = std::make_shared<MyConstraint>(robot, m_z_desired);
    constraint->setTolerance(m_z_tolerance);
    std::cout << "constraint tolerance " << constraint->getTolerance() << std::endl;

    // create the constrained state space based on the robot state space
    auto space = std::make_shared<ompl::base::ProjectedStateSpace>(subspace, constraint);
    m_space = space;

    auto si(std::make_shared<ompl::base::ConstrainedSpaceInformation>(space));

    si->setStateValidityChecker(&Application::isStateValid);

    auto planner(std::make_shared<ompl::geometric::PRM>(si));
    planner->setup();

    for (size_t i = 0; i < m_run_nb; ++i) {
        ompl::base::ScopedState<> start(space);
        do {
            start.random();
        } while (!isStateValid(start.get()) ||
                 constraint->distance(start.get()->as<StateType>()) > constraint->getTolerance());
        //        (*start.get()->as<StateType>())[0]=robot->getDegreeOfFreedomInfo(0,mdl::StateType::Position).getMaximum()*0.9;
        ompl::base::ScopedState<> goal(space);
        do {
            goal.random();
        } while (!isStateValid(goal.get()) ||
                 constraint->distance(goal.get()->as<StateType>()) > constraint->getTolerance());
        //        (*goal.get()->as<StateType>())[0]=robot->getDegreeOfFreedomInfo(0,mdl::StateType::Position).getMinimum()*0.9;

        auto q = *robot->getState();
        putIn(*space, *start.get()->as<StateType>(), q);
        robot->applyState(q);
        m_viewerIF->update(*m_scene->getCurrentWorldState());
        Eigen::VectorXd constr_out(1);
        constraint->function(*start.get()->as<StateType>(), constr_out);
        std::cout << "start : " << constr_out[0] << std::endl;
        //        this_thread::sleep_for(std::chrono::seconds(1));
        //        this_thread::sleep_for(std::chrono::seconds(1));
        putIn(*space, *goal.get()->as<StateType>(), q);
        robot->applyState(q);
        m_viewerIF->update(*m_scene->getCurrentWorldState());
        constraint->function(*goal.get()->as<StateType>(), constr_out);
        std::cout << "goal : " << constr_out[0] << std::endl;
        //        this_thread::sleep_for(std::chrono::seconds(1));

        planner->clearQuery();
        auto pdef(std::make_shared<ompl::base::ProblemDefinition>(si));
        pdef->setStartAndGoalStates(start, goal);
        planner->setProblemDefinition(pdef);
        ompl::base::PlannerStatus status = planner->ompl::base::Planner::solve(m_time_limit);
        if (status && !pdef->hasApproximateSolution()) {
            ompl::base::PathPtr path = pdef->getSolutionPath();
            std::cout << "Found Solution" << std::endl;
            path->print(std::cout);
            auto gpath = path->as<ompl::geometric::PathGeometric>();
            gpath->interpolate(100);
            for (size_t i = 0; i < gpath->getStateCount(); ++i) {
                auto x = *robot->getState();
                putIn(*m_space, *gpath->getState(i)->as<StateType>(), x);
                robot->applyState(x);
                m_viewerIF->update(*m_scene->getCurrentWorldState());
                this_thread::sleep_for(std::chrono::milliseconds(1000 / 60));
            }
        } else if (pdef->hasApproximateSolution()) {
            std::cout << "Approximate solution" << std::endl;
        } else {
            std::cout << "No solution Found" << std::endl;
        }
    }
}

int main(int argc, char *argv[])
{
    Application app(argc, argv);
    app.run();
    return 0;
}

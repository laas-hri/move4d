//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//

#include "Planner.h"
#include "move4d/mdl/Movable.h"
#include "move4d/plan/Goal.h"
#include "move4d/plan/RobotSpace.h"
#include "move4d/plan/WorldStateGoal.h"
#include "move4d/plan/rl/UniformSampler.h"

#include <rl/plan/LinearNearestNeighbors.h>
#include <rl/plan/Planner.h>
#include <rl/plan/RrtConCon.h>
#include <rl/plan/SimpleModel.h>
#include <rl/plan/UniformSampler.h>

namespace move4d
{
namespace plan
{
namespace rl
{

Planner::Planner(std::shared_ptr<RobotSpace> space, mdl::CollisionManagerPtr collisionManager)
    : plan::Planner(std::move(space), std::move(collisionManager)),
      m_planner(new ::rl::plan::RrtConCon())
{
}

bool Planner::solve(float timeLimit)
{
    std::shared_ptr<RobotSpace> space = std::dynamic_pointer_cast<RobotSpace>(m_searchSpace);
    const auto &goal = dynamic_cast<const WorldStateGoal *>(m_goal.get());
    auto planner =
        dynamic_cast<::rl::plan::RrtConCon *>(m_planner); // TODO(Jules): this causes a memory leak

    // TODO(Jules) : this is probably not safe (const_cast):
    planner->model = space->robot()->model().get();
    //    planner->model = const_cast<::rl::plan::SimpleModel *>(space->robot()->model().get());
    // TODO(Jules): this causes a memory leak:
    planner->goal = new ::rl::math::Vector(
        goal->getGoalWorldState()->getState(space->robot()->getId())(mdl::StateType::Position));
    // TODO(Jules): this causes a memory leak:
    planner->start =
        new ::rl::math::Vector((*m_start)[space->robot()->getId()](mdl::StateType::Position));

    for (size_t i = 0; i < 2; ++i) {
        m_nearestNeighbors.push_back(
            std::make_shared<::rl::plan::LinearNearestNeighbors>(planner->model));
        planner->setNearestNeighbors(m_nearestNeighbors[i].get(), i);
    }

    m_sampler = std::make_shared<move4d::plan::rl::UniformSampler>(space);
    m_sampler->model = planner->model;
    planner->sampler = m_sampler.get();
    planner->delta = 1. * ::rl::math::DEG2RAD;
    planner->epsilon = 1e-2 * ::rl::math::DEG2RAD;
    planner->duration = std::chrono::seconds(3);

    return planner->solve();
}

::rl::plan::VectorList Planner::getPath() const { return m_planner->getPath(); }

void Planner::setViewer(::rl::plan::Viewer *viewer) { m_planner->viewer = viewer; }

} // namespace rl
} // namespace plan
} // namespace move4d

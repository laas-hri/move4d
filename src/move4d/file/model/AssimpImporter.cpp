//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//

#include "move4d/file/model/AssimpImporter.h"
#include "move4d/mdl/SceneObject.h"

#include <Inventor/VRMLnodes/SoVRMLAppearance.h>
#include <Inventor/VRMLnodes/SoVRMLColor.h>
#include <Inventor/VRMLnodes/SoVRMLCoordinate.h>
#include <Inventor/VRMLnodes/SoVRMLIndexedFaceSet.h>
#include <Inventor/VRMLnodes/SoVRMLMaterial.h>
#include <Inventor/VRMLnodes/SoVRMLShape.h>
#include <Inventor/VRMLnodes/SoVRMLVertexShape.h>
#include <assimp/Importer.hpp>
#include <assimp/mesh.h>
#include <assimp/postprocess.h>
#include <assimp/scene.h>

#include <rl/sg/Body.h>
#include <rl/sg/Shape.h>

namespace move4d
{

namespace file
{

namespace model
{

void AssimpImporter::load(const std::string &filepath, rl::sg::Body *body,
                          const move4d::math::Vector3 &scale, const move4d::math::Transform &pos,
                          std::optional<Eigen::Vector4f> color)
{

    Assimp::Importer importer;
    const aiScene *sc =
        importer.ReadFile(filepath.c_str(), aiProcess_SortByPType | aiProcess_Triangulate);

    if (sc == nullptr) {
        std::cout << "failed to load " << filepath << std::endl;
        throw(LoadException("assimp failed to load " + filepath));
    }
    if (sc->mRootNode == nullptr) {
        throw(LoadException("assimp failed to parse " + filepath));
    }

    if (sc->HasMeshes()) {
        loadMesh(sc->mRootNode, sc, body, scale, pos, color);
    }
}

void AssimpImporter::loadMesh(const aiNode *mesh, const aiScene *scene, rl::sg::Body *body,
                              const move4d::math::Vector3 &scale,
                              const move4d::math::Transform &pos,
                              std::optional<Eigen::Vector4f> &color)
{

    for (std::size_t i = 0; i < mesh->mNumMeshes; ++i) {
        auto *geometry = new ::SoVRMLIndexedFaceSet();
        geometry->ref();
        aiMesh *inputMesh = scene->mMeshes[mesh->mMeshes[i]];

        // copy vertices
        auto coord = new SoVRMLCoordinate;
        coord->ref();
        coord->point.setNum(int(inputMesh->mNumVertices));
        SoMFVec3f *points = &coord->point;
        // auto points=new SoMFVec3f;
        points->setNum(int(inputMesh->mNumVertices));
        points->startEditing();
        Eigen::Vector3f scalef{float(scale.x()), float(scale.y()), float(scale.z())};
        for (std::size_t j = 0; j < inputMesh->mNumVertices; ++j) {
            points->set1Value(int(j), inputMesh->mVertices[j].x *scalef.x(),
                              inputMesh->mVertices[j].y *scalef.y(),
                              inputMesh->mVertices[j].z *scalef.z());
        }
        points->finishEditing();
        geometry->coord.setValue(coord);

        // copy faces
        SoMFInt32 coordIndex;
        coordIndex.setNum(int(inputMesh->mNumVertices + std::ceil(inputMesh->mNumVertices / 3)));
        coordIndex.startEditing();
        int idx{0};
        for (std::size_t j = 0; j < inputMesh->mNumFaces; ++j) {
            // assert(inputMesh->mFaces[j].mNumIndices==3);
            for (std::size_t k = 0; k < inputMesh->mFaces[j].mNumIndices; ++k) {
                coordIndex.set1Value(idx++, static_cast<int>(inputMesh->mFaces[j].mIndices[k]));
            }
            coordIndex.set1Value(idx++, -1);
        }
        assert(idx == coordIndex.getNum());
        coordIndex.finishEditing();
        geometry->coordIndex = coordIndex;
        assert(geometry->isOfType(SoVRMLIndexedFaceSet::getClassTypeId()));

        auto shape = new SoVRMLShape;
        shape->ref();
        shape->geometry = geometry;

        geometry->colorPerVertex.setValue(false);

        // material
        auto material = new SoVRMLMaterial();
        if (color) {
            material->diffuseColor.setValue(color.value().x(), color.value().y(),
                                            color.value().z());
            material->transparency = 1 - color.value()[3];
        } else if (scene->mNumMaterials > inputMesh->mMaterialIndex) {
            aiColor3D diffuse, ambiant;
            auto mat = scene->mMaterials[inputMesh->mMaterialIndex];
            float transparent;
            mat->Get(AI_MATKEY_COLOR_DIFFUSE, diffuse);
            // mat->Get(AI_MATKEY_COLOR_AMBIENT,diffuse);
            material->diffuseColor.setValue(diffuse.r, diffuse.g, diffuse.b);
            if (mat->Get(AI_MATKEY_OPACITY, transparent) == aiReturn_SUCCESS) {
                std::cout << "transparency:" << transparent << std::endl;
                //                material->transparency=transparent;
            }
        }
        auto appearance = new SoVRMLAppearance();
        shape->appearance = appearance;
        appearance->material = material;

        auto rlshape = body->create(shape);
        rlshape->setTransform(pos);
        shape->unref();
        geometry->unref();
    }
    for (std::size_t i = 0; i < mesh->mNumChildren; ++i) {
        this->loadMesh(mesh->mChildren[i], scene, body, scale, pos, color);
    }
}

} // namespace model

} // namespace file

} // namespace move4d

//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//

#include "move4d/mdl/SceneObject.h"
#include "move4d/mdl/DegreeOfFreedomInfo.h"
#include "move4d/mdl/Scene.h"
#include "move4d/mdl/WorldState.h"
#include "move4d/view/Viewer.h"
#include "rl/mdl/Joint.h"
#include "rl/mdl/Kinematic.h"
#include "rl/sg/Body.h"
#include <rl/mdl/JacobianInverseKinematics.h>
#include <rl/mdl/SixDof.h>
#include <rl/plan/SimpleModel.h>

#include <utility>

namespace move4d
{

namespace mdl
{
INIT_MOVE4D_STATIC_LOGGER(SceneObject, "move4d.mdl.sceneobject");

SceneObject::SceneObject(const Scene &scene, std::shared_ptr<Model> bodies,
                         std::shared_ptr<Kinematic> kin, std::string name, move4d::Identifier id,
                         std::size_t index)
    : m_scene(scene), m_bodies(std::move(bodies)), m_kinematic(std::move(kin)), m_id(std::move(id)),
      m_name(std::move(name)), m_index(index)
{
    // init the rl::plan::Model
    m_model = std::make_shared<::rl::plan::SimpleModel>();
    m_model->mdl = m_kinematic.get();
    m_model->model = m_bodies.get();
    m_model->scene = m_bodies->getScene();
    //
    updateModels();
}

SceneObject::~SceneObject()
{
    m_bodies->getScene()->remove(m_bodies.get()); // doesn't actually desctruct *m_model
}

void SceneObject::setName(std::string value) { m_name = std::move(value); }

void SceneObject::setId(move4d::Identifier value) { m_id = std::move(value); }

void SceneObject::applyState(State state)
{
    Expects(m_bodies->getNumBodies() > 0);
    bool doUpdateModel{true};
    this->m_model->setPosition(state(StateType::Position));
    this->m_model->updateFrames(doUpdateModel);

    // set the content of this->m_state
    // do not change the shared_ptr
    *this->m_state = std::move(state);
}

rl::mdl::Joint *SceneObject::getJointOfDof(std::size_t dof_index, std::size_t &index_in_joint) const
{
    // TODO(Jules): uses only DofPosition here, is that OK?
    std::size_t jntFirstDof{0};
    std::size_t iJnt{0};
    ::rl::mdl::Joint *jnt = nullptr;
    while (jntFirstDof < m_kinematic->getDofPosition()) {
        jnt = m_kinematic->getJoint(iJnt);
        if (jntFirstDof + jnt->getDofPosition() > dof_index) {
            index_in_joint = dof_index - jntFirstDof;
            return jnt;
        }
        jntFirstDof = jntFirstDof + jnt->getDofPosition();
        ++iJnt;
    }
    throw std::runtime_error("no joint for DoF " + std::to_string(dof_index) + " in robot " +
                             m_name);
}

size_t SceneObject::getJointIndexOfDof(std::size_t dof_index, std::size_t &index_in_joint) const
{
    // TODO(Jules): uses only DofPosition here, is that OK?
    std::size_t jntFirstDof{0};
    std::size_t iJnt{0};
    ::rl::mdl::Joint *jnt = nullptr;
    while (jntFirstDof < m_kinematic->getDofPosition()) {
        jnt = m_kinematic->getJoint(iJnt);
        if (jntFirstDof + jnt->getDofPosition() > dof_index) {
            index_in_joint = dof_index - jntFirstDof;
            return iJnt;
        }
        jntFirstDof = jntFirstDof + jnt->getDofPosition();
        ++iJnt;
    }
    throw std::runtime_error("no joint for DoF " + std::to_string(dof_index) + " in robot " +
                             m_name);
}

size_t SceneObject::getJointFirstDof(std::size_t joint_index) const
{
    // TODO(Jules): uses only DofPosition here, is that OK?
    std::size_t jntFirstDof{0};
    ::rl::mdl::Joint *jnt = nullptr;
    for (size_t i = 0; i < joint_index; ++i) {
        jnt = m_kinematic->getJoint(i);
        jntFirstDof = jntFirstDof + jnt->getDofPosition();
    }
    return jntFirstDof;
}

std::size_t SceneObject::getNumberOfDof(StateType::Type type) const
{
    Expects(this->m_kinematic);
    if (type == StateType::Position) {
        return this->m_kinematic->getDofPosition();
    } else {
        return this->m_kinematic->getDof();
    }
}

std::size_t SceneObject::getNumberOfBodies() const { return this->m_bodies->getNumBodies(); }

::rl::sg::Body *SceneObject::getBody(const std::string &name)
{
    for (auto &b : *m_bodies) {
        if (b->getName() == name) {
            return b;
        }
    }
    return nullptr;
}

::rl::mdl::Frame *SceneObject::getLink(const std::string &name) const
{
    for (size_t i = 0; i < m_kinematic->getFrames(); ++i) {
        auto f = m_kinematic->getFrame(i);
        if (f->getName() == name) {
            return f;
        }
    }
    return nullptr;
}

int SceneObject::getLinkIndex(const std::string &name) const
{
    for (size_t i = 0; i < m_kinematic->getFrames(); ++i) {
        auto f = m_kinematic->getFrame(i);
        if (f->getName() == name) {
            return (int)i;
        }
    }
    return -1;
}

int SceneObject::getJointIndex(const std::string &name) const
{
    for (size_t i = 0; i < m_kinematic->getJoints(); ++i) {
        auto j = m_kinematic->getJoint(i);
        if (j->getName() == name) {
            return (int)i;
        }
    }
    return -1;
}

::rl::mdl::Joint *SceneObject::getJoint(const std::string &name) const
{
    int i = getJointIndex(name);
    if (i >= 0) {
        return m_kinematic->getJoint((size_t)i);
    } else {
        return nullptr;
    }
}

rl::mdl::Joint *SceneObject::getJoint(size_t i) const
{
    Expects(i < m_kinematic->getJoints());
    if (i >= m_kinematic->getJoints())
        throw std::out_of_range("index " + std::to_string(i) +
                                " is out of range in joint list of object " + getName() +
                                " (max=" + std::to_string(m_kinematic->getJoints()) + ")");
    return m_kinematic->getJoint(i);
}

size_t SceneObject::getJointNumber() const { return m_kinematic->getJoints(); }

DegreeOfFreedomInfo SceneObject::getDegreeOfFreedomInfo(std::size_t index,
                                                        StateType::Type type) const
{
    return DegreeOfFreedomInfo(getId(), getMinimum(index, type), getMaximum(index, type),
                               wrapAround(index, type), type, index);
}

std::size_t SceneObject::getSceneIndex() const { return this->m_index; }

/**
 * minimal value of the indicated DoF
 */
move4d::Real SceneObject::getMinimum(std::size_t index, StateType::Type type) const
{
    return (*this->m_minimum)(index, type);
}

/**
 * maximal value of the indicated DoF
 */
move4d::Real SceneObject::getMaximum(std::size_t index, StateType::Type type) const
{
    return (*this->m_maximum)(index, type);
}

/**
 * does the degree of freedom wraps around (e.g. is the position of a continuous rotation DoF)
 *
 * it should never be true for something else than POSITION
 */
bool SceneObject::wrapAround(std::size_t index, StateType::Type type) const
{
    std::size_t inDof{};
    return getJointOfDof(index, inDof)->wraparound(inDof);
}

move4d::Real SceneObject::distanceSquared(const State &q1, const State &q2,
                                          StateType::Type type) const
{
    assert(q1.size(type) == this->getNumberOfDof(type));
    assert(q2.size(type) == this->getNumberOfDof(type));

    Real d = 0;

    for (::std::size_t i = 0; i < this->getNumberOfDof(type); ++i) {
        Real delta = ::std::abs(q2(i, type) - q1(i, type));

        if (type == StateType::Position && this->wrapAround(i, type)) {
            Real range = ::std::abs(this->getMaximum(i, type) - this->getMinimum(i, type));
            d += ::std::pow(::std::min(delta, ::std::abs(range - delta)), 2);
        } else {
            d += ::std::pow(delta, 2);
        }
    }

    return d;
}

/**
 * need to be called whenever models are modified.
 *
 * refreshes m_state to fit the number of dof
 */
void SceneObject::updateModels()
{
    Expects(this->m_kinematic && this->m_bodies && this->m_model &&
            this->m_kinematic.get() == this->m_model->mdl &&
            this->m_bodies.get() == this->m_model->model &&
            this->m_bodies->getNumBodies() == this->m_kinematic->getBodies());

    for (size_t ijnt = 0; ijnt < m_kinematic->getJoints(); ++ijnt) {
        auto sixdof = dynamic_cast<::rl::mdl::SixDof *>(m_kinematic->getJoint(ijnt));
        // TODO(Jules): limits of sixdof joints might not be the scene bounds
        if (sixdof != nullptr) {
            sixdof->min.segment(0, 3) = m_scene.getSpaceBounds().first;
            sixdof->max.segment(0, 3) = m_scene.getSpaceBounds().second;
        }
    }
    math::Vector q_init(m_kinematic->getDofPosition());
    q_init.setZero();
    this->m_kinematic->normalize(q_init);
    this->m_kinematic->setPosition(q_init);
    this->m_state =
        std::make_shared<State>(this, this->m_model->getDofPosition(), this->m_model->getDof());
    this->m_state->operator()(mdl::StateType::Position) = q_init;

    this->m_minimum =
        std::make_shared<State>(this, this->m_model->getDofPosition(), this->m_model->getDof());
    this->m_maximum =
        std::make_shared<State>(this, this->m_model->getDofPosition(), this->m_model->getDof());

    (*this->m_minimum)(StateType::Position) = this->m_kinematic->getMinimum();
    (*this->m_maximum)(StateType::Position) = this->m_kinematic->getMaximum();

    Ensures(this->m_state->size(StateType::Position) == this->m_model->getDofPosition());
    Ensures(this->m_state->size(StateType::Speed) == this->m_model->getDof());

    Ensures((*this->m_minimum)(StateType::Position) == this->m_model->getMinimum());
    Ensures((*this->m_maximum)(StateType::Position) == this->m_model->getMaximum());
}

void SceneObject::setKinematic(std::shared_ptr<Kinematic> kin)
{
    m_kinematic = std::move(kin);
    m_model->mdl = m_kinematic.get();
    m_model->updateFrames();
}

bool SceneObject::computeInverseKinematic(const Chain &chain, const math::Transform &pose,
                                          const mdl::State &state, math::Vector &x_in_out) const
{
    Expects(x_in_out.size() == chain.kinematic->getDof());
    m_kinematic->setPosition(state(StateType::Position));
    m_kinematic->forwardPosition();

    // The first transform is the fixed from the world to the first frame of the chain
    auto chain_fixed_root = chain.kinematic->getTransform(0);
    chain_fixed_root->x.transform() = chain.rootFrame->x.transform();
    chain.kinematic->setPosition(x_in_out);

    ::rl::mdl::JacobianInverseKinematics solver(chain.kinematic.get());
    // We assume the first tool (and the only one) in the chain is the end_effector.
    solver.goals.emplace_back(pose, 0);
    solver.svd = true;
    solver.delta = 1e-1;
    solver.epsilon = 1e-4;
    bool valid = solver.solve();
    // M4D_DEBUG("computeInverseKinematic dist = "
    //           << chain.kinematic->distance(x_in_out, chain.kinematic->getPosition()));
    x_in_out = chain.kinematic->getPosition();
    chain.kinematic->calculateJacobian();
    valid = valid && !chain.kinematic->isSingular();
    return valid;
}

bool SceneObject::computeForwardKinematic(const Chain &chain, math::Transform &pose_out,
                                          const State &state, const math::Vector &x_in) const
{
    m_kinematic->setPosition(state(StateType::Position));
    m_kinematic->forwardPosition();
    // The first transform is the fixed from the world to the first frame of the chain
    auto chain_fixed_root = chain.kinematic->getTransform(0);
    Expects(chain.kinematic->isValid(x_in));
    chain_fixed_root->x.transform() = chain.rootFrame->x.transform();
    chain.kinematic->setPosition(x_in);
    chain.kinematic->forwardPosition();
    pose_out = chain.kinematic->getOperationalPosition(0);
    return true;
}

math::Vector Chain::configFromState(const State &state) const
{
    math::Vector x(bijection.size());
    for (size_t j = 0; j < bijection.size(); ++j) {
        x[j] = state(bijection[j], mdl::StateType::Position);
    }
    return x;
}

math::Vector Chain::configFromWorldState(const WorldState &ws, Identifier object_id) const
{
    return configFromState(ws[object_id]);
}

} // namespace mdl

} // namespace move4d

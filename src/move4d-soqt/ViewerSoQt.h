//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef _MOVE4D_SOQT_VIEWERSOQT_H
#define _MOVE4D_SOQT_VIEWERSOQT_H

#include "move4d/view/Viewer.h"
#include <QWidget>

class SoQtExaminerViewer;
class SoVRMLGroup;
namespace rl
{
namespace sg
{
namespace so
{
class Scene;
} // namespace so
} // namespace sg
} // namespace rl

namespace move4d
{
namespace view
{

class ViewerSoQt : public QWidget, public Viewer
{
    Q_OBJECT
  public:
    using InvalidTypeException = std::invalid_argument;
    ViewerSoQt(QWidget *parent = nullptr, const Qt::WindowFlags &f = 0);
    ~ViewerSoQt() override;

    void setScene(std::shared_ptr<rl::sg::Scene> scene) final;
    void updateModels() final;

    // ViewerInterface interface
  public slots:
    void update(const move4d::mdl::WorldPositions &positions) override;
    void update(const move4d::mdl::WorldState &positions) override;
    void drawConfiguration(size_t robot_index, const rl::math::Vector &q) override;
    virtual void drawConfiguration(unsigned int robot_index, const rl::math::Vector &q);
    void drawState(const move4d::mdl::State &state) override;
    void drawPose(const rl::math::Transform &pose) override;
    void drawStampedPose(const rl::math::Transform &pose, const rl::math::Transform &parent,
                         const std::string &name) override;
    void resetPoses() override;

    void drawConfiguration(const rl::math::Vector &q) override;
    void drawConfigurationEdge(const rl::math::Vector &q0, const rl::math::Vector &q1,
                               const bool &free) override;
    void drawConfigurationPath(const rl::plan::VectorList &path) override;
    void drawConfigurationVertex(const rl::math::Vector &q, const bool &free) override;
    void drawLine(const rl::math::Vector &xyz0, const rl::math::Vector &xyz1) override;
    void drawPoint(const rl::math::Vector &xyz) override;
    void drawSphere(const rl::math::Vector &center, const double &radius) override;
    void drawWork(const rl::math::Transform &t) override;
    void drawWorkEdge(const rl::math::Vector &q0, const rl::math::Vector &q1) override;
    void drawWorkPath(const rl::plan::VectorList &path) override;
    void drawWorkVertex(const rl::math::Vector &q) override;
    void reset() override;
    void resetEdges() override;
    void resetLines() override;
    void resetPoints() override;
    void resetSpheres() override;
    void resetVertices() override;
    void showMessage(const std::string &message) override;

    void saveImage(const std::string &filename) override;
    void saveImage();
    /// this blocks the display until the function exits. Prefer to trigger update() from an other
    /// thread for each path state
    void playPath(const move4d::mdl::WorldState &ws_init,
                  const move4d::motion::GeometricPath &path) override;
    void toggleRecording(bool record) override;

    void viewAll();

  protected:
    static std::shared_ptr<rl::sg::so::Scene> createScene();

    void paint() override;

    std::unique_ptr<SoQtExaminerViewer> m_viewer;

  private:
    struct PImpl;
    std::unique_ptr<PImpl> pimpl;
    bool _recording_enabled = false;
    QTimer *_recording_timer;
};

} // namespace view
} // namespace move4d

#endif // _MOVE4D_SOQT_VIEWERSOQT_H

//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#include "move4d/plan/ompl/KinematicGraphStateSpace.h"
#include "move4d/common.h"
#include "move4d/mdl/SceneObject.h"
#include "move4d/mdl/kin/KinematicSolver.h"
#include "move4d/plan//KinematicGraphSearchSpace.h"
#include "move4d/plan/SO3Space.h"
#include "move4d/plan/ompl/converters/StateSpaceFactory.h"
#include "move4d/plan/ompl/converters/converters.h"
#include "move4d/utils/io/print_to_stream.hpp"

namespace move4d
{
namespace plan
{
namespace ompl
{

INIT_MOVE4D_STATIC_LOGGER(KinematicGraphStateSpace, "move4d.plan.ompl.kinematicgraphstatespace");
INIT_MOVE4D_STATIC_LOGGER(KinematicGraphStateSampler,
                          "move4d.plan.ompl.kinematicgraphstatesampler");
INIT_MOVE4D_STATIC_LOGGER(KinematicDataChain, "move4d.plan.ompl.kinematicdatachain");

KinematicGraphStateSpace::KinematicGraphStateSpace(std::unique_ptr<KinematicGraphSearchSpace> space,
                                                   mdl::WorldStatePtr init_ws)
    : StateSpace(*space, std::move(init_ws)), m_search_space(std::move(space))
{
    addPoseComponents(*m_search_space);
}
KinematicGraphStateSpace::KinematicGraphStateSpace(const KinematicGraphSearchSpace &space,
                                                   mdl::WorldStatePtr init_ws)
    : StateSpace(space, std::move(init_ws)),
      m_search_space(space.cloneSearchSpace().release()->as<KinematicGraphSearchSpace>())
{
    addPoseComponents(*m_search_space);
}

void KinematicGraphStateSpace::interpolate(const ::ompl::base::State *from_,
                                           const ::ompl::base::State *to_, double t,
                                           ::ompl::base::State *state_) const
{
    // TODO(Jules): WIP KinematicGraphStateSpace::interpolate
    // auto from = from_->as<StateType>();
    // auto to = to_->as<StateType>();
    auto state = state_->as<StateType>();
    state->flags = 0;

    // interpolate FK chain configurations and sampled poses, compute FK (update poses), compute IK

    // 1. interpolate Forward chains

    StateSpace::interpolate(from_, to_, t, state_);

    // ====
    // size_t first_dof = 0;
    // for (size_t i = 0; i < m_elements.size(); ++i) {
    //     auto &sswrap = m_subspaces[m_poses[i].subspace];
    //     auto &ss = sswrap->getStateSpace();
    //     if (m_poses[i].method == PoseComponent::Method::FkOnly) {
    //         auto sfrom = convertStateForSubspace(sswrap, from, first_dof);
    //         auto sto = convertStateForSubspace(sswrap, to, first_dof);
    //         auto s_state = createStateForSubspace(sswrap, state, first_dof);
    //         M4D_TRACE("substate " << i << ":\n" << PRINT_TO_STREAM(ss->printState(s_state,
    //         oss))); ss->interpolate(sfrom, sto, t, s_state); copyValuesFromSubState(state,
    //         sswrap, s_state, first_dof);
    //     }
    //     first_dof += ss->getDimension();
    // }
    //
    // first_dof = 0;

    // TODO(Jules): 2. interpolate free poses (that are not given by any Forward chain)

    // 3. compute FK and IK
    computeK(state);
    M4D_TRACE("KinematicGraphStateSpace::interpolate - "
              << PRINT_TO_STREAM(this->readableFlag(state->flags, oss)));
}

::ompl::base::StateSamplerPtr KinematicGraphStateSpace::allocDefaultStateSampler() const
{
    return ::ompl::base::StateSamplerPtr(new KinematicGraphValidStateSampler(this));
}

bool KinematicGraphStateSpace::computeIK(size_t pose_i, StateType *state_,
                                         const mdl::WorldState &ws,
                                         const StateSpaceWrapperPtr &wrapper) const
{
    assert(satisfiesBounds(state_));
    if (m_elements[pose_i].method != Element::Method::Inverse)
        return true;
    auto state = state_->as<StateType>();
    const auto &element = m_elements[pose_i];
    M4D_TRACE("computeIK for pose " << pose_i << " source=" << element.source
                                    << " target=" << element.target);

    bool valid = element.kinematic->computeIK(ws, state, element);

    if (valid) {
        // check bounds
        valid = valid && this->satisfiesBounds(state);
        if (!valid) {
            M4D_TRACE("invalid IK because out of bounds");
        }
    } else {
        M4D_TRACE("invalid IK");
    }

    if (!valid) {
        for (size_t i = 0; i < element.bijection.size(); ++i) {
            state->values[element.bijection[i]] = 0;
        }
        state->flags |= StateType::ValidityComputed;
        state->flags &= ~StateType::IsValid;
        return false;
    }
    return true;
}
bool KinematicGraphStateSpace::computeFK(size_t pose_i, StateType *state_,
                                         const mdl::WorldState &ws,
                                         const StateSpaceWrapperPtr &wrapper) const
{
    assert(satisfiesBounds(state_));
    Expects(!m_wrapper.expired()); // need to call setWrapper before
    if (m_elements[pose_i].method != Element::Method::Forward) {
        return true; // don't compute FK for this one
    }

    auto state = state_->as<StateType>();
    const auto &element = m_elements[pose_i];

    M4D_TRACE("computeFK for pose " << pose_i << " source=" << element.source
                                    << " target=" << element.target);

    bool valid = element.kinematic->computeFK(ws, state, element, state->vertices[element.source],
                                              state->vertices[element.target]);

    M4D_TRACE("computed FK (valid=" << valid << ")\n" << state->vertices[element.target].matrix());
    converters::toState(state->vertices[element.target], state->poses[element.target]);
    if (!valid) {
        state->flags |= StateType::ValidityComputed;
        state->flags &= ~StateType::IsValid;
        std::cout << "invalid fk\n";
        return false;
    }

    return true;
}

bool KinematicGraphStateSpace::computeIK(KinematicGraphStateSpace::StateType *state) const
{
    Expects(m_initWorldState);     // need to call setInitWorldState before
    Expects(!m_wrapper.expired()); // need to call setWrapper before
    auto wrapper = m_wrapper.lock();
    if (!wrapper) {
        throw std::runtime_error(
            "move4d::plan::ompl::KinematicGraphStateSpace StateSpaceWrapper unset or destructed");
    }

    if (state->flags & StateType::IkComputed) {
        return state->flags & StateType::IsValid;
    }
    state->flags |= StateType::IkComputed;
    // Compute the worldstate based on the current state
    mdl::WorldState ws = *m_initWorldState;
    wrapper->applyState(state, ws);
    for (size_t i = 0; i < getNumberOfElements(); i++) {
        if (!computeIK(i, state, ws, wrapper))
            return false;
    }
    state->flags |= StateType::IsValid;
    return true;
}

bool KinematicGraphStateSpace::computeFK(KinematicGraphStateSpace::StateType *state) const
{
    Expects(m_initWorldState);     // call setInitWorldState before
    Expects(!m_wrapper.expired()); // need to call setWrapper before
    auto wrapper = m_wrapper.lock();
    if (!wrapper) {
        throw std::runtime_error(
            "move4d::plan::ompl::KinematicGraphStateSpace StateSpaceWrapper unset or destructed");
    }

    // TODO(Jules): this Expects(satisfiesBounds) was here for some reason, but breaks with a
    // Reeds&Shepp part when interpolating close to environment limits
    // Expects(this->satisfiesBounds(state));
    if (state->flags & StateType::FkComputed) {
        return state->flags & StateType::IsValid;
    }
    state->flags |= StateType::FkComputed;
    // Compute the worldstate based on the current state
    mdl::WorldState ws = *m_initWorldState;
    wrapper->applyState(state, ws);
    for (size_t i = 0; i < getNumberOfElements(); i++) {
        if (!computeFK(i, state, ws, wrapper)) {
            return false;
        }
    }
    state->flags |= StateType::IsValid;
    return true;
}

bool KinematicGraphStateSpace::computeK(KinematicGraphStateSpace::StateType *state) const
{
    Expects(m_initWorldState);     // call setInitWorldState before
    Expects(!m_wrapper.expired()); // need to call setWrapper before
    auto wrapper = m_wrapper.lock();
    if (!wrapper) {
        throw std::runtime_error(
            "move4d::plan::ompl::KinematicGraphStateSpace StateSpaceWrapper unset or destructed");
    }
    mdl::WorldState ws = *m_initWorldState;
    wrapper->applyState(state, ws);
    bool res = true;

    for (size_t i = 0; res && i < m_elements.size(); ++i) {
        const auto &element = m_elements[i];
        switch (element.method) {
        case Element::Inverse:
            res = res && computeIK(i, state, ws, wrapper);
            break;
        case Element::Forward:
            res = res && computeFK(i, state, ws, wrapper);
            break;
        }
    }
    state->flags |= StateType::FkComputed | StateType::IkComputed;
    if (res) {
        state->flags |= StateType::IsValid;
    }
    Ensures(res == state->isValid());
    Ensures(!res || state->kinComputed()); // (valid => kin computed)
    return res;
}

::ompl::base::State *KinematicGraphStateSpace::allocState() const
{
    auto state = new StateType();
    allocStateMembers(state);
    return state;
}

void KinematicGraphStateSpace::freeState(::ompl::base::State *s) const
{
    auto state = s->as<StateType>();
    delete[] state->vertices;
    for (size_t i = 0; i < m_vertices_count; ++i) {
        m_vertices_state_spaces[i]->freeState(state->as<StateType>()->poses[i]);
    }
    delete[] state->as<StateType>()->poses;
    StateSpace::freeState(s);
}

void KinematicGraphStateSpace::printState(const ::ompl::base::State *state_in,
                                          std::ostream &out) const
{
    auto state = state_in->as<StateType>();
    size_t current_first_dof = 0;
    out << "move4d::plan::ompl::KinematicGraphStateSpace::StateType of dimension " << getDimension()
        << "[[subspaces:[\n";
    for (const auto &ss : m_subspaces) {
        auto ss1 = convertStateForSubspace(ss, state->as<StateType>(), current_first_dof,
                                           current_first_dof);
        ss->getStateSpace()->printState(ss1, out);
        freeSubspaceState(ss, ss1);
    }
    assert(current_first_dof == getVariableCount());
    out << "],\nelements:[\n";
    for (size_t i = 0; i < m_elements.size(); ++i) {
        out << "method: ";
        switch (m_elements[i].method) {
        case Element::Method::Forward:
            out << "FK";
            break;
        case Element::Method::Inverse:
            out << "IK";
            break;
        }
        out << " - bijection=" << m_elements[i].bijection;
        out << "\n";
    }
    out << "],\nvertices:[\n";
    for (size_t i = 0; i < m_vertices_count; ++i) {
        m_vertices_state_spaces[i]->printState(state->poses[i], out);
        out << "\t[" << state->vertices[i].translation().transpose() << ",\n\t "
            << math::Quaternion(state->vertices[i].rotation()).coeffs().transpose() << "]\n";
    }
    out << "]\n";
    readableFlag(state->flags, out);
    out << "]\n";
}
void KinematicGraphStateSpace::readableFlag(int flags, std::ostream &out) const
{
    out << "flags: ";
    if (flags & StateSpace::StateType::IsGoal) {
        out << "goal; ";
    }
    if (flags & StateSpace::StateType::IsStart) {
        out << "start; ";
    }
    if (flags & StateSpace::StateType::ValidityComputed) {
        if (flags & StateSpace::StateType::IsValid) {
            out << "valid; ";
        } else {
            out << "invalid; ";
        }
    }
    if (flags & StateType::FkComputed) {
        if (flags & StateSpace::StateType::IsValid) {
            out << "fk valid; ";
        } else {
            out << "fk computed; ";
        }
    }
    if (flags & StateType::IkComputed) {
        if (flags & StateSpace::StateType::IsValid) {
            out << "ik valid; ";
        } else {
            out << "ik computed; ";
        }
    }
    out << "\n";
}

void KinematicGraphStateSpace::copyState(::ompl::base::State *destination,
                                         const ::ompl::base::State *source) const
{
    StateSpace::copyState(destination, source);
    for (size_t i = 0; i < m_vertices_count; ++i) {
        m_vertices_state_spaces[i]->copyState(destination->as<StateType>()->poses[i],
                                              source->as<StateType>()->poses[i]);
    }
}

void KinematicGraphStateSpace::addPoseComponents(const SearchSpace &searchSpace)
{
    size_t i = 0;
    addPoseComponents(searchSpace, i);
    assert(i == this->getVariableCount());
    // Ensures(i == searchSpace->getDimension());
}

void KinematicGraphStateSpace::addPoseComponents(const SearchSpace &searchSpace, size_t &firstDof)
{
    M4D_DEBUG("addPoseComponents " << std::quoted(searchSpace.getName())
                                   << " dimension=" << searchSpace.getDimension());
    switch (searchSpace.getType()) {
    case SearchSpace::Type::Compound: {
        M4D_DEBUG("... of type Compound");
        auto &subspaces = dynamic_cast<const CompoundSearchSpace &>(searchSpace).getSubspaces();
        for (size_t i = 0; i < subspaces.size(); ++i) {
            addPoseComponents(subspaces[i], firstDof);
        }
        break;
    }
    case SearchSpace::Type::KinematicGroup: {
        M4D_DEBUG("... of type KinematicGroup");
        const auto &space = dynamic_cast<const RobotKinematicGroupSpace &>(searchSpace);
        const auto &chain = *space.getKinematicChain();
        std::vector<size_t> bijection;
        bijection.reserve(chain.bijection.size());
        // for (size_t i = firstDof; i < firstDof + chain.bijection.size(); ++i) {
        for (size_t i = firstDof; i < firstDof + searchSpace.getDimension(); ++i) {
            bijection.push_back(i);
        }
        M4D_TRACE("    bijection=" << bijection);
        assert(bijection.size() == searchSpace.getDimension());
        size_t subspace_i = 0;
        size_t current_subspace_dof = 0;
        while (current_subspace_dof < firstDof) {
            current_subspace_dof += getSubspaces()[subspace_i]->getSearchSpace()->getDimension();
            M4D_TRACE(
                "subspace " << subspace_i << " "
                            << std::quoted(getSubspaces()[subspace_i]->getStateSpace()->getName())
                            << " dimension = "
                            << getSubspaces()[subspace_i]->getSearchSpace()->getDimension());
            ++subspace_i;
        }

        assert(firstDof == current_subspace_dof);
        assert(subspace_i < getSubspaces().size());
        // assert(bijection.size() == space->getDimension());
        // assert(bijection.size() == getSubspaces()[subspace_i]->getStateSpace()->getDimension());

        // TODO(Jules): FIX URGENT not necessarily FkOnly nor IkOnly
        std::unique_ptr<KinematicData> kinematic = std::make_unique<KinematicDataChain>(
            space.robot(), space.getKinematicChain(), space.getKinematicSolver());
        m_elements.emplace_back();
        auto &element = m_elements.back();
        element.bijection = std::move(bijection);
        // element.subspace = subspace_i;
        element.kinematic = std::move(kinematic);
        firstDof += searchSpace.getDimension();
        auto kin_wrapper = dynamic_cast<const KinematicSpaceWrapper *>(&space);
        if (kin_wrapper && (kin_wrapper->getSpace().getType() == SearchSpace::Type::SO3 ||
                            kin_wrapper->getSpace().getType() == SearchSpace::Type::SE3)) {
            // TODO(Jules): hack for quaternions having 4 values but only 3 dofs
            // --firstDof;
        }

        break;
    }
    case SearchSpace::Type::KinematicGraph: {
        M4D_DEBUG("... of type KinematicGraph");
        // maps vertex names as they appear in the SearchSpace to their index as they will
        // appear in the state (StateType::vertices)
        std::unordered_map<std::string, size_t> vertices;
        size_t index{};
        // function to try insert an element in the vertices map
        // side-effect: increments index when a vertex is added
        auto insert = [&vertices, &index](const std::string &str) {
            if (vertices.find(str) == vertices.end()) {
                if (vertices.insert({str, index}).second) {
                    // if the vertex was not already in the map
                    ++index;
                }
            }
        };
        // for each element of the searchspace, try insert the source and target vertices
        std::for_each(m_search_space->order().begin(), m_search_space->order().end(),
                      [&insert, this](const auto &edge) {
                          insert(edge.source);
                          insert(edge.target);
                      });
        m_vertices_count = index;
        {
            Real inf = std::numeric_limits<Real>::max() / 2;
            ::ompl::base::RealVectorBounds bounds(3);
            bounds.setLow(-inf);
            bounds.setHigh(inf);
            for (size_t i = 0; i < m_vertices_count; ++i) {
                m_vertices_state_spaces.emplace_back(new ::ompl::base::SE3StateSpace());
                m_vertices_state_spaces.back()->setBounds(bounds);
            }
        }
        M4D_DEBUG("vertices " << vertices);

        const auto &space = dynamic_cast<const KinematicGraphSearchSpace &>(searchSpace);
        for (size_t ed = 0; ed < space.order().size(); ++ed) {
            auto &x = space.getEdge(ed);
            if (x.type != KinematicGraphSearchSpace::Edge::Fixed) {
                addPoseComponents(*x.space, firstDof);
                switch (x.type) {
                case KinematicGraphSearchSpace::Edge::Forward:
                    m_elements.back().method = Element::Method::Forward;
                    break;
                case KinematicGraphSearchSpace::Edge::Inverse:
                    m_elements.back().method = Element::Method::Inverse;
                    break;
                default:
                    break;
                }
            } else {
                M4D_DEBUG("addPoseComponent fixed");
                m_elements.emplace_back();
                auto &element = m_elements.back();
                element.method = KinematicGraphStateSpace::Element::Method::Forward;
                element.kinematic = std::make_unique<KinematicDataFixed>(x.transform.get());
            }
            std::vector<size_t> args; // image of e.sampler_args but as
                                      // indices of poses in KinematicGraphStateSpace::StateType
            const auto &e = m_search_space->getEdge(ed);
            for (auto &a : e.sampler_args) {
                auto search = vertices.find(a);
                if (search != vertices.end()) {
                    args.push_back(search->second);
                } else {
                    assert(false && "unknown pose");
                    throw KinematicGraphException("unknown pose");
                }
            }
            M4D_DEBUG("add element " << m_elements.size() - 1 << " from "
                                     << m_search_space->getSource(ed) << " to "
                                     << m_search_space->getTarget(ed));
            auto &elem = m_elements.back();
            if (e.sampler_allocator) {
                // share a const reference to the search space among the samplers
                std::shared_ptr<const SearchSpace> sptr(e.space->cloneSearchSpace());
                auto spl_alloc = e.sampler_allocator;
                elem.sampler_allocator = [spl_alloc, sptr]() { return spl_alloc(sptr); };
            }
            elem.sampler_args = std::move(args);
            elem.source = vertices[m_search_space->getSource(ed)];
            elem.target = vertices[m_search_space->getTarget(ed)];
        }

        // add the attachments
        // link the searchspace of the attached object to the state space of the pose
        for (const auto &attachments : m_search_space->attachments()) {
            auto search_vertex = vertices.find(attachments.first);
            if (search_vertex == vertices.end()) {
                throw(KinematicGraphException(std::string(__PRETTY_FUNCTION__) + " vertex " +
                                              attachments.first + " not found"));
            }
            for (const auto &a : attachments.second) {
                // link the move4d search space to the ompl state space representing the pose
                m_vertices_state_spaces[search_vertex->second]->setup();
                m_attachments.push_back(
                    {std::make_shared<StateSpaceWrapper>(
                         a->cloneSearchSpace(), m_vertices_state_spaces[search_vertex->second]),
                     search_vertex->second});
            }
        }
        break;
    }
    default:
        M4D_DEBUG("... skipped: sub space " << searchSpace.getName());
        firstDof += searchSpace.getDimension();
        if (searchSpace.getType() == SearchSpace::Type::SO3 ||
            searchSpace.getType() == SearchSpace::Type::SE3) {
            // TODO(Jules): hack for quaternions having 4 values but only 3 dofs
            // --firstDof;
        }
        break;
    }
    M4D_DEBUG("next dof (incremented firstDof)=" << firstDof);
}
void KinematicGraphStateSpace::allocStateMembers(StateSpace::StateType *s) const
{
    StateSpace::allocStateMembers(s);
    auto *state = s->as<StateType>();
    state->poses = new ::ompl::base::SE3StateSpace::StateType *[m_vertices_count];
    for (size_t i = 0; i < m_vertices_count; ++i) {
        state->poses[i] =
            m_vertices_state_spaces[i]->allocState()->as<::ompl::base::SE3StateSpace::StateType>();
        m_vertices_state_spaces[i]->enforceBounds(state->poses[i]);
    }
    state->vertices = new math::Transform[m_vertices_count];
    for (size_t i = 0; i < m_vertices_count; ++i) {
        state->vertices[i] = math::Transform::Identity();
    }
}

const std::vector<std::shared_ptr<::ompl::base::SE3StateSpace>> &
KinematicGraphStateSpace::getVerticesStateSpaces() const
{
    return m_vertices_state_spaces;
}

void KinematicGraphStateSpace::sanityChecks() const
{
    uint flags = ~(0
                   // | STATESPACE_DISTANCE_DIFFERENT_STATES //
                   // | STATESPACE_DISTANCE_SYMMETRIC      //
                   | STATESPACE_INTERPOLATION //
                   | STATESPACE_TRIANGLE_INEQUALITY
                   // | STATESPACE_DISTANCE_BOUND          //
                   // | STATESPACE_RESPECT_BOUNDS          //
                   // | STATESPACE_ENFORCE_BOUNDS_NO_OP    //
                   // |
                   | STATESPACE_SERIALIZATION);
    return sanityChecks(std::numeric_limits<double>::min() * 128,
                        std::numeric_limits<double>::epsilon() * 2048, flags);
}

const std::vector<std::pair<StateSpaceWrapperPtr, size_t>> &
KinematicGraphStateSpace::getAttachments() const
{
    return m_attachments;
}

const std::vector<KinematicGraphStateSpace::Element> &KinematicGraphStateSpace::getElements() const
{
    return m_elements;
}

KinematicGraphStateSampler::KinematicGraphStateSampler(const KinematicGraphStateSpace *space)
    : DefaultStateSampler(space), m_samplers(space->getElements().size())
{
    for (size_t i = 0; i < m_samplers.size(); ++i) {
        const auto &el = space->getElements()[i];
        if (el.sampler_allocator) {
            m_samplers[i] = el.sampler_allocator();
        }
    }
}

void KinematicGraphStateSampler::sampleUniform(::ompl::base::State *s)
{
    Expects(space_->as<KinematicGraphStateSpace>()->getInitWorldState());
    Expects(!space_->as<KinematicGraphStateSpace>()->getWrapper().expired());
    auto space = space_->as<KinematicGraphStateSpace>();
    auto state = s->as<KinematicGraphStateSpace::StateType>();
    do {
        state->flags = 0;
        // shoot degrees of freedom
        DefaultStateSampler::sampleUniform(s);

        mdl::WorldState ws = *space->getInitWorldState();
        auto wrapper = space->getWrapper().lock();
        wrapper->applyState(state, ws);
        for (size_t i = 0; i < m_samplers.size(); ++i) {
            const auto &el = space->getElements()[i];
            if (m_samplers[i]) {
                assert(std::is_sorted(el.bijection.begin(), el.bijection.end()));
                auto state_span =
                    gsl::span<double>(state->values + el.bijection.front(), el.bijection.size());
                auto view = views::DofsViewContainer<gsl::span<double>>(state_span);
                M4D_TRACE("using user-defined sampler for pose " << i << " ");
                //<< std::quoted(space->getSubspaces()[space->getElements()[i].subspace]
                //                   ->getStateSpace()
                //                   ->getName()));
                // use user-defined sampler
                std::vector<math::Transform> inputs(el.sampler_args.size());
                auto tfs = iterators::make_transform_iterator(
                    el.sampler_args.begin(), el.sampler_args.end(),
                    [&state](const size_t &s) -> math::Transform { return state->vertices[s]; });
                std::copy(tfs.first, tfs.second, inputs.begin());

                m_samplers[i]->sampleUniform(view, state->vertices[el.source],
                                             state->vertices[el.target], inputs);

                converters::toState(state->vertices[el.target], state->poses[el.target]);

                M4D_TRACE(
                    "shot = ("
                    << el.target << ") t=[" << state->vertices[el.target].translation().transpose()
                    << "] r=["
                    << math::Quaternion(state->vertices[el.target].rotation()).coeffs().transpose()
                    << "] " << PRINT_TO_STREAM(space->readableFlag(state->flags, oss)));
                M4D_TRACE(PRINT_TO_STREAM(space->printState(state, oss)));
            } else if (space->getElements()[i].method ==
                       KinematicGraphStateSpace::Element::Method::Inverse) {
                // FIXME(Jules): the poses (vertices) are not taken into account by the kinematic
                // solver.
                wrapper->applyState(state, ws); // the fix
                M4D_TRACE("ik between (" << el.source << ") ["
                                         << state->vertices[el.source].translation().transpose()
                                         << "] - (" << el.target << ") ["
                                         << state->vertices[el.target].translation().transpose()
                                         << "]\n"
                                         << PRINT_TO_STREAM(space->printState(state, oss)));
                if (!space->computeIK(i, state, ws, wrapper)) {
                    M4D_TRACE("invalid IK for element " << i);
                    break;
                }
            } else if (space->getElements()[i].method ==
                       KinematicGraphStateSpace::Element::Method::Forward) {
                if (!space->computeFK(i, state, ws, wrapper)) {
                    M4D_TRACE("invalid FK");
                    break;
                }
                M4D_TRACE("forward pose = ("
                          << el.target << ") "
                          << state->vertices[el.target].translation().transpose() << " "
                          << PRINT_TO_STREAM(space->readableFlag(state->flags, oss)));
            } else {
                assert(false);
            }
            assert(space->satisfiesBounds(s));
        }

    } while (M4D_WHILE_CONDITION(state->validityComputed() == true && state->isValid() == false));

    state->flags |= KinematicGraphStateSpace::StateType::FkComputed |
                    KinematicGraphStateSpace::StateType::IkComputed;

    M4D_TRACE("sampleUniform result: " << PRINT_TO_STREAM(space->printState(s, oss)));

    assert(space->satisfiesBounds(s) || move4d::isStopping());
}

void KinematicGraphStateSampler::sampleUniformNear(::ompl::base::State *state,
                                                   const ::ompl::base::State *near, double distance)
{
    assert(false);
}

void KinematicGraphStateSampler::sampleGaussian(::ompl::base::State *state,
                                                const ::ompl::base::State *mean, double stdDev)
{
    assert(false);
}

void KinematicGraphStateSampler::setSampler(size_t i, BaseSamplerPtr sampler)
{
    m_samplers[i] = std::move(sampler);
}

const BaseSamplerPtr &KinematicGraphStateSampler::getSampler(size_t i) const
{
    return m_samplers[i];
}

KinematicGraphValidStateSampler::KinematicGraphValidStateSampler(
    const KinematicGraphStateSpace *space)
    : KinematicGraphStateSampler(space)
{
}

void KinematicGraphValidStateSampler::sampleUniform(::ompl::base::State *state_)
{
    auto *state = state_->as<StateType>();
    // TODO(Jules): optimize valid state sampler by splitting KinematicGraphStateSampler methods to
    // be able to reuse them with less overhead
    do {
        KinematicGraphStateSampler::sampleUniform(state);
    } while (M4D_WHILE_CONDITION(state->validityComputed() == true && state->isValid() == false));
}

void KinematicGraphValidStateSampler::sampleUniformNear(::ompl::base::State *state,
                                                        const ::ompl::base::State *near,
                                                        double distance)
{
    assert(false);
}

void KinematicGraphValidStateSampler::sampleGaussian(::ompl::base::State *state,
                                                     const ::ompl::base::State *mean, double stdDev)
{
    assert(false);
}

bool KinematicDataChain::computeIK(const mdl::WorldState &ws_in,
                                   KinematicGraphStateSpace::StateType *state,
                                   const KinematicGraphStateSpace::Element &element) const
{
    // TODO(Jules): we probably can avoid a copy of the state values in
    // KinematicGraphStateSpace::computeK by creating a math::Vector from the raw data

    // get the values of the WorldState to the vector (because some values of the chain may not be
    // represented in the state)
    math::Vector conf = chain->configFromWorldState(ws_in, object->getId());

    kin::KinematicSolverPtr solver =
        kin::KinematicSolverFactory::instance().create(kinematicSolver);
    bool valid = solver->computeInverseKinematic(*chain, state->vertices[element.source],
                                                 state->vertices[element.target], conf);
    if (!valid) {
        // reset the config
        conf = chain->configFromWorldState(ws_in, object->getId());
    }
    // put the values in state
    for (size_t b = 0; b < element.bijection.size(); ++b) {
        state->values[element.bijection[b]] = conf[b];
    }

    return valid;
}

bool KinematicDataChain::computeFK(const mdl::WorldState &ws_in,
                                   KinematicGraphStateSpace::StateType *state,
                                   const KinematicGraphStateSpace::Element &element,
                                   const math::Transform &base_pose,
                                   math::Transform &end_pose_out) const
{
    M4D_TRACE("computeFK for element - chain " << chain->name);
    // TODO(Jules): we probably can avoid a copy of the state values in
    // KinematicGraphStateSpace::computeK by creating a math::Vector from the raw data
    math::Vector x = chain->configFromWorldState(ws_in, object->getId());

    kin::KinematicSolverPtr solver =
        kin::KinematicSolverFactory::instance().create(kinematicSolver);
    return solver->computeForwardKinematic(*chain, base_pose, end_pose_out, x);
    //    return object->computeForwardKinematic(*endEffector, *chain, end_pose_out,
    //                                           *ws_in.getState(object->getId()), x);
}

bool KinematicDataFixed::computeIK(const mdl::WorldState &ws_in,
                                   KinematicGraphStateSpace::StateType *state_in_out,
                                   const KinematicGraphStateSpace::Element &element) const
{
    assert(false && "you shouldn't be here.");
    abort();
    return false;
}

bool KinematicDataFixed::computeFK(const mdl::WorldState &ws_in,
                                   KinematicGraphStateSpace::StateType *state,
                                   const KinematicGraphStateSpace::Element &element,
                                   const math::Transform &base_pose,
                                   math::Transform &end_pose_out) const
{
    end_pose_out = base_pose * transform;
    assert(end_pose_out.matrix().determinant() != 0);
    return true;
}

} // namespace ompl
} // namespace plan
} // namespace move4d

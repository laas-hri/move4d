//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_MDL_WORLDPOSITIONS_H
#define MOVE4D_MDL_WORLDPOSITIONS_H

#include "move4d/math/Vector.h"
#include <vector>

namespace move4d
{

namespace mdl
{

class WorldPositions
{
  public:
    typedef std::vector<move4d::math::Transform> BodyPositions;

  protected:
    std::vector<BodyPositions> m_position;
};

} // namespace mdl

} // namespace move4d
#endif

//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_PLAN_ROBOTPART_H
#define MOVE4D_PLAN_ROBOTPART_H

#include "move4d/common.h"
#include "move4d/plan/SearchSpace.h"
#include <unordered_map>

namespace move4d
{
namespace mdl
{
class SceneObject;
} // namespace mdl
namespace plan
{

class RobotPart : public CompoundSearchSpace
{
  public:
    RobotPart() = default;
    RobotPart(const RobotPart &) = default;
    RobotPart(RobotPart &&) = default;
    RobotPart &operator=(const RobotPart &) = default;
    RobotPart &operator=(RobotPart &&) = default;
    virtual ~RobotPart() = default;

    RobotPart(const std::string &name, mdl::SceneObject *object);
    inline const Identifier &getRobotId() const;

  protected:
    Identifier m_robotId;
};

class RobotPartLists
{
  public:
    void add(RobotPart part);
    const RobotPart &get(const Identifier &robotId, const std::string &partName) const;
    /// get the list of parts registered for the given robot
    std::vector<std::string> get(const Identifier &robotId) const;

  protected:
    std::unordered_map<Identifier, std::unordered_map<std::string, RobotPart>> m_list;
};

const Identifier &RobotPart::getRobotId() const { return m_robotId; }

} // namespace plan
} // namespace move4d

#endif // MOVE4D_PLAN_ROBOTPART_H

//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#include <boost/test/unit_test.hpp>
#include <dlfcn.h>

#include "move4d/mdl/kin/KinematicSolver.h"
#include "tests/GlobalFixtures.h"

namespace move4d
{
namespace tests
{
BOOST_AUTO_TEST_SUITE(ikfast)

std::vector<void *> loaded_plugins;

BOOST_AUTO_TEST_CASE(load_plugin_single_lwr_robot)
{
    // FIXME(Jules): plugin loading in tests shouldn't be done this way
    char *plugin_dir = std::getenv("MOVE4D_PLUGIN_DIR");
    BOOST_TEST_REQUIRE(plugin_dir, "environment variable MOVE4D_PLUGIN_DIR must be defined");
    std::string dir(plugin_dir);
    BOOST_TEST_REQUIRE(dir.size(), "environment variable MOVE4D_PLUGIN_DIR must be set");
    std::string plugin_path = dir + "/libmove4d_ikfast_single_lwr_robot_plugin.so";
    dlerror();
    // note: it is OK to call dlopen several time with the same library. Other calls won't load the
    // library
    void *plugin = dlopen(plugin_path.c_str(), RTLD_NOW | RTLD_GLOBAL);
    char *err = dlerror();
    BOOST_TEST_REQUIRE(plugin, std::string("failed to load plugin at ") + plugin_path +
                                   ", with error: " + (err ? err : "unknown"));
}

BOOST_FIXTURE_TEST_CASE(inverse_kinematic, FixtureSRDF, *boost::unit_test::expected_failures(1))
{
    kin::KinematicSolverPtr ikfast_solver;
    BOOST_REQUIRE_NO_THROW(ikfast_solver =
                               kin::KinematicSolverFactory::instance().create("ikfast"));
    BOOST_REQUIRE(ikfast_solver);

    // ikfast_solver->computeInverseKinematic()
    BOOST_TEST_CHECK(false, "unimplemented test");
}

BOOST_FIXTURE_TEST_CASE(forward_kinematic, FixtureSRDF, *boost::unit_test::expected_failures(1))
{
    kin::KinematicSolverPtr ikfast_solver;
    BOOST_REQUIRE_NO_THROW(ikfast_solver =
                               kin::KinematicSolverFactory::instance().create("ikfast"));
    BOOST_REQUIRE(ikfast_solver);

    // ikfast_solver->computeForwardKinematic()
    BOOST_TEST_CHECK(false, "unimplemented test");
}

BOOST_AUTO_TEST_SUITE_END()

} // namespace tests

} // namespace move4d

//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_PLAN_OMPL_SEARCHSPACEOMPL_H
#define MOVE4D_PLAN_OMPL_SEARCHSPACEOMPL_H

#include "move4d/plan/SearchSpace.h"

namespace move4d
{
namespace plan
{
namespace ompl
{

class StateSpaceWrapper;
using StateSpaceWrapperPtr = std::shared_ptr<StateSpaceWrapper>;

/**
 * @brief Access OMPL StateSpace related stuff through a move4d SearchSpace interface
 */
class SearchSpaceOmpl : public move4d::plan::SearchSpace
{
  public:
    SearchSpaceOmpl(const SearchSpace &other);

    std::unique_ptr<SearchSpace> cloneSearchSpace() const override
    {
        assert(typeid(*this) == typeid(SearchSpaceOmpl) && "must reimplement in subclasses");
        return std::make_unique<SearchSpaceOmpl>(*this);
    }

    /**
     * @brief shootUniform
     * @param ws
     * @deprecated do not use in production code (heavy useless memory allocation)
     */
    [[deprecated]] void shootUniform(mdl::WorldState &ws) const override;

    const StateSpaceWrapperPtr &getWrapper() const;

    void sanityChecks(const mdl::WorldState &ws) const;

  private:
    StateSpaceWrapperPtr _wrapper;
};

} // namespace ompl
} // namespace plan
} // namespace move4d

#endif // MOVE4D_PLAN_OMPL_SEARCHSPACEOMPL_H

//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_PLAN_PLANNER_H
#define MOVE4D_PLAN_PLANNER_H

#include "move4d/mdl/Types.h"
#include <memory>

namespace move4d
{

namespace plan
{

class Goal;
class SearchSpace;

class Planner
{
  public:
    Planner(std::shared_ptr<SearchSpace> space, mdl::CollisionManagerPtr collisionManager);
    virtual ~Planner() = default;

    virtual bool solve(float timeLimit) = 0;

    const std::shared_ptr<const mdl::WorldState> &getStart() const;
    void setStart(std::shared_ptr<const mdl::WorldState> start);
    void setStart(const mdl::WorldState &start);

    const std::shared_ptr<const Goal> &getGoal() const;
    void setGoal(std::shared_ptr<const Goal> goal);

  protected:
    mdl::CollisionManagerPtr m_collisionManager;
    std::shared_ptr<const mdl::WorldState> m_start;
    std::shared_ptr<const Goal> m_goal;

    std::shared_ptr<SearchSpace> m_searchSpace;
};

} // namespace plan

} // namespace move4d
#endif

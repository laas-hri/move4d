//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#include "tests/GlobalFixtures.h"
#include "move4d/file/model/URDFImporter.h"
#include "move4d/file/sem/SRDFImporter.h"
#include "move4d/mdl/CollisionManager.h"
#include "move4d/mdl/CollisionScene.h"
#include "move4d/mdl/Movable.h"
#include <boost/test/unit_test_log.hpp>

namespace move4d
{
namespace tests
{

GlobalFixture::GlobalFixture()
{
    auto collisionScene = mdl::CollisionSceneFactory::instance().create("fcl");
    _scene = std::make_shared<mdl::Scene>(collisionScene);
    _scene->setSpaceBounds({{-1, -1, 0}, {1, 1, 3}});
    _collisionManager = std::make_shared<mdl::CollisionManager>(_scene, collisionScene);

    file::model::URDFImporter urdf_importer;
    lwr_ff_base = urdf_importer.load("assets/freeflyer.urdf", _scene.get(), "");
}

FixtureSRDF::FixtureSRDF()
{
    file::sem::SRDFImporter srdf_importer;
    srdf_importer.loadFile("assets/freeflyer.srdf", _scene);
    arm_chain = lwr_ff_base.get()->as<mdl::Movable>()->chains[0];
    arm_6_chain = lwr_ff_base.get()->as<mdl::Movable>()->chains[1];
    eef = lwr_ff_base.get()->as<mdl::Movable>()->endEffectors[0];
}

} // namespace tests
} // namespace move4d

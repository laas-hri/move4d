//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef PRINT_TO_STREAM_HPP
#define PRINT_TO_STREAM_HPP

#include <functional>
#include <ostream>
#include <sstream>
#include <string>
#include <utility>

/** @file print_to_stream.hpp **/

class print_to_stream
{
  public:
    print_to_stream(std::function<void(std::ostream &)> fun) : _fun(std::move(fun)) {}

    std::function<void(std::ostream &)> _fun;
};

inline std::ostream &operator<<(std::ostream &os, const print_to_stream &pts)
{
    std::ostringstream oss;
    pts._fun(oss);
    std::string s = oss.str();
    os << s;
    return os;
}

/**
 * example:
 * \code{.cpp}
 * struct A{ ... };
 * void printA(const A &a, std::ostream &s);
 * ...
 * A a;
 * std::cout<<PRINT_TO_STREAM(printA(a,oss));
 * \endcode
 */
#define PRINT_TO_STREAM(call) print_to_stream([&](auto &oss) { call; })

#endif // PRINT_TO_STREAM_HPP

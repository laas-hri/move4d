//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_PLAN_OMPL_GOALINHYPERBOXSAMPLER_H
#define MOVE4D_PLAN_OMPL_GOALINHYPERBOXSAMPLER_H

#include "move4d/common.h"
#include "move4d/math/Vector.h"
#include "move4d/plan/Goal.h"
#include "move4d/plan/ompl/Types.h"
#include <ompl/base/StateSpace.h>
#include <ompl/base/goals/GoalSampleableRegion.h>

namespace move4d
{
namespace plan
{
namespace ompl
{

class Hyperbox
{
  public:
    Hyperbox(::ompl::base::SpaceInformationPtr si,
             std::map<size_t, std::pair<Real, Real>> box_limits = {});

    inline void setHigh(size_t index, Real high) { _limits[index].second = high; }
    inline Real getHigh(size_t index) const
    {
        auto search = _limits.find(index);
        if (search != _limits.end()) {
            return search->second.second;
        }
        return std::numeric_limits<Real>::max();
    }
    inline void setLow(size_t index, Real low) { _limits[index].first = low; }
    inline Real getLow(size_t index) const
    {
        auto search = _limits.find(index);
        if (search != _limits.end()) {
            return search->second.first;
        }
        return -std::numeric_limits<Real>::max();
    }

    bool isIn(const ::ompl::base::State *st) const;
    /// smallest euclidean distance between the state and the box
    Real distance(const ::ompl::base::State *st) const;

  private:
    std::map<size_t, std::pair<Real, Real>> _limits;
    ::ompl::base::SpaceInformationPtr _si;
};

class GoalSpaceSampler : public ::ompl::base::GoalSampleableRegion
{
    MOVE4D_STATIC_LOGGER;

  public:
    /**
     * @brief GoalSpaceSampler
     * @param si
     * @param goal_space
     * @param goal_to_space_injection maps each dof index of goal_space to a dof index of the search
     * space
     */
    GoalSpaceSampler(::ompl::base::SpaceInformationPtr si, StateSpaceWrapperPtr goal_space_wrapper,
                     std::vector<size_t> goal_to_space_bijection,
                     std::vector<size_t> goal_to_space_poses_bijection);

    double distanceGoal(const ::ompl::base::State *st) const override;

    void sampleGoal(::ompl::base::State *st) const override;
    unsigned int maxSampleCount() const override { return UINT_MAX; }

  protected:
    StateSpaceWrapperPtr _goal_space_wrapper;
    KinematicGraphStateSpacePtr _goal_space;
    ::ompl::base::StateSamplerPtr _sampler;
    std::vector<size_t> _goal_to_space_bijection;
    std::vector<size_t> _goal_to_space_poses_bijection;
};

} // namespace ompl
} // namespace plan
} // namespace move4d

#endif // MOVE4D_PLAN_OMPL_GOALINHYPERBOXSAMPLER_H

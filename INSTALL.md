# Installation Instructions {#Installation}

Move4d has many dependencies, and is compatible only with UNIX systems (at
most).

<!-- MarkdownTOC levels="1,2" autolink="true" -->

- [System Dependencies](#system-dependencies)
- [Other Dependencies](#other-dependencies)
- [Getting move4d](#getting-move4d)
- [Build](#build)
- [Build and read the doc](#build-and-read-the-doc)
- [And then](#and-then)

<!-- /MarkdownTOC -->


## System Dependencies

First identify the system you are running on, if you don't know it, e.g. with
`lsb_release -ds`. Find the corresponding file in `move4d/dependencies/`. You
can inspect it before running (it requires admin privileges but should ask you
before doing stuff -- like removing conflicting packages).

## Other Dependencies

Other dependencies are needed, either because specific versions are needed or
they are not found in standard system repositories.

A script to build them is provided:
[dependencies/build_dependencies.sh](dependencies/build_dependencies.sh). It can be quite
long to run as it will compile many dependencies.

## Getting move4d

Get move4d sources. You can `git clone` (if you want to contribute or keep updated) or get an archive containing the sources (if you just want to give a try or don't care about updates -- but you should...).

### git clone

#### via https

```shell
git clone https://gitlab.com/laas-hri/move4d.git
```

#### via ssh

```shell
git clone git@gitlab.com:laas-hri/move4d.git
```

### source archive

You can download directly from the web interface of the [project on gitlab](https://gitlab.com/laas-hri/move4d), or use the URLs like so:

```shell
wget https://gitlab.com/laas-hri/move4d/-/archive/master/move4d-master.tar.gz
```

Then extract the archive, and opitonally rename the created folder to `move4d`

## Build

First some environment variable must be correctly set to point to where you
installed your dependencies. Something like that:

```shell
LD_LIBRARY_PATH=$INSTALL_PREFIX/lib:$LD_LIBRARY_PATH
CMAKE_PREFIX_PATH=$CMAKE_PREFIX_PATH:$INSTALL_PREFIX
```

`cd` into your source directory (`cd move4d`), and

```shell
mkdir -p build/Release
cd build/Release
cmake ../.. -DCMAKE_INSTALL_PREFIX=$INSTALL_PREFIX -DCMAKE_BUILD_TYPE=Release
make -j`nproc`
make install
```

## Build and read the doc

From the build directory run `make doc`, the Doxygen documentation is created in `doc/html` (access that directory from a web browser).

## And then

`move4d` is a library. Some usage examples are provided in the `examples` directory.

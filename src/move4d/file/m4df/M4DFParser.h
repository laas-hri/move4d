#ifndef MOVE4D_FILE_M4DF_M4DFPARSER_H
#define MOVE4D_FILE_M4DF_M4DFPARSER_H

#include "move4d/common.h"
#include "move4d/file/m4df/M4DFDefinition.h"

namespace rl
{
namespace mdl
{
class Joint;
}
} // namespace rl

namespace tinyxml2
{
class XMLElement;
class XMLDocument;
} // namespace tinyxml2
namespace move4d
{
namespace file
{
namespace m4df
{

class M4DFException : public std::runtime_error
{
  public:
    M4DFException(const std::string &what) : std::runtime_error(what) {}
    M4DFException(const std::string &file, const std::string &what)
        : std::runtime_error("error while parsing M4DF file " + file + " : " + what)
    {
    }
    M4DFException setFile(const std::string &file) const
    {
        return M4DFException(file, this->what());
    }

  private:
};

class M4DFParser
{
    MOVE4D_STATIC_LOGGER;

  public:
    M4DFParser() = default;
    M4DFParser(mdl::ScenePtr scene, view::Viewer *viewer = nullptr);
    std::unique_ptr<M4DFDefinition> loadFile(const std::string &filepath);
    std::unique_ptr<M4DFDefinition> loadString(const std::string &xml_string,
                                               const std::string &path = {});
    std::unique_ptr<M4DFDefinition> loadXml(const tinyxml2::XMLElement &xml_element,
                                            const std::string &path = {});
    std::unique_ptr<M4DFDefinition> loadXml(const tinyxml2::XMLDocument &doc,
                                            const std::string &path = {});

    void loadObject(const tinyxml2::XMLElement &xml_element, M4DFDefinition &definition);
    std::unique_ptr<plan::KinematicGraphSearchSpaceBuilder>
    parseKinematicGraphBuilder(const tinyxml2::XMLElement &xml_element, M4DFDefinition &definition);

    std::unique_ptr<plan::RobotKinematicGroupSpace>
    parseKinematicGraphChain(const tinyxml2::XMLElement &part_element, M4DFDefinition &definition);

    ptr_wrapper<plan::SearchSpace> parseSearchSpace(const tinyxml2::XMLElement &element,
                                                    M4DFDefinition &definition);

    void parserRobotJoint(const tinyxml2::XMLElement &element, mdl::SceneObjectPtr &robot,
                          size_t &joint_index, M4DFDefinition &definition);

    bool getLoadCollisionInView() const;
    void setLoadCollisionInView(bool load_collision_in_view);

  private:
    mdl::ScenePtr _scene;
    view::Viewer *_viewer;
    std::string _path = {};
    bool _load_collision_in_view = false;
};

} // namespace m4df
} // namespace file
} // namespace move4d

#endif // MOVE4D_FILE_M4DF_M4DFPARSER_H

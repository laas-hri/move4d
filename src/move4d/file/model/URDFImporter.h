//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_FILE_MODEL_URDFIMPORTER_H
#define MOVE4D_FILE_MODEL_URDFIMPORTER_H

#include "move4d/mdl/Types.h"
#include <string>

namespace move4d
{
namespace mdl
{
class SceneObject;
class Scene;
} // namespace mdl
namespace view
{
class Viewer;
}
} // namespace move4d

namespace move4d
{

namespace file
{

namespace model
{

class URDFImporter
{
  public:
    URDFImporter();
    ~URDFImporter();

    move4d::mdl::SceneObjectPtr load(const std::string &filepath, move4d::mdl::Scene *scene,
                                     const std::string &name, const move4d::Identifier &id = {});

    void loadView(const std::string &filepath, move4d::view::Viewer *viewer,
                  const std::string &name, const move4d::Identifier &id = {},
                  bool load_collisions = false);

  protected:
    class PImpl;
    PImpl *pimpl;
};

} // namespace model

} // namespace file

} // namespace move4d
#endif

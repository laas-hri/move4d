//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_PLAN_GOALSAMPLER_H
#define MOVE4D_PLAN_GOALSAMPLER_H

#include "move4d/plan/Goal.h"
#include "move4d/plan/KinematicGraphSearchSpace.h"

namespace move4d
{
namespace plan
{

class GoalSampler : public Goal
{
  public:
    GoalSampler(std::map<size_t, BaseSamplerPtr> samplers);

    const std::map<size_t, BaseSamplerPtr> &getSamplers() const;
    void setSamplers(std::map<size_t, BaseSamplerPtr> samplers);

    Real distance(const mdl::WorldState &ws) const override;
    bool check(const mdl::WorldState &ws) const override;

  protected:
    std::map<size_t, BaseSamplerPtr> _samplers;

    void impl_compute(result_ref result, const_argument_ref argument) const override;
};

class GoalSpaceSampler : public Goal
{
  public:
    GoalSpaceSampler(KinematicGraphSearchSpacePtr goal_sampling_space,
                     KinematicGraphSearchSpacePtr search_space, Real threshold);

    Real distance(const mdl::WorldState &ws) const override;
    bool check(const mdl::WorldState &ws) const override;

    void getSample(mdl::WorldState &ws) const;

    const std::vector<size_t> &getBijetion() const { return _bijection_sampling_to_search_space; }
    const std::vector<size_t> &getVertexBijection() const
    {
        return _bijection_vertices_sampling_to_search_space;
    }

    const KinematicGraphSearchSpacePtr &getGoalSpace() const { return _sampling_space; }
    const KinematicGraphSearchSpacePtr &getSearchSpace() const { return _search_space; }

  protected:
    KinematicGraphSearchSpacePtr _sampling_space, _search_space;
    std::vector<size_t> _bijection_sampling_to_search_space;
    std::vector<size_t> _bijection_vertices_sampling_to_search_space;
    Real _threshold;

    void impl_compute(result_ref result, const_argument_ref argument) const override;
};

class FixSampler : public BaseSampler
{
    MOVE4D_STATIC_LOGGER;

  public:
    FixSampler(std::shared_ptr<const plan::SearchSpace> space, math::Vector x,
               math::Transform end_pose)
        : plan::BaseSampler(std::move(space)), fixed(std::move(x)), end_pose(std::move(end_pose))
    {
    }

    void sampleUniform(views::DofsView &conf_in_out, const math::Transform &base_pose,
                       math::Transform &end_pose_out,
                       const std::vector<math::Transform> &input_poses) override
    {
        assert(conf_in_out.size() == fixed.size()); // should have first_dof == 0
        for (size_t i = 0; i < fixed.size(); ++i) {
            conf_in_out[i] = fixed[i];
        }
        end_pose_out = end_pose;
    }
    void sampleUniformNear(views::DofsView &conf_in_out, const math::Transform &base_pose,
                           math::Transform &end_pose_out,
                           const std::vector<math::Transform> &input_poses, views::DofsView &near,
                           Real distance) override
    {
        return sampleUniform(conf_in_out, base_pose, end_pose_out, input_poses);
    }
    void sampleGaussian(views::DofsView &conf_in_out, const math::Transform &base_pose,
                        math::Transform &end_pose_out,
                        const std::vector<math::Transform> &input_poses, views::DofsView &mean,
                        Real stdev) override
    {
        return sampleUniform(conf_in_out, base_pose, end_pose_out, input_poses);
    }

    static SamplerAllocator make_allocator(math::Vector conf, math::Transform end_pose);

  protected:
    math::Vector fixed;
    math::Transform end_pose;
};

} // namespace plan
} // namespace move4d

#endif // MOVE4D_PLAN_GOALSAMPLER_H

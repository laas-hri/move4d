//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_PLAN_ROBOTSPACE_H
#define MOVE4D_PLAN_ROBOTSPACE_H

#include "move4d/math/Vector.h"
#include "move4d/mdl/Types.h"
#include "move4d/plan/SearchSpace.h"

namespace move4d
{
namespace mdl
{
class Movable;
class SceneObject;
using SceneObjectPtr = std::shared_ptr<SceneObject>;
struct EndEffector;
using EndEffectorPtr = std::shared_ptr<EndEffector>;
struct Chain;
using ChainPtr = std::shared_ptr<Chain>;
} // namespace mdl

namespace plan
{

class RobotSpace : public SearchSpace
{
  public:
    RobotSpace() = delete;
    RobotSpace(const RobotSpace &) = default;
    RobotSpace(RobotSpace &&) = default;
    RobotSpace &operator=(const RobotSpace &) = default;
    RobotSpace &operator=(RobotSpace &&) = delete; // suppress a warning
    virtual ~RobotSpace() = default;
    RobotSpace(const mdl::Movable *robot, std::string name = "");

    RobotSpace(const mdl::Movable *robot, const Eigen::Matrix<bool, 1, -1> &enabledDofs,
               std::string name = "");

    RobotSpace(const mdl::Movable *robot, const std::vector<size_t> &enabledDofs,
               std::string name = "");
    /**
     * @brief RobotSpace
     * @param robot
     * @param enabledDofs (no deep-copy)
     * @param name
     */
    RobotSpace(const mdl::Movable *robot, std::vector<mdl::DegreeOfFreedomInfo> enabledDofs,
               std::string name = "");

    std::unique_ptr<SearchSpace> cloneSearchSpace() const override
    {
        assert(typeid(*this) == typeid(RobotSpace) && "must reimplement in subclasses");
        return std::make_unique<RobotSpace>(*this);
    }

    const mdl::SceneObject *robot() const;

    // Eigen::Matrix<bool, 1, -1> enabledDofs();

    void shootUniform(mdl::WorldState &ws) const override;

  private:
    const mdl::Movable *m_robot;
    // Eigen::Matrix<bool, 1, -1> m_enabledDofs;
};

/**
 * @brief The RobotKinematicGroupSpace class
 * getType()==Type::KinematicGroup
 */
class RobotKinematicGroupSpace : public KinematicSearchSpace, public RobotSpace
{
  public:
    RobotKinematicGroupSpace() = delete;
    RobotKinematicGroupSpace(const RobotKinematicGroupSpace &) = default;
    RobotKinematicGroupSpace(RobotKinematicGroupSpace &&) = default;
    RobotKinematicGroupSpace &operator=(const RobotKinematicGroupSpace &) = default;
    RobotKinematicGroupSpace &operator=(RobotKinematicGroupSpace &&) = default;
    virtual ~RobotKinematicGroupSpace() = default;

  public:
    static std::unique_ptr<RobotKinematicGroupSpace>
    create(const mdl::Movable *robot, const std::string &chain_name, const std::string &ik_method);
    /**
     * @brief RobotKinematicGroupSpace
     * @param robot
     * @param kinematicChain
     * @param redundant_dofs indices of redundant degrees of freedom, corresponding to rank in
     * kinematicChain
     */
    RobotKinematicGroupSpace(const mdl::Movable *robot, mdl::ChainPtr kinematicChain,
                             std::string ik_method, std::string name = "");
    /** @brief constructor version with bijection.
     *
     *  informally:
     *  this.dimension <- bijection.size(),
     *  this.dof[i] <=> kinematicChain.dof[bijection[i]]
     *  this.dof[i] <=> robot.dof[chain.bijection[bijection[i]]
     */
    RobotKinematicGroupSpace(const mdl::Movable *robot, mdl::ChainPtr kinematicChain,
                             std::string ik_method, std::vector<size_t> bijection,
                             std::string name = "");
    RobotKinematicGroupSpace(const mdl::Movable *robot, mdl::ChainPtr kinematicChain,
                             std::string ik_method,
                             const std::vector<mdl::DegreeOfFreedomInfo> &dofs,
                             std::string name = "");

    std::unique_ptr<SearchSpace> cloneSearchSpace() const override
    {
        assert(typeid(*this) == typeid(RobotKinematicGroupSpace) &&
               "must reimplement in subclasses");
        return std::make_unique<RobotKinematicGroupSpace>(*this);
    }
    const mdl::ChainPtr &getKinematicChain() const;
    void setKinematicChain(mdl::ChainPtr kinematicChain);

    bool computeIK(mdl::WorldState &ws, const math::Transform &pose) const override;
    bool computeIK(mdl::State &state, const math::Transform &pose) const override;

    bool computeFK(const mdl::WorldState &ws, math::Transform &pose) const override;
    bool computeFK(const mdl::State &state, math::Transform &pose) const override;

    void setPoseBounds(math::Vector3 low, math::Vector3 high) override;
    using KinematicSearchSpace::setPoseBounds;
    const math::Vector3 &getPoseLowerBounds() const override { return m_min; }
    const math::Vector3 &getPoseHigherBounds() const override { return m_max; }

    void shootUniform(mdl::WorldState &ws) const override;

    const std::string &getKinematicSolver() const;

  private:
    mdl::ChainPtr m_kinematicChain;
    math::Vector3 m_min = {}, m_max = {};
    std::string m_kinematicSolver;
    std::vector<size_t> _bijection; // TODO(Jules): useless? just for OMPL?
    void _init();
};

} // namespace plan
} // namespace move4d

#endif // MOVE4D_PLAN_ROBOTSPACE_H

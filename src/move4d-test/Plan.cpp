//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#include "move4d/mdl/CollisionManager.h"
#include "move4d/mdl/CollisionWhiteList.h"
#include "move4d/mdl/Movable.h"
#include "move4d/mdl/Scene.h"
#include "move4d/mdl/SceneObject.h"
#include "move4d/mdl/Types.h"
#include "move4d/mdl/WorldState.h"

#include "move4d/plan/RobotSpace.h"
#include "move4d/plan/WorldStateGoal.h"
#include "move4d/plan/rl/Planner.h"
#include "move4d/plan/rl/UniformSampler.h"

#include "move4d/file/model/URDFImporter.h"

#include "move4d-soqt/ViewerSoQt.h"
#include "move4d-soqt/ViewerThreadQt.h"
#include <Inventor/Qt/SoQt.h>
#include <Inventor/Qt/viewers/SoQtExaminerViewer.h>

#include <rl/math/Vector.h>
#include <rl/mdl/Joint.h>
#include <rl/plan/SimpleModel.h>
#include <rl/sg/so/Scene.h>

#include <QApplication>
#include <QMainWindow>
#include <QVBoxLayout>

#include <chrono>
#include <thread>

using namespace move4d;

class Application
{
  public:
    Application(int argc, char *argv[]);

    void run();

  protected:
    void f();

    move4d::mdl::Scene *m_scene;
    rl::sg::Scene *m_sceneView;
    QWidget *m_mainWidget;
    view::ViewerSoQt *m_viewer;
    view::ViewerThreadQt *m_viewerIF;

    size_t m_run_nb{1};
    bool m_showPath{false};
};

Application::Application(int argc, char *argv[])
{
    QWidget *mainwindow = SoQt::init(argv[0]);

    std::string engine{"undef"};
    std::vector<std::string> files;
    for (int a = 0; a < argc; ++a) {
        std::cout << "\"" << argv[a] << "\"" << std::endl;
    }
    for (int a = 0; a < argc; ++a) {
        if (strcmp(argv[a], "-n") == 0) {
            m_run_nb = std::atoi(argv[++a]);
        } else if (strcmp(argv[a], "-e") == 0 || strcmp(argv[a], "--engine") == 0) {
            engine = argv[++a];
        } else if (strcmp(argv[a], "-f") == 0) {
            files.push_back(argv[++a]);
        } else if (strcmp(argv[a], "-v") == 0) {
            m_showPath = true;
        }
    }

    // the main scene for computation
    m_scene = new mdl::Scene(engine);

    // the scene for visualisation
    m_viewer = new view::ViewerSoQt();
    m_sceneView = view::ViewerSoQt::createScene();
    m_viewer->setScene(m_sceneView);
    //  for the interface (async)
    m_viewerIF = new view::ViewerThreadQt();
    if (!view::ViewerThreadQt::connect(m_viewerIF, m_viewer)) {
        throw std::runtime_error("failed to connect viewer interface to asynchronous viewer");
    }

    auto *layout = new QVBoxLayout();
    layout->addWidget(m_viewer);
    mainwindow->setLayout(layout);

    file::model::URDFImporter importer;
    for (auto f : files) {
        // load URDF for collision
        auto r = importer.load(f, m_scene, f);
        // load URDF for visualisation
        importer.loadView(f, m_viewer, f);
        auto jnt0 = r->model()->mdl->getJoint(0);
        std::cout << "jnt0=" << jnt0->getName() << " with " << jnt0->getDofPosition() << " dofs"
                  << std::endl;
        if (jnt0->getDofPosition() == 7) {
            for (size_t i = 0; i < 3; ++i) {
                jnt0->min[i] = -0;
                jnt0->max[i] = 0;
            }
        }
        r->updateModels();
    }
    m_viewer->setScene(m_sceneView);

    m_scene->resetCollisionWhiteList();
    for (auto r : *m_scene) {
        m_scene->getCollisionWhitelist()->removeSelfColliding(*r.second,
                                                              *m_scene->getCollisionManager());
    }

    m_viewer->show();

    mainwindow->resize(800, 600);
    // mainwindow.show();
    SoQt::show(mainwindow);
}
void Application::run()
{
    std::thread the_thread(&Application::f, this);
    SoQt::mainLoop();
    the_thread.join();
}

void Application::f()
{
    using namespace std;

    auto robot = m_scene->begin()->second;
    auto space = std::make_shared<plan::RobotSpace>(dynamic_cast<mdl::Movable &>(*robot));

    for (uint count = 0; count < m_run_nb; ++count) {
        auto from = m_scene->getCurrentWorldState();
        std::shared_ptr<mdl::WorldState> target;
        move4d::plan::rl::UniformSampler spl(space);
        do {
            move4d::math::Vector pos = spl.generate();
            auto state = robot->getState();
            state->operator()(mdl::StateType::Position) = pos;
            robot->applyState(*state);
        } while (m_scene->getCollisionManager()->isColliding());
        target = m_scene->getCurrentWorldState();

        std::this_thread::sleep_for(std::chrono::seconds(2));
        std::this_thread::sleep_for(std::chrono::seconds(2));

        std::cout << "goal conf" << std::endl;
        m_viewerIF->update(*target);

        std::cout << "start planning" << std::endl;
        plan::rl::Planner rrt(space, std::make_shared<plan::WorldStateGoal>(target));
        rrt.setViewer(m_viewerIF);
        auto begin = std::chrono::high_resolution_clock::now();
        if (rrt.solve()) {
            auto end = std::chrono::high_resolution_clock::now();
            std::cout << "done in "
                      << std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count()
                      << "ms" << std::endl;
            if (m_showPath) {
                auto path = rrt.getPath();
                size_t i{0};
                ::rl::math::Vector prev;
                auto next_frame_date = std::chrono::steady_clock::now();
                for (auto x : path) {
                    std::cout << "step " << i << std::endl;
                    if (i) {
                        mdl::State q(robot.get(), robot->getNumberOfDof(mdl::StateType::Position),
                                     robot->getNumberOfDof(mdl::StateType::Speed));
                        for (Real alpha = 0.; alpha < 1; alpha += 0.01) {
                            math::Vector inter = q(mdl::StateType::Position);
                            // math::Vector inter(x.size());
                            robot->model()->interpolate(prev, x, alpha, inter);
                            for (long i = 0; i < x.size(); ++i) {
                                q(i, mdl::StateType::Position) = inter(i);
                            }
                            robot->applyState(q);
                            std::this_thread::sleep_until(next_frame_date);
                            next_frame_date = std::chrono::steady_clock::now() +
                                              std::chrono::milliseconds(1000 / 60);
                            m_viewerIF->update(*m_scene->getCurrentWorldState());
                        }
                    }
                    ++i;
                    prev = x;
                }
            }
        }
    }
}

int main(int argc, char *argv[])
{
    Application app(argc, argv);
    app.run();
    return 0;
}

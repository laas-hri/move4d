//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
// Created by gbuisan on 09/01/19.
//

#include "SDFImporter.h"
#include "move4d/file/model/AssimpImporter.h"
#include "move4d/file/model/URDFImporter.h"
#include "move4d/mdl/Model.h"
#include "move4d/mdl/Scene.h"
#include "move4d/mdl/SceneObject.h"
#include "move4d/mdl/State.h"
#include "move4d/utils/filesystem.h"
#include "move4d/view/Viewer.h"
#include <Inventor/VRMLnodes/SoVRMLAppearance.h>
#include <Inventor/VRMLnodes/SoVRMLBox.h>
#include <Inventor/VRMLnodes/SoVRMLCylinder.h>
#include <Inventor/VRMLnodes/SoVRMLMaterial.h>
#include <Inventor/VRMLnodes/SoVRMLShape.h>
#include <Inventor/VRMLnodes/SoVRMLSphere.h>
#include <iomanip>
#include <rl/mdl/Body.h>
#include <rl/mdl/Compound.h>
#include <rl/mdl/Dynamic.h>
#include <rl/mdl/Fixed.h>
#include <rl/mdl/Frame.h>
#include <rl/mdl/Prismatic.h>
#include <rl/mdl/Revolute.h>
#include <rl/mdl/SixDof.h>
#include <rl/mdl/World.h>
#include <rl/plan/Model.h>
#include <rl/plan/SimpleModel.h>
#include <rl/sg/Body.h>
#include <rl/sg/Shape.h>
#include <sdf/Console.hh>
#include <sdf/Types.hh>
#include <sdf/sdf.hh>

namespace move4d
{
namespace file
{
namespace world
{

class SDFImporter::PImpl
{
    MOVE4D_STATIC_LOGGER;

  public:
    void setShapePose(const ignition::math::Pose3d &pose, rl::sg::Shape *shape);
    std::vector<std::unordered_map<std::string, sdf::ElementPtr>>
    loadModel(const sdf::ElementPtr &sdfModel, rl::mdl::Dynamic *kin,
              const ignition::math::Pose3d &rootPose);

    void loadCollision(const std::shared_ptr<rl::mdl::Dynamic> &kin,
                       const move4d::mdl::SceneObjectPtr &sceneObject,
                       std::unordered_map<std::string, sdf::ElementPtr> name2Collision,
                       const std::string &pathPrefix = "");

    void loadVisual(rl::plan::Model *model,
                    std::unordered_map<std::string, sdf::ElementPtr> name2Visual,
                    const std::string &pathPrefix = "");

    void loadGeometry(const sdf::ElementPtr &geometry, ignition::math::Pose3d pose,
                      rl::sg::Body *body, SoVRMLAppearance *appearance = nullptr,
                      const std::string &pathPrefix = "");
    std::string filenameFromURI(const std::string &uri)
    {
        // URI are: model://path/to/model.dae we remove the "model://"
        return uri.substr(8, uri.size());
    };
    std::string pathFromFile(const std::string &filepath)
    {
        return filepath.substr(0, filepath.find_last_of("/\\"));
    }

    static std::string findCallback(const std::string &s)
    {
        std::filesystem::path p;
        auto search = s.find("://");
        if (search != std::string::npos) {
            p = s.substr(search + 3);
            assert(s.substr(0, search).find_first_of(":/.") == std::string::npos);
        } else {
            p = s;
        }
        p = std::filesystem::canonical(p);
        if (std::filesystem::is_regular_file(p)) {
            return p.string();
        }
        return {};
    }
};

INIT_MOVE4D_STATIC_LOGGER(SDFImporter, "move4d.file.sdfimporter");
INIT_MOVE4D_STATIC_LOGGER(SDFImporter::PImpl, "move4d.file.sdfimporter.pimpl");

SDFImporter::SDFImporter() : pImpl(new PImpl) {}

SDFImporter::~SDFImporter() { delete pImpl; }

void SDFImporter::load(const std::string &filepath, move4d::mdl::Scene *scene)
{
    M4D_DEBUG("parse SDF " << std::quoted(filepath));
    sdf::SDFPtr sdfElement(new sdf::SDF());
    sdf::init(sdfElement);
    sdf::Console::Instance()->SetQuiet(false);
    sdf::setFindCallback(pImpl->findCallback);
    if (!sdf::readFile(filepath, sdfElement)) {
        // Error
        throw std::runtime_error("error parsing SDF file");
    }

    sdfElement->PrintValues();
    // Parsing world
    const sdf::ElementPtr rootElement = sdfElement->Root();
    if (!rootElement->HasElement("world")) {
        // Error, the file does not start with a world tag
        throw std::runtime_error("the SDF file does not start with a world tag");
    }
    const sdf::ElementPtr worldElement = rootElement->GetElement("world");

    M4D_DEBUG("Loading world " << worldElement->Get<std::string>("name"));

    // Parse includes
    sdf::ElementPtr includeElement = worldElement->GetElement("include");
    while (includeElement) {
        const std::string uri = includeElement->GetElement("uri")->Get<std::string>();
        M4D_DEBUG("include " << std::quoted(uri));
        if (uri.rfind("urdf://", 0) == 0) {
            // URI to an URDF file
            move4d::file::model::URDFImporter urdfImporter;
            const std::string modelName = includeElement->GetElement("name")->Get<std::string>();
            M4D_DEBUG("loading a URDF file " << std::quoted(uri) << " for object "
                                             << std::quoted(modelName));
            auto r = urdfImporter.load(uri.substr(7), scene, "", modelName);
            M4D_ASSERT(r);
            M4D_ASSERT(r->getId() == modelName);
        }
        includeElement = includeElement->GetNextElement("include");
    }

    // Parse models
    sdf::ElementPtr modelElement = worldElement->GetElement("model");

    while (modelElement) {
        move4d::mdl::SceneObjectPtr sceneObject =
            scene->create("Manipulable", modelElement->Get<std::string>("name"));

        // TODO(Jules): self-collide tag
        M4D_DEBUG("Loading model " << modelElement->Get<std::string>("name"));
        ignition::math::Pose3d pose;
        if (modelElement->HasElement("pose")) {
            pose = modelElement->GetElement("pose")->Get<ignition::math::Pose3d>();
        }

        auto kin = std::make_shared<::rl::mdl::Dynamic>();
        auto collisionVisual = pImpl->loadModel(modelElement, kin.get(), pose);
        pImpl->loadCollision(kin, sceneObject, collisionVisual[0], pImpl->pathFromFile(filepath));
        modelElement = modelElement->GetNextElement("model");
    }
}

// TODO(Jules): add options like in URDFImporter
void SDFImporter::loadView(const std::string &filepath, move4d::view::Viewer *viewer)
{
    sdf::SDFPtr sdfElement(new sdf::SDF());
    sdf::init(sdfElement);
    if (!sdf::readFile(filepath, sdfElement)) {
        // Error
        throw std::runtime_error("error parsing SDF file");
    }

    // Parsing world
    const sdf::ElementPtr rootElement = sdfElement->Root();
    if (!rootElement->HasElement("world")) {
        // Error, the file does not start with a world tag
        throw std::runtime_error("the SDF file does not start with a world tag");
    }
    const sdf::ElementPtr worldElement = rootElement->GetElement("world");

    M4D_DEBUG("Loading world " << worldElement->Get<std::string>("name"));

    // Parse includes
    sdf::ElementPtr includeElement = worldElement->GetElement("include");
    while (includeElement) {
        const std::string uri = includeElement->GetElement("uri")->Get<std::string>();
        if (uri.rfind("urdf://", 0) == 0) {
            // URI to an URDF file
            move4d::file::model::URDFImporter urdfImporter;
            const std::string modelName = includeElement->GetElement("name")->Get<std::string>();
            urdfImporter.loadView(uri.substr(7), viewer, modelName);
            if (includeElement->HasElement("pose")) {
                // sdf::Pose pose = includeElement->GetElement("pose")->Get<sdf::Pose>();
            }
        }
        includeElement = includeElement->GetNextElement("include");
    }

    // Parse models
    sdf::ElementPtr modelElement = worldElement->GetElement("model");

    while (modelElement) {
        const auto &m = viewer->create();

        m->mdl = new rl::mdl::Dynamic(); // TODO(Jules): check this causes a leak
        M4D_DEBUG("Loading model " << modelElement->Get<std::string>("name"));
        ignition::math::Pose3d pose;
        if (modelElement->HasElement("pose")) {
            pose = includeElement->GetElement("pose")->Get<ignition::math::Pose3d>();
        }
        auto collisionVisual = pImpl->loadModel(modelElement, m->mdl, pose);
        pImpl->loadVisual(m.get(), collisionVisual[1], pImpl->pathFromFile(filepath));

        modelElement = modelElement->GetNextElement("model");
    }
    viewer->updateModels();
}

void SDFImporter::PImpl::setShapePose(const ignition::math::Pose3d &pose, rl::sg::Shape *shape)
{
    move4d::math::Transform transform;
    move4d::math::Quaternion q(pose.Rot().W(), pose.Rot().X(), pose.Rot().Y(), pose.Rot().Z());
    move4d::math::Translation t(pose.Pos().X(), pose.Pos().Y(), pose.Pos().Z());

    transform = t * q;

    M4D_DEBUG("Setting pose: " << pose.Pos() << "; " << pose.Rot());
    shape->setTransform(transform);
}

std::vector<std::unordered_map<std::string, sdf::ElementPtr>>
SDFImporter::PImpl::loadModel(const sdf::ElementPtr &sdfModel, rl::mdl::Dynamic *kin,
                              const ignition::math::Pose3d &rootPose)
{
    Expects(kin && kin->getBodies() == 0 && kin->getJoints() == 0);
    // Parsing links
    sdf::ElementPtr link = sdfModel->GetElement("link");
    std::unordered_map<std::string, rl::mdl::Frame *> name2frame;
    std::unordered_map<std::string, ignition::math::Pose3d> name2Pose;
    std::unordered_map<std::string, sdf::ElementPtr> name2Collision;
    std::unordered_map<std::string, sdf::ElementPtr> name2Visual;
    int i = 0;
    while (link) {
        M4D_DEBUG("Loading link " << link->Get<std::string>("name") << ++i);
        auto *b = new rl::mdl::Body(); // TODO(Jules): to check: this causes a leak. Check urdf
        // import too
        kin->add(b);

        b->setName(link->Get<std::string>("name"));
        assert(name2frame.count(b->getName()) == 0);
        name2frame[b->getName()] = b;

        auto pose = ignition::math::Pose3d();
        if (link->HasElement("pose")) {
            pose = link->GetElement("pose")->Get<ignition::math::Pose3d>();
            assert(name2Pose.count(b->getName()) == 0);
            name2Pose[b->getName()] = pose;
        } else {
            name2Pose[b->getName()] = ignition::math::Pose3d::Zero;
        }
        b->setCenterOfMass(pose.Pos().X(), pose.Pos().Y(), pose.Pos().Z());

        if (link->HasElement("inertial")) {
            auto inertial = link->GetElement("inertial");
            if (inertial->HasElement("mass")) {
                b->setMass(inertial->GetElement("mass")->Get<double>());
            } else {
                b->setMass(1);
            }

            if (inertial->HasElement("inertia")) {
                auto inertia = inertial->GetElement("inertia");
                b->setInertia(inertia->GetElement("ixx")->Get<double>(),
                              inertia->GetElement("iyy")->Get<double>(),
                              inertia->GetElement("izz")->Get<double>(),
                              inertia->GetElement("iyz")->Get<double>(),
                              inertia->GetElement("ixz")->Get<double>(),
                              inertia->GetElement("ixy")->Get<double>());
            } else {
                b->setInertia(1, 1, 1, 0, 0, 0);
            }
        }

        if (link->HasElement("collision")) {
            assert(name2Collision.count(b->getName()) == 0);
            name2Collision[b->getName()] = link->GetElement("collision");
        }
        if (link->HasElement("visual")) {
            assert(name2Visual.count(b->getName()) == 0);
            name2Visual[b->getName()] = link->GetElement("visual");
        }

        link = link->GetNextElement("link");
    }

    // Parsing joints
    std::unordered_map<std::string, std::string> child2parent;
    sdf::ElementPtr joint = sdfModel->GetElement("joint");
    i = 0;
    while (joint && joint->Get<std::string>("name") != "__default__") {
        M4D_DEBUG("Loading joint " << joint->Get<std::string>("name") << ++i);
        if (child2parent.find(joint->GetElement("child")->Get<std::string>()) !=
            child2parent.end()) {
            throw std::runtime_error("Child link has more than one parent...");
        }
        child2parent[joint->GetElement("child")->Get<std::string>()] =
            joint->GetElement("parent")->Get<std::string>();
        rl::mdl::Frame *parent = name2frame[joint->GetElement("parent")->Get<std::string>()];
        rl::mdl::Frame *child = name2frame[joint->GetElement("child")->Get<std::string>()];

        if (parent == nullptr || child == nullptr) {
            throw std::runtime_error("Missing parent or child in joint: " +
                                     joint->Get<std::string>("name"));
        }

        // frame
        auto *frame = new rl::mdl::Frame(); // TODO(Jules): to check: this causes a leak. Check
        // urdf import too
        kin->add(frame);
        frame->setName("framej_" + joint->Get<std::string>("name"));

        // fixed
        auto fixed = new rl::mdl::Fixed(); // TODO(Jules): to check: this causes a leak. Check
        // urdf import too
        fixed->setName("fixedj_" + joint->Get<std::string>("name"));

        // if (joint->GetElement("axis")->GetElement("use_parent_model_frame")->Get<bool>()) {
        ignition::math::Matrix4d parentPose, childPose, relativePose;
        parentPose =
            ignition::math::Matrix4d(name2Pose[joint->GetElement("parent")->Get<std::string>()]);
        childPose =
            ignition::math::Matrix4d(name2Pose[joint->GetElement("child")->Get<std::string>()]);
        relativePose = parentPose.Inverse() * childPose;
        ignition::math::Pose3d p(relativePose.Pose());
        auto quat = ::rl::math::Quaternion(p.Rot().W(), p.Rot().X(), p.Rot().Y(), p.Rot().Z());
        fixed->x.transform().linear() = quat.toRotationMatrix();
        fixed->x.transform().translation().x() = p.Pos().X();
        fixed->x.transform().translation().y() = p.Pos().Y();
        fixed->x.transform().translation().z() = p.Pos().Z();
        //}
        if (joint->HasElement("pose")) {
            ignition::math::Matrix4d pose(joint->GetElement("pose")->Get<ignition::math::Pose3d>());
            move4d::math::Transform t;
            move4d::math::Quaternion q(pose.Rotation().W(), pose.Rotation().X(),
                                       pose.Rotation().Y(), pose.Rotation().Z());
            t.translation().x() = pose.Translation().X();
            t.translation().y() = pose.Translation().Y();
            t.translation().z() = pose.Translation().Z();
            t.linear() = q.toRotationMatrix();
            fixed->x.transform().matrix() *= t.matrix();

            auto invertedPose = pose.Inverse();
            auto iquat =
                ::rl::math::Quaternion(invertedPose.Rotation().W(), invertedPose.Rotation().X(),
                                       invertedPose.Rotation().Y(), invertedPose.Rotation().Z());

            auto *fr = new rl::mdl::Frame(); // TODO(Jules): to check: this causes a leak. Check
            // urdf import too
            kin->add(fr);
            fr->setName("frameafterjoint_" + joint->Get<std::string>("name"));

            auto f = new rl::mdl::Fixed(); // TODO(Jules): to check: this causes a leak. Check
            // urdf import too
            f->setName("fixedafterjoint_" + joint->Get<std::string>("name"));
            f->x.linear() = iquat.toRotationMatrix();

            f->x.translation().x() = invertedPose.Translation().X();
            f->x.translation().y() = invertedPose.Translation().Y();
            f->x.translation().z() = invertedPose.Translation().Z();
            kin->add(f, fr, child);
            child = fr;
        }

        // joint
        std::string type = joint->Get<std::string>("type");
        // If joint is a fixed or if its upper and lower limits are the same
        if (type == "fixed" ||
            (joint->HasElement("axis") && joint->GetElement("axis")->HasElement("limit") &&
             joint->GetElement("axis")->GetElement("limit")->HasElement("upper") &&
             joint->GetElement("axis")->GetElement("limit")->HasElement("lower") &&
             joint->GetElement("axis")->GetElement("limit")->GetElement("upper")->Get<double>() ==
                 joint->GetElement("axis")
                     ->GetElement("limit")
                     ->GetElement("lower")
                     ->Get<double>())) {
            kin->add(fixed, parent, child);
            fixed->setName(joint->Get<std::string>("name"));
        } else if (type == "prismatic") {
            auto p = new rl::mdl::Prismatic(); // TODO(Jules): to check: this causes a leak.
            // Check urdf import too
            auto axis = joint->GetElement("axis");
            kin->add(fixed, parent, frame);
            kin->add(p, frame, child);
            p->max(0) = axis->GetElement("limit")->GetElement("upper")->Get<double>();
            p->min(0) = axis->GetElement("limit")->GetElement("lower")->Get<double>();
            p->offset(0) = 0;
            if (axis->GetElement("limit")->HasElement("velocity")) {
                p->speed(0) = axis->GetElement("limit")->GetElement("velocity")->Get<double>();
            } else {
                p->speed(0) = 0;
            }
            p->wraparound(0) = false;
            auto axisxyz = axis->GetElement("xyz")->Get<ignition::math::Vector3d>();
            p->S(3, 0) = axisxyz.X();
            p->S(4, 0) = axisxyz.Y();
            p->S(5, 0) = axisxyz.Z();
            p->setName(joint->Get<std::string>("name"));
        } else if (type == "revolute") {
            auto r = new rl::mdl::Revolute(); // TODO(Jules): to check: this causes a leak.
            // Check urdf import too
            auto axis = joint->GetElement("axis");
            kin->add(fixed, parent, frame);
            kin->add(r, frame, child);

            if (axis->HasElement("limit") && axis->GetElement("limit")->HasElement("upper") &&
                axis->GetElement("limit")->HasElement("lower")) {
                r->max(0) = axis->GetElement("limit")->GetElement("upper")->Get<double>();
                r->min(0) = axis->GetElement("limit")->GetElement("lower")->Get<double>();
                r->wraparound(0) = false;

            } else {
                r->max(0) = 180 * ::rl::math::DEG2RAD;
                r->min(0) = -180 * ::rl::math::DEG2RAD;
                r->wraparound(0) = true;
            }
            r->offset(0) = 0;
            if (axis->HasElement("limit") && axis->GetElement("limit")->HasElement("velocity")) {
                r->speed(0) = axis->GetElement("limit")->GetElement("velocity")->Get<double>();
            } else {
                r->speed(0) = 0;
            }
            auto axisxyz = axis->GetElement("xyz")->Get<ignition::math::Vector3d>();
            r->S(0, 0) = axisxyz.X();
            r->S(1, 0) = axisxyz.Y();
            r->S(2, 0) = axisxyz.Z();
            r->setName(joint->Get<std::string>("name"));
        }
        joint = joint->GetNextElement("joint");
    }

    // root

    auto world = new rl::mdl::World(); // TODO(Jules): to check: this causes a leak. Check urdf
    // import too
    kin->add(world);

    rl::mdl::Frame *root = nullptr;
    for (auto frame = name2frame.begin(); frame != name2frame.end(); frame++) {
        if (child2parent.find(frame->first) == child2parent.end()) {
            if (root == nullptr) {
                root = name2frame[frame->first];
                auto baseJoint = new rl::mdl::SixDof(); // TODO(Jules): to check: this causes a
                // leak. Check urdf import too
                // TODO(Jules): correctly set the limits of the SixDof joints
                baseJoint->min(0) = -10;
                baseJoint->max(0) = 10;
                baseJoint->min(1) = -10;
                baseJoint->max(1) = 10;
                baseJoint->min(2) = -10;
                baseJoint->max(2) = 10;
                baseJoint->setName("baseJoint");
                kin->add(baseJoint, world, root);
            } else {
                throw std::runtime_error("Model has more than one root...");
            }
        }
    }

    if (root == nullptr) {
        throw std::runtime_error("Model has no root...");
    }

    kin->update();
    return {name2Collision, name2Visual};
}

void SDFImporter::PImpl::loadCollision(
    const std::shared_ptr<rl::mdl::Dynamic> &kin, const mdl::SceneObjectPtr &sceneObject,
    std::unordered_map<std::string, sdf::ElementPtr> name2Collision, const std::string &pathPrefix)
{
    std::cout << sceneObject->m_model->model->getNumBodies() << "; " << kin->getBodies()
              << std::endl;

    for (size_t j = 0; j < kin->getBodies(); j++) {
        auto b_mesh = sceneObject->m_bodies->create();
        if (name2Collision.find(kin->getBody(j)->getName()) != name2Collision.end()) {
            M4D_DEBUG("Loading collision.");
            auto col = name2Collision[kin->getBody(j)->getName()];
            while (col) {
                auto geometry = col->GetElement("geometry");
                ignition::math::Pose3d pose;
                if (col->HasElement("pose")) {
                    pose = col->GetElement("pose")->Get<ignition::math::Pose3d>();
                }
                auto geo = col->GetElement("geometry");
                loadGeometry(geo, pose, b_mesh, nullptr, pathPrefix);
                col = col->GetNextElement("collision");
            }
        }
    }

    sceneObject->setKinematic(kin);
    sceneObject->updateModels();
    for (std::size_t j = 0; j < kin->getTransforms(); j++) {
        std::cout << kin->getTransform(j)->in->getName() << "\t\t->"
                  << kin->getTransform(j)->getName() << "\t\t->"
                  << kin->getTransform(j)->out->getName() << std::endl;
        if (kin->getTransform(j)->in != nullptr) {
            M4D_DEBUG(kin->getTransform(j)->in->x.transform().matrix());
        }
        M4D_DEBUG(kin->getTransform(j)->x.transform().matrix());
        if (kin->getTransform(j)->out != nullptr) {
            M4D_DEBUG(kin->getTransform(j)->out->x.transform().matrix());
        }
    }
}

void SDFImporter::PImpl::loadVisual(rl::plan::Model *model,
                                    std::unordered_map<std::string, sdf::ElementPtr> name2Visual,
                                    const std::string &pathPrefix)
{
    for (size_t i = 0; i < model->mdl->getBodies(); i++) {
        auto b_mesh = model->model->create();
        if (name2Visual.find(model->mdl->getBody(i)->getName()) != name2Visual.end()) {
            M4D_DEBUG("Loading visual.");
            ignition::math::Pose3d pose;
            auto visual = name2Visual[model->mdl->getBody(i)->getName()];
            while (visual) {
                if (visual->HasElement("pose")) {
                    pose = visual->GetElement("pose")->Get<ignition::math::Pose3d>();
                }
                gsl::owner<SoVRMLAppearance *> appearance = nullptr;
                if (visual->HasElement("material")) {
                    M4D_DEBUG("Loading material: ");
                    auto matElt = visual->GetElement("material");
                    gsl::owner<SoVRMLMaterial *> material = new SoVRMLMaterial();
                    appearance = new SoVRMLAppearance();
                    if (matElt->HasElement("diffuse")) {
                        auto diffuse = matElt->GetElement("diffuse")->Get<sdf::Color>();
                        material->diffuseColor.setValue(diffuse.r, diffuse.g, diffuse.b);
                        material->transparency.setValue(1 - diffuse.a);
                        std::cout << "\tDiffuse: r: " << diffuse.r << "\tg: " << diffuse.g
                                  << "\tb: " << diffuse.b << "\t a: " << diffuse.a << std::endl;
                    }
                    if (matElt->HasElement("specular")) {
                        auto specular = matElt->GetElement("specular")->Get<sdf::Color>();
                        material->specularColor.setValue(specular.r, specular.g, specular.b);
                    }
                    if (matElt->HasElement("emissive")) {
                        auto emissive = matElt->GetElement("emissive")->Get<sdf::Color>();
                        material->emissiveColor.setValue(emissive.r, emissive.g, emissive.b);
                    }
                    appearance->material = material;
                }
                auto geo = visual->GetElement("geometry");
                loadGeometry(geo, pose, b_mesh, appearance, pathPrefix);
                visual = visual->GetNextElement("visual");
            }
        }
    }
    model->updateFrames();
    for (std::size_t j = 0; j < model->mdl->getTransforms(); j++) {
        std::cout << model->mdl->getTransform(j)->in->getName() << "\t\t->"
                  << model->mdl->getTransform(j)->getName() << "\t\t->"
                  << model->mdl->getTransform(j)->out->getName() << std::endl;
        if (model->mdl->getTransform(j)->in != nullptr) {
            M4D_DEBUG(model->mdl->getTransform(j)->in->x.transform().matrix());
        }
        M4D_DEBUG(model->mdl->getTransform(j)->x.transform().matrix());
        if (model->mdl->getTransform(j)->out != nullptr) {
            M4D_DEBUG(model->mdl->getTransform(j)->out->x.transform().matrix());
        }
    }
}

void SDFImporter::PImpl::loadGeometry(const sdf::ElementPtr &geometry, ignition::math::Pose3d pose,
                                      rl::sg::Body *body, SoVRMLAppearance *appearance,
                                      const std::string &pathPrefix)
{
    if (geometry->HasElement("empty")) {
        // It is an empty body
    } else if (geometry->HasElement("box")) {
        M4D_DEBUG("Element is a box, loading...");
        auto box = geometry->GetElement("box");
        auto size = box->GetElement("size")->Get<ignition::math::Vector3d>();
        auto shape = new SoVRMLShape();
        auto vrmlBox = new SoVRMLBox();
        vrmlBox->size.setValue(size.X(), size.Y(), size.Z());
        shape->geometry = vrmlBox;
        if (appearance != nullptr) {
            shape->appearance = appearance;
        }
        auto rlShape = body->create(shape);
        setShapePose(pose, rlShape);
    } else if (geometry->HasElement("cylinder")) {
        M4D_DEBUG("Element is a cylinder, loading...");
        auto cylinder = geometry->GetElement("cylinder");
        auto radius = cylinder->GetElement("radius")->Get<double>();
        auto length = cylinder->GetElement("length")->Get<double>();
        auto shape = new SoVRMLShape;
        auto vrmlCylinder = new SoVRMLCylinder;
        vrmlCylinder->height = length;
        vrmlCylinder->radius = radius;
        shape->geometry = vrmlCylinder;
        if (appearance != nullptr) {
            shape->appearance = appearance;
        }
        auto rlShape = body->create(shape);
        ignition::math::Quaterniond rotOffset(M_PI_2, 0.0, 0.0);
        pose.Rot() = rotOffset * pose.Rot();
        setShapePose(pose, rlShape);
    } else if (geometry->HasElement("plane")) {
        auto plane = geometry->GetElement("plane");
        // TODO(Jules) : Not a VRML primitive
    } else if (geometry->HasElement("sphere")) {
        auto sphere = geometry->GetElement("sphere");
        auto radius = sphere->GetElement("radius")->Get<double>();
        auto shape = new SoVRMLShape();
        auto vrmlSphere = new SoVRMLSphere();
        vrmlSphere->radius = radius;
        shape->geometry = vrmlSphere;
        if (appearance != nullptr) {
            shape->appearance = appearance;
        }
        auto rlShape = body->create(shape);
        setShapePose(pose, rlShape);
    } else if (geometry->HasElement("mesh")) {
        auto mesh = geometry->GetElement("mesh");
        model::AssimpImporter assimpImport;
        math::Vector3 scale;
        if (mesh->HasElement("scale")) {
            auto sdfScale = mesh->GetElement("scale")->Get<ignition::math::Vector3d>();
            scale = math::Vector3(sdfScale.X(), sdfScale.Y(), sdfScale.Z());
        } else {
            scale.x() = 1;
            scale.y() = 1;
            scale.z() = 1;
        }
        move4d::math::Transform transform;
        move4d::math::Quaternion q(pose.Rot().W(), pose.Rot().X(), pose.Rot().Y(), pose.Rot().Z());
        move4d::math::Translation t(pose.Pos().X(), pose.Pos().Y(), pose.Pos().Z());

        transform = t * q;
        // std::cout << "Loading mesh: " <<
        // filenameFromURI(mesh->GetElement("uri")->Get<std::string>()) << "with pose: " <<
        // pos.translation() <<","<< pos.rotation() << std::endl;
        if (appearance != nullptr) {
            auto mat = dynamic_cast<SoVRMLMaterial *>(appearance->material.getValue());
            auto c = mat->diffuseColor.getValue();
            auto color = math::Vector4f(c[0], c[1], c[2], 1.f);
            assimpImport.load(pathPrefix + "/" + mesh->GetElement("uri")->Get<std::string>(), body,
                              scale, transform, color);
        } else {
            assimpImport.load(pathPrefix + "/" + mesh->GetElement("uri")->Get<std::string>(), body,
                              scale, transform);
        }
    }
}

} // namespace world
} // namespace file
} // namespace move4d

//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_FACTORY_H
#define MOVE4D_FACTORY_H

#include "move4d/Singleton.h"
#include <map>
#include <string>
#include <vector>

#include <functional> //likely to be required to register creators
#include <iostream>
namespace move4d
{

template <class T, class U> class Factory;

template <class T, typename Base, typename... Args>
class Factory<T, Base(Args...)> : public Singleton<T>
{
  public:
    typedef std::runtime_error UnknownTypeException;
    typedef std::function<Base(Args...)> CreatorFn;
    inline virtual void registerCreator(const std::string &name, const CreatorFn &creator);

    Base create(const std::string &type, Args... u)
    {
        auto creator = this->m_creators.find(type);
        if (creator != this->m_creators.end()) {
            return creator->second(u...);
        } else {
            throw UnknownTypeException("Type unknown to factory: " + type);
        }
    }

  protected:
    std::map<std::string, CreatorFn> m_creators;

  public:
    inline std::vector<std::string> getTypes();
};

template <class U> class SimpleFactory;

template <typename Base, typename... Args>
class SimpleFactory<Base(Args...)> : public Factory<SimpleFactory<Base(Args...)>, Base(Args...)>
{
};

template <class T, typename Base, typename... Args>
inline void Factory<T, Base(Args...)>::registerCreator(const std::string &name,
                                                       const CreatorFn &creator)
{
    m_creators[name] = creator;
}

template <class T, typename Base, typename... Args>
inline std::vector<std::string> Factory<T, Base(Args...)>::getTypes()
{
    std::vector<std::string> v;
    for (auto &c : m_creators) {
        v.push_back(c.first);
    }
    return v;
}

} // namespace move4d

// credits to github.com/ros/class_loader for the macro
#define FACTORY_REGISTER_CREATOR_INTERNAL(Factory, Derived, Function, UniqueID)                    \
    namespace                                                                                      \
    {                                                                                              \
    struct ProxyExec##UniqueID {                                                                   \
        ProxyExec##UniqueID() noexcept { Factory::instance().registerCreator(Derived, Function); } \
    };                                                                                             \
    static ProxyExec##UniqueID g_register_plugin_##UniqueID;                                       \
    }

// this one needed to have the needed supplementary level of macro expansion
#define FACTORY_REGISTER_CREATOR_INTERNAL_HOP1(Factory, Derived, Function, UniqueID)               \
    FACTORY_REGISTER_CREATOR_INTERNAL(Factory, Derived, Function, UniqueID)
#define FACTORY_REGISTER_FACTORY(factory_class, created_class, creator_function)                   \
    FACTORY_REGISTER_CREATOR_INTERNAL_HOP1(factory_class, created_class, creator_function,         \
                                           __COUNTER__)

#endif

//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_MATH_UTILS_H
#define MOVE4D_MATH_UTILS_H

#include "move4d/math/Vector.h"
#include "move4d/utils/random.h"

namespace move4d
{
namespace math
{

template <class T> struct Bounds {
    T low, high;
    bool check() { return (high - low).squaredNorm() > 1.e-10; }
    bool isIn(const T &value) { return value >= low && value <= high; }
};

using Vector3Bounds = Bounds<Vector3>;
using VectorBounds = Bounds<Vector>;
using Vector3fBounds = Bounds<Vector3f>;
using Vector4fBounds = Bounds<Vector4f>;

template <class T> T shootUniformInRange(const Bounds<T> &range)
{
    auto &rnd = random::instance();
    T res(range.low.size());
    for (typename T::Index i = 0; i < range.low.size(); ++i) {
        res[i] = rnd.uniform(range.low[i], range.high[i]);
    }
    return res;
}

/// compares two floating point number with a precision specified in ULPs (Unit in the  Least Place)
/// from https://en.cppreference.com/w/cpp/types/numeric_limits/epsilon
template <class T>
typename std::enable_if<!std::numeric_limits<T>::is_integer, bool>::type almost_equal(T x, T y,
                                                                                      int ulp)
{
    // the machine epsilon has to be scaled to the magnitude of the values used
    // and multiplied by the desired precision in ULPs (units in the last place)
    return std::fabs(x - y) <= std::numeric_limits<T>::epsilon() * std::fabs(x + y) * ulp
           // unless the result is subnormal
           || std::fabs(x - y) < std::numeric_limits<T>::min();
}

/// from
/// https://stackoverflow.com/questions/4915462/how-should-i-do-floating-point-comparison/32334103#32334103
template <class T>
typename std::enable_if<!std::numeric_limits<T>::is_integer, bool>::type
nearly_equal(T a, T b, T epsilon, T relth)
// those defaults are arbitrary and could be removed
{
    assert(std::numeric_limits<T>::epsilon() <= epsilon);
    assert(epsilon < T(1.));

    if (a == b)
        return true;

    auto diff = std::abs(a - b);
    auto norm = std::min((std::abs(a) + std::abs(b)), std::numeric_limits<T>::max());
    return diff < std::max(relth, epsilon * norm);
}
} // namespace math

} // namespace move4d

#endif // MOVE4D_MATH_UTILS_H

//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_PLAN_OMPL_KINEMATICGRAPHSTATESPACE_H
#define MOVE4D_PLAN_OMPL_KINEMATICGRAPHSTATESPACE_H

#include <ompl/base/spaces/SE3StateSpace.h>

#include "move4d/plan/Types.h"
#include "move4d/plan/ompl/StateSpace.h"
#include "move4d/plan/ompl/Types.h"
#include "move4d/utils/containers/ptr_wrapper.hpp"

namespace move4d
{
namespace plan
{
namespace ompl
{
class KinematicData;
class KinematicGraphStateSpace : public move4d::plan::ompl::StateSpace
{
    MOVE4D_STATIC_LOGGER;

  public:
    struct StateType : public StateSpace::StateType {
        //::ompl::base::SE3StateSpace::StateType **vertices;
        math::Transform *vertices;
        // FIXME(Jules): URGENT : why both poses and vertices ?????
        ::ompl::base::SE3StateSpace::StateType **poses = nullptr;
        enum {
            IkComputed = 1 << 6,
            FkComputed = 1 << 7,
        };
        bool kinComputed() const { return this->flags & IkComputed && this->flags & FkComputed; }
    };
    struct Element {
        enum Method { Inverse, Forward };
        Method method;
        std::function<BaseSamplerPtr()> sampler_allocator;
        std::vector<size_t> sampler_args;
        size_t source, target;

        /// indices of the kinematic group in StateType::values
        std::vector<size_t> bijection;
        std::unique_ptr<KinematicData> kinematic;
    };
    KinematicGraphStateSpace(std::unique_ptr<KinematicGraphSearchSpace> space,
                             mdl::WorldStatePtr init_ws = {});

    KinematicGraphStateSpace(const KinematicGraphSearchSpace &space,
                             mdl::WorldStatePtr init_ws = {});

    void interpolate(const ::ompl::base::State *from, const ::ompl::base::State *to, double t,
                     ::ompl::base::State *state) const override;
    ::ompl::base::StateSamplerPtr allocDefaultStateSampler() const override;

    virtual bool computeIK(size_t pose_i, StateType *state, const mdl::WorldState &ws,
                           const StateSpaceWrapperPtr &wrapper) const;
    virtual bool computeFK(size_t pose_i, StateType *state, const mdl::WorldState &ws,
                           const StateSpaceWrapperPtr &wrapper) const;
    virtual bool computeIK(StateType *state) const;
    virtual bool computeFK(StateType *state) const;
    virtual bool computeK(StateType *state) const;

    ::ompl::base::State *allocState() const override;
    void freeState(::ompl::base::State *state) const override;
    void printState(const ::ompl::base::State *state, std::ostream &out) const override;
    void readableFlag(int flags, std::ostream &out) const;
    void copyState(::ompl::base::State *destination,
                   const ::ompl::base::State *source) const override;

    size_t getNumberOfElements() const { return m_elements.size(); }
    const std::vector<Element> &getElements() const;

    const ptr_wrapper<KinematicGraphSearchSpace> &getSearchSpace() const { return m_search_space; }

    const std::vector<std::pair<StateSpaceWrapperPtr, size_t>> &getAttachments() const;

    size_t getNumberOfVertices() const { return m_vertices_count; }
    const std::vector<std::shared_ptr<::ompl::base::SE3StateSpace>> &getVerticesStateSpaces() const;

    using StateSpace::sanityChecks;
    void sanityChecks() const override;

  protected:
    void addPoseComponents(const SearchSpace &searchSpace);
    void addPoseComponents(const SearchSpace &searchSpace, size_t &firstDof);
    void allocStateMembers(StateSpace::StateType *state) const override;

  private:
    size_t m_vertices_count;
    std::vector<std::shared_ptr<::ompl::base::SE3StateSpace>> m_vertices_state_spaces;
    std::vector<Element> m_elements; // same order as m_poses
    ptr_wrapper<KinematicGraphSearchSpace> m_search_space;
    std::vector<std::pair<StateSpaceWrapperPtr, size_t>> m_attachments;
};

class KinematicGraphStateSampler : public move4d::plan::ompl::DefaultStateSampler
{
    MOVE4D_STATIC_LOGGER;

  public:
    using StateType = KinematicGraphStateSpace::StateType;
    KinematicGraphStateSampler(const KinematicGraphStateSpace *space);
    void sampleUniform(::ompl::base::State *state) override;
    void sampleUniformNear(::ompl::base::State *state, const ::ompl::base::State *near,
                           double distance) override;
    void sampleGaussian(::ompl::base::State *state, const ::ompl::base::State *mean,
                        double stdDev) override;

    /**
     * @brief set a custom sampler for the ith element.
     * @param i
     * @param sampler
     *
     * By default they are created from the space definition (custom samplers). But you can
     * change/set them to get a different behaviour. Like creating a goal state sampler.
     */
    void setSampler(size_t i, BaseSamplerPtr sampler);
    const BaseSamplerPtr &getSampler(size_t i) const;

  private:
    std::vector<BaseSamplerPtr> m_samplers;
};

class KinematicGraphValidStateSampler : public KinematicGraphStateSampler
{
  public:
    KinematicGraphValidStateSampler(const KinematicGraphStateSpace *space);
    void sampleUniform(::ompl::base::State *state) override;
    void sampleUniformNear(::ompl::base::State *state, const ::ompl::base::State *near,
                           double distance) override;
    void sampleGaussian(::ompl::base::State *state, const ::ompl::base::State *mean,
                        double stdDev) override;
};

class KinematicData
{
  public:
    virtual bool computeIK(const mdl::WorldState &ws_in,
                           KinematicGraphStateSpace::StateType *state_in_out,
                           const KinematicGraphStateSpace::Element &element) const = 0;
    virtual bool computeFK(const mdl::WorldState &ws_in, KinematicGraphStateSpace::StateType *state,
                           const KinematicGraphStateSpace::Element &element,
                           const math::Transform &base_pose,
                           math::Transform &end_pose_out) const = 0;
};

class KinematicDataChain : public KinematicData
{
    MOVE4D_STATIC_LOGGER;

  public:
    KinematicDataChain(const mdl::SceneObject *object, mdl::ChainPtr chain,
                       std::string kinematicSolver)
        : object(object), chain(std::move(chain)), kinematicSolver(std::move(kinematicSolver))
    {
    }
    bool computeIK(const mdl::WorldState &ws_in, KinematicGraphStateSpace::StateType *state_in_out,
                   const KinematicGraphStateSpace::Element &element) const override;
    bool computeFK(const mdl::WorldState &ws_in, KinematicGraphStateSpace::StateType *state,
                   const KinematicGraphStateSpace::Element &element,
                   const math::Transform &base_pose, math::Transform &end_pose_out) const override;

  protected:
    const mdl::SceneObject *object;
    mdl::ChainPtr chain;
    std::string kinematicSolver;
};
class KinematicDataFixed : public KinematicData
{
  public:
    KinematicDataFixed(math::Transform tf) : transform(std::move(tf)) {}

    bool computeIK(const mdl::WorldState &ws_in, KinematicGraphStateSpace::StateType *state_in_out,
                   const KinematicGraphStateSpace::Element &element) const override;
    bool computeFK(const mdl::WorldState &ws_in, KinematicGraphStateSpace::StateType *state,
                   const KinematicGraphStateSpace::Element &element,
                   const math::Transform &base_pose, math::Transform &end_pose_out) const override;

  protected:
    math::Transform transform;
};

} // namespace ompl
} // namespace plan
} // namespace move4d

#endif // MOVE4D_PLAN_OMPL_KINEMATICGRAPHSTATESPACE_H

//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_MDL_WORLDSTATE_H
#define MOVE4D_MDL_WORLDSTATE_H

#include "move4d/common.h"
#include "move4d/mdl/State.h"
#include "move4d/mdl/Types.h"
#include <boost/serialization/access.hpp>
#include <boost/serialization/unordered_map.hpp>
#include <memory>
#include <string>
#include <unordered_map>

namespace move4d
{

namespace mdl
{

class State;
class Scene;
class SceneObject;
class WorldState;
using WorldStatePtr = std::shared_ptr<WorldState>;

/**
 * represents a snapshot of the states of all objects of the scene
 */
class WorldState
{
  public:
    typedef std::unordered_map<move4d::Identifier, State>::iterator iterator;

    typedef std::unordered_map<move4d::Identifier, State>::const_iterator const_iterator;

    WorldState() = default;
    WorldState(const Scene *scene);
    WorldState(const WorldState &);
    WorldState(WorldState &&) = default;
    WorldState &operator=(const WorldState &);
    WorldState &operator=(WorldState &&) = default;
    virtual ~WorldState() = default;

    virtual std::unique_ptr<WorldState> cloneWorldState() const
    {
        // must reimplement in subclasses
        Expects(typeid(*this) == typeid(WorldState));
        return std::make_unique<WorldState>(*this);
    }

    bool operator==(const WorldState &other) const;
    bool operator!=(const WorldState &other) const { return !(*this == other); }

    inline const std::unordered_map<move4d::Identifier, State> getStates() const;

    inline const Scene *getScene() const;

    const State &getState(const std::string &id) const;
    State &getState(const std::string &id);

    /**
     * the sum of squared distances between every state of this and other
     */
    move4d::Real distanceSquared(const WorldState &other,
                                 StateType::Type type = StateType::Position) const;

    /**
     * return the distance between the two worldstates
     */
    move4d::Real distance(const WorldState &other,
                          StateType::Type type = StateType::Position) const;

    const State &operator[](const move4d::Identifier &id) const;
    State &operator[](const move4d::Identifier &id);

  protected:
    const Scene *m_scene;

    std::unordered_map<move4d::Identifier, State> m_states;

  public:
    inline iterator begin();

    inline const_iterator begin() const;

    inline iterator end();

    inline const_iterator end() const;

  private:
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive &ar, const unsigned int version);
};

template <typename Archive> void WorldState::serialize(Archive &ar, const unsigned int version)
{
    // clang-format off
    ar & m_states;
    // clang-format on
}
inline const std::unordered_map<Identifier, State> WorldState::getStates() const
{
    return m_states;
}

inline const Scene *WorldState::getScene() const { return m_scene; }

inline WorldState::iterator WorldState::begin() { return m_states.begin(); }

inline WorldState::const_iterator WorldState::begin() const { return m_states.begin(); }

inline WorldState::iterator WorldState::end() { return m_states.end(); }

inline WorldState::const_iterator WorldState::end() const { return m_states.end(); }

} // namespace mdl

} // namespace move4d
#endif

//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//

#include "move4d/mdl/State.h"
#include "move4d/mdl/SceneObject.h"
#include <rl/mdl/Dynamic.h>
#include <rl/mdl/SixDof.h>
#include <rl/mdl/Spherical.h>

namespace move4d
{

namespace mdl
{

State::State(SceneObject *object, std::size_t nbDofPosition, std::size_t nbDofDerivatives)
    : m_object(object)
{
    m_value.assign(StateType::types().size(), {});
    reset();
}

State::State(SceneObject *object) : m_object(object)
{
    m_value.assign(StateType::types().size(), {});
    reset();
}

/**
 * resize to fit the new number of dofs. Keep existing data
 */
void State::resize(std::size_t nbDofPosition, std::size_t nbDofDerivatives)
{
    m_value[StateType::Position].conservativeResize(nbDofPosition);
    m_object->m_kinematic->clamp(m_value[StateType::Position]);

    m_value[StateType::Torque].conservativeResize(nbDofDerivatives);
    m_value[StateType::Speed].conservativeResize(nbDofDerivatives);
    m_value[StateType::Acceleration].conservativeResize(nbDofDerivatives);
    m_value[StateType::Jerk].conservativeResize(nbDofDerivatives);
    m_value[StateType::Snap].conservativeResize(nbDofDerivatives);
}

void State::reset()
{
    std::size_t nbDofPosition = m_object->getNumberOfDof(StateType::Position);
    std::size_t nbDofDerivatives = m_object->getNumberOfDof(StateType::Speed);
    m_value[StateType::Position].setZero(nbDofPosition);
    m_object->m_kinematic->clamp(m_value[StateType::Position]);
    m_object->m_kinematic->normalize(m_value[StateType::Position]);

    m_value[StateType::Torque].setZero(nbDofDerivatives);
    m_value[StateType::Speed].setZero(nbDofDerivatives);
    m_value[StateType::Acceleration].setZero(nbDofDerivatives);
    m_value[StateType::Jerk].setZero(nbDofDerivatives);
    m_value[StateType::Snap].setZero(nbDofDerivatives);
}

move4d::Real &State::operator()(std::size_t index, StateType::Type type)
{
    Expects(m_value.size() == StateType::types().size());
    if (index >= m_value[type].size()) {
        throw std::out_of_range("State has no data for " + StateType::strings()[type] + " @ " +
                                std::to_string(index));
    }
    return m_value[type][index];
}

const move4d::Real &State::operator()(std::size_t index, StateType::Type type) const
{
    Expects(m_value.size() == StateType::types().size());
    if (index >= m_value[type].size()) {
        throw std::out_of_range("State has no data for " + StateType::strings()[type] + " @ " +
                                std::to_string(index));
    }
    return m_value[type][index];
}

void State::setValues(std::size_t fromDof, const move4d::math::Transform &position)
{
    auto trans = position.translation();
    auto rot = math::Quaternion(position.rotation());
    (*this)(fromDof + 0, StateType::Position) = trans.x();
    (*this)(fromDof + 1, StateType::Position) = trans.y();
    (*this)(fromDof + 2, StateType::Position) = trans.z();
    (*this)(fromDof + 3, StateType::Position) = rot.x();
    (*this)(fromDof + 4, StateType::Position) = rot.y();
    (*this)(fromDof + 5, StateType::Position) = rot.z();
    (*this)(fromDof + 6, StateType::Position) = rot.w();
}

/**
 * the sum of square distance between each dof of this and other
 */
move4d::Real State::distanceSquared(const State &other, StateType::Type type) const
{
    return this->m_object->distanceSquared(*this, other, type);
}

bool State::operator==(const State &other) const
{
    bool eq = true;
    for (auto t : StateType::types()) {
        eq &= ::move4d::math::utilities::equals(this->distanceSquared(other, t), 0.);
    }
    return eq;
}

bool State::isInBounds() const
{
    for (auto t : StateType::types()) {
        for (size_t i = 0; i < m_value[t].size(); ++i) {
            if (m_object->getMinimum(i, t) > m_value[t][i] ||
                m_object->getMaximum(i, t) < m_value[t][i])
                return false;
        }
    }
    return true;
}

} // namespace mdl

} // namespace move4d

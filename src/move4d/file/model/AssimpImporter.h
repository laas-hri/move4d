//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_FILE_MODEL_ASSIMPIMPORTER_H
#define MOVE4D_FILE_MODEL_ASSIMPIMPORTER_H

#include <string>
#if __has_include(<optional>)
#include <optional>
#elif __has_include(<experimental/optional>)
#include <experimental/optional>
namespace std
{
template <typename T> using optional = experimental::optional<T>;
}
#else
#error no std::optional
#endif

#include "move4d/common.h"
#include "move4d/math/Vector.h"
#include "move4d/mdl/Types.h"

class aiNode;
class aiScene;

namespace rl
{
namespace sg
{
class Body;
}
} // namespace rl

namespace move4d
{

namespace file
{

namespace model
{

class LoadException : std::runtime_error
{
  public:
    LoadException(const std::string &desc) : std::runtime_error(desc) {}
};

class AssimpImporter
{
  public:
    void load(const std::string &filepath, rl::sg::Body *body, const move4d::math::Vector3 &scale,
              const math::Transform &pos, std::optional<Eigen::Vector4f> color = {});
    void loadMesh(const aiNode *mesh, const aiScene *scene, rl::sg::Body *body,
                  const move4d::math::Vector3 &scale, const math::Transform &pos,
                  std::optional<Eigen::Vector4f> &color);
};

} // namespace model

} // namespace file

} // namespace move4d
#endif

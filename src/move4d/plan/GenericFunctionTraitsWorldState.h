//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_PLAN_GENERICFUNCTIONTRAITSWORLDSTATE_H
#define MOVE4D_PLAN_GENERICFUNCTIONTRAITSWORLDSTATE_H

#undef foreach
#include "move4d/common.h"
#include "move4d/mdl/WorldState.h"
#include <roboptim/core/function.hh>

namespace roboptim
{

template <> struct GenericFunctionTraits<move4d::mdl::WorldState> {
    static const int StorageOrder = roboptim::StorageOrder;

    typedef move4d::Real value_type;

    typedef Eigen::Matrix<value_type, Eigen::Dynamic, Eigen::Dynamic, StorageOrder> matrix_t;

    typedef Eigen::Ref<matrix_t> matrix_ref;

    typedef const Eigen::Ref<const matrix_t> &const_matrix_ref;

    typedef Eigen::Matrix<value_type, Eigen::Dynamic, 1> vector_t;

    typedef Eigen::Ref<vector_t> vector_ref;

    typedef const Eigen::Ref<const vector_t> &const_vector_ref;

    typedef Eigen::Matrix<value_type, 1, Eigen::Dynamic> rowVector_t;

    typedef Eigen::Ref<rowVector_t> rowVector_ref;

    typedef const Eigen::Ref<const rowVector_t> &const_rowVector_ref;

    typedef matrix_t::Index size_type;

    typedef vector_t result_t;

    typedef Eigen::Ref<result_t> result_ref;

    typedef const Eigen::Ref<const result_t> &const_result_ref;

    typedef move4d::mdl::WorldState argument_t;

    typedef argument_t &argument_ref;

    typedef const argument_t &const_argument_ref;

    typedef rowVector_t gradient_t;

    typedef Eigen::Ref<gradient_t, 0, detail::row_vector_stride<StorageOrder>::type> gradient_ref;

    typedef const Eigen::Ref<const gradient_t, 0, detail::row_vector_stride<StorageOrder>::type>
        &const_gradient_ref;

    typedef matrix_t jacobian_t;

    typedef Eigen::Ref<jacobian_t> jacobian_ref;

    typedef const Eigen::Ref<const jacobian_t> &const_jacobian_ref;

    typedef matrix_t hessian_t;

    typedef Eigen::Ref<hessian_t> hessian_ref;

    typedef const Eigen::Ref<const hessian_t> &const_hessian_ref;

    typedef vector_t derivative_t;

    typedef Eigen::Ref<derivative_t> derivative_ref;

    typedef const Eigen::Ref<const derivative_t> &const_derivative_ref;
};

} // namespace roboptim
#endif

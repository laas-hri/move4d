//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#include "move4d/plan/ompl/PoseStateSpace.h"
#include "move4d/mdl/SceneObject.h"
#include "move4d/mdl/WorldState.h"
#include "move4d/mdl/kin/KinematicSolver.h"
#include "move4d/plan/ClosedLoopSearchSpace.h"
#include "move4d/plan/KinematicGraphSearchSpace.h"
#include "move4d/plan/RobotSpace.h"
#include "move4d/plan/SearchSpace.h"
#include "move4d/plan/ompl/converters/StateSpaceFactory.h"
#include "move4d/plan/ompl/converters/converters.h"

namespace move4d
{
namespace plan
{
namespace ompl
{

INIT_MOVE4D_STATIC_LOGGER(KinematicValidStateSampler,
                          "move4d.plan.ompl.posestatespace.kinematicvalidstatesampler");
INIT_MOVE4D_STATIC_LOGGER(KinematicStateSampler,
                          "move4d.plan.ompl.posestatespace.kinematicstatesampler");
INIT_MOVE4D_STATIC_LOGGER(PoseStateSpace, "move4d.plan.ompl.posestatespace");

PoseStateSpace::PoseStateSpace(const SearchSpace &searchSpace, mdl::WorldStatePtr initWs)
    : StateSpace(searchSpace, initWs)
{
    addPoseComponents(searchSpace);
}

double PoseStateSpace::distance(const ::ompl::base::State *state1,
                                const ::ompl::base::State *state2) const
{
    // TODO(Jules): PoseStateSpace::distance
    return StateSpace::distance(state1, state2);
}

void PoseStateSpace::interpolate(const ::ompl::base::State *from, const ::ompl::base::State *to,
                                 double t, ::ompl::base::State *state) const
{
    StateSpace::interpolate(from, to, t, state);
    auto x = state->as<StateType>();
    auto xfrom = from->as<StateType>();
    auto xto = to->as<StateType>();
    Expects(isStopping() || xfrom->kinComputed());
    Expects(isStopping() || xto->kinComputed());
    // interpolate the poses
    for (size_t i = 0; i < m_posesCount; ++i) {
        if (m_poses[i].method == PoseComponent::Method::IkOnly) {
            m_poses[i].stateSpace->interpolate(xfrom->poses[i], xto->poses[i], t, x->poses[i]);
        }
    }
    // recompute the IK, using interpolated dof position as seed for the ik
    computeK(x);

    Ensures(t < 1. || this->equalStates(state, to));
}

::ompl::base::StateSamplerPtr PoseStateSpace::allocDefaultStateSampler() const
{
    return std::make_shared<KinematicValidStateSampler>(this);
}

void PoseStateSpace::allocStateMembers(StateSpace::StateType *s) const
{
    auto *state = s->as<StateType>();
    StateSpace::allocStateMembers(state);
    state->poses = new ::ompl::base::SE3StateSpace::StateType *[m_posesCount];
    for (size_t i = 0; i < m_posesCount; ++i) {
        state->poses[i] =
            m_poses[i].stateSpace->allocState()->as<::ompl::base::SE3StateSpace::StateType>();
    }
}

::ompl::base::State *PoseStateSpace::allocState() const
{
    auto state = new StateType;
    allocStateMembers(state);
    return state;
}

void PoseStateSpace::freeState(::ompl::base::State *state) const
{
    for (size_t i = 0; i < m_posesCount; ++i) {
        m_poses[i].stateSpace->freeState(state->as<StateType>()->poses[i]);
    }
    delete[] state->as<StateType>()->poses;
    StateSpace::freeState(state);
}

void PoseStateSpace::copyState(::ompl::base::State *destination,
                               const ::ompl::base::State *source) const
{
    StateSpace::copyState(destination, source);
    for (size_t i = 0; i < m_posesCount; ++i) {
        m_poses[i].stateSpace->copyState(destination->as<StateType>()->poses[i],
                                         source->as<StateType>()->poses[i]);
    }
}

void PoseStateSpace::readableFlag(int flags, std::ostream &out) const
{
    out << "flags: ";
    if (flags & StateSpace::StateType::IsGoal) {
        out << "goal; ";
    }
    if (flags & StateSpace::StateType::IsStart) {
        out << "start; ";
    }
    if (flags & StateSpace::StateType::ValidityComputed) {
        if (flags & StateSpace::StateType::IsValid) {
            out << "valid; ";
        } else {
            out << "invalid; ";
        }
    }
    if (flags & PoseStateSpace::StateType::FkComputed) {
        if (flags & StateSpace::StateType::IsValid) {
            out << "fk valid; ";
        } else {
            out << "fk computed; ";
        }
    }
    if (flags & PoseStateSpace::StateType::IkComputed) {
        if (flags & StateSpace::StateType::IsValid) {
            out << "ik valid; ";
        } else {
            out << "ik computed; ";
        }
    }
    out << "\n";
}
void PoseStateSpace::printState(const ::ompl::base::State *state_in, std::ostream &out) const
{
    auto state = state_in->as<StateType>();
    size_t current_first_dof = 0;
    out << "move4d::plan::ompl::PoseStateSpace::StateType of dimension " << getDimension()
        << "[[subspaces:[\n";
    for (const auto &ss : m_subspaces) {
        auto ss1 = convertStateForSubspace(ss, state->as<StateType>(), current_first_dof,
                                           current_first_dof);
        ss->getStateSpace()->printState(ss1, out);
        freeSubspaceState(ss, ss1);
    }
    assert(current_first_dof == getVariableCount());
    out << "],\nposes:[\n";
    for (size_t i = 0; i < m_posesCount; ++i) {
        out << "method: ";
        switch (m_poses[i].method) {
        case PoseComponent::Method::FkOnly:
            out << "FK";
            break;
        case PoseComponent::Method::IkOnly:
            out << "IK";
            break;
        case PoseComponent::Method::ForeignIkTarget:
            out << "Foreign";
            break;
        }
        out << "\n";
        m_poses[i].stateSpace->printState(state->poses[i], out);
    }
    out << "],\n";
    readableFlag(state->flags, out);
    out << "]\n";
}

bool PoseStateSpace::computeIK(PoseStateSpace::StateType *state) const
{
    Expects(m_initWorldState);     // need to call setInitWorldState before
    Expects(!m_wrapper.expired()); // need to call setWrapper before
    auto wrapper = m_wrapper.lock();
    if (!wrapper) {
        throw std::runtime_error(
            "move4d::plan::ompl::PoseStateSpace StateSpaceWrapper unset or destructed");
    }

    if (state->flags & StateType::IkComputed) {
        return state->flags & StateType::IsValid;
    }
    state->flags |= StateType::IkComputed;
    // Compute the worldstate based on the current state
    mdl::WorldState ws = *m_initWorldState;
    wrapper->applyState(state, ws);
    for (size_t i = 0; i < m_posesCount; i++) {
        if (!computeIK(i, state, ws, wrapper))
            return false;
    }
    state->flags |= StateType::IsValid;
    return true;
}

bool PoseStateSpace::computeIK(size_t pose_i, PoseStateSpace::StateType *state,
                               const mdl::WorldState &ws, const StateSpaceWrapperPtr &wrapper) const
{
    if (m_poses[pose_i].method == PoseComponent::Method::FkOnly)
        return true;
    M4D_TRACE("computeIK for pose " << pose_i);
    math::Transform pose;
    if (m_poses[pose_i].method == PoseComponent::Method::ForeignIkTarget) {
        assert(m_poses[pose_i].foreignIkTarget != -1u);
        pose = converters::toTransform(state->poses[m_poses[pose_i].foreignIkTarget]);
    } else {
        pose = converters::toTransform(state->poses[pose_i]);
    }
    math::Vector x =
        m_poses[pose_i].chain->configFromWorldState(ws, m_subspaces[m_poses[pose_i].subspace]
                                                            ->getSearchSpace()
                                                            ->as<RobotSpace>()
                                                            ->robot()
                                                            ->getId());
    // bool valid = m_poses[i].object->computeInverseKinematic(
    //    *m_poses[i].endEffector, *m_poses[i].chain, pose,
    //    *m_initWorldState->getState(m_poses[i].object->getId()), x);
    kin::KinematicSolverPtr solver =
        kin::KinematicSolverFactory::instance().create(m_poses[pose_i].kinematicSolver);
    // TODO(Jules) fix: the state to pass to computeInverseKinematic needs to account for the
    // state passed in
    bool valid =
        solver->computeInverseKinematic(*m_poses[pose_i].object, *m_poses[pose_i].chain, pose,
                                        ws.getState(m_poses[pose_i].object->getId()), x);
    if (valid) {
        // check bounds
        for (size_t b = 0; b < m_poses[pose_i].bijection.size(); ++b) {
            state->values[m_poses[pose_i].bijection[b]] = x[b];
        }
        valid = valid && this->satisfiesBounds(state);
    }
    if (!valid) {
        state->flags |= StateType::ValidityComputed;
        state->flags &= ~StateType::IsValid;
        return false;
    }
    return true;
}

bool PoseStateSpace::computeFK(PoseStateSpace::StateType *state) const
{
    Expects(m_initWorldState);     // call setInitWorldState before
    Expects(!m_wrapper.expired()); // need to call setWrapper before
    auto wrapper = m_wrapper.lock();
    if (!wrapper) {
        throw std::runtime_error(
            "move4d::plan::ompl::PoseStateSpace StateSpaceWrapper unset or destructed");
    }

    // TODO(Jules): this Expects(satisfiesBounds) was here for some reason, but breaks with a
    // Reeds&Shepp part when interpolating close to environment limits
    // Expects(this->satisfiesBounds(state));
    if (state->flags & StateType::FkComputed) {
        return state->flags & StateType::IsValid;
    }
    state->flags |= StateType::FkComputed;
    // Compute the worldstate based on the current state
    mdl::WorldState ws = *m_initWorldState;
    wrapper->applyState(state, ws);
    for (size_t i = 0; i < m_posesCount; i++) {
        if (!computeFK(i, state, ws, wrapper)) {
            return false;
        }
    }
    state->flags |= StateType::IsValid;
    return true;
}

bool PoseStateSpace::computeFK(size_t pose_i, PoseStateSpace::StateType *state,
                               const mdl::WorldState &ws, const StateSpaceWrapperPtr &wrapper) const
{
    if (m_poses[pose_i].method != PoseComponent::Method::FkOnly) {
        return true; // don't compute FK for this one
    }
    M4D_TRACE("computeFK for pose " << pose_i);
    math::Transform pose;
    // get x from the worldstate
    // x is the configuration expected for the chain
    // some of its values may not be found in state
    math::Vector x =
        m_poses[pose_i].chain->configFromWorldState(ws, m_subspaces[m_poses[pose_i].subspace]
                                                            ->getSearchSpace()
                                                            ->as<RobotSpace>()
                                                            ->robot()
                                                            ->getId());
    // TODO(Jules): we probably can avoid a copy of the state values in PoseStateSpace::computeK
    // by creating a math::Vector from the raw data

    // TODO(Jules): allow computeForwardKinematic to take in the base pose of the kinematic
    // chain
    bool valid = m_poses[pose_i].object->computeForwardKinematic(
        *m_poses[pose_i].chain, pose, ws.getState(m_poses[pose_i].object->getId()), x);
    if (!valid) {
        state->flags |= StateType::ValidityComputed;
        state->flags &= ~StateType::IsValid;
        std::cout << "invalid fk\n";
        return false;
    }
    converters::toState(pose, state->poses[pose_i]);
    return true;
}

bool PoseStateSpace::computeK(PoseStateSpace::StateType *state) const
{
    bool res = computeFK(state) && computeIK(state);
    Ensures(res == state->isValid());
    Ensures(!res || state->kinComputed()); // (valid => kin computed)
    return res;
}

size_t PoseStateSpace::getPosesCount() const { return m_posesCount; }

const PoseStateSpace::PoseComponent &PoseStateSpace::getPoseComponent(size_t i) const
{
    Expects(i < m_poses.size() && i < m_posesCount);
    return m_poses[i];
}

void PoseStateSpace::sanityChecks() const
{
    uint flags = ~(0
                   // | STATESPACE_DISTANCE_DIFFERENT_STATES //
                   // | STATESPACE_DISTANCE_SYMMETRIC      //
                   // | STATESPACE_INTERPOLATION
                   // | STATESPACE_TRIANGLE_INEQUALITY     //
                   // | STATESPACE_DISTANCE_BOUND          //
                   // | STATESPACE_RESPECT_BOUNDS          //
                   // | STATESPACE_ENFORCE_BOUNDS_NO_OP    //
                   // |
                   | STATESPACE_SERIALIZATION);
    double zero = 1e-10; // std::numeric_limits<double>::epsilon();
    double eps = 3e-10;  // std::numeric_limits<double>::epsilon();
    move4d::plan::ompl::StateSpace::sanityChecks(zero, eps, flags);
}

void PoseStateSpace::addPoseComponents(const SearchSpace &searchSpace)
{
    size_t i = 0;
    addPoseComponents(searchSpace, i);
    Ensures(i == this->getDimension());
    Ensures(i == searchSpace.getDimension());
}

void PoseStateSpace::addPoseComponents(const SearchSpace &searchSpace, size_t &firstDof)
{

    M4D_DEBUG("addPoseComponents " << std::quoted(searchSpace.getName())
                                   << " dimension=" << searchSpace.getDimension());
    switch (searchSpace.getType()) {
    case SearchSpace::Type::Compound: {
        M4D_DEBUG("... of type Compound");
        auto &subspaces = dynamic_cast<const CompoundSearchSpace &>(searchSpace).getSubspaces();
        for (size_t i = 0; i < subspaces.size(); ++i) {
            addPoseComponents(subspaces[i], firstDof);
        }
        break;
    }
    case SearchSpace::Type::KinematicGroup: {
        M4D_DEBUG("... of type KinematicGroup");
        const auto &space = dynamic_cast<const RobotKinematicGroupSpace &>(searchSpace);
        const auto &chain = *space.getKinematicChain();
        ++m_posesCount;
        std::vector<size_t> bijection;
        bijection.reserve(chain.bijection.size());
        for (size_t i = firstDof; i < firstDof + chain.bijection.size(); ++i) {
            bijection.push_back(i);
        }
        size_t subspace_i = 0;
        size_t current_subspace_dof = 0;
        while (current_subspace_dof < firstDof) {
            current_subspace_dof += getSubspaces()[subspace_i]->getStateSpace()->getDimension();
            ++subspace_i;
        }

        assert(firstDof == current_subspace_dof);
        assert(subspace_i < getSubspaces().size());
        // assert(bijection.size() == space->getDimension());
        // assert(bijection.size() == getSubspaces()[subspace_i]->getStateSpace()->getDimension());

        // TODO(Jules): FIX URGENT not necessarily FkOnly nor IkOnly
        m_poses.emplace_back(new ::ompl::base::SE3StateSpace, space.robot(),
                             space.getKinematicChain(), subspace_i, bijection,
                             space.getKinematicSolver(), PoseComponent::Method::FkOnly);
        ::ompl::base::RealVectorBounds bounds(3);
        for (size_t i = 0; i < 3; ++i) {
            bounds.low[i] = space.getPoseLowerBounds()[i];
            bounds.high[i] = space.getPoseHigherBounds()[i];
        }
        m_poses.back().stateSpace->setBounds(bounds);
        firstDof += searchSpace.getDimension();

        break;
    }
    case SearchSpace::Type::ClosedLoop: {
        M4D_DEBUG("... of type ClosedLoop");
        const auto &space = dynamic_cast<const ClosedLoopSearchSpace &>(searchSpace);
        PoseComponent::Method ik_method = PoseComponent::Method::IkOnly;
        size_t foreignIkTarget = 0;
        // they are not part of the kinematic loop, but need to be shot before (e.g. base,...)
        if (space.getPre()) {
            firstDof += space.getPre()->getDimension();
        }
        // if there is a forward chain, we use its tip pose as the target for the
        // inverse kinematic chains (bellow)
        if (space.getFkChain()) {
            addPoseComponents(*space.getFkChain(), firstDof);
            m_poses.back().method = PoseComponent::Method::FkOnly;
            ik_method = PoseComponent::Method::ForeignIkTarget;
            // TODO(Jules): if it's a ForeignIkTarget, there is no need to store the values?
            foreignIkTarget = m_poses.size() - 1;
        }
        // the inverse kinematic chains have all the same target,
        // which can be the FK chain tip if any (ik_method=ForeignIkTarget),
        // otherwise it is pose randomly shot (ik_method=IkOnly).
        for (auto &ss : space.getIkChains()) {
            addPoseComponents(*ss, firstDof);
            m_poses.back().method = ik_method;
            m_poses.back().foreignIkTarget = foreignIkTarget;
        }
        // something that needs to be shot after the closed loop
        if (space.getPost()) {
            firstDof += space.getPost()->getDimension();
        }
        break;
    }
    case SearchSpace::Type::KinematicGraph: {
        M4D_DEBUG("... of type KinematicGraph");
        const auto &space = dynamic_cast<const KinematicGraphSearchSpace &>(searchSpace);
        for (auto &x : space) {
            if (x.type != KinematicGraphSearchSpace::Edge::Fixed) {
                addPoseComponents(*x.space, firstDof);
                switch (x.type) {
                case KinematicGraphSearchSpace::Edge::Forward:
                    m_poses.back().method = PoseComponent::Method::FkOnly;
                    break;
                case KinematicGraphSearchSpace::Edge::Inverse:
                    m_poses.back().method = PoseComponent::Method::ForeignIkTarget;
                    m_poses.back().foreignIkTarget = -1;
                    break;
                default:
                    break;
                }
            }
        }
        break;
    }
    default:
        M4D_DEBUG("... skipped: sub space " << searchSpace.getName());
        firstDof += searchSpace.getDimension();
        break;
    }
}

size_t KinematicValidStateSampler::count_total_ = 0;
size_t KinematicValidStateSampler::count_success_ = 0;
KinematicValidStateSampler::KinematicValidStateSampler(const PoseStateSpace *space)
    : DefaultStateSampler(space)
{
    for (size_t i = 0; i < space->getPosesCount(); ++i) {
        poseSamplers_.push_back(space->getPoseComponent(i).stateSpace->allocStateSampler());
    }
}

void KinematicValidStateSampler::sampleUniform(::ompl::base::State *state)
{
    auto x = state->as<PoseStateSpace::StateType>();
    auto space = space_->as<PoseStateSpace>();
    bool ikFound = false;
    size_t count = 0;
    while (M4D_WHILE_CONDITION(!ikFound)) {
        ++count_total_;
        // M4D_DEBUG("will shoot using ");
        // space_->printState(state, std::cout);
        if (++count % 100 == 0) {
            M4D_WARN("shot " << count << " random positions without success");
        }
        // shoot degrees of freedom
        DefaultStateSampler::sampleUniform(state);
        // shoot poses
        for (size_t i = 0; i < space->getPosesCount(); ++i) {
            if (space->getPoseComponent(i).method ==
                PoseStateSpace::PoseComponent::Method::IkOnly) {
                poseSamplers_[i]->sampleUniform(x->poses[i]);
            }
        }
        // M4D_DEBUG("shot ");
        // space_->printState(state, std::cout);
        // compute kinematics
        x->flags = 0;
        ikFound = space_->as<PoseStateSpace>()->computeK(x);
        // M4D_DEBUG("ik " << ikFound);
        // space_->printState(state, std::cout);
        assert(!ikFound || space->satisfiesBounds(x));
    }
    if (ikFound) {
        ++count_success_;
    }
    M4D_TRACE("KinematicValidStateSampler::sampleUniform(): found at shot #" << count);
    M4D_INFO("KinematicValidStateSampler success rate = "
             << count_success_ << "/" << count_total_ << " = "
             << (double)(count_success_) / count_total_);
    Ensures(space->satisfiesBounds(x) || move4d::isStopping());
}

void KinematicValidStateSampler::sampleUniformNear(::ompl::base::State *state,
                                                   const ::ompl::base::State *near, double distance)
{
    auto x = state->as<PoseStateSpace::StateType>();
    auto x_near = near->as<PoseStateSpace::StateType>();
    auto space = space_->as<PoseStateSpace>();
    bool ikFound = false;
    size_t count = 0;
    while (M4D_WHILE_CONDITION(!ikFound)) {
        if (++count % 100 == 0) {
            M4D_WARN("shot " << count << " random positions without success");
        }
        // shoot degrees of freedom
        DefaultStateSampler::sampleUniformNear(state, near, distance);
        // shoot poses
        for (size_t i = 0; i < space->getPosesCount(); ++i) {
            if (space->getPoseComponent(i).method ==
                PoseStateSpace::PoseComponent::Method::IkOnly) {
                poseSamplers_[i]->sampleUniformNear(x->poses[i], x_near->poses[i], distance);
            }
        }
        // M4D_DEBUG("shot ");
        // space_->printState(state, std::cout);
        // compute kinematics
        x->flags = 0;
        ikFound = space_->as<PoseStateSpace>()->computeK(x);
        // M4D_DEBUG("ik " << ikFound);
        // space_->printState(state, std::cout);
        assert(!ikFound || space->satisfiesBounds(x));
    }
    Ensures(space->satisfiesBounds(x) || move4d::isStopping());
}

void KinematicValidStateSampler::sampleGaussian(::ompl::base::State *state,
                                                const ::ompl::base::State *mean, double stdDev)
{
    auto x = state->as<PoseStateSpace::StateType>();
    auto x_mean = mean->as<PoseStateSpace::StateType>();
    auto space = space_->as<PoseStateSpace>();
    bool ikFound = false;
    size_t count = 0;
    while (M4D_WHILE_CONDITION(!ikFound)) {
        // M4D_DEBUG("will shoot using ");
        // space_->printState(state, std::cout);

        if (++count % 100 == 0) {
            M4D_WARN("shot " << count << " random positions without success");
        }
        // shoot degrees of freedom
        DefaultStateSampler::sampleGaussian(state, mean, stdDev);
        // shoot poses
        for (size_t i = 0; i < space->getPosesCount(); ++i) {
            if (space->getPoseComponent(i).method ==
                PoseStateSpace::PoseComponent::Method::IkOnly) {
                poseSamplers_[i]->sampleGaussian(x->poses[i], x_mean->poses[i], stdDev);
            }
        }
        // M4D_DEBUG("shot ");
        // space_->printState(state, std::cout);
        // compute kinematics
        x->flags = 0;
        ikFound = space_->as<PoseStateSpace>()->computeK(x);
        // M4D_DEBUG("ik " << ikFound);
        // space_->printState(state, std::cout);
        assert(!ikFound || space->satisfiesBounds(x));
    }
    Ensures(space->satisfiesBounds(x) || move4d::isStopping());
}

KinematicStateSampler::KinematicStateSampler(const PoseStateSpace *space)
    : DefaultStateSampler(space)
{
    for (size_t i = 0; i < space->getPosesCount(); ++i) {
        poseSamplers_.push_back(space->getPoseComponent(i).stateSpace->allocStateSampler());
    }
}

void KinematicStateSampler::sampleUniform(::ompl::base::State *state)
{
    auto x = state->as<PoseStateSpace::StateType>();
    auto space = space_->as<PoseStateSpace>();
    bool ikFound = false;
    // M4D_DEBUG("will shoot using ");
    // space_->printState(state, std::cout);
    // shoot degrees of freedom
    DefaultStateSampler::sampleUniform(state);
    // shoot poses
    for (size_t i = 0; i < space->getPosesCount(); ++i) {
        if (space->getPoseComponent(i).method == PoseStateSpace::PoseComponent::Method::IkOnly) {
            poseSamplers_[i]->sampleUniform(x->poses[i]);
        }
    }
    // M4D_DEBUG("shot ");
    // space_->printState(state, std::cout);
    // compute kinematics
    x->flags = 0;
    ikFound = space_->as<PoseStateSpace>()->computeK(x);
    // M4D_DEBUG("ik " << ikFound);
    // space_->printState(state, std::cout);
    assert(!ikFound || space->satisfiesBounds(x));
    // Ensures(space->satisfiesBounds(x) || move4d::isStopping());
}

void KinematicStateSampler::sampleUniformNear(::ompl::base::State *state,
                                              const ::ompl::base::State *near, double distance)
{
    auto x = state->as<PoseStateSpace::StateType>();
    auto x_near = near->as<PoseStateSpace::StateType>();
    auto space = space_->as<PoseStateSpace>();
    bool ikFound = false;
    // shoot degrees of freedom
    DefaultStateSampler::sampleUniformNear(state, near, distance);
    // shoot poses
    for (size_t i = 0; i < space->getPosesCount(); ++i) {
        if (space->getPoseComponent(i).method == PoseStateSpace::PoseComponent::Method::IkOnly) {
            poseSamplers_[i]->sampleUniformNear(x->poses[i], x_near->poses[i], distance);
        }
    }
    // compute kinematics
    x->flags = 0;
    ikFound = space_->as<PoseStateSpace>()->computeK(x);
    assert(!ikFound || space->satisfiesBounds(x));
    // Ensures(space->satisfiesBounds(x) || move4d::isStopping());
}

void KinematicStateSampler::sampleGaussian(::ompl::base::State *state,
                                           const ::ompl::base::State *mean, double stdDev)
{
    auto x = state->as<PoseStateSpace::StateType>();
    auto x_mean = mean->as<PoseStateSpace::StateType>();
    auto space = space_->as<PoseStateSpace>();
    bool ikFound = false;
    // shoot degrees of freedom
    DefaultStateSampler::sampleGaussian(state, mean, stdDev);
    // shoot poses
    for (size_t i = 0; i < space->getPosesCount(); ++i) {
        if (space->getPoseComponent(i).method == PoseStateSpace::PoseComponent::Method::IkOnly) {
            poseSamplers_[i]->sampleGaussian(x->poses[i], x_mean->poses[i], stdDev);
        }
    }
    // compute kinematics
    x->flags = 0;
    ikFound = space_->as<PoseStateSpace>()->computeK(x);
    assert(!ikFound || space->satisfiesBounds(x));
    // Ensures(space->satisfiesBounds(x) || move4d::isStopping());
}

} // namespace ompl
} // namespace plan
} // namespace move4d

//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//

#include "move4d/common.h"
#include <atomic>

namespace move4d
{

INIT_MOVE4D_LOG("move4d.stop");

static volatile std::atomic<bool> sg_isStopping(false);

void FastStop(const std::string &reason) noexcept
{
    try {
        M4D_FATAL("FATAL, terminating: " << reason);
    } catch (...) {
        try {
            std::cerr << "FATAL, terminating: " << reason << std::endl;
            std::cout.flush();
        } catch (...) {
            try {
                std::cerr.flush();
                std::cout.flush();
            } catch (...) {
                // do.not.throw.
            }
        }
    }
    std::terminate();
}

void Stop(const std::string &reason)
{
    if (isStopping())
        return;
    M4D_INFO("stop requested: " << reason);
    sg_isStopping.store(true);
}

bool isStopping() noexcept { return m4d_unlikely(sg_isStopping.load()); }

} // namespace move4d

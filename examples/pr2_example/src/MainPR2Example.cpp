#include <QApplication>
#include <move4d-soqt/Application.h>
#include <move4d/common.h>
#include <move4d/file/m4df/M4DFDefinition.h> // M4DFDefinition
#include <move4d/motion/GeometricPath.h>
#include <move4d/plan/SearchSpace.h> // SearchSpace
#include <move4d/view/Viewer.h>

/**
 * @file MainPR2Example.cpp
 * @brief example using the Pr2
 *
 * @see The example is documented in \ref pr2_example/src/MainPR2Example.cpp
 */

class Application
{
  public:
    Application(int argc, char *argv[]);
    void start();

  protected:
    void run();
    std::unique_ptr<move4d::SoQt::Application> m_app;

  private:
    bool m_running;
};

Application::Application(int argc, char *argv[])
{

    auto cli_desc = move4d::SoQt::Application::defaultCliDescritpions();
    boost::program_options::variables_map cli_options;
    boost::program_options::store(boost::program_options::parse_command_line(argc, argv, cli_desc),
                                  cli_options);

    m_app = std::make_unique<move4d::SoQt::Application>(cli_options,
                                                        std::bind(&Application::run, this));
}

void Application::start() { this->m_app->start(); }

void Application::run()
{
    using namespace move4d;
    m_running = true;

    auto ss = m_app->getM4DFDefinition()->searchSpace("hand_in_hand");
    if (!ss) {
        return;
    }

    mdl::WorldState ws(m_app->getScene().get());

    motion::GeometricPath path(ss->cloneSearchSpace());
    // shoot waypoints
    for (size_t i = 0; i < 10; ++i) {
        ss->shootUniform(ws);
        path.append(ss->getFrom(ws));
    }

    path.interpolate(ws);
    while (M4D_WHILE_CONDITION(true)) {
        m_app->getViewer()->playPath(ws, path);
        std::this_thread::sleep_for(std::chrono::milliseconds(500));
    }
}

int main(int argc, char *argv[])
{
    Application app(argc, argv);
    app.start();
    return 0;
}

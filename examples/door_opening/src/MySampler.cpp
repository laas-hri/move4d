#include "MySampler.h"
#include <move4d/mdl/DegreeOfFreedomInterface.h>
#include <move4d/plan/SearchSpace.h>
#include <move4d/plan/ompl/converters/StateSpaceFactory.h>
#include <move4d/utils/random.h>
#include <ompl/base/spaces/ReedsSheppStateSpace.h>

namespace move4d
{
using namespace plan::ompl;
INIT_MOVE4D_STATIC_LOGGER(SampleKukaBaseFromEef, "move4d.demo.pianomover.samplekukabasefromeef");

SampleKukaBaseFromEef::SampleKukaBaseFromEef(std::shared_ptr<const plan::SearchSpace> space,
                                             math::Transform base_to_arm_tf, Real arm_reach,
                                             size_t first_dof)
    : plan::BaseSampler(std::move(space)), arm_reach_(arm_reach), inner_radius_(0),
      base_to_arm_tf_(std::move(base_to_arm_tf)), first_dof(first_dof)
{
}

void SampleKukaBaseFromEef::sampleUniform(views::DofsView &conf_in_out,
                                          const math::Transform &base_pose,
                                          math::Transform &end_pose_out,
                                          const std::vector<math::Transform> &input_poses)
{
    M4D_TRACE("SampleKukaBaseFromEef::sampleUniform");
    assert(conf_in_out.size() == 3);
    Real arm_base_height = base_to_arm_tf_.translation()[2];
    const auto &eef_pose = input_poses[0];
    Real dh = std::abs(eef_pose.translation()[2] - arm_base_height);
    M4D_TRACE("eef_pose =\n" << eef_pose.matrix());
    if (dh <= arm_reach_) {
        Real radius = std::sqrt(arm_reach_ * arm_reach_ - dh * dh);
        math::Vector2 pos_in_disc;
        if (inner_radius_ <= 0) {
            pos_in_disc = move4d::random::instance().uniform_disc(radius);
        } else {
            pos_in_disc = move4d::random::instance().uniform_annulus(inner_radius_, radius);
        }
        math::Vector3 arm_base_pos{pos_in_disc[0] + eef_pose.translation()[0],
                                   pos_in_disc[1] + eef_pose.translation()[1], arm_base_height};
        _space->getDegreesOfFreedom()[first_dof + 0].clip(arm_base_pos[0]);
        _space->getDegreesOfFreedom()[first_dof + 1].clip(arm_base_pos[1]);
        Real angle = move4d::random::instance().uniform(-M_PI, M_PI);
        _space->getDegreesOfFreedom()[first_dof + 2].clip(angle);

        // base pose:
        math::Transform base_pose_abs =
            math::Translation(arm_base_pos) * math::Translation(0., 0., -arm_base_height) *
            math::Quaternion::AngleAxisType(angle, math::Vector3::UnitZ()) *
            math::Translation(-base_to_arm_tf_.translation()[0], -base_to_arm_tf_.translation()[1],
                              0.);
        math::Transform base_pose_local = base_pose.inverse() * base_pose_abs;
        Eigen::AngleAxis<Real> aa;
        aa = base_pose_local.rotation();
        // assert(aa.angle() >= 0 && aa.angle() <= M_PI); // expected from eigen doc -> fails...
        Real sign = ((aa.axis() - math::Vector3::UnitZ()).norm() < 1e-6 ? 1 : -1);
        assert((aa.axis() * sign - math::Vector3::UnitZ()).norm() < 1e-6);
        Real angle_new = aa.angle() * sign;
        while (angle_new > M_PI) {
            angle_new -= M_PI * 2;
        }
        while (angle_new < -M_PI) {
            angle_new += M_PI * 2;
        }
        assert(angle_new >= -M_PI && angle_new <= M_PI);

        conf_in_out[first_dof + 0] = base_pose_local(0, 3);
        conf_in_out[first_dof + 1] = base_pose_local(1, 3);
        conf_in_out[first_dof + 2] = angle_new;

        M4D_TRACE("setting conf to : " << base_pose_local(0, 3) << " " << base_pose_local(1, 3)
                                       << " " << angle_new);
        M4D_TRACE("base_pose_local =\n" << base_pose_local.matrix());
        M4D_TRACE("base_pose =\n" << base_pose.matrix());
        M4D_TRACE("rotation axis = " << aa.axis().transpose() << " angle = " << aa.angle());

        end_pose_out = base_pose_abs * base_to_arm_tf_;
        assert((end_pose_out.translation() - arm_base_pos).norm() < 1e-6);
        M4D_TRACE("base_pose_abs = \n" << base_pose_abs.matrix());
        M4D_TRACE("conf_in_out = \n" << conf_in_out);
    } else {
        // the handle cannot be reached
        M4D_TRACE("Handle unreachable, handle height=" << eef_pose.translation()[2]
                                                       << " arm base height=" << arm_base_height
                                                       << " arm reach=" << arm_reach_);
        // throw std::runtime_error("unreachable handle");
        return;
    }
}

void SampleKukaBaseFromEef::sampleUniformNear(views::DofsView &conf_in_out,
                                              const math::Transform &base_pose,
                                              math::Transform &end_pose_out,
                                              const std::vector<math::Transform> &input_poses,
                                              views::DofsView &near, Real distance)
{
    assert(false && "not implemented");
}

void SampleKukaBaseFromEef::sampleGaussian(views::DofsView &conf_in_out,
                                           const math::Transform &base_pose,
                                           math::Transform &end_pose_out,
                                           const std::vector<math::Transform> &input_poses,
                                           views::DofsView &mean, Real stdev)
{
    assert(false && "not implemented");
}

plan::SamplerAllocator SampleKukaBaseFromEef::make_allocator(math::Transform base_to_arm_tf,
                                                             Real arm_reach)
{
    return [base_to_arm_tf, arm_reach](auto space_ptr) {
        return std::make_shared<SampleKukaBaseFromEef>(std::move(space_ptr), base_to_arm_tf,
                                                       arm_reach, 0);
    };
}

} // namespace move4d

Move4d
======

Move4d is a library for motion planning focused on Human-Robot Interaction (HRI) applications.

Move4d is available on [Gitlab.com](https://gitlab.com/laas-hri/move4d). You can read the online documentation [here][doc]

Installation
------------

See [Installation Instructions](@ref Installation) (or [INSTALL.md](./INSTALL.md)).

Examples
--------

The [examples/](examples/) folder contains some examples. They are documented with Doxygen (see [online documentation][doc])

[doc]: https://laas-hri.gitlab.io/move4d/

//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_MDL_KIN_KINEMATICSOLVER_H
#define MOVE4D_MDL_KIN_KINEMATICSOLVER_H

#include "move4d/Factory.h"
#include "move4d/math/Vector.h"
#include "move4d/mdl/Types.h"

namespace move4d
{
namespace kin
{

class KinematicSolver;
using KinematicSolverPtr = std::shared_ptr<KinematicSolver>;

using KinematicSolverFactory = SimpleFactory<KinematicSolverPtr()>;

/**
 * @brief Inherit from this class to implement other kinematic solvers
 *
 * rembember to register them to the KinematicSolverFactory
 */
class KinematicSolver
{
  public:
    virtual bool computeInverseKinematic(const mdl::SceneObject &object, const mdl::Chain &chain,
                                         const math::Transform &pose, const mdl::State &state,
                                         math::Vector &x_in_out);

    /// version with specifying base and end poses
    virtual bool computeInverseKinematic(const mdl::Chain &chain, const math::Transform &base_pose,
                                         const math::Transform &end_pose,
                                         math::Vector &x_in_out) = 0;

    virtual bool computeForwardKinematic(const mdl::SceneObject &object, const mdl::Chain &chain,
                                         const mdl::State &state, const math::Vector x_in,
                                         math::Transform &pose_out);

    virtual bool computeForwardKinematic(const mdl::Chain &chain, const math::Transform &base_pose,
                                         math::Transform &end_pose_out,
                                         const math::Vector &x_in) = 0;
};

} // namespace kin
} // namespace move4d

#endif // MOVE4D_MDL_KIN_KINEMATICSOLVER_H

//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#include "move4d/plan/ClosedLoopSearchSpace.h"
#include "move4d/common.h"
#include "move4d/plan/RobotSpace.h"

namespace move4d
{
namespace plan
{

ClosedLoopSearchSpace::ClosedLoopSearchSpace(std::vector<RobotKinematicGroupSpacePtr> ik_chains,
                                             std::vector<math::Transform> eef_to_target_transforms,
                                             math::Vector3Bounds pose_bounds, std::string name)
    : SearchSpace(Type::ClosedLoop, std::move(name)), _ik_chains(std::move(ik_chains)), _fk_chain(),
      _pose_bounds(std::move(pose_bounds)), _eef_to_target_tfs(std::move(eef_to_target_transforms)),
      _fk_chain_eef_to_target_tf()
{
    Expects((_pose_bounds.high - _pose_bounds.low).squaredNorm() > 1e-6);
    Expects(!_ik_chains.empty());
    Expects(_ik_chains.size() == _eef_to_target_tfs.size());
    Expects([](auto &tfs) {
        for (auto &tf : tfs)
            if (std::abs(tf.matrix().determinant()) < 1e-10)
                return false;
        return true;
    }(_eef_to_target_tfs));
    _initDegreeOfFreedomList();
    Ensures(!_fk_chain);
}

ClosedLoopSearchSpace::ClosedLoopSearchSpace(RobotKinematicGroupSpacePtr fk_chain,
                                             math::Transform eef_to_target,
                                             std::vector<RobotKinematicGroupSpacePtr> ik_chains,
                                             std::vector<math::Transform> eef_to_target_transforms,
                                             std::string name)
    : SearchSpace(Type::ClosedLoop, std::move(name)), _ik_chains(std::move(ik_chains)),
      _fk_chain(std::move(fk_chain)), _pose_bounds(),
      _eef_to_target_tfs(std::move(eef_to_target_transforms)),
      _fk_chain_eef_to_target_tf(std::move(eef_to_target))
{
    Expects(_fk_chain);
    Expects(!_ik_chains.empty());
    Expects(_ik_chains.size() == _eef_to_target_tfs.size());
    Expects(std::abs(_fk_chain_eef_to_target_tf.matrix().determinant()) >
            1.e-10); // determinant != 0 <=> inversible
    Expects([](auto &tfs) {
        for (auto &tf : tfs)
            if (std::abs(tf.matrix().determinant()) < 1e-10)
                return false;
        return true;
    }(_eef_to_target_tfs));
    _initDegreeOfFreedomList();
}

void ClosedLoopSearchSpace::shootUniform(mdl::WorldState &ws) const
{
    Expects(_ik_chains.size() == _eef_to_target_tfs.size());
    bool found = false;
    while (M4D_WHILE_CONDITION(!found)) {
        math::Transform target_pose;
        if (_pre) {
            // shoot inside the loop because it can influence success of the following
            _pre->shootUniform(ws);
        }
        if (_fk_chain) {
            math::Transform fk_pose;
            _fk_chain->RobotSpace::shootUniform(ws); // shoot in configuration space
            found = _fk_chain->computeFK(ws, fk_pose);
            if (!found) {
                continue;
            }
            target_pose = fk_pose * _fk_chain_eef_to_target_tf;
        } else {
            math::Vector3 trans = math::shootUniformInRange(_pose_bounds);
            math::Quaternion rot = math::Quaternion::Random();
            target_pose = math::Translation(trans) * rot;
        }

        for (size_t i = 0; i < _ik_chains.size(); ++i) {
            _ik_chains[i]->RobotSpace::shootUniform(ws);
            math::Transform eef_target = target_pose * _eef_to_target_tfs[i].inverse();
            assert((eef_target * _eef_to_target_tfs[i]).isApprox(target_pose));
            if (!_ik_chains[i]->computeIK(ws, eef_target)) {
                found = false;
                break;
            }
        }
    }
    if (found && _post) {
        _post->shootUniform(ws);
    }
}

const std::vector<RobotKinematicGroupSpacePtr> &ClosedLoopSearchSpace::getIkChains() const
{
    return _ik_chains;
}

void ClosedLoopSearchSpace::setIkChains(std::vector<RobotKinematicGroupSpacePtr> ik_chains)
{
    _ik_chains = std::move(ik_chains);
}

const RobotKinematicGroupSpacePtr &ClosedLoopSearchSpace::getFkChain() const { return _fk_chain; }

void ClosedLoopSearchSpace::setFkChain(RobotKinematicGroupSpacePtr fk_chain)
{
    _fk_chain = std::move(fk_chain);
}

const SearchSpacePtr &ClosedLoopSearchSpace::getPre() const { return _pre; }

void ClosedLoopSearchSpace::setPre(SearchSpacePtr pre)
{
    _pre = std::move(pre);
    _initDegreeOfFreedomList();
}

const SearchSpacePtr &ClosedLoopSearchSpace::getPost() const { return _post; }

void ClosedLoopSearchSpace::setPost(SearchSpacePtr post)
{
    _post = std::move(post);
    _initDegreeOfFreedomList();
}

void ClosedLoopSearchSpace::_initDegreeOfFreedomList()
{
    m_degreesOfFreedom.clear();
    if (_pre) {
        m_degreesOfFreedom.insert(m_degreesOfFreedom.end(), _pre->getDegreesOfFreedom().begin(),
                                  _pre->getDegreesOfFreedom().end());
    }
    if (_fk_chain) {
        m_degreesOfFreedom.insert(m_degreesOfFreedom.end(),
                                  _fk_chain->getDegreesOfFreedom().begin(),
                                  _fk_chain->getDegreesOfFreedom().end());
    }
    for (auto &s : _ik_chains) {
        m_degreesOfFreedom.insert(m_degreesOfFreedom.end(), s->getDegreesOfFreedom().begin(),
                                  s->getDegreesOfFreedom().end());
    }
    if (_post) {
        m_degreesOfFreedom.insert(m_degreesOfFreedom.end(), _post->getDegreesOfFreedom().begin(),
                                  _post->getDegreesOfFreedom().end());
    }
}

} // namespace plan
} // namespace move4d

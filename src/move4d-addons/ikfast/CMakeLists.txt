set (SRC_FILES src/Solver.cpp)
add_library(move4d_ikfast SHARED ${SRC_FILES})
target_link_libraries(move4d_ikfast
    PUBLIC
    ${RL_LIBRARIES}
    move4d
    )

# -Wl: pass option to the linker
# --no-as-needed: force link against the library even if symbols are not used
# then reset to --as-needed for other libraries
## That's because symbols may not directly be used by the project requiring this
## but when the library is linked, it automatically register the factory and is
## then accessible through move4d
list(APPEND ADDON_TARGETS -Wl,--no-as-needed move4d_ikfast -Wl,--as-needed)
set(ADDON_TARGETS ${ADDON_TARGETS} PARENT_SCOPE)

install(TARGETS
    move4d_ikfast
    DESTINATION lib
    EXPORT ${PROJECT_NAME}-targets)

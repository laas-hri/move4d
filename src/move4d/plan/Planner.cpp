//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//

#include "move4d/plan/Planner.h"

#include "move4d/plan/Goal.h"
#include "move4d/plan/SearchSpace.h"
#include <utility>

namespace move4d
{

namespace plan
{

Planner::Planner(std::shared_ptr<SearchSpace> space, mdl::CollisionManagerPtr collisionManager)
    : m_collisionManager(std::move(collisionManager)), m_searchSpace(std::move(space))
{
}

const std::shared_ptr<const mdl::WorldState> &Planner::getStart() const { return m_start; }

void Planner::setStart(std::shared_ptr<const mdl::WorldState> start) { m_start = std::move(start); }

void Planner::setStart(const mdl::WorldState &start)
{
    m_start = std::make_shared<const mdl::WorldState>(start);
}

const std::shared_ptr<const Goal> &Planner::getGoal() const { return m_goal; }

void Planner::setGoal(std::shared_ptr<const Goal> goal) { m_goal = std::move(goal); }

} // namespace plan

} // namespace move4d

//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#include "move4d/io/Logger.h"
#ifndef MOVE4D_LOG_OFF
#include <log4cxx/basicconfigurator.h>
#include <log4cxx/config/propertysetter.h>
#include <log4cxx/consoleappender.h>
#include <log4cxx/defaultconfigurator.h>
#include <log4cxx/helpers/exception.h>
#include <log4cxx/mdc.h>
#include <log4cxx/ndc.h>
#include <log4cxx/patternlayout.h>
#include <log4cxx/propertyconfigurator.h>
#endif
#include "move4d/Singleton.h"
#include <cstdlib>
#include <iostream>
#include <stdio.h>

namespace move4d
{
namespace log
{
INIT_MOVE4D_LOG("move4d.logger");

struct DefaultOutputHandler : public Singleton<DefaultOutputHandler> {
    DefaultOutputHandler() = default;

  public:
    OutputHandler *get();
    void set(OutputHandler *oh);

    bool isEnabledFor(LogLevel, LoggerPtr logger);
    void log(LogLevel lvl, LoggerPtr logger, const std::string &message);

  private:
    OutputHandler *_output_handler = nullptr;
    std::mutex _lock;
};

#ifndef MOVE4D_LOG_OFF

bool autoConfigureFromFile()
{
    bool ok = false;
    std::string m4dhome, def_file("logm4d.properties"), def_cxx_file("log4cxx.properties");
    std::string filename;

    char *g = getenv("HOME_MOVE4D");
    if (g) {
        m4dhome = g;
        m4dhome += "/";
    }
    FILE *f;
    enum STATE { DEF_VAR_M = 0, DEF_VAR_C, DEF_M, DEF_C, H_DEF_M, H_DEF_C, DEF_SIZE };
    STATE i = STATE(0);
    while (!ok && i < DEF_SIZE) {

        f = NULL;
        filename.clear();
        switch (i) {
        case DEF_VAR_M:
            g = getenv("LOGM4D_CONFIGURATION");
            if (g)
                filename = g;
            break;
        case DEF_VAR_C:
            g = getenv("LOG4CXX_CONFIGURATION");
            if (g)
                filename = g;
            break;
        case DEF_M:
            filename = def_file;
            break;
        case DEF_C:
            filename = def_cxx_file;
            break;
        case H_DEF_M:
            filename = m4dhome;
            if (filename.size())
                filename.append(def_file);
            break;
        case H_DEF_C:
            filename = m4dhome;
            if (filename.size())
                filename.append(def_cxx_file);
            break;
        default:
            filename.clear();
        }

        if (filename.size()) {
            f = fopen(filename.c_str(), "r");
        }
        if (f) {
            fclose(f);
            ok = true;
            break;
        }
        i = (STATE)((int)i + 1);
    }
    if (ok) {
        log4cxx::PropertyConfigurator::configure(filename);
        M4D_INFO("using log4cxx configuration file " << filename);

        using namespace std;
        for (unsigned int i = 0; i < log4cxx::Logger::getRootLogger()->getAllAppenders().size();
             i++) {
            log4cxx::AppenderPtr ap = log4cxx::Logger::getRootLogger()->getAllAppenders()[i];
            ap->setOption("application", "move4d");
        }
    }

    return ok;
}
#endif

void initializeLogger()
{
    static bool logger_init_ok = false;
    if (logger_init_ok)
        return;
    logger_init_ok = true;

#ifdef MOVE4D_LOG_OFF
    return;
#else
    setOutputHandler(new OutputHandlerLog4cxx());
    bool ok = false;
    log4cxx::NDC::push("planners");
    if (log4cxx::Logger::getRootLogger()->getAllAppenders().size() == 0) {
        ok = autoConfigureFromFile();
    }
    if (!ok) {
        log4cxx::ConsoleAppender *console = new log4cxx::ConsoleAppender(log4cxx::LayoutPtr(
            new log4cxx::PatternLayout("%d{HH:mm:ss,SSS} \33[33m%-5p\33[0m %c - %m%n")));
        console->setOption("application", "move4d");
        log4cxx::BasicConfigurator::configure(log4cxx::AppenderPtr(console));
        log4cxx::Logger::getRootLogger()->setLevel(log4cxx::Level::getDebug());
        M4D_INFO("using progammatically defined default logger settings for log4cxx");
    }
#endif
}

LoggerPtr getLogger(const std::string &name)
{
#ifdef MOVE4D_LOG_OFF
    return 0;
#else
    return log4cxx::Logger::getLogger(name);
#endif
}

void log(LogLevel level, LoggerPtr logger, const std::string &message)
{
    DefaultOutputHandler::instance().log(level, logger, message);
}

bool isEnabledFor(LogLevel level, LoggerPtr logger)
{
    return DefaultOutputHandler::instance().isEnabledFor(level, logger);
}

log4cxx::LevelPtr OutputHandlerLog4cxx::getLevel(LogLevel level)
{
    switch (level) {
    case LogLevel::TRACE:
        return log4cxx::Level::getTrace();
    case LogLevel::DEBUG:
        return log4cxx::Level::getDebug();
    case LogLevel::INFO:
        return log4cxx::Level::getInfo();
    case LogLevel::WARN:
        return log4cxx::Level::getWarn();
    case LogLevel::ERROR:
        return log4cxx::Level::getError();
    case LogLevel::FATAL:
        return log4cxx::Level::getFatal();
    }
}

void OutputHandlerLog4cxx::log(LogLevel level, LoggerPtr logger, const std::string &message)
{
    log4cxx::LoggerPtr logger_obj = log4cxx::Logger::getLogger(logger->getName());

    logger_obj->forcedLog(getLevel(level), message);
}

bool OutputHandlerLog4cxx::isEnabledFor(LogLevel level, LoggerPtr logger)
{
    log4cxx::LoggerPtr logger_obj = log4cxx::Logger::getLogger(logger->getName());
    return logger_obj->isEnabledFor(getLevel(level));
}

OutputHandler *DefaultOutputHandler::get()
{
    std::lock_guard<std::mutex> l(_lock);
    return _output_handler;
}

void DefaultOutputHandler::set(OutputHandler *oh)
{
    std::lock_guard<std::mutex> l(_lock);
    if (_output_handler)
        delete _output_handler;
    _output_handler = oh;
}

bool DefaultOutputHandler::isEnabledFor(LogLevel level, LoggerPtr logger)
{
    std::lock_guard<std::mutex> l(_lock);
    return _output_handler && _output_handler->isEnabledFor(level, logger);
}

void DefaultOutputHandler::log(LogLevel lvl, LoggerPtr logger, const std::string &message)
{
    std::lock_guard<std::mutex> l(_lock);
    if (_output_handler)
        _output_handler->log(lvl, logger, message);
}

void setOutputHandler(OutputHandler *handler) { DefaultOutputHandler::instance().set(handler); }

void noHandler() { DefaultOutputHandler::instance().set(nullptr); }

} // namespace log
} // namespace move4d

#include <log4cxx/helpers/pool.h>
#include <log4cxx/patternlayout.h>
namespace log4cxx
{
class LOG4CXX_EXPORT ColorPatternLayout : public log4cxx::PatternLayout
{
  public:
    DECLARE_LOG4CXX_OBJECT(ColorPatternLayout)
    BEGIN_LOG4CXX_CAST_MAP()
    LOG4CXX_CAST_ENTRY(ColorPatternLayout)
    LOG4CXX_CAST_ENTRY_CHAIN(Layout)
    END_LOG4CXX_CAST_MAP()

    ColorPatternLayout();
    ColorPatternLayout(const log4cxx::LogString &s);
    virtual void format(log4cxx::LogString &output, const log4cxx::spi::LoggingEventPtr &event,
                        log4cxx::helpers::Pool &pool) const override;
};
LOG4CXX_PTR_DEF(ColorPatternLayout);
} // namespace log4cxx

using namespace log4cxx;
IMPLEMENT_LOG4CXX_OBJECT(ColorPatternLayout);
ColorPatternLayout::ColorPatternLayout() : log4cxx::PatternLayout() {}

ColorPatternLayout::ColorPatternLayout(const LogString &s) : log4cxx::PatternLayout(s) {}

void ColorPatternLayout::format(LogString &output, const spi::LoggingEventPtr &event,
                                helpers::Pool &pool) const
{
    log4cxx::LogString tmp;
    log4cxx::PatternLayout::format(tmp, event, pool);
    log4cxx::LevelPtr lvl = event->getLevel();
    switch (lvl->toInt()) {
    case log4cxx::Level::FATAL_INT:
        output.append("\u001b[0;41m"); // red BG
        break;
    case log4cxx::Level::ERROR_INT:
        output.append("\u001b[0;31m"); // red FG
        break;
    case log4cxx::Level::WARN_INT:
        output.append("\u001b[0;33m"); // Yellow FG
        break;
    case log4cxx::Level::INFO_INT:
        output.append("\u001b[1m"); // Bright
        break;
    case log4cxx::Level::DEBUG_INT:
        output.append("\u001b[2;32m"); // Green FG
        break;
    case log4cxx::Level::TRACE_INT:
        output.append("\u001b[0;30m"); // Black FG
        break;
    default:
        break;
    }
    output.append(tmp);
    output.append("\u001b[m");
}

//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//

#include "move4d-test/Main.h"
#include "move4d/mdl/Manipulable.h"
#include "move4d/mdl/Model.h"
#include "move4d/mdl/ObjectFactory.h"
#include "move4d/mdl/Scene.h"

#include <Inventor/VRMLnodes/SoVRMLCylinder.h>
#include <Inventor/VRMLnodes/SoVRMLGroup.h>
#include <rl/sg/Body.h>
#include <rl/sg/bullet/Scene.h>
int main(int argc, char *argv[])
{
    Move4dTestApplication app;
    app.run();
    return 0;
}

bool Move4dTestApplication::run()
{
    using namespace std::placeholders;
    using namespace std;

    auto sceneGraph = new ::rl::sg::bullet::Scene();
    m_scene = new move4d::mdl::Scene(sceneGraph);

    std::cout << "object factory creators: ";
    auto vv = move4d::mdl::ObjectFactory::instance().getTypes();
    for (auto &v : vv) {
        std::cout << v << " ";
    }
    std::cout << std::endl;

    // init //
    ::SoDB::init();
    auto root = new ::SoVRMLGroup();

    auto o1 = m_scene->create("Manipulable", "object");
    cout << o1->getName() << " " << o1->getId() << " " << o1->m_bodies << endl;

    auto *shape1 = new ::SoVRMLShape();
    auto *geom = new ::SoVRMLCylinder();
    geom->radius = 10.;
    geom->height = 100.;
    shape1->geometry = geom;

    auto body1 = o1->m_bodies->create();
    body1->create(shape1);

    // --------------//
    // second object //
    // --------------//
    auto o2 = m_scene->create("Manipulable", "object");
    cout << o2->getName() << " " << o2->getId() << " " << o2->m_bodies << endl;

    auto *shape2 = new ::SoVRMLShape();
    auto *geom2 = new ::SoVRMLCylinder();
    geom2->radius = 10.;
    geom2->height = 100.;
    shape2->geometry = geom2;

    auto body2 = o2->m_bodies->create();
    body2->create(shape2);
    ::rl::math::Transform pos2;
    pos2.translate(::rl::math::Vector3(1000, 0, 0));
    body2->setFrame(pos2);

    cout << "collision=" << sceneGraph->isColliding() << endl;

    return true;
}

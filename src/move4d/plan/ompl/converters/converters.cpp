//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#include "move4d/plan/ompl/converters/converters.h"
#include "move4d/motion/GeometricPath.h"
#include "move4d/plan/ompl/converters/StateSpaceFactory.h"
#include <ompl/geometric/PathGeometric.h>

namespace move4d
{
namespace plan
{
namespace ompl
{
namespace converters
{

math::Transform toTransform(const ::ompl::base::SE3StateSpace::StateType *se3)
{
    math::Transform pose;
    auto &rot = se3->rotation();
    pose = math::Translation(se3->getX(), se3->getY(), se3->getZ());
    pose *= math::Quaternion(rot.w, rot.x, rot.y, rot.z);
    return pose;
}

void toState(const math::Transform &transform, ::ompl::base::SE3StateSpace::StateType *se3state)
{
    Expects(se3state);
    auto &rot = se3state->rotation();
    se3state->setX(transform.translation().x());
    se3state->setY(transform.translation().y());
    se3state->setZ(transform.translation().z());

    math::Quaternion q(transform.rotation());

    rot.w = q.w();
    rot.x = q.x();
    rot.y = q.y();
    rot.z = q.z();
}

std::unique_ptr<::ompl::geometric::PathGeometric>
geometricPath(const motion::GeometricPath &path_in, const StateSpaceWrapper &wrapper,
              const ::ompl::base::SpaceInformationPtr &si)
{
    auto &space = *si->getStateSpace();
    auto path_out = std::make_unique<::ompl::geometric::PathGeometric>(si);
    auto &path = *path_out;

    for (const auto &x : path_in.values()) {
        auto scoped_state = wrapper.stateFromValues(x);
        auto *state = space.cloneState(scoped_state.get());
        path.append(state);
    }

    return path_out;
}

} // namespace converters
} // namespace ompl
} // namespace plan
} // namespace move4d

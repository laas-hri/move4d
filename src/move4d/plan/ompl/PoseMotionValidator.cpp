//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#include "move4d/plan/ompl/PoseMotionValidator.h"
#include "move4d/utils/io/print_to_stream.hpp"
#include "move4d/view/Viewer.h"
#include <ompl/base/SpaceInformation.h>

#ifndef NDEBUG
#include "move4d/plan/ompl/StateValidityChecker.h"
#endif

namespace move4d
{
namespace plan
{
namespace ompl
{

INIT_MOVE4D_STATIC_LOGGER(PoseMotionValidator, "move4d.plan.ompl.posemotionvalidator");

PoseMotionValidator::PoseMotionValidator(::ompl::base::SpaceInformation *si)
    : ::ompl::base::MotionValidator(si)
{
}

PoseMotionValidator::PoseMotionValidator(const ::ompl::base::SpaceInformationPtr &si)
    : ::ompl::base::MotionValidator(si)
{
}

bool move4d::plan::ompl::PoseMotionValidator::checkMotion(const ::ompl::base::State *s1,
                                                          const ::ompl::base::State *s2) const
{
    // Expects(si_->isValid(s1));
    // assume motion starts in a valid configuration state, so s1 is valid (see Expects)
    assert(dynamic_cast<move4d::plan::ompl::StateValidityChecker *>(
        si_->getStateValidityChecker().get()));
    M4D_TRACE("PoseMotionValidator::checkMotion");
    M4D_TRACE("from\n" << PRINT_TO_STREAM(si_->getStateSpace()->printState(s1, oss)));
    M4D_TRACE("to\n" << PRINT_TO_STREAM(si_->getStateSpace()->printState(s2, oss)));
    if (!si_->isValid(s1)) {
        M4D_TRACE("invalid start state");
        ++invalid_;
        return false;
    }
    if (!si_->isValid(s2)) {
        M4D_TRACE("invalid target state");
        ++invalid_;
        return false;
    }

    bool result = true;
    auto stateSpace = si_->getStateSpace();
    M4D_TRACE("si longestValidSegmentLength"
              << si_->getStateSpace()->getLongestValidSegmentLength());
    uint nd = stateSpace->validSegmentCount(s1, s2);
    M4D_TRACE("will test with " << nd << " samples on a distance of "
                                << stateSpace->distance(s1, s2));
    if (nd >= 2) {
        ::ompl::base::State *test = si_->allocState();
        ::ompl::base::State *prev = si_->allocState();
        si_->copyState(prev, s1);
        for (uint i = 1; i < nd; ++i) {
            stateSpace->interpolate(s1, s2, Real(i) / Real(nd), test);
            // TODO(Jules): "jump" test in PoseMotionValidator, enable or remove code
            auto dist = si_->distance(prev, test);
            if (dist > maxJump_) {
                std::cout << "jump : " << dist << std::endl;
            }
            if (!si_->isValid(test) || dist > maxJump_) {
                result = false;
                break;
            }
            // need to copy, not swap, to properly seed the IK solver in interpolate:
            si_->copyState(prev, test);
        }
        si_->freeState(test);
    }
    if (result) {
        M4D_TRACE("checkMotion: valid");
        ++valid_;
    } else {
        M4D_TRACE("checkMotion: invalid");
        ++invalid_;
    }

    return result;
}

bool move4d::plan::ompl::PoseMotionValidator::checkMotion(
    const ::ompl::base::State *s1, const ::ompl::base::State *s2,
    std::pair<::ompl::base::State *, double> &lastValid) const
{
    Expects(si_->isValid(s1));
    // assume motion starts in a valid configuration state, so s1 is valid (see Expects)
    assert(dynamic_cast<move4d::plan::ompl::StateValidityChecker *>(
        si_->getStateValidityChecker().get()));
    M4D_TRACE("PoseMotionValidator::checkMotion with last valid");

    bool result = true;
    auto stateSpace = si_->getStateSpace();
    uint nd = stateSpace->validSegmentCount(s1, s2);
    M4D_TRACE("will test with " << nd << " samples on a distance of "
                                << stateSpace->distance(s1, s2));
    if (nd >= 2) {
        ::ompl::base::State *test = si_->allocState();
        for (uint i = 1; i < nd; ++i) {
            stateSpace->interpolate(s1, s2, Real(i) / Real(nd), test);
            if (!si_->isValid(test)) {
                lastValid.second = Real(i - 1) / Real(nd);
                if (lastValid.first != nullptr) {
                    stateSpace->interpolate(s1, s2, lastValid.second, lastValid.first);
                }
                result = false;
                break;
            }
        }
        si_->freeState(test);
    }

    if (result) {
        if (!si_->isValid(s2)) {
            lastValid.second = Real(nd - 1) / Real(nd);
            if (lastValid.first != nullptr) {
                stateSpace->interpolate(s1, s2, lastValid.second, lastValid.first);
            }
            result = false;
        }
    }
    if (result) {
        M4D_TRACE("checkMotion: valid");
        ++valid_;
    } else {
        M4D_TRACE("checkMotion: invalid");
        ++invalid_;
    }

    return result;
}

} // namespace ompl
} // namespace plan
} // namespace move4d

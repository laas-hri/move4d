//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_PLAN_GOALLIST_H
#define MOVE4D_PLAN_GOALLIST_H

#include "move4d/plan/Goal.h"
#include <memory>
#include <vector>

namespace move4d
{

namespace plan
{

/**
 * An abstract base class for goals being actually a set of goals
 */
class GoalList : public Goal
{
  protected:
    std::vector<std::shared_ptr<Goal>> m_goals;

  public:
    inline const std::vector<std::shared_ptr<Goal>> &getGoals() const { return m_goals; }
    inline std::vector<std::shared_ptr<Goal>> &getGoals() { return m_goals; }

    void setGoals(std::vector<std::shared_ptr<Goal>> value);
};

} // namespace plan

} // namespace move4d
#endif

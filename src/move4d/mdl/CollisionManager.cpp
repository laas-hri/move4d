//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//

#include "move4d/mdl/CollisionManager.h"
#include "CollisionManager.h"
#include "move4d/mdl/CollisionWhiteList.h"
#include "move4d/mdl/Model.h"
#include "move4d/mdl/Scene.h"
#include "move4d/mdl/SceneObject.h"
#include "move4d/utils/containers/iterators.h"
#include "rl/plan/SimpleModel.h"
#include "rl/sg/Body.h"
#include "rl/sg/DepthScene.h"
#include "rl/sg/DistanceScene.h"
#include "rl/sg/RaycastScene.h"
#include "rl/sg/SimpleScene.h"

namespace move4d
{

namespace mdl
{

INIT_MOVE4D_STATIC_LOGGER(CollisionManager, "move4d.mdl.collisionmanager");

CollisionManager::CollisionManager(std::shared_ptr<Scene> scene,
                                   std::shared_ptr<rl::sg::SimpleScene> collisionScene,
                                   std::shared_ptr<rl::sg::DistanceScene> distanceScene,
                                   std::shared_ptr<rl::sg::DepthScene> depthScene,
                                   std::shared_ptr<rl::sg::RaycastScene> raycastScene)
    : m_scene(std::move(scene)), m_collisionScene(std::move(collisionScene)),
      m_distanceScene(std::move(distanceScene)), m_depthScene(std::move(depthScene)),
      m_raycastScene(std::move(raycastScene))
{
}

void CollisionManager::applyWorldState(const WorldState &ws) { m_scene->applyWorldState(ws); }

bool CollisionManager::isColliding() const
{
    Expects(m_scene && m_scene->getCollisionWhitelist());
    M4D_TRACE("CollisionManager::isColliding start");
    if (m_scene->getCollisionWhitelist()->size() == 0) {
        M4D_WARN("no collision pairs set in scene collision whitelist. Did you call "
                 "Scene::resetCollisionWhiteList or equivalent?");
    }
    for (auto p : *m_scene->getCollisionWhitelist()) {
        rl::sg::Model *obj1 =
            m_collisionScene->getModel(m_scene->getObject(p.first().sceneObj)->getSceneIndex());
        rl::sg::Body *b1 = obj1->getBody(p.first().bodyIndex);
        rl::sg::Model *obj2 =
            m_collisionScene->getModel(m_scene->getObject(p.second().sceneObj)->getSceneIndex());
        rl::sg::Body *b2 = obj2->getBody(p.second().bodyIndex);
        // M4D_TRACE("check collision between " << obj1->getName() << "." << b1->getName() << " and
        // "
        //                                      << obj2->getName() << "." << b2->getName());
        if (m_collisionScene->areColliding(b1, b2)) {
            // M4D_DEBUG("got collision between " << obj1->getName() << "." << b1->getName() << "
            // and "
            //                                    << obj2->getName() << "." << b2->getName());
            return true;
        }
    }
    return false;
}

bool CollisionManager::areColliding(CollisionManager::object_ref a_,
                                    CollisionManager::object_ref b_) const
{
    Identifier a, b;
    a = a_.getId();
    b = b_.getId();
    if (a > b)
        std::swap(a, b);
    auto range = iterators::make_filter_iterator(
        m_scene->getCollisionWhitelist()->begin(), m_scene->getCollisionWhitelist()->end(),
        [&a, &b](const CollisionPair &p) {
            return p.first().sceneObj == a && p.second().sceneObj == b;
        });

    for (auto &p = range.first; p != range.second; ++p) {
        if (this->areColliding(p->first(), p->second())) {
            return true;
        }
    }
    return false;
}

bool CollisionManager::areColliding(object_ref a, body_ref b) const
{
    // TODO(gbuisan): use the whitelist
    assert(false && "whitelist not implemented");
    for (size_t i = 0; i < a.getNumberOfBodies(); i++) {
        SceneBody body{a.getId(), i};
        if (this->areColliding(body, b)) {
            return true;
        }
    }
    return false;
}

bool CollisionManager::areColliding(body_ref a, body_ref b) const
{
    rl::sg::Body *ab, *bb;
    ab = m_scene->getObject(a.sceneObj)->model()->getBody(a.bodyIndex);
    bb = m_scene->getObject(b.sceneObj)->model()->getBody(b.bodyIndex);

    return m_collisionScene->areColliding(ab, bb);
}

std::vector<CollisionPair> CollisionManager::collisionList() const
{
    Expects(m_scene && m_scene->getCollisionWhitelist());
    if (m_scene->getCollisionWhitelist()->size() == 0) {
        M4D_WARN("no collision pairs set in scene collision whitelist. Did you call "
                 "Scene::resetCollisionWhiteList or equivalent?");
    }
    std::vector<CollisionPair> list;
    list.reserve(m_scene->getCollisionWhitelist()->size() / 4); // 4=just a guess
    for (auto p : *m_scene->getCollisionWhitelist()) {
        rl::sg::Model *obj1 =
            m_collisionScene->getModel(m_scene->getObject(p.first().sceneObj)->getSceneIndex());
        rl::sg::Body *b1 = obj1->getBody(p.first().bodyIndex);
        rl::sg::Model *obj2 =
            m_collisionScene->getModel(m_scene->getObject(p.second().sceneObj)->getSceneIndex());
        rl::sg::Body *b2 = obj2->getBody(p.second().bodyIndex);
        if (m_collisionScene->areColliding(b1, b2)) {
            list.push_back(p);
        }
    }
    list.shrink_to_fit();
    return list;
}

move4d::Real CollisionManager::distance(object_ref a, object_ref b) const
{
    // TODO(gbuisan): fix, see areColliding
    // rl::math::Vector3 p1, p2;
    // return m_distanceScene->distance(&a, &b, p1, p2);
}

move4d::Real CollisionManager::distance(object_ref a, body_ref b) const
{
    // TODO(gbuisan): fix, see areColliding
    // rl::math::Vector3 p1, p2;
    // move4d::Real minDist = std::numeric_limits<move4d::Real>::max();
    // for (size_t i = 0; i < a.getNumBodies(); i++){
    //     rl::sg::Body *ab = a.getBody(i);
    //     move4d::Real d = m_distanceScene->distance(ab, &b, p1, p2);
    //     if ( d < minDist ){
    //         minDist = d;
    //     }
    // }
    // return minDist;
}

move4d::Real CollisionManager::distance(body_ref a, body_ref b) const
{
    // TODO(gbuisan): fix, see areColliding
    // rl::math::Vector3 p1, p2;
    // return m_distanceScene->distance(&a, &b, p1, p2);
}

move4d::Real CollisionManager::depth(object_ref a, object_ref b) const
{
    // TODO(gbuisan): fix, see areColliding
    // rl::math::Vector3 p1, p2;
    // return m_depthScene->depth(&a, &b, p1, p2);
}

move4d::Real CollisionManager::depth(object_ref a, body_ref b) const
{
    // TODO(gbuisan): fix, see areColliding
    // rl::math::Vector3 p1, p2;
    // move4d::Real maxDepth = 0;
    // for (size_t i = 0; i < a.getNumBodies(); i++){
    //     rl::sg::Body *ab = a.getBody(i);
    //     move4d::Real d = m_depthScene->depth(ab, &b, p1, p2);
    //     if ( d > maxDepth ){
    //         maxDepth = d;
    //     }
    // }
    // return maxDepth;
}

move4d::Real CollisionManager::depth(body_ref a, body_ref b) const
{
    // TODO(gbuisan): fix, see areColliding
    // rl::math::Vector3 p1, p2;
    // return m_depthScene->depth(&a, &b, p1, p2);
}

void CollisionManager::updateFrames()
{
    // TODO
}

const std::shared_ptr<Scene> &CollisionManager::getScene() const { return m_scene; }

} // namespace mdl

} // namespace move4d

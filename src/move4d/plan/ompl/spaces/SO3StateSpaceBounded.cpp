//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#include "move4d/plan/ompl/spaces/SO3StateSpaceBounded.h"
#include "move4d/utils/io/print_to_stream.hpp"

namespace move4d
{
namespace plan
{
namespace ompl
{

INIT_MOVE4D_STATIC_LOGGER(SO3StateSpaceBounded, "move4d.plan.ompl.so3statespacebounded");
INIT_MOVE4D_STATIC_LOGGER(SO3StateSpaceBounded::Sampler,
                          "move4d.plan.ompl.so3statespacebounded.sampler");

SO3StateSpaceBounded::SO3StateSpaceBounded(std::vector<Real> reference_quaternion_xyzw,
                                           Real tolerance)
    : _tolerance(std::min(tolerance, M_PI))
{
    assert(_tolerance >= 0 && _tolerance <= M_PI);
    if (tolerance < 0)
        throw std::invalid_argument("SO3StateSpaceBounded tolerance must be >=0");
    _reference.x = reference_quaternion_xyzw[0];
    _reference.y = reference_quaternion_xyzw[1];
    _reference.z = reference_quaternion_xyzw[2];
    _reference.w = reference_quaternion_xyzw[3];
}

SO3StateSpaceBounded::SO3StateSpaceBounded(const math::Quaternion &reference, Real tolerance)
    : _tolerance(std::min(tolerance, M_PI))
{

    assert(_tolerance >= 0 && _tolerance <= M_PI);
    if (tolerance < 0)
        throw std::invalid_argument("SO3StateSpaceBounded tolerance must be >=0");
    _reference.x = reference.x();
    _reference.y = reference.y();
    _reference.z = reference.z();
    _reference.w = reference.w();
}

double SO3StateSpaceBounded::getMaximumExtent() const { return 0.5 * _tolerance; }

double SO3StateSpaceBounded::getMeasure() const { return _tolerance * M_PI; }

void SO3StateSpaceBounded::enforceBounds(::ompl::base::State *state) const
{
    double d = distance(state, &_reference);
    if (d < _tolerance)
        return;
    double t = 1. - _tolerance / d;
    interpolate(state, &_reference, t, state);
    M4D_ASSERT_MSG(satisfiesBounds(state), "stated doesn't satisfies bounds : "
                                               << PRINT_TO_STREAM(this->printState(state, oss))
                                               << " distance = " << distance(&_reference, state)
                                               << " tolerance = " << _tolerance);
    assert(std::abs(distance(&_reference, state) - _tolerance) < 1e-6);
}

bool SO3StateSpaceBounded::satisfiesBounds(const ::ompl::base::State *state) const
{
    return distance(&_reference, state) < _tolerance;
}

::ompl::base::StateSamplerPtr SO3StateSpaceBounded::allocDefaultStateSampler() const
{
    if (_tolerance >= M_PI) {
        return SO3StateSpace::allocDefaultStateSampler();
    }
    return std::make_shared<Sampler>(this);
}

Real SO3StateSpaceBounded::getTolerance() const { return _tolerance; }

const ::ompl::base::SO3StateSpace::StateType &SO3StateSpaceBounded::getReference() const
{
    return _reference;
}

SO3StateSpaceBounded::Sampler::Sampler(const SO3StateSpaceBounded *space)
    : ::ompl::base::SO3StateSampler(space)
{
}

void SO3StateSpaceBounded::Sampler::sampleUniform(::ompl::base::State *state)
{
    auto space = space_->as<SO3StateSpaceBounded>();
    ::ompl::base::SO3StateSampler::sampleUniform(state);
    double d0 = space->distance(&space->getReference(), state);
    if (d0 < space->getTolerance()) // already ok
        return;
    double desired_dist = rng_.uniformReal(0, space->getTolerance());
    double t = desired_dist / d0;

    space->interpolate(&space->getReference(), state, t, state);

    // checks
    double new_dist = space->distance(&space->getReference(), state);
    M4D_TRACE("desired distance=" << desired_dist << "; d0=" << d0 << "; t=" << t
                                  << "; result distance=" << new_dist);
    M4D_ASSERT_MSG(std::abs(new_dist - desired_dist) < 1e-5,
                   "new_dist=" << new_dist << " desired_dist= " << desired_dist << "\nreference="
                               << PRINT_TO_STREAM(space->printState(&space->getReference(), oss))
                               << "\nstate=" << PRINT_TO_STREAM(space->printState(state, oss)));
    assert(fabs(space->norm(static_cast<const StateType *>(state)) - 1.0) < 1e-9);
    assert(std::abs(new_dist - desired_dist) < 1e-4);
}

void SO3StateSpaceBounded::Sampler::sampleUniformNear(::ompl::base::State *state,
                                                      const ::ompl::base::State *near,
                                                      double distance)
{
    abort();
}

void SO3StateSpaceBounded::Sampler::sampleGaussian(::ompl::base::State *state,
                                                   const ::ompl::base::State *mean, double stdDev)
{
    abort();
}

} // namespace ompl
} // namespace plan
} // namespace move4d

//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_PLAN_SO3SPACE_H
#define MOVE4D_PLAN_SO3SPACE_H

#include "move4d/plan/SearchSpace.h"

namespace move4d
{
namespace mdl
{
class Movable;
class State;
} // namespace mdl
namespace plan
{

class RobotSpace;
using RobotSpacePtr = std::shared_ptr<RobotSpace>;
class SO3Space;
using SO3SpacePtr = std::shared_ptr<SO3Space>;
class SO2Space;
using SO2SpacePtr = std::shared_ptr<SO2Space>;

class SO3Space : public move4d::plan::SearchSpace
{
  public:
    SO3Space(const mdl::Movable *robot, std::size_t firstQuaternionDof, std::string name = "");
    /// constructor specifying a constraint
    SO3Space(const mdl::Movable *robot, std::size_t firstQuaternionDof,
             const math::Quaternion &reference, Real tolerance, std::string name = "");
    void shootUniform(mdl::WorldState &ws) const override;
    static void shootUniform(mdl::State &state, std::size_t firstQuaternionDof);
    std::unique_ptr<SearchSpace> cloneSearchSpace() const override
    {
        assert(typeid(*this) == typeid(SO3Space) && "must reimplement in subclasses");
        return std::make_unique<SO3Space>(*this);
    }

    void setBounds(math::Quaternion reference, Real tolerance);
    bool isBounded() const;
    const math::Quaternion &getReference() const;
    Real getTolerance() const;

  private:
    void initDegreesOfFreedom(const mdl::Movable *robot, std::size_t firstQuaternionDof);

    math::Quaternion _reference = math::Quaternion::Identity(); // for bounded quaternions
    Real _tolerance = M_PI;                                     // default to no constraint
};

class SE3Space : public move4d::plan::SearchSpace
{
  public:
    SE3Space(const mdl::Movable *robot, std::size_t firstFloatingJointDof, std::string name = "");
    SE3Space(const mdl::Movable *robot, std::size_t firstFloatingJointDof,
             const math::Quaternion &reference, Real tolerance, std::string name = "");
    void shootUniform(mdl::WorldState &ws) const override;
    std::pair<RobotSpacePtr, SO3SpacePtr> split() const;
    std::unique_ptr<SearchSpace> cloneSearchSpace() const override
    {
        assert(typeid(*this) == typeid(SE3Space) && "must reimplement in subclasses");
        return std::make_unique<SE3Space>(*this);
    }

    virtual void setBounds(const math::Vector3 &min, const math::Vector3 &max);
    void setBounds(math::Quaternion reference, Real tolerance);
    using SearchSpace::setIn;
    virtual void setIn(const math::Transform &t, mdl::WorldState &ws);

  private:
    const mdl::Movable *m_robot;

    void initDegreesOfFreedom(std::size_t firstFloatingJointDof);

    math::Quaternion _reference = math::Quaternion::Identity(); // for bounded quaternions
    Real _tolerance = M_PI;                                     // default to no constraint
};

class SO2Space : public move4d::plan::SearchSpace
{
  public:
    SO2Space(const mdl::Movable *robot, mdl::DegreeOfFreedomInfo dof, std::string name = "");
    std::unique_ptr<SearchSpace> cloneSearchSpace() const override
    {
        assert(typeid(*this) == typeid(SO2Space) && "must reimplement in subclasses");
        return std::make_unique<SO2Space>(*this);
    }
};
class SE2Space : public move4d::plan::SearchSpace
{
  public:
    enum class SE2Type {
        Holonomic = int(Type::SE2),
        Dubins = int(Type::Dubins),
        ReedsShepp = int(Type::ReedsShepp)
    };
    SE2Space(const mdl::Movable *robot, std::size_t firstFloatingJointDof,
             SE2Type type = SE2Type::Holonomic, std::string name = "");
    SE2Space(const mdl::Movable *robot, std::size_t dofTransX, std::size_t dofTransY,
             std::size_t dofRotAngle, SE2Type type = SE2Type::Holonomic, std::string name = "");
    std::unique_ptr<SearchSpace> cloneSearchSpace() const override
    {
        assert(typeid(*this) == typeid(SE2Space) && "must reimplement in subclasses");
        return std::make_unique<SE2Space>(*this);
    }
    virtual void setBounds(const math::Vector2 &min, const math::Vector2 &max);

    std::pair<RobotSpacePtr, SO2SpacePtr> split() const;

  private:
    const mdl::Movable *m_robot;
};

} // namespace plan
} // namespace move4d

#endif // MOVE4D_PLAN_SO3SPACE_H

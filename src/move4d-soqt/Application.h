//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef _MOVE4D_SOQT_APPLICATION_H
#define _MOVE4D_SOQT_APPLICATION_H

#include "move4d-soqt/ViewerThreadQt.h"
#include "move4d/common.h"
#include <boost/program_options.hpp>
#include <functional>
#include <string>
#include <thread>

#include <QWidget>

class QSplashScreen;

namespace move4d
{
namespace mdl
{
class Scene;
using ScenePtr = std::shared_ptr<Scene>;
class CollisionManager;
using CollisionManagerPtr = std::shared_ptr<CollisionManager>;
} // namespace mdl
namespace file
{
namespace m4df
{
class M4DFDefinition;
}
} // namespace file
namespace view
{
class ThreadedViewer;
using ThreadedViewerPtr = std::shared_ptr<ThreadedViewer>;
} // namespace view
namespace qt
{
class MainWindow;

} // namespace qt

namespace SoQt
{

/**
 * @brief Automatically creates a Scene and a viewer for you. Brings some usefull functions
 */
class Application
{
  public:
    Application(const std::string &collisionEngine, std::function<void()> runnerFn = {});

    /**
     * @brief Application construct from given CLI options, assuming they where generated with the
     * default description.
     *
     * @param cli_options option values compatible with \ref defaultCliDescritpions
     * @param runnerFn function to run when \ref start() is called
     *
     * @see \ref defaultCliDescritpions
     */
    Application(boost::program_options::variables_map cli_options,
                std::function<void()> runnerFn = {});
    virtual ~Application();

    boost::program_options::variables_map &getCliOptions() { return m_cli_options; }

    /**
     * @brief loadFile automatically load URDF, SRDF, M4DF files
     * @param path
     *
     * if the file is a m4df file, its content is appended to the M4DFDefintion owned by this
     * instance (see \ref getM4DFDefinition)
     */
    void loadFile(const std::string &path);

    const mdl::ScenePtr &getScene() const;
    const view::ThreadedViewerPtr &getViewer() const;
    const mdl::CollisionManagerPtr &getCollisionManager() const;
    const std::unique_ptr<file::m4df::M4DFDefinition> &getM4DFDefinition() const
    {
        return m_m4df_definition;
    }

    /// @brief creates a new thread to call run(), also start the viewer
    void start();
    void stop();
    void setRunFn(std::function<void()> fn);
    void unsetRunFn();

    bool getLoadCollisionsInView() const;
    void setLoadCollisionsInView(bool loadCollisionsInView);

    /// generate default options_description for parsing Command Line Interface options
    static boost::program_options::options_description defaultCliDescritpions();

    bool checkCollisionEngine(const std::string &collision_engine) const;
    bool checkPlanner(const std::string &planner) const;

  protected:
    /**
     * @brief override this function to your needs. It is called by start in a new thread
     */
    virtual void run();

    void init(const std::string &collisionEngine, std::function<void()> runnerFn);

    boost::program_options::variables_map m_cli_options;

    std::shared_ptr<QSplashScreen> m_splashScreen;
    QWidget *m_mainWidget;
    std::unique_ptr<move4d::qt::MainWindow> m_mainWindow;
    view::ThreadedViewerPtr m_viewer;

    mdl::ScenePtr m_scene;
    mdl::CollisionManagerPtr m_collisionManager;
    std::unique_ptr<file::m4df::M4DFDefinition> m_m4df_definition;

    std::thread m_runnerThread;
    std::function<void()> m_runnerFunction;

    bool m_loadCollisionsInView = false;
};

} // namespace SoQt
} // namespace move4d

#endif // _MOVE4D_SOQT_APPLICATION_H

//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#include "move4d/plan/ompl/StateValidityChecker.h"

#include "move4d/mdl/CollisionManager.h"
#include "move4d/mdl/Scene.h"
#include "move4d/mdl/WorldState.h"
#include "move4d/plan/SearchSpace.h"
#include "move4d/plan/ompl/KinematicGraphStateSpace.h"
#include "move4d/plan/ompl/PoseStateSpace.h"
#include "move4d/plan/ompl/StateSpace.h"
#include "move4d/plan/ompl/converters/StateSpaceFactory.h"
#include <utility>

namespace move4d
{
namespace plan
{
namespace ompl
{

INIT_MOVE4D_STATIC_LOGGER(StateValidityChecker, "move4d.plan.ompl.statevaliditychecker");

StateValidityChecker::StateValidityChecker(
    mdl::CollisionManagerPtr collisionManager, std::shared_ptr<const mdl::WorldState> initWs,
    StateSpaceWrapperPtr spaceWrapper, const ::ompl::base::SpaceInformationPtr &spaceInformation)
    : ::ompl::base::StateValidityChecker(spaceInformation.get()),
      m_spaceWrapper(std::move(spaceWrapper)), m_collisionManager(std::move(collisionManager)),
      m_initWorldState(std::move(initWs))
{
}

bool StateValidityChecker::isValid(const ::ompl::base::State *state) const
{
    M4D_TRACE("StateValidityChecker::isValid()");
    if (state->as<StateSpace::StateType>() &&
        state->as<StateSpace::StateType>()->validityComputed()) {
        M4D_TRACE("validity already computed: " << state->as<StateSpace::StateType>()->isValid());
        return state->as<StateSpace::StateType>()->isValid();
    }
    // check that (is a pose state space => kinematic is computed)
    assert(dynamic_cast<const PoseStateSpace *>(m_spaceWrapper->getStateSpace().get()) == nullptr ||
           dynamic_cast<const PoseStateSpace::StateType *>(state)->kinComputed());
    assert(dynamic_cast<const KinematicGraphStateSpace *>(m_spaceWrapper->getStateSpace().get()) ==
               nullptr ||
           dynamic_cast<const KinematicGraphStateSpace::StateType *>(state)->kinComputed());
    bool valid = m_spaceWrapper->getStateSpace()->satisfiesBounds(state);
    if (!valid) {
        M4D_TRACE("StateValidityChecker::isValid state does not statisfy bounds");
        return false;
    }
    mdl::WorldState ws(*m_initWorldState);

    m_spaceWrapper->applyState(state, ws);
    m_collisionManager->applyWorldState(ws);
    valid = !m_collisionManager->isColliding();
    M4D_TRACE("StateValidityChecker::isValid(): " << valid);
    return valid;
}

} // namespace ompl
} // namespace plan
} // namespace move4d

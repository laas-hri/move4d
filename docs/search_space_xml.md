# Search Space file format {#search_space_xml}

## Example

This is not yet implemented, see [Status](#impl_status).

\code{xml}
    <move4d>
      <!--some SE2 spaces-->
      <!-- holonomic-->
      <space name="base" type="SE2">
        <robot id="robot-id"/>
        <dofs x="0" y="1" theta="2"/>
      </space>
      <space name="base-floating" type="SE2">
        <robot id="robot2-id"/>
        <freeflyer first_dof="0"/>
      </space>
      <!-- non-holonomic-->
      <space name="base-rs" type="ReedsShepp"> <!--also available "Dubins"-->
        <robot id="robot2-id"/>
        <freeflyer first_dof="0"/> <!-- or <dofs/> version-->
      </space>

      <!-- SE3 space-->
      <space name="floating" type="SE3">
        <robot id="robot-id"/>
        <freeflyer first_dof="0"/>
      </space>

      <!-- S03 space-->
      <space name="spherical" type="S03">
        <robot id="robot-id"/>
        <quaternion first_dof="0"/>
      </space>

      <!-- S02 space-->
      <space name="revolute" type="S02">
        <robot id="robot-id"/>
        <dof>4</dof>
      </space>

      <!-- Robot Space -->
      <space name="arm1" type="part">
        <robot id="robot-id"/>
        <dofs>4 5 6 7 8 9 11 12</dofs>
      </space>

      <!-- Robot Kinematic group space -->
      <space name="arm1" type="part">
        <robot id="robot-id"/>
        <end_effector>gripper_left</end_effector>
        <chain>arm_left</chain>
        <ik_method>kdl</ik_method> <!-- or ikfast, ikfast_redundant -->
      </space>

      <!-- Compound search space -->
      <space name="space1" type="compound">
        <subspace> <space> <!-- ... --> </space> </subspace>
        <subspace> <space> <!-- ... --> </space> </subspace>
        <subspace name="spherical"/> <!-- any space name already declared in this file-->
        <subspace name="other_file.xml/spherical"/> <!-- any space name already declared in the other_file.xml file previously parsed -->
      </space>
    </move4d>
\endcode

ok.

## Status ##

TBD;

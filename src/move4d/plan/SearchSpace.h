//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_PLAN_SEARCHSPACE_H
#define MOVE4D_PLAN_SEARCHSPACE_H

#include "move4d/math/Vector.h"
#include "move4d/mdl/DegreeOfFreedomInfo.h"
#include "move4d/mdl/Types.h"
#include <boost/checked_delete.hpp>
#include <boost/ptr_container/ptr_vector.hpp>
#include <gsl/gsl>
#include <memory>
#include <string>
#include <vector>

#include <ompl/base/StateSampler.h>

namespace move4d
{
namespace mdl
{
class Scene;
class DegreeOfFreedomInfo;
class WorldState;
using WorldStatePtr = std::shared_ptr<WorldState>;
class CollisionManager;
using CollisionManagerPtr = std::shared_ptr<CollisionManager>;
} // namespace mdl
} // namespace move4d

namespace move4d
{

namespace plan
{

class SearchSpace;
using SearchSpacePtr = std::shared_ptr<SearchSpace>;

/**
 * @brief The SearchSpace class is the base class for representing search spaces.
 * In most cases you need specializations of this class.
 */
class SearchSpace
{
    MOVE4D_STATIC_LOGGER;

  public:
    enum class Type {
        Undef,
        Compound,
        RealVector,
        SO2,
        SO3,
        SE2,
        ReedsShepp,
        Dubins,
        SE3,
        KinematicGroup,
        ClosedLoop,
        KinematicGraph,
        Other,
        Size
    };
    explicit SearchSpace(Type type, std::string name = "");
    virtual ~SearchSpace() = default;

    template <class T> T *as() { return dynamic_cast<T *>(this); }
    template <class T> const T *as() const { return dynamic_cast<const T *>(this); }

    virtual std::unique_ptr<SearchSpace> cloneSearchSpace() const
    {
        assert(typeid(*this) == typeid(SearchSpace) && "must reimplement in subclasses");
        return std::make_unique<SearchSpace>(*this);
    }

    inline std::size_t getDimension() const { return m_degreesOfFreedom.size(); }
    inline const std::vector<move4d::mdl::DegreeOfFreedomInfo> &getDegreesOfFreedom() const;
    inline std::vector<move4d::mdl::DegreeOfFreedomInfo> &getDegreesOfFreedom()
    {
        return m_degreesOfFreedom;
    }
    inline const std::vector<move4d::mdl::DegreeOfFreedomInfo> &getAdditionalDofs() const
    {
        return m_additionalDoFs;
    }
    inline const std::string &getName() const;

    Type getType() const;
    static const std::string &typeString(Type type);

    virtual void shootUniform(mdl::WorldState &ws) const;

    std::string toString(const mdl::WorldState &ws) const;

    /**
     * @brief setIn sets the \e values in the \e ws
     * @param[in] values in the order of the degrees of freedom of this space
     * @param[in,out] ws
     */
    void setIn(const math::Vector &values, move4d::mdl::WorldState &ws);
    /**
     * @brief getFrom gets the values of the degrees of freedom of this space from the \e ws
     * @param ws
     * @return
     */
    math::Vector getFrom(const move4d::mdl::WorldState &ws);

    // TODO(Jules): temporary sampler allocator in move4d::plan::SearchSpace
    void setStateSamplerAllocator(::ompl::base::StateSamplerAllocator allocator);
    const ::ompl::base::StateSamplerAllocator &getStateSamplerAllocator() const;

    std::vector<size_t> bijection() const;

  protected:
    std::vector<move4d::mdl::DegreeOfFreedomInfo> m_degreesOfFreedom;
    /// degrees of freedom that are not controlled but are set by the search space.
    /// primarily created for supporting attachments (they contain attached objects position dofs)
    std::vector<move4d::mdl::DegreeOfFreedomInfo> m_additionalDoFs;
    Type m_type{};
    std::string m_name;

    ::ompl::base::StateSamplerAllocator m_state_sampler_allocator;
};

class CompoundSearchSpace : public SearchSpace
{
  public:
    explicit CompoundSearchSpace(std::string name = "");

    std::unique_ptr<SearchSpace> cloneSearchSpace() const override
    {
        assert(typeid(*this) == typeid(CompoundSearchSpace) && "must reimplement in subclasses");
        return std::make_unique<CompoundSearchSpace>(*this);
    }

    /// gives ownership
    void addSubspace(gsl::owner<SearchSpace *> &subspace);
    /// copy space
    void addSubspace(const SearchSpace &subspace);
    inline const boost::ptr_vector<SearchSpace> &getSubspaces() const { return m_subspaces; }

    void shootUniform(mdl::WorldState &ws) const override;

  protected:
    boost::ptr_vector<SearchSpace> m_subspaces;
};

/// a virtual interface to augment SearchSpace with kinematic methods
class KinematicSearchSpace
{
  public:
    virtual bool computeIK(mdl::WorldState &ws, const math::Transform &pose) const = 0;
    virtual bool computeIK(mdl::State &state, const math::Transform &pose) const = 0;

    virtual bool computeFK(const mdl::WorldState &ws, math::Transform &pose) const = 0;
    virtual bool computeFK(const mdl::State &state, math::Transform &pose) const = 0;

    virtual void setPoseBounds(math::Vector3 low, math::Vector3 high) = 0;
    virtual void setPoseBounds(const std::pair<math::Vector3, math::Vector3> &bounds);
    virtual const math::Vector3 &getPoseLowerBounds() const = 0;
    virtual const math::Vector3 &getPoseHigherBounds() const = 0;
};

const std::vector<mdl::DegreeOfFreedomInfo> &SearchSpace::getDegreesOfFreedom() const
{
    return m_degreesOfFreedom;
}

const std::string &SearchSpace::getName() const { return m_name; }

template <typename T, class = typename std::enable_if_t<std::is_base_of<SearchSpace, T>::value>>
inline gsl::owner<T *> new_clone(const T *t)
{
    return reinterpret_cast<T *>(t->cloneSearchSpace().release());
}
inline void delete_clone(const SearchSpace *t) { boost::checked_delete(t); }

} // namespace plan

} // namespace move4d
#endif

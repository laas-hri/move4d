#include <QApplication>
#include <boost/program_options.hpp>
#include <move4d-soqt/Application.h>
#include <move4d-soqt/ViewerSoQt.h>
#include <move4d-soqt/ViewerThreadQt.h>
#include <move4d/3rdParty/prettyprint/prettyprint.hpp>
#include <move4d/common.h>
#include <move4d/file/find_file.h>
#include <move4d/file/m4df/M4DFDefinition.h>
#include <move4d/file/m4df/M4DFParser.h>
#include <move4d/mdl/CollisionScene.h>
#include <move4d/mdl/Movable.h>
#include <move4d/mdl/Scene.h>
#include <move4d/plan/KinematicGraphSearchSpace.h>
#include <move4d/plan/SO3Space.h>
#include <move4d/plan/ompl/converters/PlannerFactory.h>
#include <move4d/utils/filesystem.h>
#include <move4d/utils/random.h>

class Application
{
  public:
    Application(int argc, char *argv[]);
    void start();

  protected:
    void run();
    std::unique_ptr<move4d::SoQt::Application> m_app;

    bool checkCollisionEngine(const std::string &collision_engine) const;
    bool checkPlanner(const std::string &planner) const;

    std::unique_ptr<move4d::plan::KinematicGraphSearchSpace> loadXML();

  private:
    bool m_running;
    boost::program_options::variables_map _cli_options;
};

Application::Application(int argc, char *argv[])
{
    using namespace move4d;
    // use the first collision engine available -- you probably want to use a
    // different one
    namespace po = boost::program_options;
    po::options_description desc("Allowed options");
    // clang-format off
    desc.add_options()("help,h", "produce help message and exit")
            ("file,f", po::value<std::vector<std::string>>()->default_value({}), "load a file")
            ("view-collisions","")
            ("vc", "display collision bodies in the viewer")
            ("engine,c", po::value<std::string>()->default_value(move4d::mdl::CollisionSceneFactory::instance().getTypes().at(0)), "collision engine to use")
            ("planner,p", po::value<std::string>()->default_value("RRTConnect"), "planning algorithm to use")
            ("path", po::value<std::string>(), "play a path instead of planning")
            ("time-limit,t",po::value<float>(), "planning algorithm time limit")
            ("seed", po::value<unsigned long int>(), "seed for the random number generator, "
                                                     "defaults to a random number from your computer random number device");
    po::store(po::parse_command_line(argc, argv, desc), _cli_options);
    po::notify(_cli_options);

    if(_cli_options.count("help")){
        std::cout<<desc<<"\n";
        move4d::Stop("");
        return;
    }
    if(!checkCollisionEngine(_cli_options["engine"].as<std::string>()) || !checkPlanner(_cli_options["planner"].as<std::string>())){
        move4d::Stop("");
        return;
    }

    // creates a high level object that creates and manages for you multiple
    // aspects of move4d:
    // - models for computation (collisions, kinematic and dynamics)
    // - visualization
    // the 2nd parameter is a function object that will be executed when
    // m_app->start() will be called
    m_app = std::make_unique<move4d::SoQt::Application>(
                _cli_options["engine"].as<std::string>(), std::bind(&Application::run, this));
    m_app->getScene()->setSpaceBounds({{-5, -5, 0}, {5, 5, 3}});

    if (_cli_options.count("seed")) {
        move4d::random::instance().seed(_cli_options["seed"].as<unsigned long int>());
    }
    if (_cli_options.count("view-collisions") || _cli_options.count("vc")) {
        m_app->setLoadCollisionsInView(true);
    }

    for(auto &f : _cli_options["file"].as<std::vector<std::string>>()){
        m_app->loadFile(f);
    }
}

void Application::start() {
    // starts the visualisation and call the function object passed in the
    // construction it returns when the user closes the window and the function is
    // finished
    this->m_app->start();
}

void Application::run() {
    m_running = true;

    auto kinematic_graph = loadXML();
    move4d::mdl::WorldState ws(m_app->getScene().get());

    while (M4D_WHILE_CONDITION(true)) {
        kinematic_graph->shootUniform(ws);
        m_app->getViewer()->update(ws);
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    }
}

bool Application::checkCollisionEngine(const std::string &collision_engine) const
{
    auto types = move4d::mdl::CollisionSceneFactory::instance().getTypes();
    auto search = std::find(types.begin(),types.end(),collision_engine);
    if(search==types.end()){
        std::cerr<<"choose a collision engine among the available types:\n\t"<<types<<"\n";
        return false;
    }
    return true;
}

bool Application::checkPlanner(const std::string &planner) const
{
        auto types = move4d::plan::ompl::convert::PlannerFactory::instance().getTypes();
        auto search = std::find(types.begin(), types.end(), planner);
        if (search == types.end()) {
            std::cerr << "available planner types are:\n\t " << types << "\n";
            return false;
        }
        return true;
}

std::unique_ptr<move4d::plan::KinematicGraphSearchSpace> Application::loadXML()
{
    using namespace move4d::file;
    m4df::M4DFParser parser(m_app->getScene(),m_app->getViewer()->getViewer());
    move4d::file::filePath().push_back(std::filesystem::canonical("../../assets"));
    try{
    auto def = parser.loadFile("hand_in_hand.m4df");
    auto ss = def->searchSpace("hand_in_hand");
    auto find_builder = def->getKinematicGraphBuilders().find("hand_in_hand");
    ptr_wrapper<move4d::plan::KinematicGraphSearchSpaceBuilder> builder;
    if(find_builder != def->getKinematicGraphBuilders().end()){
        builder = find_builder->second;
    }else{
        std::cerr<<"expected a <search_space name=\"hand_in_hand\" type=\"kinematic_graph\"> in input file hand_in_hand.m4df"<<std::endl;
        abort();
    }
    if(ss && dynamic_cast<move4d::plan::KinematicGraphSearchSpace*>(&*ss)){
        return std::unique_ptr<move4d::plan::KinematicGraphSearchSpace>(ss.release()->as<move4d::plan::KinematicGraphSearchSpace>());
    }
    }catch(const m4df::M4DFException &e){
        std::cerr<<e.what()<<"\n";
        throw e;
    }

    return {};
}

int main(int argc, char *argv[]) {
    Application app(argc, argv);
    app.start();
    return 0;
}

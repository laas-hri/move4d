//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#include "move4d/plan/ompl/Planner.h"
#include "move4d/Factory.h"
#include "move4d/motion/GeometricPath.h"
#include "move4d/plan/SearchSpace.h"
#include "move4d/plan/WorldStateGoal.h"
#include "move4d/plan/ompl/PoseMotionValidator.h"
#include "move4d/plan/ompl/PoseStateSpace.h"
#include "move4d/plan/ompl/StateSpace.h"
#include "move4d/plan/ompl/StateValidityChecker.h"
#include "move4d/plan/ompl/converters/OmplGoalFactory.h"
#include "move4d/plan/ompl/converters/PlannerFactory.h"
#include "move4d/plan/ompl/converters/StateSpaceFactory.h"
#include "move4d/plan/ompl/magic.h"
#include <ompl/base/Planner.h>
#include <ompl/geometric/PathGeometric.h>
#include <ompl/geometric/PathSimplifier.h>
#include <ompl/util/Time.h>

INIT_MOVE4D_STATIC_LOGGER(move4d::plan::ompl::Planner, "move4d.plan.ompl.planner");

namespace move4d
{
namespace plan
{
namespace ompl
{

Planner::Planner(const std::shared_ptr<SearchSpace> &space,
                 const mdl::CollisionManagerPtr &collisionManager, const std::string &plannerType)
    : move4d::plan::Planner(space, collisionManager)
{
    convert::SearchSpaceFactory factory;
    m_spaceWrapper = factory.createForPlanning(*m_searchSpace);
    // auto ompl_space = std::make_shared<PoseStateSpace>(space);
    // m_spaceWrapper = StateSpaceWrapper::create(space, ompl_space);
    m_spaceInformation =
        std::make_shared<::ompl::base::SpaceInformation>(m_spaceWrapper->getStateSpace());
    // m_planner = std::make_shared<::ompl::geometric::RRTConnect>(m_spaceInformation);
    m_planner = convert::PlannerFactory::instance().create(plannerType, m_spaceInformation);
}

bool Planner::solve(float timeLimit)
{
    Expects(dynamic_cast<StateSpace *>(m_spaceWrapper->getStateSpace().get()));
    m_spaceWrapper->getStateSpace()->as<StateSpace>()->setInitWorldState(m_start);
    m_spaceInformation->setStateValidityChecker(
        std::make_shared<move4d::plan::ompl::StateValidityChecker>(
            m_collisionManager, m_start, m_spaceWrapper, m_spaceInformation));
    m_spaceInformation->setStateValidityCheckingResolution(
        magic::unwanted::DEFAULT_SAMPLING_RESOLUTION);
    m_spaceInformation->setMotionValidator(
        std::make_shared<PoseMotionValidator>(m_spaceInformation));
    assert(m_spaceInformation->getStateValidityChecker());
    m_spaceInformation->setup();
    m_problemDefinition = std::make_shared<::ompl::base::ProblemDefinition>(m_spaceInformation);
    auto start = m_spaceWrapper->extractState(*m_start);

    start->as<StateSpace::StateType>()->flags |= StateSpace::StateType::IsStart;

    auto segmFrac = m_spaceWrapper->getStateSpace()->getLongestValidSegmentFraction();
    auto segmLen = m_spaceWrapper->getStateSpace()->getLongestValidSegmentLength();
    M4D_INFO("space longuest valid segment fraction=" << segmFrac << " (len=" << segmLen << ")");

    m_problemDefinition->addStartState(start);
    {
        convert::OmplGoalFactory goal_factory(m_spaceInformation);
        m_problemDefinition->setGoal(goal_factory.convert(*m_goal));
    }
    m_planner->setProblemDefinition(m_problemDefinition);

    assert(m_planner->getSpaceInformation()->getStateValidityChecker());
    assert(m_planner->getProblemDefinition()->getSpaceInformation()->getStateValidityChecker());
    assert(m_planner->getProblemDefinition()->getSpaceInformation()->getStateValidityChecker() ==
           m_planner->getSpaceInformation()->getStateValidityChecker());
    assert(std::dynamic_pointer_cast<move4d::plan::ompl::StateValidityChecker>(
        m_planner->getSpaceInformation()->getStateValidityChecker()));

    m_planner->setup();

    const ::ompl::time::point endTime(::ompl::time::now() + ::ompl::time::seconds(timeLimit));
    ::ompl::base::PlannerTerminationCondition terminalCond(
        [endTime]() { return M4D_STOP_CONDITION(::ompl::time::now() > endTime); },
        std::min(0.1f, timeLimit / 100.f));
    ::ompl::base::PlannerStatus status = m_planner->solve(terminalCond);

    return status && m_problemDefinition->hasExactSolution();
}

StateSpaceWrapperPtr Planner::getSpaceWrapper() const { return m_spaceWrapper; }

::ompl::base::PathPtr Planner::getPathOmpl(bool simplified, double max_time) const
{
    if (m_problemDefinition && m_problemDefinition->hasExactSolution()) {
        ::ompl::geometric::PathSimplifier simplifier(m_spaceInformation);
        auto path = *m_problemDefinition->getSolutionPath()->as<::ompl::geometric::PathGeometric>();
        bool simplify_ok = false;
        if (simplified) {
            simplify_ok = simplifier.simplify(path, max_time, true);
        }
        if (simplify_ok) {
            // return the simplified path
            return std::make_shared<::ompl::geometric::PathGeometric>(path);
        } else {
            // return the original path (copied)
            return std::make_shared<::ompl::geometric::PathGeometric>(
                *m_problemDefinition->getSolutionPath()->as<::ompl::geometric::PathGeometric>());
        }
    }
    return {};
}

motion::GeometricPathPtr Planner::getPath(bool simplified, double max_time) const
{
    motion::GeometricPathPtr path = std::make_shared<motion::GeometricPath>(m_searchSpace);

    auto ompl_path_ = getPathOmpl(simplified, max_time);
    if (ompl_path_) {
        auto ompl_path = ompl_path_->as<::ompl::geometric::PathGeometric>();
        for (const auto &s : ompl_path->getStates()) {
            path->append(m_spaceWrapper->valuesFromState(s));
        }
    }
    return path;
}

} // namespace ompl
} // namespace plan
} // namespace move4d

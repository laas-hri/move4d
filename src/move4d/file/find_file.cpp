#include "move4d/file/find_file.h"
#include "move4d/io/Logger.h"
#include "move4d/utils/filesystem.h"
#include <assert.h>
#include <iomanip>

namespace move4d
{
namespace file
{

std::vector<std::string> _file_path;
std::function<std::string(const std::string &)> _find_file_callback;

std::string remove_protocol(std::string &path)
{
    size_t search = path.find("://");
    if (search != std::string::npos) {
        std::string protocol = path.substr(0, search);
        if (protocol.find_first_of(":/.") != std::string::npos) {
            // a protocol name should not contain any of ':' '.' '/'
            return "";
        }
        std::string path_new = path.substr(search + 3);
        assert(protocol + "://" + path_new == path);
        path = path_new;
        return protocol;
    }
    return "";
}

std::string findFile(const std::string &file_name_,
                     const std::vector<std::string> &additional_paths)
{
    INIT_MOVE4D_LOG("move4d.file.findfile");
    std::string file_name = file_name_;
    auto protocol = remove_protocol(file_name);
    std::filesystem::path path(file_name);

    // try with the find file callback
    if (_find_file_callback) {
        std::string file = _find_file_callback(file_name_);
        if (!file.empty()) {
            return file;
        }
    }

    // if a protocol is specified, throw
    if (!protocol.empty()) {
        M4D_ERROR("cannot manage file with protocol "
                  << std::quoted(protocol) << "(file name: " << std::quoted(file_name_) << ")");
        throw FileNotFound(file_name_);
    }

    // check if absolute
    if (path.is_absolute() && std::filesystem::is_regular_file(path)) {
        return std::filesystem::canonical(path);
    } else if (path.is_absolute()) {
        throw FileNotFound(file_name_);
    }

    // check in given additional paths
    for (auto &p_ : additional_paths) {
        std::filesystem::path p(p_);
        assert(p.is_absolute());
        if (!p.is_absolute()) {
            throw FileError("path in additional_paths must be absolute (\"" + p_ + "\")");
        }
        if (std::filesystem::is_regular_file(p / path)) {
            return p / path;
        }
    }

    // check if in current dir
    if (std::filesystem::is_regular_file(path)) {
        return std::filesystem::canonical(path);
    }

    // check in _file_path
    for (auto &p_ : filePath()) {
        std::filesystem::path p(p_);
        assert(p.is_absolute());
        if (!p.is_absolute()) {
            throw FileError("path in filePath must be absolute (\"" + p_ + "\")");
        }
        if (std::filesystem::is_regular_file(p / path)) {
            return p / path;
        }
    }

    throw FileNotFound(file_name_);
}

void setFindFileCallback(std::function<std::string(const std::string &)> cb)
{
    _find_file_callback = cb;
}

std::vector<std::string> &filePath() { return _file_path; }

FileNotFound::FileNotFound(const std::string &path)
    : FileError("couldn't find a file matching " + path)
{
}

std::function<std::string(const std::string &)> findFileCallback() { return _find_file_callback; }

} // namespace file

} // namespace move4d

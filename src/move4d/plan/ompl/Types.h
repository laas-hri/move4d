//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_PLAN_OMPL_TYPES_H
#define MOVE4D_PLAN_OMPL_TYPES_H

#ifndef TYPES_H
#define TYPES_H

#include <memory>

/**
 * @file
 * @brief forward declarations of common types in the move4d::plan::ompl namespace
 */

namespace move4d
{
namespace plan
{
namespace ompl
{

class StateSpaceWrapper;
using StateSpaceWrapperPtr = std::shared_ptr<StateSpaceWrapper>;

class StateSpace;
using StateSpacePtr = std::shared_ptr<StateSpace>;
class PoseStateSpace;
using PoseStateSpacePtr = std::shared_ptr<PoseStateSpace>;
class KinematicGraphStateSpace;
using KinematicGraphStateSpacePtr = std::shared_ptr<KinematicGraphStateSpace>;

} // namespace ompl

} // namespace plan

} // namespace move4d

#endif // TYPES_H

#endif

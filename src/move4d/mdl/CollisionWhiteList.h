//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
// Created by gbuisan on 20/12/18.
//

#ifndef MOVE4D_MDL_COLLISIONWHITELIST_H
#define MOVE4D_MDL_COLLISIONWHITELIST_H

#include "move4d/mdl/Types.h"
#include <move4d/common.h>
#include <unordered_map>
#include <vector>

namespace rl
{
namespace sg
{
class Model;
}
} // namespace rl
namespace rl
{
namespace sg
{
class Body;
}
} // namespace rl

namespace move4d
{

namespace mdl
{

/**
 * Manages whitelist/blacklist of colliding objects/bodies.
 *
 * disabled pairs are for all queries except when the pair is explicitly requested
 */

class CollisionWhiteList
{
  public:
    typedef SceneObject &object_ref;
    typedef SceneBody &body_ref;
    typedef std::vector<CollisionPair> CollisionList;

    CollisionWhiteList() = default;

    CollisionWhiteList(
        const std::unordered_map<move4d::Identifier, std::shared_ptr<SceneObject>> &objects);

    CollisionWhiteList(const CollisionWhiteList &whiteList);

    CollisionWhiteList(const CollisionList &whitelist);

    CollisionWhiteList(
        const std::unordered_map<move4d::Identifier, std::shared_ptr<SceneObject>> &objects,
        const CollisionList &blackList);

    void reset(const std::unordered_map<move4d::Identifier, std::shared_ptr<SceneObject>> &objects);

    void disable(object_ref a, object_ref b);

    void disable(object_ref a, body_ref b);

    void disable(body_ref a, body_ref b);

    void enable(object_ref a, object_ref b);

    void enable(object_ref a, body_ref b);

    void enable(body_ref a, body_ref b);

    void removeNeighbourBodies(object_ref obj);
    void removeNeighbourBodies2(object_ref obj);
    void removeSelfColliding(object_ref obj, CollisionManager &collisionManager);

    inline CollisionList::iterator begin() { return m_whiteList.begin(); }
    inline CollisionList::iterator end() { return m_whiteList.end(); }
    inline size_t size() { return m_whiteList.size(); }

    friend std::ostream &operator<<(std::ostream &os, const CollisionWhiteList &whitelist);

  private:
    CollisionList m_whiteList;
};

// std::ostream &operator<<(std::ostream &os, const CollisionWhiteList &whitelist);
} // namespace mdl
} // namespace move4d

#endif // MOVE4D_MDL_COLLISIONWHITELIST_H

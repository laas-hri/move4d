//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "move4d/Singleton.h"
#include <QMainWindow>

namespace Ui
{
class MainWindow;
}

namespace move4d
{
namespace SoQt
{
class Application;
} // namespace SoQt
namespace qt
{

class MainWindow : public QMainWindow, public Singleton<MainWindow>
{
    Q_OBJECT

  public:
    MainWindow(move4d::SoQt::Application &app, QWidget *parent = nullptr);
    ~MainWindow();

  private slots:
    void on_actionLoad_file_triggered();

    void on_actionQuit_triggered();

  protected:
    // QWidget interface
    void closeEvent(QCloseEvent *) override;

  private:
    Ui::MainWindow *ui;
    move4d::SoQt::Application &_app;
};

} // namespace qt

} // namespace move4d

#endif // MAINWINDOW_H

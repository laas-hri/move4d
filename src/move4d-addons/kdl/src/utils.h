//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef _ADDONS_KDL_SRC_UTILS_H
#define _ADDONS_KDL_SRC_UTILS_H

#include "move4d/common.h"
#include "move4d/math/Vector.h"

namespace KDL
{
class Chain;
class JntArray;
} // namespace KDL

namespace move4d
{
namespace kin
{
namespace kdl
{
namespace utils
{
void displayChainFrames(const KDL::Chain &chain, const KDL::JntArray &q,
                        const math::Transform &base_pose);

} // namespace utils
} // namespace kdl
} // namespace kin
} // namespace move4d

#endif // _ADDONS_KDL_SRC_UTILS_H

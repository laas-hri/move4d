//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//

#include "move4d/mdl/Scene.h"
#include "move4d/mdl/CollisionManager.h"
#include "move4d/mdl/CollisionWhiteList.h"
#include "move4d/mdl/Kinematic.h"
#include "move4d/mdl/Model.h"
#include "move4d/mdl/ObjectFactory.h"
#include "move4d/mdl/SceneObject.h"
#include "move4d/mdl/WorldState.h"

namespace move4d
{

namespace mdl
{

INIT_MOVE4D_STATIC_LOGGER(Scene, "move4d.mdl.scene");

Scene::Scene(std::shared_ptr<CollisionScene> collisionScene)
    : m_collisionScene(std::move(collisionScene))
{
    m_collisionWhitelist = std::make_shared<CollisionWhiteList>(m_objects);
}

SceneObjectPtr Scene::create(const std::string &objecttype, const std::string &name,
                             const move4d::Identifier &id)
{
    SceneObjectPtr ob;

    Identifier mId = id;
    if (mId.empty()) {
        mId = this->createId(name);
    } else {
        auto id_search = this->m_objects.find(mId);
        if (id_search != this->m_objects.end()) {
            M4D_ERROR("an object with id " << mId << " already exist");
            // TODO throw?
            return ob;
        }
    }

    std::size_t index = m_collisionScene->getNumModels();
    auto m = std::shared_ptr<Model>(m_collisionScene->create());
    m->setName(mId);
    auto dyn = std::make_shared<Kinematic>();
    dyn->setName(mId);
    ob = ObjectFactory::instance().create(objecttype, *this, m, dyn, name, mId, index);
    this->m_objects[mId] = ob;
    this->m_nameMap[name].push_back(ob);
    return ob;
}

void Scene::remove(const move4d::Identifier &id)
{
    // TODO(jules): remove from maps + remove from rl::sg::Scene
    auto search = m_objects.find(id);
    SceneObjectPtr ptr;
    if (search != m_objects.end()) {
        ptr = std::move(search->second);
        m_objects.erase(search);
    }
    if (ptr) {
        m_nameMap.erase(ptr->getName());
    }

    Ensures(m_nameMap.count(ptr->getName()) == 0 && m_objects.count(ptr->getId()) == 0);
    // SceneObject will be deleted by shared_ptr
}

move4d::Identifier Scene::createId(const std::string &name)
{
    Identifier id(name);
    size_t i{0};

    auto id_search = this->m_objects.find(id);
    while (id_search != this->m_objects.end()) {
        id = name + std::to_string(i);
        id_search = this->m_objects.find(id);
        ++i;
    }
    return id;
}

SceneObjectPtr Scene::getObject(const move4d::Identifier &id)
{
    auto search = m_objects.find(id);
    if (search != m_objects.end()) {
        return search->second;
    }
    M4D_WARN("no object with ID " << id);
    return nullptr;
}

SceneObjectPtr Scene::getObjectByName(const std::string &name) const
{
    auto search = m_nameMap.find(name);
    if (search != m_nameMap.end()) {
        if (search->second.size() > 1) {
            M4D_WARN("multiple objects with name " << name);
        }
        return search->second[0];
    }
    M4D_WARN("no object with name " << name);
    return nullptr;
}

std::vector<SceneObjectPtr> Scene::getObjectsByName(const std::string &name) const
{
    auto search = m_nameMap.find(name);
    if (search != m_nameMap.end()) {
        return search->second;
    }
    M4D_WARN("no objects with name " << name);
    return {};
}

/**
 * creates a new worldstate with copies of all the current states
 */
std::shared_ptr<move4d::mdl::WorldState> Scene::getCurrentWorldState() const
{
    return std::make_shared<WorldState>(this);
}

void Scene::applyWorldState(const WorldState &ws)
{
    for (const auto &ob : *this) {
        ob.second->applyState(ws[ob.first]);
    }
}

const std::shared_ptr<CollisionWhiteList> &Scene::getCollisionWhitelist()
{
    return m_collisionWhitelist;
}

void Scene::resetCollisionWhiteList() { m_collisionWhitelist->reset(m_objects); }

const std::pair<math::Vector3, math::Vector3> &Scene::getSpaceBounds() const
{
    assert(m_space_bounds.first != m_space_bounds.second && "Scene spacebounds probably unset");
    return m_space_bounds;
}

void Scene::setSpaceBounds(std::pair<math::Vector3, math::Vector3> space_bounds)
{
    m_space_bounds = std::move(space_bounds);
}

} // namespace mdl

} // namespace move4d

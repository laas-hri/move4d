//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef GEOMETRICPATH_H
#define GEOMETRICPATH_H

#include "move4d/motion/Path.h"
#include "move4d/plan/ompl/Types.h"
#include <boost/serialization/access.hpp>

namespace move4d
{
namespace motion
{

/**
 * @brief The GeometricPath class reprensents a path using waypoints.
 *
 * The interpolation method is the one defined by the \ref plan::SearchSpace given to the
 * constructor.
 *
 * A state is represented by a \ref math::Vector where the values are in the order defined
 * in the \ref plan::SearchSpace.
 */
class GeometricPath : public Path
{
  public:
    GeometricPath() = default;
    GeometricPath(const GeometricPath &) = default;
    GeometricPath(GeometricPath &&) = default;
    GeometricPath &operator=(const GeometricPath &) = default;
    GeometricPath &operator=(GeometricPath &&) = default;
    virtual ~GeometricPath() = default;

    /**
     * @brief creates an empty path
     * @param search_space
     */
    GeometricPath(plan::SearchSpacePtr search_space);

    bool check(const mdl::WorldStatePtr &init_ws,
               const mdl::CollisionManagerPtr &collision_manager) const override;

    /// interpolate (in place) so values are approximately those used for motion validation
    void interpolate(const mdl::WorldStatePtr &init_ws);
    void interpolate(const mdl::WorldState &init_ws);

    /// adds a state at the end of the path
    void append(math::Vector value);
    /// set the states of the path
    void set(std::vector<math::Vector> values) { _values = std::move(values); }
    /// access to the internal storage with read-write access
    std::vector<math::Vector> &values() { return _values; }
    /// access to a const reference to the internal storage (read-only)
    const std::vector<math::Vector> &values() const { return _values; }

  private:
    plan::ompl::StateSpaceWrapperPtr _space_wrapper;

    std::vector<math::Vector> _values;

    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive &ar, const unsigned int version);
};

template <typename Archive> void GeometricPath::serialize(Archive &ar, const unsigned int version)
{
    // clang-format off
    ar & _values;
    // clang-format on
}

} // namespace motion
} // namespace move4d

#endif // GEOMETRICPATH_H

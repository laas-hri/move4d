//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//

#include <thread>

#include "move4d-soqt/ViewerSoQt.h"
#include "move4d/io/readable_time.h"
#include "move4d/motion/GeometricPath.h"
#include "move4d/plan/SearchSpace.h"
#include "move4d/utils/config/MagicConstants.h"
#include <Inventor/Qt/viewers/SoQtExaminerViewer.h>
#include <QResizeEvent>
#include <QTimer>
#include <rl/mdl/Joint.h>
#include <rl/mdl/Model.h>
#include <rl/plan/Model.h>
#include <rl/sg/so/Scene.h>

#include <Inventor/VRMLnodes/SoVRMLBillboard.h>
#include <Inventor/VRMLnodes/SoVRMLColor.h>
#include <Inventor/VRMLnodes/SoVRMLCoordinate.h>
#include <Inventor/VRMLnodes/SoVRMLFontStyle.h>
#include <Inventor/VRMLnodes/SoVRMLIndexedFaceSet.h>
#include <Inventor/VRMLnodes/SoVRMLIndexedLineSet.h>
#include <Inventor/VRMLnodes/SoVRMLSwitch.h>
#include <Inventor/VRMLnodes/SoVRMLText.h>
#include <Inventor/VRMLnodes/SoVRMLTransform.h>

#include <QGLWidget>

namespace move4d
{
namespace view
{

struct ViewerSoQt::PImpl {
    struct BillboardText {
        SoGroup *root;
        SoVRMLSwitch *group;
        SoVRMLTransform *transf;
        SoVRMLText *text;
        BillboardText(SoGroup *root, move4d::math::Transform position, const std::string &text);
        ~BillboardText();
    };

    struct Frames {
        SoVRMLGroup *group;
        SoVRMLTransform *transform;
        SoVRMLTransform *textTransform;
        SoVRMLIndexedLineSet *lines;
        SoVRMLText *name;
        std::vector<BillboardText> texts;
        explicit Frames(SoVRMLGroup *root);
        ~Frames() { clear(); }
        void addFrame(move4d::math::Transform frame, Real scale, const std::string &name = "");
        void clear();

      private:
        MOVE4D_STATIC_LOGGER;
    };
    std::unique_ptr<Frames> frames;
    std::unique_ptr<Frames> persistentFrames;

    explicit PImpl(SoVRMLGroup *root)
        : frames(std::make_unique<Frames>(root)), persistentFrames(std::make_unique<Frames>(root))
    {
    }
};

INIT_MOVE4D_STATIC_LOGGER(ViewerSoQt::PImpl::Frames, "move4d.view.soqt.pimpl.frames");

std::shared_ptr<rl::sg::so::Scene> ViewerSoQt::createScene()
{
    return std::make_shared<::rl::sg::so::Scene>();
}

ViewerSoQt::ViewerSoQt(QWidget *parent, const Qt::WindowFlags &f)
    : QWidget(parent, f),
      m_viewer(new SoQtExaminerViewer(this, nullptr, TRUE, SoQtFullViewer::BUILD_POPUP)),
      _recording_timer(new QTimer(this))
{
    m_viewer->setTransparencyType(SoGLRenderAction::SORTED_OBJECT_BLEND);
    m_viewer->setBackgroundColor(SbColor(0.9, 0.9, 0.9));
    setScene(createScene());
}

// NOLINTNEXTLINE(hicpp-use-equals-default,modernize-use-equals-default)
ViewerSoQt::~ViewerSoQt()
{
    // destructor must be declared where unique_ptr template types have a complete declaration
}

void ViewerSoQt::setScene(std::shared_ptr<rl::sg::Scene> scene)
{
    if (!std::dynamic_pointer_cast<rl::sg::so::Scene>(scene)) {
        throw InvalidTypeException("ViewerSoQt::setScene expects a rl::sg::so::Scene*");
    }
    m_scene = std::move(scene);
    updateModels();
}

void ViewerSoQt::updateModels()
{
    auto so_scene = std::dynamic_pointer_cast<rl::sg::so::Scene>(m_scene);
    m_viewer->setSceneGraph(so_scene->root);
    pimpl = std::make_unique<PImpl>(so_scene->root);
    m_viewer->viewAll();
}

void ViewerSoQt::update(const mdl::WorldPositions &positions) { Viewer::update(positions); }

void ViewerSoQt::update(const mdl::WorldState &positions) { Viewer::update(positions); }

void ViewerSoQt::drawConfiguration(size_t robot_index, const rl::math::Vector &q)
{
    Viewer::drawConfiguration(robot_index, q);
}

void ViewerSoQt::drawConfiguration(unsigned int robot_index, const rl::math::Vector &q)
{
    drawConfiguration(std::size_t(robot_index), q);
}

void ViewerSoQt::drawState(const mdl::State &state) { Viewer::drawState(state); }

void ViewerSoQt::drawPose(const math::Transform &pose)
{
    pimpl->persistentFrames->addFrame(pose, .1);
}

void ViewerSoQt::drawStampedPose(const rl::math::Transform &pose, const rl::math::Transform &parent,
                                 const std::string &name)
{
    // TODO(Jules): drawStampedPose() draw parent and link to parent
    pimpl->persistentFrames->addFrame(pose, .1, name);
}

void ViewerSoQt::resetPoses() { pimpl->persistentFrames->clear(); }

void ViewerSoQt::paint()
{
    // update joint frame postions
    // this->pimpl->frames->clear();
    for (const auto &m : m_models) {
        // for (size_t i = 0; i < m->mdl->getFrames(); ++i) {
        //     pimpl->frames->addFrame(m->mdl->getFrame(i)->x.transform(), 0.25,
        //                             m->mdl->getFrame(i)->getName());
        // }

        // for (size_t i = 0; i < m->mdl->getJoints(); ++i) {
        //     auto *jnt = m->mdl->getJoint(i);
        //     pimpl->frames->addFrame(math::changeFrame(jnt->in->x.transform(), jnt->x.transform(),
        //                                               math::Transform::Identity()),
        //                             .1, m->mdl->getJoint(i)->getName());
        // }
    }
}

ViewerSoQt::PImpl::BillboardText::BillboardText(SoGroup *root, math::Transform position,
                                                const std::string &text)
    : root(root)
{
    this->text = new SoVRMLText();
    this->group = new SoVRMLSwitch();
    this->transf = new SoVRMLTransform();

    auto *font = new SoVRMLFontStyle;
    font->family = SoVRMLFontStyle::TYPEWRITER;
    font->style = SoVRMLFontStyle::BOLD;
    this->text->fontStyle = font;
    font->size = .05;
    auto *billboard = new SoVRMLBillboard;
    billboard->addChild(this->text);

    this->transf->addChild(billboard);

    this->transf->translation.setValue(position.translation().x(), position.translation().y(),
                                       position.translation().z());
    this->text->string.set(text.c_str());

    group->addChild(this->transf);
    group->whichChoice = 0;
    root->addChild(group);
}

ViewerSoQt::PImpl::BillboardText::~BillboardText() { root->removeChild(this->group); }

ViewerSoQt::PImpl::Frames::Frames(SoVRMLGroup *root)
    : group(new SoVRMLGroup()), transform(new SoVRMLTransform()),
      textTransform(new SoVRMLTransform()), lines(new SoVRMLIndexedLineSet())
{

    this->lines->colorPerVertex = FALSE;

    this->lines->color = new SoVRMLColor();
    auto frameColor = dynamic_cast<SoVRMLColor *>(this->lines->color.getValue());
    frameColor->color.set1Value(0, 1.0f, 0.0f, 0.0f);
    frameColor->color.set1Value(1, 0.0f, 1.0f, 0.0f);
    frameColor->color.set1Value(2, 0.0f, 0.0f, 1.0f);

    auto *frameCoordinate = new SoVRMLCoordinate();
    this->lines->coord = frameCoordinate;

    transform->addChild(lines);
    // group->whichChoice = 0;
    group->addChild(transform);
    root->addChild(group);
}

void ViewerSoQt::PImpl::Frames::addFrame(math::Transform frame, Real scale, const std::string &name)
{
    M4D_DEBUG("frame with name " << name);
    int i = this->lines->colorIndex.getNum();
    this->lines->colorIndex.set1Value(i + 0, 0);
    this->lines->colorIndex.set1Value(i + 1, 1);
    this->lines->colorIndex.set1Value(i + 2, 2);

    auto frameCoordinate = reinterpret_cast<SoVRMLCoordinate *>(this->lines->coord.getValue());
    math::Vector3 x, y, z;
    auto o = frame.translation();
    x = frame * (scale * math::Vector3::UnitX());
    y = frame * (scale * math::Vector3::UnitY());
    z = frame * (scale * math::Vector3::UnitZ());
    int k = frameCoordinate->point.getNum();
    frameCoordinate->point.set1Value(k + 0, o.x(), o.y(), o.z());
    frameCoordinate->point.set1Value(k + 1, x.x(), x.y(), x.z());
    frameCoordinate->point.set1Value(k + 2, y.x(), y.y(), y.z());
    frameCoordinate->point.set1Value(k + 3, z.x(), z.y(), z.z());
    // frameCoordinate->point.set1Value(k+0, offset.x()+scale*0.0f, offset.y()+scale*0.0f,
    // offset.z()+scale*0.0f); frameCoordinate->point.set1Value(k+1, offset.x()+scale*1.0f,
    // offset.y()+scale*0.0f, offset.z()+scale*0.0f); frameCoordinate->point.set1Value(k+2,
    // offset.x()+scale*0.0f, offset.y()+scale*1.0f, offset.z()+scale*0.0f);
    // frameCoordinate->point.set1Value(k+3, offset.x()+scale*0.0f, offset.y()+scale*0.0f,
    // offset.z()+scale*1.0f);

    int j = this->lines->coordIndex.getNum();
    this->lines->coordIndex.set1Value(j + 0, k + 0);
    this->lines->coordIndex.set1Value(j + 1, k + 1);
    this->lines->coordIndex.set1Value(j + 2, SO_END_FACE_INDEX);
    this->lines->coordIndex.set1Value(j + 3, k + 0);
    this->lines->coordIndex.set1Value(j + 4, k + 2);
    this->lines->coordIndex.set1Value(j + 5, SO_END_FACE_INDEX);
    this->lines->coordIndex.set1Value(j + 6, k + 0);
    this->lines->coordIndex.set1Value(j + 7, k + 3);
    this->lines->coordIndex.set1Value(j + 8, SO_END_FACE_INDEX);

    if (!name.empty()) {
        texts.emplace_back(this->group, frame, name);
        // this->textTransform->translation.setValue(o.x(), o.y(), o.z());
        // this->name->string.set(name.c_str());
    }
}

void ViewerSoQt::PImpl::Frames::clear()
{
    M4D_DEBUG("clear frames");
    this->lines->colorIndex.setNum(0);
    auto frameCoordinate = reinterpret_cast<SoVRMLCoordinate *>(this->lines->coord.getValue());
    frameCoordinate->point.setNum(0);
    this->lines->coordIndex.setNum(0);
    this->texts.clear();

    this->addFrame(math::Transform::Identity(), 1.);
}

void ViewerSoQt::drawConfiguration(const rl::math::Vector &q) { Viewer::drawConfiguration(q); }
void ViewerSoQt::drawConfigurationEdge(const rl::math::Vector &q0, const rl::math::Vector &q1,
                                       const bool &free)
{
    Viewer::drawConfigurationEdge(q0, q1, free);
}
void ViewerSoQt::drawConfigurationPath(const rl::plan::VectorList &path)
{
    Viewer::drawConfigurationPath(path);
}
void ViewerSoQt::drawConfigurationVertex(const rl::math::Vector &q, const bool &free)
{
    Viewer::drawConfigurationVertex(q, free);
}
void ViewerSoQt::drawLine(const rl::math::Vector &xyz0, const rl::math::Vector &xyz1)
{
    Viewer::drawLine(xyz0, xyz1);
}
void ViewerSoQt::drawPoint(const rl::math::Vector &xyz) { Viewer::drawPoint(xyz); }
void ViewerSoQt::drawSphere(const rl::math::Vector &center, const rl::math::Real &radius)
{
    Viewer::drawSphere(center, radius);
}
void ViewerSoQt::drawWork(const rl::math::Transform &t) { Viewer::drawWork(t); }
void ViewerSoQt::drawWorkEdge(const rl::math::Vector &q0, const rl::math::Vector &q1)
{
    Viewer::drawWorkEdge(q0, q1);
}
void ViewerSoQt::drawWorkPath(const rl::plan::VectorList &path) { Viewer::drawWorkPath(path); }
void ViewerSoQt::drawWorkVertex(const rl::math::Vector &q) { Viewer::drawWorkVertex(q); }
void ViewerSoQt::reset() { Viewer::reset(); }
void ViewerSoQt::resetEdges() { Viewer::resetEdges(); }
void ViewerSoQt::resetLines() { Viewer::resetLines(); }
void ViewerSoQt::resetPoints() { Viewer::resetPoints(); }
void ViewerSoQt::resetSpheres() { Viewer::resetSpheres(); }
void ViewerSoQt::resetVertices() { Viewer::resetVertices(); }
void ViewerSoQt::showMessage(const std::string &message) { Viewer::showMessage(message); }

void ViewerSoQt::saveImage(const std::string &filename)
{
    // m_viewer->render();
    // glReadBuffer(GL_FRONT);
    glFlush();
    QImage image = static_cast<QGLWidget *>(m_viewer->getGLWidget())->grabFrameBuffer(false);
    image.save(QString(filename.c_str()), "JPEG", 90);
}

void ViewerSoQt::saveImage()
{
    static size_t count = 0;
    char str_i[6];
    snprintf(str_i, 6, "%05lu", count++);
    std::string name = "/tmp/move4d/record/Image_" + std::string(str_i) + ".jpg";
    saveImage(name);
}

void ViewerSoQt::playPath(const mdl::WorldState &ws_init, const move4d::motion::GeometricPath &path)
{
    INIT_MOVE4D_LOG("move4d.view.soqt.viewersoqt.playpath");
    mdl::WorldState ws(ws_init);
    Real fps = move4d::magic::view::PlayMotionDefaultFPS;
    std::chrono::steady_clock::time_point next_tp;
    for (auto &x : path.values()) {
        auto next_tp = std::chrono::steady_clock::now() +
                       std::chrono::milliseconds(size_t(std::round(1000. / fps)));
        path.getSearchSpace()->setIn(x, ws);
        M4D_TRACE("display state " << x.transpose());
        update(ws);
        // m_viewer->scheduleRedraw(); // doesn't work (no rendering)
        m_viewer->render(); // ok, but can't interact with the viewer (move camera...) until the
                            // function returns
        std::this_thread::sleep_until(next_tp);
    }
}

void ViewerSoQt::toggleRecording(bool record)
{
    _recording_enabled = record;
    if (_recording_enabled) {
        connect(_recording_timer, SIGNAL(timeout()), this, SLOT(saveImage()));
        _recording_timer->start(20);
    } else {
        connect(_recording_timer, SIGNAL(timeout()), this, SLOT(saveImage()));
        _recording_timer->stop();
    }
}

void ViewerSoQt::viewAll() { m_viewer->viewAll(); }

} // namespace view
} // namespace move4d

//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#include "move4d/utils/containers/DofsView.h"
#include "move4d/mdl/DegreeOfFreedomInterface.h"
#include "move4d/mdl/WorldState.h"
#include "move4d/plan/SearchSpace.h"

namespace move4d
{
namespace views
{

DofsViewWorldstate::DofsViewWorldstate(mdl::WorldState &ws, const plan::SearchSpace &space)
    : _ws(ws), _space(space)
{
}

DofView DofsViewWorldstate::operator[](size_t i)
{
    return DofViewWorldState::create(_ws, _space.getDegreesOfFreedom()[i]);
}

const DofView DofsViewWorldstate::operator[](size_t i) const
{
    return DofViewWorldState::create(_ws, _space.getDegreesOfFreedom()[i]);
}

size_t DofsViewWorldstate::size() const { return _space.getDimension(); }

} // namespace views

} // namespace move4d

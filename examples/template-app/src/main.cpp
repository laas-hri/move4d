#include <QApplication>
#include <move4d-soqt/Application.h>
#include <move4d/common.h>
#include <move4d/mdl/CollisionScene.h>

class Application {
public:
  Application(int argc, char *argv[]);
  void start();

protected:
  void run();
  std::unique_ptr<move4d::SoQt::Application> m_app;

private:
  bool m_running;
};

Application::Application(int argc, char *argv[]) {

  auto cli_desc = move4d::SoQt::Application::defaultCliDescritpions();
  boost::program_options::variables_map cli_options;
  boost::program_options::store(
      boost::program_options::parse_command_line(argc, argv, cli_desc),
      cli_options);
  m_app = std::make_unique<move4d::SoQt::Application>(
      cli_options, std::bind(&Application::run, this));
}

void Application::start() {
  // starts the visualisation and call the function object passed in the
  // construction it returns when the user closes the window and the function is
  // finished
  this->m_app->start();
}

void Application::run() {
  m_running = true;

  while (M4D_WHILE_CONDITION(true)) {
    //...
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
  }
}

int main(int argc, char *argv[]) {
  Application app(argc, argv);
  app.start();
  return 0;
}

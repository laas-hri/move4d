//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#include "move4d/utils/random.h"

namespace move4d
{

INIT_MOVE4D_STATIC_LOGGER(random, "move4d.random");

random::random()
{
    std::random_device dev;
    this->seed(dev());
}

void random::seed(std::random_device::result_type s)
{
    _engine.seed(s);
    M4D_INFO("setting random seed to " << s);
}

Real random::uniform()
{
    std::uniform_real_distribution<Real> distrib(0., 1.);
    return distrib(_engine);
}

Real random::uniform(Real from, Real to)
{
    std::uniform_real_distribution<Real> distrib(from, to);
    return distrib(_engine);
}

math::Vector2 random::uniform_disc(Real radius)
{
    // http://www.anderswallin.net/2009/05/uniform-random-points-in-a-circle-using-polar-coordinates/
    // https://stackoverflow.com/questions/5837572/generate-a-random-point-within-a-circle-uniformly/
    Real r = radius * std::sqrt(uniform());
    Real theta = uniform() * 2 * M_PI;
    return {r * std::cos(theta), r * std::sin(theta)};
}

math::Vector2 random::uniform_annulus(Real inner_radius, Real outter_radius)
{
    Real r = outter_radius * std::sqrt(uniform(std::pow(inner_radius / outter_radius, 2), 1.));
    Real theta = uniform() * 2 * M_PI;
    return {r * std::cos(theta), r * std::sin(theta)};
}

} // namespace move4d

//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
// Created by gbuisan on 22/01/19.
//

#include "move4d/utils/model/GeometryCreator.h"
#include "move4d/math/Vector.h"
#include <Inventor/VRMLnodes/SoVRMLBox.h>
#include <Inventor/VRMLnodes/SoVRMLCylinder.h>
#include <Inventor/VRMLnodes/SoVRMLShape.h>
#include <Inventor/VRMLnodes/SoVRMLSphere.h>
#include <iostream>
#include <rl/sg/Body.h>
#include <rl/sg/Shape.h>

#include <Inventor/VRMLnodes/SoVRMLAppearance.h>
#include <Inventor/VRMLnodes/SoVRMLMaterial.h>

namespace move4d
{
namespace utils
{
namespace model
{

void applyMaterial(SoVRMLShape &shape, std::optional<Eigen::Vector4f> &color)
{
    auto material = new SoVRMLMaterial();
    if (color) {
        material->diffuseColor.setValue(color.value().x(), color.value().y(), color.value().z());
        material->transparency = 1 - color.value()[3];
    }
    auto appearance = new SoVRMLAppearance();
    shape.appearance = appearance;
    appearance->material = material;
}
void addSphereToBody(float radius, rl::sg::Body *body, const math::Transform &pos,
                     std::optional<Eigen::Vector4f> color)
{
    auto shape = new SoVRMLShape();
    auto vrmlSphere = new SoVRMLSphere();
    vrmlSphere->radius = radius;
    shape->geometry = vrmlSphere;
    applyMaterial(*shape, color);
    auto rlShape = body->create(shape);
    rlShape->setTransform(pos);
}

void addCylinderToBody(float radius, float length, rl::sg::Body *body, const math::Transform &pos,
                       std::optional<Eigen::Vector4f> color)
{
    auto shape = new SoVRMLShape();
    auto vrmlCylinder = new SoVRMLCylinder;
    vrmlCylinder->height = length;
    vrmlCylinder->radius = radius;
    shape->geometry = vrmlCylinder;
    applyMaterial(*shape, color);
    auto rlShape = body->create(shape);
    // in URDF, cylinder is aligned on Z axis, while in VRML it is on Y axis
    rlShape->setTransform(
        pos * math::Quaternion(math::Quaternion::AngleAxisType(M_PI_2, math::Vector3::UnitX())));
}

void addBoxToBody(const Eigen::Vector3f &size, rl::sg::Body *body, const math::Transform &pos,
                  std::optional<Eigen::Vector4f> color)
{
    auto shape = new SoVRMLShape();
    auto vrmlBox = new SoVRMLBox();
    vrmlBox->size.setValue(size.x(), size.y(), size.z());
    shape->geometry = vrmlBox;
    applyMaterial(*shape, color);
    auto rlShape = body->create(shape);
    rlShape->setTransform(pos);
}

} // namespace model
} // namespace utils
} // namespace move4d

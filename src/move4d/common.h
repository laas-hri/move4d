//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_COMMON_H
#define MOVE4D_COMMON_H

#include <gsl/gsl>
#include <iostream>
#ifndef MOVE4D_REAL_TYPE
#define MOVE4D_REAL_TYPE double
#endif

#ifndef MOVE4D_IDENTIFIER_TYPE
#include <string>
#define MOVE4D_IDENTIFIER_TYPE std::string
#endif

#include "move4d/io/Logger.h"

#ifndef m4d_likely
#define m4d_likely(x) __builtin_expect(!!(x), 1)
#endif
#ifndef m4d_unlikely
#define m4d_unlikely(x) __builtin_expect(!!(x), 0)
#endif

#define M4D_FILE_LINE_FUNC                                                                         \
    std::string(std::string("in ") + __PRETTY_FUNCTION__ + " at " + __FILE__ + ":" +               \
                std::to_string(__LINE__) + "\n")

/**
 * @brief the main namespace under which all code related to the library functionalities can be
 * found.
 *
 * It excludes actual visualization and applications
 */
namespace move4d
{

/// the C++ type to use for real numbers math (defaults to double)
using Real = MOVE4D_REAL_TYPE;
/// type to use for unique identifiers (defaults to std::string)
using Identifier = MOVE4D_IDENTIFIER_TYPE;

class Exception : public std::runtime_error
{
  public:
    explicit Exception(const std::string &arg) : std::runtime_error(arg) {}
    explicit Exception(const char *arg) : std::runtime_error(arg) {}
};

/// @defgroup Stopping Program Termination
/// We provide here functions to help cleanly terminate the program
/// @{

/// @brief request a gentle stop
void Stop(const std::string &reason);
/// @brief output reason to user and call std::terminate.
/// This should be used only "in response to a critical program error for which no recovery is
/// possible" (CERT ERR50-CPP-EX1)
[[noreturn]] void FastStop(const std::string &reason) noexcept;
/// @brief return true if Stop has been called
bool isStopping() noexcept;

#define M4D_STOP_CONDITION(cond) (m4d_unlikely(::move4d::isStopping()) || (cond))
#define M4D_WHILE_CONDITION(cond) (!m4d_unlikely(::move4d::isStopping()) && (cond))

///@}

} // namespace move4d
#endif

#include <QApplication>
#include <boost/program_options.hpp>
#include <move4d-soqt/Application.h>
#include <move4d-soqt/ViewerSoQt.h>
#include <move4d-soqt/ViewerThreadQt.h>
#include <move4d/3rdParty/prettyprint/prettyprint.hpp>
#include <move4d/common.h>
#include <move4d/file/m4df/M4DFDefinition.h>
#include <move4d/file/m4df/M4DFParser.h>
#include <move4d/mdl/CollisionScene.h>
#include <move4d/mdl/Movable.h>
#include <move4d/mdl/Scene.h>
#include <move4d/mdl/SceneObject.h>
#include <move4d/motion/GeometricPath.h>
#include <move4d/plan/KinematicGraphSearchSpace.h>
#include <move4d/plan/SO3Space.h>
#include <move4d/plan/WorldStateGoal.h>
#include <move4d/plan/ompl/Planner.h>
#include <move4d/plan/ompl/converters/PlannerFactory.h>
#include <move4d/utils/random.h>

/**
 * @example ompl/src/ompl_example.cpp
 */

using namespace move4d;

class Application
{
  public:
    Application(int argc, char *argv[]);
    void start();

  protected:
    void run();
    std::unique_ptr<move4d::SoQt::Application> m_app;

  private:
    bool m_running;
    boost::program_options::variables_map _cli_options;
    bool checkCollisionEngine(const std::string &collision_engine) const;
    bool checkPlanner(const std::string &planner) const;
};

bool Application::checkCollisionEngine(const std::string &collision_engine) const
{
    auto types = move4d::mdl::CollisionSceneFactory::instance().getTypes();
    auto search = std::find(types.begin(), types.end(), collision_engine);
    if (search == types.end()) {
        std::cerr << "choose a collision engine among the available types:\n\t" << types << "\n";
        return false;
    }
    return true;
}
bool Application::checkPlanner(const std::string &planner) const
{
    auto types = move4d::plan::ompl::convert::PlannerFactory::instance().getTypes();
    auto search = std::find(types.begin(), types.end(), planner);
    if (search == types.end()) {
        std::cerr << "available planner types are:\n\t " << types << "\n";
        return false;
    }
    return true;
}
Application::Application(int argc, char *argv[])
{
    // use the first collision engine available -- you probably want to use a
    // different one
    namespace po = boost::program_options;
    po::options_description desc("Allowed options");
    // clang-format off
    desc.add_options()("help,h", "produce help message and exit")
            ("view-collisions","")
            ("vc", "display collision bodies in the viewer")
            ("engine,c", po::value<std::string>()->default_value(move4d::mdl::CollisionSceneFactory::instance().getTypes().at(0)), "collision engine to use")
            ("planner,p", po::value<std::string>()->default_value("RRTConnect"), "planning algorithm to use")
            ("path", po::value<std::string>(), "play a path instead of planning")
            ("time-limit,t",po::value<float>()->default_value(10.f), "planning algorithm time limit")
            ("seed", po::value<unsigned long int>(), "seed for the random number generator, "
                                                     "defaults to a random number from your computer random number device");
    po::store(po::parse_command_line(argc, argv, desc), _cli_options);
    po::notify(_cli_options);

    if(_cli_options.count("help")){
        std::cout<<desc<<"\n";
        move4d::Stop("");
        return;
    }
    if(!checkCollisionEngine(_cli_options["engine"].as<std::string>()) || !checkPlanner(_cli_options["planner"].as<std::string>())){
        move4d::Stop("");
        return;
    }
    if(_cli_options.count("seed")){
        move4d::random::instance().seed(_cli_options["seed"].as<unsigned long int>());
    }else{
        move4d::random::instance();
    }

    // creates a high level object that creates and manages for you multiple
    // aspects of move4d:
    // - models for computation (collisions, kinematic and dynamics)
    // - visualization
    // the 2nd parameter is a function object that will be execute when
    // m_app->start() will be called
    m_app = std::make_unique<move4d::SoQt::Application>(
              _cli_options["engine"].as<std::string>(), std::bind(&Application::run, this));
    m_app->setLoadCollisionsInView(true);
    m_app->getScene()->setSpaceBounds({{-20, -20, -20}, {50, 50, 50}});
}

void Application::start() {
    // starts the visualisation and call the function object passed in the
    // construction it returns when the user closes the window and the function is
    // finished
    this->m_app->start();
}

void Application::run() {
    m_running = true;

    file::m4df::M4DFParser m4df(m_app->getScene(),m_app->getViewer()->getViewer());
    m4df.setLoadCollisionInView(m_app->getLoadCollisionsInView());
    auto definition = m4df.loadFile("assets/alpha/alpha_puzzle.m4df");
    m_app->getScene()->resetCollisionWhiteList();
    auto alpha_rob = definition->getScene()->getObject("alpha")->as<mdl::Movable>();

    mdl::WorldState start_ws(m_app->getScene().get()), goal_ws(m_app->getScene().get());
    start_ws["alpha"].setValues(0,math::Translation(-7., 8., -5) * math::utilities::quaternionFromRPY(0.,0.,M_PI/2));
    m_app->getViewer()->update(start_ws);

    goal_ws["alpha"](0,mdl::StateType::Position) = 50.;

    plan::SE3Space se3_space (alpha_rob,0,"alpha_rob_se3");
    se3_space.setBounds(m_app->getScene()->getSpaceBounds().first,m_app->getScene()->getSpaceBounds().second);

    plan::ompl::Planner planner(se3_space.cloneSearchSpace(),m_app->getCollisionManager(),
                                _cli_options["planner"].as<std::string>());

    planner.setStart(std::make_shared<mdl::WorldState>(start_ws));
    planner.setGoal(std::make_shared<plan::WorldStateGoal>(std::make_shared<mdl::WorldState>(goal_ws)));

    m_app->getViewer()->update(start_ws);
    // std::this_thread::sleep_for(std::chrono::seconds(1));
    // m_app->getViewer()->update(goal_ws);
    // std::this_thread::sleep_for(std::chrono::seconds(1));

    bool ok = planner.solve(_cli_options["time-limit"].as<float>());
    if(ok){
        std::cout<<"solution found\n";
        auto path = planner.getPath();
        path->interpolate(start_ws);
        while (M4D_WHILE_CONDITION(true)) {
            m_app->getViewer()->playPath(start_ws,*path);
            std::this_thread::sleep_for(std::chrono::seconds(1));
        }
    }else{
        std::cout<<"no solution found\n";
        return;
    }


}

int main(int argc, char *argv[]) {
    Application app(argc, argv);
    app.start();
    return 0;
}

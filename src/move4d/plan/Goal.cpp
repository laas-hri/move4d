//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//

#include "move4d/plan/Goal.h"

#include "move4d/mdl/WorldState.h"
#include <utility>

namespace move4d
{

namespace plan
{

Goal::Goal(size_type input_size, size_type output_size, std::string name)
    : WorldStateFunction(input_size, output_size, std::move(name))
{
}

} // namespace plan

} // namespace move4d

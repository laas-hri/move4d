//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//

#include "KdlSolver.h"
#include "KdlFactory.h"
#include "converters.h"
#include "move4d/io/Logger.h"
#include "move4d/mdl/SceneObject.h"
#include "move4d/view/ViewerInterface.h"
#include "utils.h"

#include <kdl/chainfksolverpos_recursive.hpp>
#include <kdl/chainiksolverpos_lma.hpp>
#include <kdl/chainiksolverpos_nr_jl.hpp>
#include <kdl/chainiksolvervel_pinv.hpp>

#include <rl/mdl/Dynamic.h>

namespace move4d
{
namespace kin
{
namespace kdl
{

INIT_MOVE4D_STATIC_LOGGER(KdlSolver, "move4d.kin.kdl.solver");

FACTORY_REGISTER_FACTORY(KinematicSolverFactory, "kdl",
                         []() { return std::make_shared<KdlSolver>(); });

bool KdlSolver::computeInverseKinematic(const mdl::Chain &chain, const math::Transform &base_pose,
                                        const math::Transform &end_pose, math::Vector &x_in_out)
{
    //    M4D_TRACE("state:\n - \t" << state(mdl::StateType::Position).transpose());
    auto kdl_chain = KdlFactory::instance().createChain(chain);
    assert(kdl_chain->getNrOfJoints() == x_in_out.size());
    assert(kdl_chain->getNrOfJoints() == chain.kinematic->getJoints());

    // KDL::ChainIkSolverPos_LMA solver(*kdl_chain, 1e-3, 500, 1e-6);

    KDL::ChainFkSolverPos_recursive fk_solver(*kdl_chain);
    KDL::ChainIkSolverVel_pinv solver_vel(*kdl_chain);
    KDL::JntArray q_min, q_max;
    q_min.data = chain.kinematic->getMinimum();
    q_max.data = chain.kinematic->getMaximum();
    assert(q_min.data.size() == x_in_out.size());
    assert(q_max.data.size() == x_in_out.size());
    KDL::ChainIkSolverPos_NR_JL solver(*kdl_chain, q_min, q_max, fk_solver, solver_vel);
    KDL::JntArray init;
    init.data = x_in_out;
    assert(init.rows() == kdl_chain->getNrOfJoints());
    assert(init.rows() == x_in_out.size());
    math::Transform local_pose =
        math::changeFrame(math::Transform::Identity(), end_pose, base_pose);

    KDL::Frame goal = converters::fromTransform(local_pose);
    KDL::JntArray out(x_in_out.size());
    // TODO(Jules) : select KDL solver (?)
    int res = solver.CartToJnt(init, goal, out);

    x_in_out = out.data;
    M4D_TRACE("kdl ik result: " << solver.strError(res));
    return res == KDL::SolverI::E_NOERROR;
}

bool KdlSolver::computeForwardKinematic(const mdl::Chain &chain, const math::Transform &base_pose,
                                        math::Transform &end_pose_out, const math::Vector &x_in)
{
    //    M4D_TRACE("state:\n - \t" << state(mdl::StateType::Position).transpose());
    auto kdl_chain = KdlFactory::instance().createChain(chain);
    assert(kdl_chain->getNrOfJoints() == x_in.size());
    assert(kdl_chain->getNrOfJoints() == chain.kinematic->getJoints());

    KDL::ChainFkSolverPos_recursive fk_solver(*kdl_chain);
    KDL::JntArray init;
    init.data = x_in;
    assert(init.rows() == kdl_chain->getNrOfJoints());
    assert(init.rows() == x_in.size());

    KDL::Frame p;
    KDL::JntArray out(x_in.size());
    // TODO(Jules) : select KDL solver (?)
    auto res = fk_solver.JntToCart(init, p);

    end_pose_out = base_pose * converters::toTransform(p);

    M4D_TRACE("kdl ik result: " << fk_solver.strError(res));
    return res == KDL::SolverI::E_NOERROR;
}

} // namespace kdl
} // namespace kin
} // namespace move4d

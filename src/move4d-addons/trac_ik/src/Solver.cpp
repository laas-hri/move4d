//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#include "Solver.h"
#include "move4d-addons/kdl/src/KdlFactory.h"
#include "move4d-addons/kdl/src/converters.h"
#include "trac_ik/trac_ik.hpp"
#include <move4d/mdl/SceneObject.h>
#include <rl/mdl/Dynamic.h>

namespace move4d
{
namespace kin
{
namespace trac_ik
{

INIT_MOVE4D_STATIC_LOGGER(Solver, "move4d.kin.trac_ik.solver");

FACTORY_REGISTER_FACTORY(KinematicSolverFactory, "trac_ik",
                         []() { return std::make_shared<Solver>(); });

bool Solver::computeInverseKinematic(const mdl::Chain &chain, const math::Transform &base_pose,
                                     const math::Transform &end_pose, math::Vector &x_in_out)
{
    auto kdl_chain = kdl::KdlFactory::instance().createChain(chain);
    assert(kdl_chain->getNrOfJoints() == x_in_out.size());
    assert(kdl_chain->getNrOfJoints() == chain.kinematic->getJoints());

    // KDL::ChainIkSolverPos_LMA solver(*kdl_chain, 1e-3, 500, 1e-6);

    KDL::ChainFkSolverPos_recursive fk_solver(*kdl_chain);
    KDL::ChainIkSolverVel_pinv solver_vel(*kdl_chain);
    KDL::JntArray q_min, q_max;
    q_min.data = chain.kinematic->getMinimum();
    q_max.data = chain.kinematic->getMaximum();
    assert(q_min.data.size() == x_in_out.size());
    assert(q_max.data.size() == x_in_out.size());
    ::TRAC_IK::TRAC_IK solver(*kdl_chain, q_min, q_max, 0.001, 1e-4, TRAC_IK::Manip2);
    KDL::JntArray init;
    init.data = x_in_out;
    assert(init.rows() == kdl_chain->getNrOfJoints());
    assert(init.rows() == x_in_out.size());
    math::Transform local_pose =
        math::changeFrame(math::Transform::Identity(), end_pose, base_pose);

    KDL::Frame goal = kdl::converters::fromTransform(local_pose);
    KDL::JntArray out(x_in_out.size());
    // TODO(Jules) : select KDL solver (?)
    KDL::Twist tol;
    int res = solver.CartToJnt(init, goal, out, tol);

    x_in_out = out.data;
    M4D_TRACE("trac_ik result: " << res);
    return res > 0;
}

} // namespace trac_ik
} // namespace kin
} // namespace move4d

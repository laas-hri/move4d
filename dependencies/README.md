# dependencies

You are encouraged to contribute specific files for other distributions.

System dependencies scripts names are generated with:

```bash
touch "`lsb_release -si`_`lsb_release -sc`_`lsb_release -sr`.sh"
```

//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_MDL_MOVABLE_H
#define MOVE4D_MDL_MOVABLE_H

#include "move4d/common.h"
#include "move4d/mdl/SceneObject.h"
#include "move4d/mdl/Types.h"
#include <string>

namespace rl
{
namespace sg
{
class Body;
}
} // namespace rl
namespace move4d
{
namespace mdl
{
class State;
}
} // namespace move4d

namespace move4d
{

namespace mdl
{

class Movable : public SceneObject
{
  public:
    Movable(const Scene &scene, std::shared_ptr<Model> model, std::shared_ptr<Kinematic> kin,
            const std::string &name, const move4d::Identifier &id, std::size_t index);
};

} // namespace mdl

} // namespace move4d
#endif

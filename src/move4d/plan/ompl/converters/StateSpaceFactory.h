//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_PLAN_OMPL_CONVERTERS_STATESPACEFACTORY_H
#define MOVE4D_PLAN_OMPL_CONVERTERS_STATESPACEFACTORY_H

#include "move4d/io/Logger.h"
#include "move4d/math/Vector.h"
#include <memory>
#include <ompl/base/ScopedState.h>

namespace ompl
{
namespace base
{
class StateSpace;
using StateSpacePtr = std::shared_ptr<StateSpace>;
class State;
using StatePtr = std::shared_ptr<State>;
} // namespace base
} // namespace ompl
namespace move4d
{
namespace mdl
{
class WorldState;
} // namespace mdl
namespace plan
{
class SearchSpace;
using SearchSpacePtr = std::shared_ptr<SearchSpace>;
namespace ompl
{

class StateSpaceWrapper;
using StateSpaceWrapperPtr = std::shared_ptr<StateSpaceWrapper>;
class KinematicGraphStateSpace;

namespace convert
{

class SearchSpaceFactory
{
  public:
    ::ompl::base::StateSpacePtr createSpace(const SearchSpace &searchSpace);
    StateSpaceWrapperPtr create(const SearchSpace &searchSpace);
    /// encapsulate in a move4d::ompl::StateSpace if neeeded
    StateSpaceWrapperPtr createForPlanning(const SearchSpace &searchSpace);
};

} // namespace convert

class StateSpaceWrapper
{
    MOVE4D_STATIC_LOGGER;
    friend convert::SearchSpaceFactory;

  public:
    /**
     * @brief StateSpaceWrapper
     * @param searchSpace
     * @param ss
     * @deprecated use StateSpaceWrapper::create
     */
    [[deprecated]] StateSpaceWrapper(std::shared_ptr<const SearchSpace> searchSpace,
                                     ::ompl::base::StateSpacePtr ss);

    /**
     * @brief create a wrapper and set it as the StateSpace wrapper
     * @param searchSpace
     * @param ss
     * @return
     * @see StateSpace::setWrapper
     */
    static StateSpaceWrapperPtr create(std::shared_ptr<const SearchSpace> searchSpace,
                                       ::ompl::base::StateSpacePtr ss);

    void applyState(const ::ompl::base::State *state, mdl::WorldState &ws) const;
    void applyAttachments(KinematicGraphStateSpace *space, const ::ompl::base::State *state,
                          mdl::WorldState &ws) const;

    ::ompl::base::ScopedState<> extractState(const mdl::WorldState &ws) const;

    ::ompl::base::ScopedState<> stateFromValues(const move4d::math::Vector &values) const;
    math::Vector valuesFromState(const ::ompl::base::State *state) const;
    void appendAttachmentValues(const ::ompl::base::State *state, math::Vector &v,
                                size_t first_index) const;

    const std::shared_ptr<::ompl::base::StateSpace> &getStateSpace() const;
    const std::shared_ptr<const SearchSpace> &getSearchSpace() const;

  protected:
    std::shared_ptr<const move4d::plan::SearchSpace> m_searchSpace;
    std::shared_ptr<::ompl::base::StateSpace> m_stateSpace;
};

} // namespace ompl
} // namespace plan
} // namespace move4d

#endif // MOVE4D_PLAN_OMPL_CONVERTERS_STATESPACEFACTORY_H

//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_MDL_DEGREEOFFREEDOMINFO_H
#define MOVE4D_MDL_DEGREEOFFREEDOMINFO_H

#include "move4d/common.h"
#include "move4d/math/Vector.h"
#include "move4d/mdl/Types.h"

namespace move4d
{

namespace mdl
{

class DegreeOfFreedomHandle
{

  public:
    DegreeOfFreedomHandle(Identifier object_id, StateType::Type type, std::size_t index)
        : m_object_id(std::move(object_id)), m_type(std::move(type)), m_index(std::move(index))
    {
    }

    virtual void setIn(State &state, move4d::Real value) const;
    virtual void setIn(WorldState &ws, move4d::Real value) const;

    virtual Real getFrom(const State &state) const;
    virtual move4d::Real getFrom(const WorldState &ws) const;

    inline Identifier getObjectId() const { return m_object_id; }
    inline std::size_t index() const { return m_index; }
    inline StateType::Type type() const { return m_type; }

  private:
    Identifier m_object_id{};
    StateType::Type m_type{};
    std::size_t m_index{};
};

class DegreeOfFreedomInfo
{
  public:
    DegreeOfFreedomInfo() = default;
    DegreeOfFreedomInfo(const DegreeOfFreedomInfo &) = default;
    DegreeOfFreedomInfo(DegreeOfFreedomInfo &&) = default;
    DegreeOfFreedomInfo &operator=(const DegreeOfFreedomInfo &) = default;
    DegreeOfFreedomInfo &operator=(DegreeOfFreedomInfo &&) = default;
    virtual ~DegreeOfFreedomInfo() = default;

    /**
     * creates the DoF with a reference to the given state
     */
    DegreeOfFreedomInfo(Identifier object_id, move4d::Real vmin, move4d::Real vmax, bool wrap,
                        StateType::Type type, std::size_t index);
    DegreeOfFreedomInfo(std::shared_ptr<const DegreeOfFreedomHandle> handle, move4d::Real vmin,
                        move4d::Real vmax, bool wrap);

    inline move4d::Real getMinimum() const { return m_vmin; }
    inline move4d::Real getMaximum() const { return m_vmax; }
    inline void setMinimum(Real min) { m_vmin = min; }
    inline void setMaximum(Real max) { m_vmax = max; }
    inline void setBounds(Real min, Real max)
    {
        m_vmin = min;
        m_vmax = max;
    }

    inline void setIn(State &state, move4d::Real value) const { m_handle->setIn(state, value); }
    inline void setIn(WorldState &ws, move4d::Real value) const { m_handle->setIn(ws, value); }

    inline Real getFrom(const State &state) const { return m_handle->getFrom(state); }
    inline move4d::Real getFrom(const WorldState &ws) const { return m_handle->getFrom(ws); }

    inline Identifier getObjectId() const { return m_handle->getObjectId(); }
    inline std::size_t index() const { return m_handle->index(); }
    inline StateType::Type type() const { return m_handle->type(); }

    move4d::Real shootUniform(const move4d::Real &rand) const;

    move4d::Real shootGaussian(const move4d::Real &rand, const move4d::Real &mean,
                               const move4d::Real &sigma) const;

    void setWrapAround(bool wrap_around) { m_wrap = wrap_around; }
    bool wrapAround() const;

    void clip(move4d::Real &value) const;

  protected:
    std::shared_ptr<const DegreeOfFreedomHandle> m_handle;

    move4d::Real m_vmin{};

    move4d::Real m_vmax{};

    bool m_wrap{};
};

class RotationInQuaternionDoF : public DegreeOfFreedomHandle
{
  public:
    /// equivalent to RotationInQuaternionDoF(object,
    /// firstQuaternionDof,firstQuaternionDof+1,firstQuaternionDof+2,firstQuaternionDof+3)
    RotationInQuaternionDoF(Identifier object_id, math::Vector3 axis,
                            std::size_t firstQuaternionDof);
    RotationInQuaternionDoF(Identifier object_id, math::Vector3 axis, std::size_t indexX,
                            std::size_t indexY, std::size_t indexZ, std::size_t indexW);

    void setIn(State &state, Real value) const override;
    Real getFrom(const State &state) const override;

    DegreeOfFreedomInfo make_info() const
    {
        // create a shared_ptr to a copy of *this
        return DegreeOfFreedomInfo(std::make_shared<RotationInQuaternionDoF>(*this), -M_PI, M_PI,
                                   true);
    }

  private:
    std::vector<std::shared_ptr<const DegreeOfFreedomHandle>> m_xyz;
    math::Vector3 m_axis;
};

} // namespace mdl

} // namespace move4d
#endif

//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_PLAN_CLOSEDLOOPSEARCHSPACE_H
#define MOVE4D_PLAN_CLOSEDLOOPSEARCHSPACE_H

#include "move4d/math/Vector.h"
#include "move4d/math/utils.h"
#include "move4d/mdl/Types.h"
#include "move4d/plan/SearchSpace.h"
#include "move4d/plan/Types.h"

namespace move4d
{
namespace plan
{

class ClosedLoopSearchSpace : public SearchSpace
{
  public:
    /**
     * @brief ClosedLoopSearchSpace created with this c'tor will shoot the target position first
     * then solve all IK.
     * @param ik_chains
     * @param eef_to_target_transforms
     */
    ClosedLoopSearchSpace(std::vector<RobotKinematicGroupSpacePtr> ik_chains,
                          std::vector<math::Transform> eef_to_target_transforms,
                          math::Vector3Bounds pose_bounds, std::string name = "");

    /**
     * @brief ClosedLoopSearchSpace created with this c'tor will first shoot dofs of fk_chain, then
     * solve IK for each chain in ik_chains.
     * @param fk_chain
     * @param eef_to_target
     * @param ik_chains
     * @param eef_to_target_transforms
     */
    ClosedLoopSearchSpace(RobotKinematicGroupSpacePtr fk_chain, math::Transform eef_to_target,
                          std::vector<RobotKinematicGroupSpacePtr> ik_chains,
                          std::vector<math::Transform> eef_to_target_transforms,
                          std::string name = "");

    std::unique_ptr<SearchSpace> cloneSearchSpace() const override
    {
        assert(typeid(*this) == typeid(ClosedLoopSearchSpace) && "must reimplement in subclasses");
        return std::make_unique<ClosedLoopSearchSpace>(*this);
    }

    void shootUniform(mdl::WorldState &ws) const override;

    const std::vector<RobotKinematicGroupSpacePtr> &getIkChains() const;
    void setIkChains(std::vector<RobotKinematicGroupSpacePtr> ik_chains);

    const RobotKinematicGroupSpacePtr &getFkChain() const;
    void setFkChain(RobotKinematicGroupSpacePtr fk_chain);

    const SearchSpacePtr &getPre() const;
    /// space to shoot before the closed loop
    void setPre(SearchSpacePtr pre);

    const SearchSpacePtr &getPost() const;
    /// space to shoot after the closed loop
    void setPost(SearchSpacePtr post);

  private:
    std::vector<RobotKinematicGroupSpacePtr> _ik_chains;
    RobotKinematicGroupSpacePtr _fk_chain;
    SearchSpacePtr _pre;
    SearchSpacePtr _post;
    math::Vector3Bounds _pose_bounds;
    std::vector<math::Transform> _eef_to_target_tfs;
    math::Transform _fk_chain_eef_to_target_tf;

    void _initDegreeOfFreedomList();
};

} // namespace plan
} // namespace move4d

#endif // MOVE4D_PLAN_CLOSEDLOOPSEARCHSPACE_H

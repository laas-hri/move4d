//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_PLAN_DISJUNCTIVEGOALLIST_H
#define MOVE4D_PLAN_DISJUNCTIVEGOALLIST_H

#include "move4d/plan/GoalList.h"

namespace move4d
{

namespace plan
{

/**
 * Only one goal of the list have to be reach by the worldstate
 */
class DisjunctiveGoalList : public GoalList
{
  public:
    Real distance(const mdl::WorldState &ws) const override;
    bool check(const mdl::WorldState &ws) const override;

  protected:
    void impl_compute(result_ref result, const_argument_ref argument) const override;
};

} // namespace plan

} // namespace move4d
#endif

//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_UTILS_COLLISIONSETTINGHELPER_H
#define MOVE4D_UTILS_COLLISIONSETTINGHELPER_H

#include "move4d/mdl/Types.h"

namespace move4d
{
namespace utils
{

class CollisionSettingHelper
{
  public:
    CollisionSettingHelper(mdl::CollisionManager *collisionManager);

    void detectRercurringCollisions(size_t tests = 100);

  private:
    mdl::CollisionManager *m_collisionManager;
};

} // namespace utils
} // namespace move4d

#endif // MOVE4D_UTILS_COLLISIONSETTINGHELPER_H

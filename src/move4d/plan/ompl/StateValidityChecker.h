//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_PLAN_OMPL_STATEVALIDITYCHECKER_H
#define MOVE4D_PLAN_OMPL_STATEVALIDITYCHECKER_H

#include "move4d/io/Logger.h"
#include <ompl/base/StateValidityChecker.h>

namespace move4d
{
namespace mdl
{
class Scene;
using ScenePtr = std::shared_ptr<Scene>;
class CollisionManager;
using CollisionManagerPtr = std::shared_ptr<CollisionManager>;
class WorldState;
using WorldStatePtr = std::shared_ptr<WorldState>;

} // namespace mdl
namespace plan
{
namespace ompl
{
class StateSpaceWrapper;
using StateSpaceWrapperPtr = std::shared_ptr<StateSpaceWrapper>;

class StateValidityChecker : public ::ompl::base::StateValidityChecker
{
    MOVE4D_STATIC_LOGGER;

  public:
    StateValidityChecker(mdl::CollisionManagerPtr collisionManager,
                         std::shared_ptr<const mdl::WorldState> initWs,
                         StateSpaceWrapperPtr spaceWrapper,
                         const ::ompl::base::SpaceInformationPtr &spaceInformation);

    bool isValid(const ::ompl::base::State *state) const override;

  protected:
    StateSpaceWrapperPtr m_spaceWrapper;
    mdl::CollisionManagerPtr m_collisionManager;
    std::shared_ptr<const mdl::WorldState> m_initWorldState;
};

} // namespace ompl
} // namespace plan
} // namespace move4d

#endif // MOVE4D_PLAN_OMPL_STATEVALIDITYCHECKER_H

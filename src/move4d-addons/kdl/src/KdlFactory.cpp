//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#include "KdlFactory.h"
#include "move4d/mdl/Kinematic.h"
#include "move4d/mdl/SceneObject.h"
#include "move4d/utils/KinematicHelper.hpp"
#include <kdl/chain.hpp>
#include <kdl/tree.hpp>
#include <rl/mdl/Body.h>
#include <rl/mdl/Fixed.h>
#include <rl/mdl/Joint.h>
#include <rl/mdl/Prismatic.h>
#include <rl/mdl/Revolute.h>

namespace move4d
{
namespace kin
{
namespace kdl
{

namespace convert
{

KDL::Vector toKdl(const math::Vector3 &v) { return KDL::Vector(v.x(), v.y(), v.z()); }
KDL::Vector toKdl(const math::Transform::TranslationPart &v)
{
    return KDL::Vector(v.x(), v.y(), v.z());
}
KDL::Rotation toKdlRotation(const math::Transform::LinearMatrixType &m)
{
    KDL::Rotation rot;
    for (size_t i = 0; i < 3; ++i) {
        for (size_t j = 0; j < 3; ++j) {
            rot(i, j) = m(i, j);
        }
    }
    return rot;
}
KDL::Frame toKdl(const math::Transform &x)
{
    return KDL::Frame(toKdlRotation(x.rotation()), toKdl(x.translation()));
}

} // namespace convert

INIT_MOVE4D_STATIC_LOGGER(KdlFactory, "move4d.kin.kdl.factory");

KDL::Segment KdlFactory::createSegment(rl::mdl::Kinematic *kin, size_t joint_index)
{
    auto &joint_name = kin->getJoint(joint_index)->getName();
    auto *jnt = kin->getJoint(joint_index);
    if (dynamic_cast<::rl::mdl::Revolute *>(jnt)) {
        M4D_DEBUG("joint " << jnt->getName() << " is revolute, jnt->out is "
                           << jnt->out->getName());
        assert(dynamic_cast<rl::mdl::Frame *>(jnt->out));

        // inertia
        KDL::RigidBodyInertia inertia;
        {
            auto body = dynamic_cast<rl::mdl::Body *>(jnt->out);
            if (body) {
                KDL::RotationalInertia Ic(body->ic(0, 0), body->ic(1, 1), body->ic(2, 2),
                                          body->ic(0, 1), body->ic(0, 2), body->ic(1, 2));
                inertia = KDL::RigidBodyInertia(body->m, convert::toKdl(body->cm), Ic);
            } else {
                inertia = KDL::RigidBodyInertia::Zero();
            }
        }

        // jnt origin:
        size_t fixed_jnt_origin_i;
        if (!utils::getParentTransform(kin, jnt->in, fixed_jnt_origin_i)) {
            throw std::runtime_error("malformed rl kinematic model " + M4D_FILE_LINE_FUNC);
        }
        auto fixed_origin = dynamic_cast<::rl::mdl::Fixed *>(kin->getTransform(fixed_jnt_origin_i));
        assert(fixed_origin);
        KDL::Vector origin = convert::toKdl(fixed_origin->x.transform().translation());

        // joint
        // KDL::Vector origin = convert::toKdl(jnt->x.transform().translation());
        // KDL::Vector axis = convert::toKdl(jnt->x.transform().linear() * math::Vector3::UnitX());
        math::Vector3 jnt_axis(jnt->S(0, 0), jnt->S(1, 0), jnt->S(2, 0));
        KDL::Vector axis =
            convert::toKdl(fixed_origin->x.transform().inverse().linear() * jnt_axis);
        KDL::Joint jnt_kdl(joint_name, origin, axis, KDL::Joint::RotAxis);

        // tip
        size_t tip_transform_index;
        bool ok = utils::getChildTransform(kin, jnt->out, tip_transform_index);
        // assert(ok);
        KDL::Frame tip;
        if (ok) {
            tip = convert::toKdl(kin->getTransform(tip_transform_index)->x.transform());
        }
        tip = convert::toKdl(fixed_origin->x.transform());
        return KDL::Segment(kin->getJoint(joint_index)->out->getName(), jnt_kdl, tip, inertia);
    } else {
        // TODO(Jules): other joint types for KDL
        throw std::invalid_argument("joint type not managed by KdlFactory yet");
    }
}
std::shared_ptr<KDL::Chain> KdlFactory::createChain(const mdl::Chain &chain)
{
    if (_cache.exist(chain.kinematic)) {
        return _cache.get(chain.kinematic);
    }

    auto kdl_chain = std::make_shared<KDL::Chain>();
    for (size_t i = 0; i < chain.kinematic->getJoints(); ++i) {
        kdl_chain->addSegment(createSegment(chain.kinematic.get(), i));
    }
    _cache.put(chain.kinematic, kdl_chain);
    return kdl_chain;
}

} // namespace kdl
} // namespace kin
} // namespace move4d

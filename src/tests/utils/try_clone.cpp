//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#include <list> // needed by some versions of boost

#include "move4d/utils/try_clone.hpp"
#include <boost/mpl/list.hpp>
#include <boost/test/data/test_case.hpp>
#include <boost/test/unit_test.hpp>

namespace move4d
{
namespace utils
{

struct try_clone_fixt {

    struct Base {
        virtual const char *f() = 0;
    };
    struct A : virtual public Base {
        const char *f() { return id(); }
        float _a = 1.11;

        static const char *id() { return "A"; }
    };
    struct B : virtual public Base {
        const char *f() { return id(); }
        std::string _b = "the default B value";
        static const char *id() { return "B"; }
    };
    struct AB : public A, public B {
        const char *f() { return id(); }
        static const char *id() { return "AB"; }
    };

    struct C : public AB {
        const char *f() { return id(); }
        bool _c = false;
        static const char *id() { return "C"; }
    };

    struct NoClone : public Base {
        const char *f() { return id(); }
        NoClone(const NoClone &) = delete;
        static const char *id() { return "NoClone"; }
    };

    struct D {
        const char *f() { return id(); }
        static const char *id() { return "D"; }
    };
    try_clone_fixt() {}
    virtual ~try_clone_fixt() {}

    C c;
    A a;

    Base *base_c = &c;
    Base *base_a = &a;
    A *a_a = &a;
    A *a_c = &c;
};

BOOST_FIXTURE_TEST_SUITE(try_clone_suite, try_clone_fixt)

BOOST_AUTO_TEST_SUITE(try_clone_implicit_suite)

BOOST_AUTO_TEST_CASE(implicit_self)
{
    auto res = try_clone<>(a_a);
    BOOST_CHECK_EQUAL(res->f(), a_a->f());
}

BOOST_AUTO_TEST_CASE(implicit_no_match)
{
    auto res = try_clone<A, B, AB>(base_c);
    BOOST_CHECK_EQUAL(res, nullptr);
}

BOOST_AUTO_TEST_CASE(implicit_first)
{
    auto res = try_clone<A, B, AB, C>(base_a);
    BOOST_CHECK_EQUAL(res->f(), base_a->f());
}
BOOST_AUTO_TEST_CASE(implicit_last)
{
    auto res = try_clone<B, AB, C, A>(base_a);
    BOOST_CHECK_EQUAL(res->f(), base_a->f());
}
BOOST_AUTO_TEST_CASE(implicit)
{
    auto res = try_clone<B, AB, A, C>(base_a);
    BOOST_CHECK_EQUAL(res->f(), base_a->f());
}
BOOST_AUTO_TEST_CASE(implicit_base)
{
    auto res = try_clone<AB, C>(a_a);
    BOOST_CHECK_EQUAL(res->f(), base_a->f());
}
BOOST_AUTO_TEST_CASE(implicit_empty_template_param)
{
    auto res = try_clone<>(base_a);
    BOOST_CHECK_EQUAL(res, nullptr);
}

BOOST_AUTO_TEST_SUITE_END()

// explicit
BOOST_AUTO_TEST_SUITE(try_clone_explicit_suite)

BOOST_AUTO_TEST_CASE(explicit_self)
{
    auto res = try_clone_explicit<>(a_a);
    BOOST_CHECK_EQUAL(res, nullptr);
}

BOOST_AUTO_TEST_CASE(explicit_no_match)
{
    auto res = try_clone_explicit<A, B, AB>(base_c);
    BOOST_CHECK_EQUAL(res, nullptr);
}

BOOST_AUTO_TEST_CASE(explicit_first)
{
    auto res = try_clone_explicit<A, B, AB, C>(base_a);
    BOOST_CHECK_EQUAL(res->f(), base_a->f());
}
BOOST_AUTO_TEST_CASE(explicit_last)
{
    auto res = try_clone_explicit<B, AB, C, A>(base_a);
    BOOST_CHECK_EQUAL(res->f(), base_a->f());
}
BOOST_AUTO_TEST_CASE(explicit_match)
{
    auto res = try_clone_explicit<B, AB, A, C>(base_a);
    BOOST_CHECK_EQUAL(res->f(), base_a->f());
}
BOOST_AUTO_TEST_CASE(explicit_base)
{
    auto res = try_clone_explicit<AB, C>(a_a);
    BOOST_CHECK_EQUAL(res, nullptr);
}
BOOST_AUTO_TEST_CASE(explicit_empty_template_param)
{
    auto res = try_clone_explicit<>(base_a);
    BOOST_CHECK_EQUAL(res, nullptr);
}

BOOST_AUTO_TEST_SUITE_END()
BOOST_AUTO_TEST_SUITE_END()

} // namespace utils
} // namespace move4d

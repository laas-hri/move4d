//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_UTILS_CONTAINERS_ITERATORS_H
#define MOVE4D_UTILS_CONTAINERS_ITERATORS_H

#include <boost/iterator/filter_iterator.hpp>
#include <boost/iterator/transform_iterator.hpp>
#include <utility>

namespace move4d
{
namespace iterators
{

template <typename InputIterator, typename UnaryFunction>
std::pair<boost::transform_iterator<UnaryFunction, InputIterator>,
          boost::transform_iterator<UnaryFunction, InputIterator>>
make_transform_iterator(InputIterator from, InputIterator to, UnaryFunction transform)
{
    using iterator = boost::transform_iterator<UnaryFunction, InputIterator>;
    return {iterator(from, transform), iterator(to, transform)};
}
template <typename InputIterator, typename UnaryFunction>
std::pair<boost::transform_iterator<UnaryFunction, InputIterator>,
          boost::transform_iterator<UnaryFunction, InputIterator>>
make_transform_iterator(std::pair<InputIterator, InputIterator> range, UnaryFunction transform)
{
    using iterator = boost::transform_iterator<UnaryFunction, InputIterator>;
    return {iterator(range.first, transform), iterator(range.second, transform)};
}

template <typename InputIterator, typename UnaryFunction>
std::pair<boost::filter_iterator<UnaryFunction, InputIterator>,
          boost::filter_iterator<UnaryFunction, InputIterator>>
make_filter_iterator(InputIterator from, InputIterator to, UnaryFunction predicate)
{
    using iterator = boost::filter_iterator<UnaryFunction, InputIterator>;
    return {iterator(predicate, from, to), iterator(predicate, to, to)};
}
template <typename InputIterator, typename UnaryFunction>
std::pair<boost::filter_iterator<UnaryFunction, InputIterator>,
          boost::filter_iterator<UnaryFunction, InputIterator>>
make_filter_iterator(std::pair<InputIterator, InputIterator> range, UnaryFunction predicate)
{
    using iterator = boost::filter_iterator<UnaryFunction, InputIterator>;
    return {iterator(predicate, range.first, range.second),
            iterator(predicate, range.second, range.second)};
}

} // namespace iterators

} // namespace move4d

#endif // MOVE4D_UTILS_CONTAINERS_ITERATORS_H

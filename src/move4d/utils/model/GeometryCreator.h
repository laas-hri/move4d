//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
// Created by gbuisan on 22/01/19.
//

#ifndef MOVE4D_UTILS_MODEL_GEOMETRYCREATOR_H
#define MOVE4D_UTILS_MODEL_GEOMETRYCREATOR_H

#if __has_include(<optional>)
#include <optional>
#elif __has_include(<experimental/optional>)
#include <experimental/optional>
namespace std
{
template <typename T> using optional = experimental::optional<T>;
}
#else
#error no std::optional
#endif

#include "move4d/math/Vector.h"
namespace rl
{
namespace sg
{
class Body;
}
} // namespace rl
class SoVRMLShape;

namespace move4d
{
namespace utils
{
namespace model
{

void applyMaterial(SoVRMLShape &shape, std::optional<Eigen::Vector4f> &color);

void addSphereToBody(float radius, rl::sg::Body *body, const move4d::math::Transform &pos,
                     std::optional<Eigen::Vector4f> color = {});
void addCylinderToBody(float radius, float length, rl::sg::Body *body,
                       const move4d::math::Transform &pos,
                       std::optional<Eigen::Vector4f> color = {});
void addBoxToBody(const Eigen::Vector3f &size, rl::sg::Body *body,
                  const move4d::math::Transform &pos, std::optional<Eigen::Vector4f> color = {});

} // namespace model
} // namespace utils
} // namespace move4d

#endif // MOVE4D_UTILS_MODEL_GEOMETRYCREATOR_H

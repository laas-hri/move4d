//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef TRY_CLONE_HPP
#define TRY_CLONE_HPP

#include "move4d/utils/try_clone_internal.hpp"
#include <type_traits>
#include <typeinfo>

/**
 * try_clone creates a copy of from if its exact type is one of Types...,
 * by calling the copy constructor.
 *
 * try_clone also tries to copy from as a Base if Base is copy constructible (and from is of exact
 * type Base)
 *
 * typeid is used to check types. An object should never be copied with the copy constructor of one
 * of its parent class
 */
template <class... Types, class Base> Base *try_clone(const Base *from)
{
    return _try_clone_internal<Base, Types...>(from);
}

/**
 * try_clone_explicit is same as try_clone, but does not try to copy as a Base
 * (only Types... are considered)
 */
template <class... Types, class Base> Base *try_clone_explicit(const Base *from)
{
    return _try_clone_explicit_internal<Base, Types...>(from);
}
#endif // TRY_CLONE_HPP

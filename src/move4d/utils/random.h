//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_UTILS_RANDOM_H
#define MOVE4D_UTILS_RANDOM_H

#include "move4d/Singleton.h"
#include "move4d/common.h"
#include "move4d/math/Vector.h"
#include <random>

namespace move4d
{

class random : public move4d::Singleton<random>
{
    MOVE4D_STATIC_LOGGER;

  protected:
    random();
    friend class Singleton<random>;

  public:
    random(const random &) = delete;
    random(random &&) = delete;
    random &operator=(const random &) = delete;
    random &operator=(random &&) = delete;
    virtual ~random() = default;

    void seed(std::random_device::result_type s);
    Real uniform();
    Real uniform(Real from, Real to);

    template <typename T> T integer()
    {
        std::uniform_int_distribution<T> dist;
        return dist(_engine);
    }
    template <typename T> T integer(T from, T to)
    {
        std::uniform_int_distribution<T> dist(from, to);
        return dist(_engine);
    }

    /// uniform sampling on a 2D disc of given radius
    /// @todo: implemented using trigonometric functions and sqrt. Maybe rejection sampling would
    /// be faster
    math::Vector2 uniform_disc(Real radius);
    /// shoot in an annulus (ring)
    math::Vector2 uniform_annulus(Real inner_radius, Real outter_radius);

  protected:
    std::mt19937 _engine;
};

} // namespace move4d

#endif // MOVE4D_UTILS_RANDOM_H

//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_PLAN_KINEMATICGRAPHSEARCHSPACE_H
#define MOVE4D_PLAN_KINEMATICGRAPHSEARCHSPACE_H

#include "move4d/common.h"
#include "move4d/mdl/SceneObject.h"
#include "move4d/mdl/Types.h"
#include "move4d/plan/RobotSpace.h"
#include "move4d/plan/SearchSpace.h"
#include "move4d/plan/Types.h"
#include "move4d/utils/containers/DofsView.h"
#include "move4d/utils/containers/iterators.h"
#include "move4d/utils/containers/ptr_wrapper.hpp"
#include <boost/optional/optional.hpp>

namespace move4d
{
namespace plan
{

class KinematicSpaceWrapper : public RobotKinematicGroupSpace
{
  public:
    KinematicSpaceWrapper(std::unique_ptr<SearchSpace> other, const mdl::Movable *object,
                          const std::string &from_frame, const std::string &to_frame,
                          const std::string &name = "");
    std::unique_ptr<SearchSpace> cloneSearchSpace() const override
    {
        return std::make_unique<KinematicSpaceWrapper>(*this);
    }
    void shootUniform(mdl::WorldState &ws) const override { _space->shootUniform(ws); }

    const SearchSpace &getSpace() const;

  private:
    static std::unique_ptr<mdl::Chain> _createChain(const SearchSpace &space,
                                                    const mdl::Movable &object,
                                                    const std::string &from_frame,
                                                    const std::string &to_frame);

    ptr_wrapper<SearchSpace> _space;
    std::pair<math::Vector3, math::Vector3> _pose_bounds;
}; // namespace plan

/**
 * @brief BaseSampler is a virtual class for implementing generic samplers.
 */
class BaseSampler
{
  public:
    BaseSampler(std::shared_ptr<const SearchSpace> space) : _space(std::move(space)) {}
    /**
     * @brief sampleUniform
     * @param[in,out] conf_in_out
     * @param[in] base_pose
     * @param[out] end_pose_out
     * @param[in] input_poses
     */
    virtual void sampleUniform(views::DofsView &conf_in_out, const math::Transform &base_pose,
                               math::Transform &end_pose_out,
                               const std::vector<math::Transform> &input_poses) = 0;
    virtual void sampleUniformNear(views::DofsView &conf_in_out, const math::Transform &base_pose,
                                   math::Transform &end_pose_out,
                                   const std::vector<math::Transform> &input_poses,
                                   views::DofsView &near, Real distance) = 0;
    virtual void sampleGaussian(views::DofsView &conf_in_out, const math::Transform &base_pose,
                                math::Transform &end_pose_out,
                                const std::vector<math::Transform> &input_poses,
                                views::DofsView &mean, Real stdev) = 0;

  protected:
    std::shared_ptr<const SearchSpace> _space;
};

using SamplerAllocator = std::function<BaseSamplerPtr(std::shared_ptr<const SearchSpace>)>;

/**
 * @brief can build functional KinematicGraphSearchSpace from an abstract description of the
 * problem.
 *
 * The user passes in a description of its kinematic graph by adding elements with following
 * methods:
 *  - \ref addForwardChain
 *  - \ref addInverseChain
 *  - \ref addFixedTransform
 *
 * The user the calls the \ref build method, that will compute the data to pass to the
 * KinematicGraphSearchSpace constructor.
 * The \ref build method computes a dependency graph from the kinematic graph, using the following
 * rules:
 *  - an inverse kinematic chain configuration depends on its base and end positions;
 *  - a forward kinematic chain configuration depends on nothing;
 *  - the tip position of a forward chain depends on the chain configuration and the chain
 * base position;
 *  - a fixed transform is considered as two opposite forward chains with a configuration of
 * dimension 0, hence a fixed transform creates a cycle in the dependency graph.
 * `addFixedTransform(A,B,T)` creates the cycle `B->T->A->T.inv->B` (`->` = *depends on*,
 * )
 * - \ref withSampler creates a dependency between the chain tip and every argument
 *
 * An order for solving the components (sampling the configurations and positions) is then deduced
 * from the dependency graph. A Tarjan algorithm is used to extract a topological order of strongly
 * connected components, then a breadth-first search is used to determine the order inside strongly
 * connected components.
 */
class KinematicGraphSearchSpaceBuilder
{
    MOVE4D_STATIC_LOGGER;

  public:
    class Exception : public std::runtime_error
    {
      protected:
        Exception(const std::string &args) : std::runtime_error(args) {}
    };
    /// exception thrown by selection methods when no matching space is found
    class NoMatch : public Exception
    {
      public:
        NoMatch(const std::string &space_name);
        NoMatch(const std::string &source_name, const std::string &target_name);
    };
    /// exception thrown by selection methods when multiple spaces match the specification
    class MultipleMatch : public Exception
    {
      public:
        MultipleMatch(const std::string &space_name, unsigned int count);
        MultipleMatch(const std::string &source_name, const std::string &target_name,
                      unsigned int count);
    };

    KinematicGraphSearchSpaceBuilder();
    ~KinematicGraphSearchSpaceBuilder();
    KinematicGraphSearchSpaceBuilder(const KinematicGraphSearchSpaceBuilder &other);
    static const std::string Fixed;

    /**
     * @brief addForwardChain
     * @param chain const ref to the kinematic chain space, will be copied.
     * @param from_vertex vertex name or Fixed
     * @param to_vertex vertex name
     * @return *this
     */
    KinematicGraphSearchSpaceBuilder &addForwardChain(const RobotKinematicGroupSpace &chain,
                                                      std::string base_vertex,
                                                      std::string tip_vertex);
    /**
     * @brief addInverseChain
     * @param chain const ref to the kinematic chain space. will be copied.
     * @param from_vertex vertex name or Fixed
     * @param to_vertex vertex name
     * @return
     */
    KinematicGraphSearchSpaceBuilder &addInverseChain(const RobotKinematicGroupSpace &chain,
                                                      std::string base_vertex,
                                                      std::string tip_vertex);
    /**
     * @brief addFixedTransform
     * @param from_vertex vertex name or Fixed
     * @param to_vertex vertex name
     * @return *this
     *
     * It is here to mention that there is a transform between two spaces, the transform
     * is obtained from the worldstate passed to methods needing it.
     */
    KinematicGraphSearchSpaceBuilder &
    addFixedTransform(std::string from_vertex, std::string to_vertex, math::Transform transform);
    /**
     * @brief use this to attach an object to a vertex.
     * @param se3space const ref to the space, will be copied.
     * @param vertex
     * @return *this
     *
     * This is primarily meant to attach manipulated objects to end-effectors. The object
     * will be set to the vertex position before performing collision checking. The provided
     * space is not used in the planning process otherwise (it just moves along with the
     * other parts).
     */
    KinematicGraphSearchSpaceBuilder &attachSE3ToVertex(const SE3Space &se3space,
                                                        std::string vertex);
    /**
     * @brief addSpace
     * @param space
     * @param from_vertex
     * @param to_vertex
     * @return
     */
    /**
     * @brief withSampler specifies a sampler for the last added space.
     * @return
     */
    KinematicGraphSearchSpaceBuilder &withSampler(SamplerAllocator sampler_allocator,
                                                  std::vector<std::string> args);

    KinematicGraphSearchSpaceBuilder &withLimits(const math::Vector &low, const math::Vector &high);

    /// @brief set the name of the space
    KinematicGraphSearchSpaceBuilder &withName(std::string name);
    const std::string &getName() const;

    /**
     * @brief select a space to apply modifiers on it (withSampler, withLimits)
     * @param space_name the name of the space to select
     * @return *this
     * @throw MultipleMatch in case of multiple match
     * @throw NoMatch in case of no match
     *
     * Example:
     * ```{.cpp}
     * builder.addForwardChain(space1, "a", "b")
     *        .withSampler(spl_alloc_1,{})
     *        .addFInverseChain(space2, "c", "b")
     * ```
     * is equivalent to:
     * ```{.cpp}
     * builder.addForwardChain(space1, "a", "b")
     *        .addFInverseChain(space2, "c", "b")
     * builder.selectByName(space1.getName())
     *        .withSampler(spl_alloc_1,{})
     * // alternatively:
     * builder.selectByEndNames("a","b")
     *        .withSampler(spl_alloc_1,{})
     * ```
     *
     * @see selectByEndNames
     *
     */
    KinematicGraphSearchSpaceBuilder &selectByName(const std::string &space_name);
    /**
     * @brief selectByEndNames
     * @param base_vertex source (base) vertex name
     * @param tip_vertex target (end effector, tip) vertex name
     * @return
     * @throw MultipleMatch in case of multiple match
     * @throw NoMatch in case of no match
     */
    KinematicGraphSearchSpaceBuilder &selectByEndNames(const std::string &base_vertex,
                                                       const std::string &tip_vertex);
    /**
     * @brief resetSpace changes the selected space by this one
     * @param chain
     * @return *this
     */
    KinematicGraphSearchSpaceBuilder &resetSpace(const RobotKinematicGroupSpace &chain);

    /**
     * @brief builds the KinematicGraphSearchSpace
     * @return
     */
    std::unique_ptr<KinematicGraphSearchSpace> build() const;

  private:
    std::string _name;
    struct PImpl;
    std::unique_ptr<PImpl> _pimpl;
};

inline gsl::owner<KinematicGraphSearchSpaceBuilder *>
new_clone(const KinematicGraphSearchSpaceBuilder *t)
{
    return new KinematicGraphSearchSpaceBuilder(*t);
}

class KinematicGraphException : public std::runtime_error
{
  public:
    KinematicGraphException(const std::string &what) : std::runtime_error(what) {}
};

/**
 * @brief The KinematicGraphSearchSpace class represents a heterogeneous kinematic graph.
 */
class KinematicGraphSearchSpace : public SearchSpace
{
    MOVE4D_STATIC_LOGGER;

  public:
    using Vertex = std::string;
    struct Edge {
        enum { Forward, Inverse, Fixed } type;
        ptr_wrapper<SearchSpace> space;
        SamplerAllocator sampler_allocator; ///< can be empty/unset
        std::vector<std::string> sampler_args;
        boost::optional<math::Transform> transform;
        boost::optional<math::Transform> transform_inverse;
        Vertex source;
        Vertex target;
    };

    using Attachments = std::map<Vertex, std::vector<std::shared_ptr<SE3Space>>>;

    KinematicGraphSearchSpace() = delete;
    KinematicGraphSearchSpace(const KinematicGraphSearchSpace &) = default;
    KinematicGraphSearchSpace(KinematicGraphSearchSpace &&) = default;
    KinematicGraphSearchSpace &operator=(const KinematicGraphSearchSpace &) = default;
    KinematicGraphSearchSpace &operator=(KinematicGraphSearchSpace &&) = default;

    /// @see KinematicGraphSearchSpaceBuilder for building
    KinematicGraphSearchSpace(std::vector<Edge> order, Attachments attachments,
                              std::string name = "");
    virtual ~KinematicGraphSearchSpace();

    std::unique_ptr<SearchSpace> cloneSearchSpace() const override
    {
        assert(typeid(*this) == typeid(KinematicGraphSearchSpace) &&
               "must reimplement in subclasses");
        return std::make_unique<KinematicGraphSearchSpace>(*this);
    }
    void shootUniform(mdl::WorldState &ws) const override;
    bool shootUniform(mdl::WorldState &ws, std::unordered_map<std::string, math::Transform> &poses,
                      size_t edge_i) const;

    using const_iterator = std::vector<Edge>::const_iterator;

    const Edge &getEdge(size_t edge_i) const { return _order[edge_i]; }
    inline const std::string &getSource(size_t edge_i) const { return _order[edge_i].source; }
    inline const std::string &getTarget(size_t edge_i) const { return _order[edge_i].target; }

    const_iterator begin() const { return _order.begin(); }
    const_iterator end() const { return _order.end(); }

    inline const std::vector<Edge> &order() const { return _order; }
    inline const Attachments &attachments() const { return _attachments; }
    inline const std::vector<std::string> &verticesOrder() const { return _vertices_order; }

    std::vector<std::pair<size_t, size_t>>
    computeEdgeBijectionTo(const KinematicGraphSearchSpace &other) const;
    std::vector<std::pair<size_t, size_t>>
    computeVertexBijectionTo(const KinematicGraphSearchSpace &other) const;

  protected:
    /// @brief eventually create, and/or return the sampler for the given edge.
    /// if there is no sampler allocator for this edge, throw an instance of KinematicGraphException
    BaseSampler &sampler(size_t edge_i);
    /// same as the non-const version but throw if the sampler is not already assigned
    BaseSampler &sampler(size_t edge_i) const;

  private:
    void initDegreeOfFreedomList();
    void initVerticesOrder();
    void resetSamplers();
    std::vector<Edge> _order;
    std::map<size_t, BaseSamplerPtr> _samplers;
    std::vector<std::string> _vertices_order;
    Attachments _attachments;
};

} // namespace plan
} // namespace move4d

#endif // MOVE4D_PLAN_KINEMATICGRAPHSEARCHSPACE_H

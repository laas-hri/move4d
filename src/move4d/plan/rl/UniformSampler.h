//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_PLAN_RL_UNIFORMSAMPLER_H
#define MOVE4D_PLAN_RL_UNIFORMSAMPLER_H

#include "move4d/plan/RobotSpace.h"
#include <rl/plan/Sampler.h>

namespace move4d
{
namespace plan
{
namespace rl
{

class UniformSampler : public ::rl::plan::Sampler
{
  public:
    UniformSampler(std::shared_ptr<RobotSpace> &space);

    virtual ~UniformSampler() {}

    virtual ::rl::math::Vector generate() override;

  protected:
    ::rl::math::Vector m_positionNormalized;
    std::shared_ptr<RobotSpace> m_space;
};

} // namespace rl
} // namespace plan
} // namespace move4d

#endif // MOVE4D_PLAN_RL_UNIFORMSAMPLER_H

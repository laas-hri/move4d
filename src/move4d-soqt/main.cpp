//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//

/**
 * @file
 * @brief this file should provide a MWE for using move4d with it's SoQt interface
 */
#include <move4d-soqt/Application.h>
#include <move4d/mdl/CollisionScene.h>
#include <move4d/mdl/Scene.h>

#include <boost/program_options.hpp>

using namespace move4d;

class Application
{
  public:
    Application(int argc, char *argv[]);

    void run();
    void start();

  protected:
    std::unique_ptr<SoQt::Application> _app;
};

Application::Application(int argc, char *argv[])
{
    // use the first collision engine available -- you probably want to use a different one
    std::string engine = mdl::CollisionSceneFactory::instance().getTypes().at(0);

    namespace po = boost::program_options;
    po::options_description desc("Allowed options");
    // clang-format off
    desc.add_options()("help,h", "produce help message")
            ("file,f", po::value<std::vector<std::string>>(), "load a file")
            ("view-collisions","")
            ("vc", "display collision bodies in the viewer")
            ("engine,c", po::value<std::string>(), "collision engine to use");
    // clang-format on
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")) {
        std::cout << desc << std::endl;
        move4d::Stop("");
        return;
    }

    if (vm.count("engine")) {
        engine = vm["engine"].as<std::string>();
    }
    _app = std::make_unique<move4d::SoQt::Application>(engine, std::bind(&Application::run, this));

    if (vm.count("view-collisions") || vm.count("vc")) {
        _app->setLoadCollisionsInView(true);
    }

    if (vm.count("file")) {
        // load a model file that can be used for computation and that will be displayed
        for (auto f : vm["file"].as<std::vector<std::string>>()) {
            _app->loadFile(f);
        }
    }
    _app->getScene()->resetCollisionWhiteList();
}
void Application::run() {}
void Application::start() { _app->start(); }

int main(int argc, char *argv[])
{
    Application app(argc, argv);
    app.start();
    return 0;
}

//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_PLAN_OMPL_PLANNER_H
#define MOVE4D_PLAN_OMPL_PLANNER_H

#include "move4d/motion/Types.h"
#include "move4d/plan/Planner.h"
#include "move4d/plan/ompl/Types.h"

namespace ompl
{
namespace base
{
class Planner;
using PlannerPtr = std::shared_ptr<Planner>;
class ProblemDefinition;
using ProblemDefinitionPtr = std::shared_ptr<ProblemDefinition>;
class SpaceInformation;
using SpaceInformationPtr = std::shared_ptr<SpaceInformation>;
class Path;
using PathPtr = std::shared_ptr<Path>;
} // namespace base

} // namespace ompl
namespace move4d
{
namespace plan
{
namespace ompl
{

class Planner : public move4d::plan::Planner
{
    MOVE4D_STATIC_LOGGER;

  public:
    /**
     * @brief Planner
     * @param space
     * @param collisionManager
     * @param plannerType name of the OMPL planner to use (see PlannerFactory.h)
     *
     * \see PlannerFactory.h to see the available types
     */
    Planner(const std::shared_ptr<SearchSpace> &space,
            const mdl::CollisionManagerPtr &collisionManager, const std::string &plannerType);

    bool solve(float timeLimit) override;

    StateSpaceWrapperPtr getSpaceWrapper() const;

    ::ompl::base::PathPtr getPathOmpl(bool simplified = false, double max_time = 0) const;
    motion::GeometricPathPtr getPath(bool simplified = false, double max_time = 0) const;

  protected:
    ::ompl::base::PlannerPtr m_planner;
    StateSpaceWrapperPtr m_spaceWrapper;
    ::ompl::base::SpaceInformationPtr m_spaceInformation;
    ::ompl::base::ProblemDefinitionPtr m_problemDefinition;
};

} // namespace ompl
} // namespace plan
} // namespace move4d

#endif // MOVE4D_PLAN_OMPL_PLANNER_H

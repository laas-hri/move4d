//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#include "move4d/plan/ompl/converters/StateSpaceFactory.h"
#include "move4d/mdl/DegreeOfFreedomInfo.h"
#include "move4d/plan/ClosedLoopSearchSpace.h"
#include "move4d/plan/KinematicGraphSearchSpace.h"
#include "move4d/plan/RobotSpace.h"
#include "move4d/plan/SO3Space.h"
#include "move4d/plan/SearchSpace.h"
#include "move4d/plan/ompl/KinematicGraphStateSpace.h"
#include "move4d/plan/ompl/spaces/SO3StateSpaceBounded.h"
#include "move4d/utils/io/print_to_stream.hpp"
#include <ompl/base/spaces/DubinsStateSpace.h>
#include <ompl/base/spaces/RealVectorStateSpace.h>
#include <ompl/base/spaces/ReedsSheppStateSpace.h>
#include <ompl/base/spaces/SE2StateSpace.h>
#include <ompl/base/spaces/SE3StateSpace.h>
#include <ompl/base/spaces/SO2StateSpace.h>
#include <ompl/base/spaces/SO3StateSpace.h>

namespace move4d
{
namespace plan
{
namespace ompl
{
namespace convert
{

::ompl::base::StateSpacePtr SearchSpaceFactory::createSpace(const SearchSpace &searchSpace)
{
    switch (searchSpace.getType()) {
    case SearchSpace::Type::KinematicGroup:
    case SearchSpace::Type::RealVector: {
        auto space =
            std::make_shared<::ompl::base::RealVectorStateSpace>(searchSpace.getDimension());
        ::ompl::base::RealVectorBounds bounds(searchSpace.getDimension());
        for (size_t i = 0; i < searchSpace.getDimension(); ++i) {
            bounds.low[i] = searchSpace.getDegreesOfFreedom()[i].getMinimum();
            bounds.high[i] = searchSpace.getDegreesOfFreedom()[i].getMaximum();
        }
        space->setBounds(bounds);
        space->setName(searchSpace.getName());

        return space;
    }
    case SearchSpace::Type::ClosedLoop: {
        auto space = std::make_shared<::ompl::base::CompoundStateSpace>();
        auto closed_loop = dynamic_cast<const ClosedLoopSearchSpace &>(searchSpace);
        if (closed_loop.getPre()) {
            space->addSubspace(createSpace(*closed_loop.getPre()), 1.);
        }
        if (closed_loop.getFkChain()) {
            space->addSubspace(
                createSpace(dynamic_cast<const SearchSpace &>(*closed_loop.getFkChain())), 1.);
        }
        for (auto &s : closed_loop.getIkChains()) {
            space->addSubspace(createSpace(dynamic_cast<const SearchSpace &>(*s)), 1.);
        }
        if (closed_loop.getPost()) {
            space->addSubspace(createSpace(*closed_loop.getPost()), 1.);
        }
        space->setName(searchSpace.getName());
        return space;
    }
    case SearchSpace::Type::SO2: {
        auto space = std::make_shared<::ompl::base::SO2StateSpace>();
        space->setName(searchSpace.getName());
        return space;
    }
    case SearchSpace::Type::SE2: {
        auto space = std::make_shared<::ompl::base::SE2StateSpace>();
        ::ompl::base::RealVectorBounds bounds(2);
        for (size_t i = 0; i < 2; ++i) {
            bounds.low[i] = searchSpace.getDegreesOfFreedom()[i].getMinimum();
            bounds.high[i] = searchSpace.getDegreesOfFreedom()[i].getMaximum();
        }
        space->setBounds(bounds);
        space->setName(searchSpace.getName());
        return space;
    }
    case SearchSpace::Type::Dubins: {
        auto space = std::make_shared<::ompl::base::DubinsStateSpace>();
        ::ompl::base::RealVectorBounds bounds(2);
        for (size_t i = 0; i < 2; ++i) {
            bounds.low[i] = searchSpace.getDegreesOfFreedom()[i].getMinimum();
            bounds.high[i] = searchSpace.getDegreesOfFreedom()[i].getMaximum();
        }
        space->setBounds(bounds);
        space->setName(searchSpace.getName());
        return space;
    }
    case SearchSpace::Type::ReedsShepp: {
        auto space = std::make_shared<::ompl::base::ReedsSheppStateSpace>();
        ::ompl::base::RealVectorBounds bounds(2);
        for (size_t i = 0; i < 2; ++i) {
            bounds.low[i] = searchSpace.getDegreesOfFreedom()[i].getMinimum();
            bounds.high[i] = searchSpace.getDegreesOfFreedom()[i].getMaximum();
        }
        space->setBounds(bounds);
        space->setName(searchSpace.getName());
        return space;
    }
    case SearchSpace::Type::SO3: {
        if (searchSpace.as<SO3Space>()->isBounded()) {
            auto s =
                std::make_shared<SO3StateSpaceBounded>(searchSpace.as<SO3Space>()->getReference(),
                                                       searchSpace.as<SO3Space>()->getTolerance());
            s->setName(searchSpace.getName());
            return s;
        } else {
            auto s = std::make_shared<::ompl::base::SO3StateSpace>();
            s->setName(searchSpace.getName());
            return s;
        }
    }
    case SearchSpace::Type::SE3: {
        auto space = std::make_shared<::ompl::base::SE3StateSpace>();
        ::ompl::base::RealVectorBounds bounds(3);
        for (size_t i = 0; i < 3; ++i) {
            bounds.low[i] = searchSpace.getDegreesOfFreedom()[i].getMinimum();
            bounds.high[i] = searchSpace.getDegreesOfFreedom()[i].getMaximum();
        }
        space->setBounds(bounds);
        space->setName(searchSpace.getName());
        return space;
    }
    case SearchSpace::Type::Compound: {
        auto space = std::make_shared<::ompl::base::CompoundStateSpace>();
        auto compound = dynamic_cast<const CompoundSearchSpace &>(searchSpace);
        for (const auto &subspace : compound.getSubspaces()) {
            space->addSubspace(createSpace(subspace), 1.);
        }
        space->setName(searchSpace.getName());
        return space;
    }
    case SearchSpace::Type::KinematicGraph: {
        auto space = std::make_shared<KinematicGraphStateSpace>(
            dynamic_cast<const KinematicGraphSearchSpace &>(searchSpace));
        space->setName(searchSpace.getName());
        return space;
    }
    default:
        throw std::invalid_argument("unsupported SearchSpace type");
    }
}

StateSpaceWrapperPtr SearchSpaceFactory::create(const SearchSpace &searchSpace)
{
    auto space = createSpace(searchSpace);
    auto w = std::make_shared<StateSpaceWrapper>(searchSpace.cloneSearchSpace(), space);
    auto ss = dynamic_cast<StateSpace *>(w->getStateSpace().get());
    if (ss)
        ss->setWrapper(w);
    return w;
}

StateSpaceWrapperPtr SearchSpaceFactory::createForPlanning(const SearchSpace &searchSpace)
{
    switch (searchSpace.getType()) {
    case SearchSpace::Type::KinematicGraph:
        return create(searchSpace);
    default:
        auto wrapper = StateSpaceWrapper::create(searchSpace.cloneSearchSpace(),
                                                 std::make_shared<StateSpace>(searchSpace));
        wrapper->getStateSpace()->as<StateSpace>()->setWrapper(wrapper);
        return wrapper;
    }
}

} // namespace convert

INIT_MOVE4D_STATIC_LOGGER(StateSpaceWrapper, "move4d.plan.ompl.statespacewrapper");

StateSpaceWrapper::StateSpaceWrapper(std::shared_ptr<const SearchSpace> searchSpace,
                                     ::ompl::base::StateSpacePtr ss)
    : m_searchSpace(std::move(searchSpace)), m_stateSpace(std::move(ss))
{
}

StateSpaceWrapperPtr StateSpaceWrapper::create(std::shared_ptr<const SearchSpace> searchSpace,
                                               ::ompl::base::StateSpacePtr ss)
{
    Expects(ss);
    Expects(dynamic_cast<StateSpace *>(ss.get()));
    auto wrapper = std::make_shared<StateSpaceWrapper>(searchSpace, ss);
    ss->as<StateSpace>()->setWrapper(wrapper);
    if (searchSpace->getStateSamplerAllocator()) {
        ss->setStateSamplerAllocator(searchSpace->getStateSamplerAllocator());
    }
    return wrapper;
}

void StateSpaceWrapper::applyState(const ::ompl::base::State *state, mdl::WorldState &ws) const
{
    const auto &locs = m_stateSpace->getValueLocations();
    M4D_ASSERT_MSG(locs.size() != 0 || m_stateSpace->getDimension() == 0,
                   "state space " << m_stateSpace->getName() << " is probably not setup");
    assert(locs.size() == m_searchSpace->getDegreesOfFreedom().size());
    for (std::size_t i = 0; i < locs.size(); ++i) {
        M4D_TRACE("apply " << i << "=" << *m_stateSpace->getValueAddressAtLocation(state, locs[i])
                           << " to " << m_searchSpace->getDegreesOfFreedom()[i].getObjectId() << " "
                           << m_searchSpace->getDegreesOfFreedom()[i].index());
        m_searchSpace->getDegreesOfFreedom()[i].setIn(
            ws, *m_stateSpace->getValueAddressAtLocation(state, locs[i]));
    }
    applyAttachments(dynamic_cast<KinematicGraphStateSpace *>(m_stateSpace.get()), state, ws);
}

void StateSpaceWrapper::applyAttachments(KinematicGraphStateSpace *space,
                                         const ::ompl::base::State *state_,
                                         mdl::WorldState &ws) const
{
    if (!space)
        return;

    auto state = state_->as<KinematicGraphStateSpace::StateType>();

    M4D_TRACE("apply " << space->getAttachments().size() << " attachments");
    for (auto &attachment : space->getAttachments()) {
        M4D_TRACE("attach " << attachment.first->getSearchSpace()->getName() << " to "
                            << attachment.first->getStateSpace()->getName());
        M4D_TRACE(PRINT_TO_STREAM(
            attachment.first->getStateSpace()->printState(state->poses[attachment.second], oss)));
        attachment.first->applyState(state->poses[attachment.second], ws);
    }
}

::ompl::base::ScopedState<> StateSpaceWrapper::extractState(const mdl::WorldState &ws) const
{
    ::ompl::base::ScopedState<> state(m_stateSpace);
    // assert(dynamic_cast<StateSpace::StateType *>(state.get()));
    // assert(state->as<StateSpace::StateType>()->flags == 0);
    const auto &locs = m_stateSpace->getValueLocations();
    M4D_ASSERT_MSG(locs.size() != 0 || m_stateSpace->getDimension() == 0,
                   "state space " << m_stateSpace->getName() << " is probably not setup");
    assert(locs.size() == m_searchSpace->getDegreesOfFreedom().size());
    for (std::size_t i = 0; i < locs.size(); ++i) {
        *m_stateSpace->getValueAddressAtLocation(state.get(), locs[i]) =
            m_searchSpace->getDegreesOfFreedom()[i].getFrom(ws);
    }
    if (dynamic_cast<KinematicGraphStateSpace::StateType *>(state.get())) {
        // TODO(Jules): enforceBounds for debug?
        m_stateSpace->enforceBounds(state.get());
        m_stateSpace->as<KinematicGraphStateSpace>()->computeFK(
            state.get()->as<KinematicGraphStateSpace::StateType>());
        // TODO(Jules): here we assume IkComputed and Ok for states extracted from worldstate
        // (extractState)
        state.get()->as<KinematicGraphStateSpace::StateType>()->flags |=
            KinematicGraphStateSpace::StateType::IkComputed;
    }
    return state;
}

::ompl::base::ScopedState<> StateSpaceWrapper::stateFromValues(const math::Vector &values) const
{
    ::ompl::base::ScopedState<> state(m_stateSpace);
    // assert(state->as<StateSpace::StateType>()->flags == 0);
    const auto &locs = m_stateSpace->getValueLocations();
    M4D_ASSERT_MSG(locs.size() != 0 || m_stateSpace->getDimension() == 0,
                   "state space " << m_stateSpace->getName() << " is probably not setup");
    assert(locs.size() == m_searchSpace->getDegreesOfFreedom().size());
    for (std::size_t i = 0; i < locs.size(); ++i) {
        *m_stateSpace->getValueAddressAtLocation(state.get(), locs[i]) = values[i];
    }
    if (dynamic_cast<KinematicGraphStateSpace::StateType *>(state.get())) {
        // TODO(Jules): enforceBounds for debug?
        m_stateSpace->enforceBounds(state.get());
        m_stateSpace->as<KinematicGraphStateSpace>()->computeFK(
            state.get()->as<KinematicGraphStateSpace::StateType>());
        // TODO(Jules): here we assume IkComputed and Ok for states extracted from values
        // (stateFromValues)
        state.get()->as<KinematicGraphStateSpace::StateType>()->flags |=
            KinematicGraphStateSpace::StateType::IkComputed;
    }
    return state;
}

math::Vector StateSpaceWrapper::valuesFromState(const ::ompl::base::State *state) const
{
    const auto &locs = m_stateSpace->getValueLocations();
    M4D_ASSERT_MSG(locs.size() != 0 || m_stateSpace->getDimension() == 0,
                   "state space " << m_stateSpace->getName() << " is probably not setup");
    assert(locs.size() == m_searchSpace->getDegreesOfFreedom().size());
    math::Vector values(locs.size() + m_searchSpace->getAdditionalDofs().size());
    for (std::size_t i = 0; i < locs.size(); ++i) {
        values[i] = *m_stateSpace->getValueAddressAtLocation(state, locs[i]);
    }

    appendAttachmentValues(state, values, locs.size());

    return values;
}

void StateSpaceWrapper::appendAttachmentValues(const ::ompl::base::State *state_, math::Vector &v,
                                               size_t first_index) const
{
    auto space = dynamic_cast<const KinematicGraphStateSpace *>(m_stateSpace.get());
    if (!space)
        return;

    auto state = state_->as<KinematicGraphStateSpace::StateType>();
    for (auto &attachment : space->getAttachments()) {
        auto x = attachment.first->valuesFromState(state->poses[attachment.second]);
        for (Eigen::Index i = 0; i < x.size(); ++i) {
            v[first_index + i] = x[i];
        }
        first_index += x.size();
    }
}

const std::shared_ptr<::ompl::base::StateSpace> &StateSpaceWrapper::getStateSpace() const
{
    return m_stateSpace;
}

const std::shared_ptr<const SearchSpace> &StateSpaceWrapper::getSearchSpace() const
{
    return m_searchSpace;
}

// namespace convert
} // namespace ompl
} // namespace plan
} // namespace move4d

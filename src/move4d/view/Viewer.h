//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_VIEW_VIEWER_H
#define MOVE4D_VIEW_VIEWER_H

#include "move4d/math/Vector.h"
#include "move4d/view/ViewerInterface.h"
#include <vector>

namespace rl
{
namespace sg
{
class Scene;
}
} // namespace rl
namespace rl
{
namespace plan
{
class Model;
}
} // namespace rl
namespace move4d
{
namespace mdl
{
class WorldPositions;
}
} // namespace move4d
namespace move4d
{
namespace mdl
{
class WorldState;
}
} // namespace move4d
namespace move4d
{
namespace mdl
{
class Scene;
}
} // namespace move4d
namespace move4d
{
namespace mdl
{
class State;
}
} // namespace move4d

namespace move4d
{

namespace view
{

class Viewer : public ViewerInterface
{
  protected:
    std::shared_ptr<rl::sg::Scene> m_scene;

    std::vector<std::shared_ptr<rl::plan::Model>> m_models;

  public:
    virtual ~Viewer() = default;

    virtual const std::shared_ptr<rl::plan::Model> &create();

    virtual void update(const move4d::mdl::WorldPositions &positions) override;

    virtual void update(const move4d::mdl::WorldState &ws) override;

    virtual void drawConfiguration(std::size_t robot_index, const move4d::math::Vector &q) override;

    virtual void drawState(const move4d::mdl::State &state) override;

    virtual void setScene(std::shared_ptr<rl::sg::Scene> scene);

    virtual void updateModels() {}
    virtual void add(std::shared_ptr<rl::plan::Model> model);

  protected:
    virtual void paint() = 0;

    // rl::plan::Viewer interface
  public:
    void drawConfiguration(const rl::math::Vector &q) override;
    void drawConfigurationEdge(const rl::math::Vector &q0, const rl::math::Vector &q1,
                               const bool &free) override;
    void drawConfigurationPath(const rl::plan::VectorList &path) override;
    void drawConfigurationVertex(const rl::math::Vector &q, const bool &free) override;
    void drawLine(const rl::math::Vector &xyz0, const rl::math::Vector &xyz1) override;
    void drawPoint(const rl::math::Vector &xyz) override;
    void drawSphere(const rl::math::Vector &center, const rl::math::Real &radius) override;
    void drawWork(const rl::math::Transform &t) override;
    void drawWorkEdge(const rl::math::Vector &q0, const rl::math::Vector &q1) override;
    void drawWorkPath(const rl::plan::VectorList &path) override;
    void drawWorkVertex(const rl::math::Vector &q) override;
    void reset() override;
    void resetEdges() override;
    void resetLines() override;
    void resetPoints() override;
    void resetSpheres() override;
    void resetVertices() override;
    void showMessage(const std::string &message) override;
};

} // namespace view

} // namespace move4d
#endif

//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#include "RobotSpace.h"
#include "move4d/mdl/DegreeOfFreedomReference.h"
#include "move4d/mdl/Movable.h"
#include "move4d/mdl/WorldState.h"
#include "move4d/mdl/kin/KinematicSolver.h"
#include "move4d/utils/random.h"
#include <rl/mdl/Kinematic.h>

namespace move4d
{
namespace plan
{

RobotSpace::RobotSpace(const mdl::Movable *robot, std::string name)
    : SearchSpace(Type::RealVector, name), m_robot(robot)
{
    for (size_t i = 0; i < robot->getNumberOfDof(mdl::StateType::Position); ++i) {
        m_degreesOfFreedom.push_back(robot->getDegreeOfFreedomInfo(i, mdl::StateType::Position));
    }
    // m_enabledDofs.setOnes(m_degreesOfFreedom.size());
}

RobotSpace::RobotSpace(const mdl::Movable *robot, const Eigen::Matrix<bool, 1, -1> &enabledDofs,
                       std::string name)
    : SearchSpace(Type::RealVector, name), m_robot(robot)
{
    Expects(enabledDofs.size() >= 0 &&
            (size_t)enabledDofs.size() == robot->getNumberOfDof(mdl::StateType::Position));
    // m_enabledDofs = enabledDofs;
    for (size_t i = 0; i < robot->getNumberOfDof(mdl::StateType::Position); ++i) {
        if (enabledDofs(i)) {
            // if (m_enabledDofs(i)) {
            m_degreesOfFreedom.push_back(
                robot->getDegreeOfFreedomInfo(i, mdl::StateType::Position));
        }
    }
}

RobotSpace::RobotSpace(const mdl::Movable *robot, const std::vector<size_t> &enabledDofs,
                       std::string name)
    : SearchSpace(Type::RealVector, name), m_robot(robot)
{
    // m_enabledDofs.setZero(robot->getNumberOfDof(mdl::StateType::Position));
    for (auto i : enabledDofs) {
        // m_enabledDofs(i) = true;
        m_degreesOfFreedom.push_back(robot->getDegreeOfFreedomInfo(i, mdl::StateType::Position));
    }
}

RobotSpace::RobotSpace(const mdl::Movable *robot, std::vector<mdl::DegreeOfFreedomInfo> enabledDofs,
                       std::string name)
    : SearchSpace(Type::RealVector, name), m_robot(robot)
{
    m_degreesOfFreedom = std::move(enabledDofs);
}

const mdl::SceneObject *RobotSpace::robot() const { return m_robot; }

// Eigen::Matrix<bool, 1, -1> RobotSpace::enabledDofs() { return m_enabledDofs; }

void RobotSpace::shootUniform(mdl::WorldState &ws) const
{
    auto &x = ws[m_robot->getId()];
    for (auto dof : m_degreesOfFreedom) {
        dof.setIn(x, dof.shootUniform(move4d::random::instance().uniform()));
    }
}

std::unique_ptr<RobotKinematicGroupSpace>
RobotKinematicGroupSpace::create(const mdl::Movable *robot, const std::string &chain_name,
                                 const std::string &ik_method)
{
    auto search = std::find_if(robot->chains.begin(), robot->chains.end(),
                               [&chain_name](auto &ptr) { return ptr->name == chain_name; });
    if (search == robot->chains.end()) {
        throw std::invalid_argument("robot " + robot->getId() + " has no chain named " +
                                    chain_name);
    }
    auto chain = *search;
    auto tip = chain->tipName;
    return std::make_unique<RobotKinematicGroupSpace>(robot, *search, ik_method,
                                                      robot->getId() + "_" + chain_name);
}

RobotKinematicGroupSpace::RobotKinematicGroupSpace(const mdl::Movable *robot,
                                                   mdl::ChainPtr kinematicChain,
                                                   std::string ik_method, std::string name)
    : RobotSpace(robot, kinematicChain->bijection, name), m_kinematicChain(kinematicChain),
      m_kinematicSolver(std::move(ik_method)), _bijection(m_kinematicChain->bijection.size())
{
    std::iota(_bijection.begin(), _bijection.end(), 0); /// 0, 1, 2, ..., n-1
    _init();
}

RobotKinematicGroupSpace::RobotKinematicGroupSpace(const mdl::Movable *robot,
                                                   mdl::ChainPtr kinematicChain,
                                                   std::string ik_method,
                                                   std::vector<size_t> bijection, std::string name)
    : RobotSpace(robot, bijection, name), m_kinematicChain(kinematicChain),
      m_kinematicSolver(std::move(ik_method)), _bijection(std::move(bijection))
{
    _init();
}

RobotKinematicGroupSpace::RobotKinematicGroupSpace(
    const mdl::Movable *robot, mdl::ChainPtr kinematicChain, std::string ik_method,
    const std::vector<mdl::DegreeOfFreedomInfo> &dofs, std::string name)
    : RobotSpace(robot, dofs, name), m_kinematicChain(kinematicChain),
      m_kinematicSolver(std::move(ik_method)), _bijection(m_kinematicChain->bijection.size())
{
    std::iota(_bijection.begin(), _bijection.end(), 0); /// 0, 1, 2, ..., n-1
    _init();
}

void RobotKinematicGroupSpace::_init() { m_type = Type::KinematicGroup; }

void RobotKinematicGroupSpace::shootUniform(mdl::WorldState &ws) const
{
    Expects((m_max - m_min).norm() > 1e-6);
    bool ikFound = false;
    while (M4D_WHILE_CONDITION(!ikFound)) {
        auto q = math::Quaternion::Random();
        math::Vector3 t;
        for (size_t i = 0; i < 3; ++i) {
            t[i] = random::instance().uniform(m_min[i], m_max[i]);
        }
        RobotSpace::shootUniform(ws);
        math::Transform pose = math::Translation(t) * q;
        ikFound = computeIK(ws, pose);
    }
}

const std::string &RobotKinematicGroupSpace::getKinematicSolver() const
{
    return m_kinematicSolver;
}

const mdl::ChainPtr &RobotKinematicGroupSpace::getKinematicChain() const
{
    return m_kinematicChain;
}

void RobotKinematicGroupSpace::setKinematicChain(mdl::ChainPtr kinematicChain)
{
    m_kinematicChain = std::move(kinematicChain);
}

bool RobotKinematicGroupSpace::computeIK(mdl::WorldState &ws, const math::Transform &pose) const
{
    return computeIK(ws.getState(robot()->getId()), pose);
}

bool RobotKinematicGroupSpace::computeIK(mdl::State &state, const math::Transform &pose) const
{
    // TODO(Jules): we must use degreeoffreedominfo, not bijection
    // TODO(Jules): we must use degreeoffreedominfo, not bijection
    math::Vector x(m_kinematicChain->bijection.size());
    // put the state of the kinematic chain from &state to x
    for (size_t i = 0; i < m_kinematicChain->bijection.size(); ++i) {
        x[i] = state(m_kinematicChain->bijection[i], mdl::StateType::Position);
    }
    // if (robot()->computeInverseKinematic(*m_endEffector, *m_kinematicChain, pose, state, x)) {
    kin::KinematicSolverPtr solver =
        kin::KinematicSolverFactory::instance().create(m_kinematicSolver);
    if (solver->computeInverseKinematic(*robot(), *m_kinematicChain, pose, state, x)) {
        // put back the new state of the kinematic chain from x to &state
        for (size_t i = 0; i < m_kinematicChain->bijection.size(); ++i) {
            state(m_kinematicChain->bijection[i], mdl::StateType::Position) = x[i];
        }
        return true;
    } else {
        return false;
    }
}

bool RobotKinematicGroupSpace::computeFK(const mdl::WorldState &ws, math::Transform &pose) const
{
    return computeFK(ws.getState(robot()->getId()), pose);
}

bool RobotKinematicGroupSpace::computeFK(const mdl::State &state, math::Transform &pose) const
{
    // TODO(Jules): we must use degreeoffreedominfo, not bijection
    // well... this looks alright...?
    // assert(false);
    math::Vector x(m_kinematicChain->bijection.size());
    // put the state of the kinematic chain from &state to x
    for (size_t i = 0; i < m_kinematicChain->bijection.size(); ++i) {
        x[i] = state(m_kinematicChain->bijection[i], mdl::StateType::Position);
    }
    kin::KinematicSolverPtr solver =
        kin::KinematicSolverFactory::instance().create(m_kinematicSolver);
    return solver->computeForwardKinematic(*robot(), *m_kinematicChain, state, x, pose);
}

void move4d::plan::RobotKinematicGroupSpace::setPoseBounds(math::Vector3 low, math::Vector3 high)
{
    m_min = std::move(low);
    m_max = std::move(high);
}

} // namespace plan
} // namespace move4d

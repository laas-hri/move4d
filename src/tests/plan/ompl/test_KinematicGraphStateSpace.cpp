//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#include "move4d/mdl/Movable.h"
#include "move4d/plan/KinematicGraphSearchSpace.h"
#include "move4d/plan/SO3Space.h"
#include "move4d/plan/ompl/KinematicGraphStateSpace.h"
#include "move4d/plan/ompl/converters/StateSpaceFactory.h"
#include "tests/GlobalFixtures.h"
#include "tests/plan/FixtureKinematicGraph.h"
#include <boost/test/unit_test.hpp>
#include <dlfcn.h>
#include <rl/mdl/Dynamic.h>

namespace move4d
{
namespace tests
{

using namespace move4d::plan;
using namespace move4d::plan::ompl;

class FixtureKinematicGraphOmpl : public FixtureKinematicGraph
{
  public:
    FixtureKinematicGraphOmpl()
    {
        loadPlugins();
        using namespace move4d::plan;
        using namespace move4d::plan::ompl;
        convert::SearchSpaceFactory factory;
        wrapper = factory.createForPlanning(*kin_graph);
        space = std::dynamic_pointer_cast<StateSpace>(wrapper->getStateSpace());
        space->setup();
    }
    virtual ~FixtureKinematicGraphOmpl() = default;

  protected:
    move4d::plan::ompl::StateSpaceWrapperPtr wrapper;
    move4d::plan::ompl::StateSpacePtr space;

  private:
    static void loadPlugins()
    {
        static bool first_time = true;
        if (first_time) {
            first_time = false;
            // FIXME(Jules): plugin loading in tests shouldn't be done this way
            std::string home(std::getenv("MOVE4D_PLUGIN_DIR"));
            std::string plugin_path = home + "/libmove4d_ikfast_single_lwr_robot_plugin.so";
            dlerror();
            void *plugin = dlopen(plugin_path.c_str(), RTLD_NOW | RTLD_GLOBAL);
            if (plugin == nullptr) {
                std::cout << (std::string("failed to load plugin at ") + plugin_path +
                              "\n\twith error:\n" + dlerror())
                          << "\n";
            }
        }
    }
};

BOOST_FIXTURE_TEST_SUITE(ompl_kinematic_graph, FixtureKinematicGraphOmpl)

BOOST_AUTO_TEST_CASE(construct,
                     *boost::unit_test::depends_on("ikfast/load_plugin_single_lwr_robot"))
{
    BOOST_REQUIRE(wrapper);
    BOOST_REQUIRE(wrapper->getStateSpace());
    BOOST_REQUIRE(space);
    BOOST_CHECK(dynamic_cast<KinematicGraphStateSpace *>(space.get()));
}

BOOST_AUTO_TEST_CASE(sanityCheck)
{
    auto ws = std::make_shared<mdl::WorldState>(_scene.get());
    space->setInitWorldState(ws);
    // NOTE: doesn't use the Valid variant because it loops forever
    space->setStateSamplerAllocator([](const ::ompl::base::StateSpace *space) {
        return std::make_shared<ompl::KinematicGraphStateSampler>(
            space->as<ompl::KinematicGraphStateSpace>());
    });
    BOOST_CHECK_NO_THROW([&]() {
        try {
            space->sanityChecks();
        } catch (std::runtime_error &e) {
            BOOST_TEST_MESSAGE(std::string("caught exception: ") + e.what());
            throw e;
        }
    }());
}

BOOST_AUTO_TEST_CASE(sample)
{
    // NOTE: doesn't use the Valid variant because it loops forever
    // TODO: maybe we should also test the valid variant
    auto sampler = std::make_shared<ompl::KinematicGraphStateSampler>(
        space->as<ompl::KinematicGraphStateSpace>());

    auto ws = std::make_shared<mdl::WorldState>(_scene.get());
    space->setInitWorldState(ws);
    ::ompl::base::ScopedState<> state = wrapper->extractState(*ws);
    for (size_t i = 0; i < 100; ++i) {
        BOOST_CHECK_NO_THROW(sampler->sampleUniform(state.get()));
        BOOST_CHECK(space->satisfiesBounds(state.get()));
    }
}

// BOOST_AUTO_TEST_CASE(interpolate)
// {
//     auto sampler = space->allocDefaultStateSampler();
//     auto ws = std::make_shared<mdl::WorldState>(_scene.get());
//     space->setInitWorldState(ws);
//     auto from = wrapper->extractState(*ws);
//     auto to = wrapper->extractState(*ws);
//     auto res = wrapper->extractState(*ws);
//
//     BOOST_CHECK_NO_THROW(sampler->sampleUniform(from.get()));
//     BOOST_CHECK(space->satisfiesBounds(from.get()));
//     for (double t = 0; t < 1.; t += .01) {
//         BOOST_CHECK_NO_THROW(space->interpolate(from.get(), to.get(), t, res.get()));
//         BOOST_CHECK(space->satisfiesBounds(res.get()));
//     }
// }

BOOST_AUTO_TEST_SUITE_END()

} // namespace tests

} // namespace move4d

//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#include <list> // needed by some versions of boost

#include "move4d/mdl/State.h"
#include "tests/GlobalFixtures.h"
#include <boost/test/unit_test.hpp>
#include <rl/mdl/Dynamic.h>

namespace move4d
{
namespace tests
{

BOOST_FIXTURE_TEST_SUITE(state, GlobalFixture)

BOOST_AUTO_TEST_CASE(new_state_is_valid)
{
    mdl::State s(lwr_ff_base.get());
    BOOST_CHECK(lwr_ff_base->m_kinematic->isValid(s(mdl::StateType::Position)));
}
BOOST_AUTO_TEST_CASE(state_equal_itself)
{
    mdl::State s1(lwr_ff_base.get());
    BOOST_CHECK(s1 == s1);
}
BOOST_AUTO_TEST_CASE(state_not_equal)
{
    mdl::State s1(lwr_ff_base.get());

    for (size_t i = 0; i < lwr_ff_base->getNumberOfDof(mdl::StateType::Position); ++i) {
        mdl::State s2(lwr_ff_base.get());
        Real v = s2(i, mdl::StateType::Position);
        Real max = lwr_ff_base->getMaximum(i, mdl::StateType::Position);
        Real min = lwr_ff_base->getMinimum(i, mdl::StateType::Position);
        if (max == min)
            continue;
        if (v == max)
            v = min;
        else
            v = max;
        s2(i, mdl::StateType::Position) = v;
        BOOST_CHECK(!(s1 == s2));
    }
}
BOOST_AUTO_TEST_CASE(copy_state_equal)
{
    mdl::State s1(lwr_ff_base.get());
    mdl::State s2(s1);
    BOOST_CHECK(s1 == s2);
}
BOOST_AUTO_TEST_CASE(new_state_equal)
{
    mdl::State s1(lwr_ff_base.get());
    mdl::State s2(lwr_ff_base.get());
    BOOST_CHECK(s1 == s2);
}

BOOST_AUTO_TEST_SUITE_END()

} // namespace tests

} // namespace move4d

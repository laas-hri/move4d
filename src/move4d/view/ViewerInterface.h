//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_VIEW_VIEWERINTERFACE_H
#define MOVE4D_VIEW_VIEWERINTERFACE_H

#include "move4d/Singleton.h"
#include "move4d/math/Vector.h"
#include <rl/plan/Viewer.h>

namespace rl
{
namespace sg
{
class Scene;
}
namespace plan
{
class Model;
}
} // namespace rl

namespace move4d
{

namespace mdl
{
class WorldPositions;
class WorldState;
class Scene;
class State;
} // namespace mdl
namespace motion
{
class GeometricPath;
} // namespace motion

namespace view
{

class ViewerInterface : virtual public ::rl::plan::Viewer
{
  public:
    // virtual ~ViewerInterface()=default;

    virtual void update(const move4d::mdl::WorldPositions &positions) = 0;

    virtual void update(const move4d::mdl::WorldState &positions) = 0;

    virtual void drawConfiguration(std::size_t robot_index, const move4d::math::Vector &q) = 0;

    virtual void drawState(const move4d::mdl::State &state) = 0;

    virtual void drawPose(const math::Transform &pose) = 0;
    virtual void drawStampedPose(const math::Transform &pose, const math::Transform &parent,
                                 const std::string &name) = 0;

    virtual void resetPoses() = 0;

    virtual void saveImage(const std::string &path) = 0;

    virtual void playPath(const mdl::WorldState &ws_init, const motion::GeometricPath &path) = 0;

    virtual void toggleRecording(bool record) = 0;
};

class ViewerInstance : public Singleton<ViewerInstance>
{
  protected:
    std::shared_ptr<ViewerInterface> viewer_;

  public:
    const std::shared_ptr<ViewerInterface> &get() { return viewer_; }
    void set(std::shared_ptr<ViewerInterface> viewer) { viewer_ = std::move(viewer); }
};

} // namespace view
} // namespace move4d

#endif // MOVE4D_VIEW_VIEWERINTERFACE_H

RUN_DIR=`pwd -P`
SRC_DIR=$RUN_DIR/src
INSTALL_PREFIX=$RUN_DIR
NPROC=`nproc`
MAYBE_SUDO=""

set -e # exit script on error
set -x # print executed commands

LD_LIBRARY_PATH=$INSTALL_PREFIX/lib:$LD_LIBRARY_PATH
CMAKE_PREFIX_PATH=$CMAKE_PREFIX_PATH:$INSTALL_PREFIX
PATH=$PATH:$INSTALL_PREFIX/bin

mkdir -p $SRC_DIR

cd $SRC_DIR && \
curl -L https://github.com/roboticslibrary/rl/archive/d96c8b4cfdbed8ad81015f1879c27d0a79d18688.tar.gz --output rl.tar.gz && \
    tar xzf rl.tar.gz && \
    rm rl.tar.gz && \
    mv rl-d96c8b4cfdbed8ad81015f1879c27d0a79d18688 rl && \
    cd rl && \
    mkdir build && \
    cd build && \
    cmake .. -DCMAKE_INSTALL_PREFIX=$INSTALL_PREFIX && \
    make -j$NPROC && \
    $MAYBE_SUDO make install

cd $SRC_DIR && \
    curl -L https://github.com/ompl/ompl/archive/1.4.1.tar.gz --output ompl.tar.gz && \
    tar xzf ompl.tar.gz && \
    rm ompl.tar.gz && \
    cd ompl-1.4.1 && \
    mkdir build && \
    cd build && \
    cmake .. -DCMAKE_INSTALL_PREFIX=$INSTALL_PREFIX  && \
    make -j$NPROC && \
    $MAYBE_SUDO make install

cd $SRC_DIR && \
    curl -L https://github.com/roboptim/roboptim-core/archive/master.tar.gz --output roboptim-core.tar.gz && \
    tar xzf roboptim-core.tar.gz && \
    rm roboptim-core.tar.gz && \
    cd roboptim-core-master && \
    curl -L https://github.com/jrl-umi3218/jrl-cmakemodules/archive/bf10c6ca3565b96ab283edaa8f18b5b533e59da9.tar.gz --output cmake.tar.gz && \
    tar xzf cmake.tar.gz && mv jrl-cmakemodules-bf10c6ca3565b96ab283edaa8f18b5b533e59da9/* cmake/ && \
    mkdir build && \
    cd build && \
    cmake .. -DCMAKE_INSTALL_PREFIX=$INSTALL_PREFIX  -DDISABLE_TESTS=ON && \
    make -j$NPROC && \
    $MAYBE_SUDO make install

cd $SRC_DIR && \
    curl -L https://github.com/assimp/assimp/archive/v4.1.0.tar.gz --output assimp.tar.gz && \
    tar xzf assimp.tar.gz && \
    rm assimp.tar.gz && \
    cd assimp-4.1.0 && \
    mkdir build && \
    cd build && \
    cmake .. -DCMAKE_INSTALL_PREFIX=$INSTALL_PREFIX  && \
    make -j$NPROC && \
    $MAYBE_SUDO make install


cd $SRC_DIR && \
    curl -L https://github.com/orocos/orocos_kinematics_dynamics/archive/v1.4.0.tar.gz --output orocos-kinematics.tar.gz && \
    tar xzf orocos-kinematics.tar.gz && \
    rm orocos-kinematics.tar.gz && \
    cd orocos_kinematics_dynamics-1.4.0/orocos_kdl && \
    mkdir build && \
    cd build && \
    cmake .. -DCMAKE_INSTALL_PREFIX=$INSTALL_PREFIX  && \
    make -j$NPROC && \
    $MAYBE_SUDO make install
    

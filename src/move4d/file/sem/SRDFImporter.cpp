//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#include "move4d/file/sem/SRDFImporter.h"
#include "move4d/mdl/Scene.h"
#include "move4d/mdl/SceneObject.h"
#include "move4d/utils/KinematicHelper.hpp"
#include <fstream>
#include <iomanip>
#include <rl/mdl/Fixed.h>
#include <rl/mdl/Joint.h>
#include <rl/mdl/World.h>
#include <rl/plan/SimpleModel.h>
#include <tinyxml.h>

INIT_MOVE4D_STATIC_LOGGER(move4d::file::sem::SRDFImporter, "move4d.file.sem.srdfimporter");
namespace move4d
{
namespace file
{
namespace sem
{

struct SRDFImporter::Impl {

    void loadVirtualJoints(const mdl::SceneObjectPtr &object, TiXmlElement *robot_xml);
    void loadEndEffectors(const mdl::SceneObjectPtr &object, TiXmlElement *robot_xml);
    void loadGroups(const mdl::SceneObjectPtr &object, TiXmlElement *robot_xml);
    int loadGroup(const mdl::SceneObjectPtr &object, TiXmlElement *group_xml);
    int loadChain(const mdl::SceneObjectPtr &object, TiXmlElement *chain_xml,
                  std::string group_name);
};

SRDFImporter::SRDFImporter() : p_impl(std::make_unique<Impl>()) {}

// defined in the implementation file, where Impl is a complete type
SRDFImporter::~SRDFImporter() = default;

bool SRDFImporter::loadFile(const std::string &filepath, const mdl::ScenePtr &scene)
{
    // get the entire file
    std::string xml_string;
    std::fstream xml_file(filepath.c_str(), std::fstream::in);
    if (xml_file.is_open()) {
        while (xml_file.good()) {
            std::string line;
            std::getline(xml_file, line);
            xml_string += (line + "\n");
        }
        xml_file.close();
        if (!loadString(xml_string, scene)) {
            M4D_ERROR("Error while parsing file '" << filepath << "'.");
            return false;
        }
        return true;
    }
    M4D_ERROR("Could not open file '" << filepath << "' for parsing.");
    return false;
}

bool SRDFImporter::loadString(const std::string &xml_string, const mdl::ScenePtr &scene)
{
    TiXmlDocument xml_doc;
    xml_doc.Parse(xml_string.c_str());
    if (xml_doc.Error()) {
        M4D_ERROR("Could not parse the SRDF XML file. " << xml_doc.ErrorDesc());
        return false;
    }
    return loadXml(xml_doc.FirstChildElement("robot"), scene);
}
bool SRDFImporter::loadXml(TiXmlElement *robot_xml, const mdl::ScenePtr &scene)
{
    if (!robot_xml || robot_xml->ValueStr() != "robot") {
        M4D_ERROR("Could not find the 'robot' element in the XML file");
        return false;
    }
    const char *rname = robot_xml->Attribute("name");

    auto robots = scene->getObjectsByName(rname);
    for (const auto &robot : robots) {
        p_impl->loadGroups(robot, robot_xml);
        // p_impl->loadVirtualJoints(robot, robot_xml);
        p_impl->loadEndEffectors(robot, robot_xml);
    }

    return true;
}

void SRDFImporter::Impl::loadVirtualJoints(const mdl::SceneObjectPtr &object,
                                           TiXmlElement *robot_xml)
{
    for (TiXmlElement *vj_xml = robot_xml->FirstChildElement("virtual_joint"); vj_xml;
         vj_xml = vj_xml->NextSiblingElement("virtual_joint")) {
        const char *jname = vj_xml->Attribute("name");
        const char *child = vj_xml->Attribute("child_link");
        const char *parent = vj_xml->Attribute("parent_frame");
        const char *type = vj_xml->Attribute("type");
        if (!jname) {
            M4D_ERROR("Name of virtual joint is not specified");
            continue;
        }
        if (!child) {
            M4D_ERROR("Child link of virtual joint is not specified");
            continue;
        }
        if (!object->getBody(child)) {
            M4D_ERROR("Virtual joint does not attach to a link on the robot (link '"
                      << child << "' is not known)");
            continue;
        }
        if (!parent) {
            M4D_ERROR("Parent frame of virtual joint is not specified");
            continue;
        }
        if (!type) {
            M4D_ERROR("Type of virtual joint is not specified");
            continue;
        }
        // TODO(Jules): SRDFImporter::loadVirtualJoints
        assert(false && "not implemented");
        // VirtualJoint vj;
        // vj.type_ = std::string(type);
        // boost::trim(vj.type_);
        // std::transform(vj.type_.begin(), vj.type_.end(), vj.type_.begin(), ::tolower);
        // if (vj.type_ != "planar" && vj.type_ != "floating" && vj.type_ != "fixed") {
        //     M4D_ERROR("Unknown type of joint: '%s'. Assuming 'fixed' instead. Other "
        //               "known types are 'planar' "
        //               "and 'floating'.",
        //               type);
        //     vj.type_ = "fixed";
        // }
        // vj.name_ = std::string(jname);
        // boost::trim(vj.name_);
        // vj.child_link_ = std::string(child);
        // boost::trim(vj.child_link_);
        // vj.parent_frame_ = std::string(parent);
        // boost::trim(vj.parent_frame_);
        // virtual_joints_.push_back(vj);
    }
}

void SRDFImporter::Impl::loadEndEffectors(const mdl::SceneObjectPtr &object,
                                          TiXmlElement *robot_xml)
{
    for (TiXmlElement *eef_xml = robot_xml->FirstChildElement("end_effector"); eef_xml;
         eef_xml = eef_xml->NextSiblingElement("end_effector")) {
        const char *ename = eef_xml->Attribute("name");
        // const char *gname = eef_xml->Attribute("group");
        const char *parent = eef_xml->Attribute("parent_link");
        // const char *parent_group = eef_xml->Attribute("parent_group");
        if (!ename) {
            M4D_ERROR("Name of end effector is not specified");
            continue;
        }
        // TODO(Jules): end effector group
        // if (!gname) {
        //     M4D_ERROR("Group not specified for end effector '" << ename << "'");
        //     continue;
        // }
        // e.component_group_ = std::string(gname);
        // bool found = false;
        // for (std::size_t k = 0; k < groups_.size(); ++k)
        //     if (groups_[k].name_ == e.component_group_) {
        //         found = true;
        //         break;
        //     }
        // if (!found) {
        //     M4D_ERROR("End effector '" << ename << "' specified for group '" << gname
        //                                << "', but that group is not known");
        //     continue;
        // }
        if (!parent) {
            M4D_ERROR("Parent link not specified for end effector '" << ename << "'");
            continue;
        }
        auto parentFrame = object->getLink(parent);

        if (!parentFrame) {
            M4D_ERROR("Link '" << parent << "' specified as parent for end effector '" << ename
                               << "' is not known to the URDF");
            continue;
        }

        // if (parent_group) {
        //     e.parent_group_ = std::string(parent_group);
        // }
        object->endEffectors.push_back(
            std::make_shared<mdl::EndEffector>(mdl::EndEffector{ename, parent}));
        M4D_DEBUG("adding end effector " << ename << " to robot " << object->getId() << " ["
                                         << object->getName() << "]");
    }
}

void SRDFImporter::Impl::loadGroups(const mdl::SceneObjectPtr &object, TiXmlElement *robot_xml)
{
    for (TiXmlElement *g_xml = robot_xml->FirstChildElement("group"); g_xml;
         g_xml = g_xml->NextSiblingElement("group")) {
        loadGroup(object, g_xml);
    }
}

int SRDFImporter::Impl::loadGroup(const mdl::SceneObjectPtr &object, TiXmlElement *group_xml)
{

    const char *gname = group_xml->Attribute("name");
    if (!gname) {
        M4D_ERROR("Name of group is not specified");
        return -1;
    }
    for (size_t i = 0; i < object->groups.size(); i++) {
        auto g = object->groups[i];
        if (g->name == gname) {
            M4D_WARN("Name of group '" << gname << "' duplicated. Keeping 1st occurrence.");
            return (int)i;
        }
    }
    auto links = std::vector<size_t>();
    auto joints = std::vector<size_t>();
    auto chains = std::vector<size_t>();

    for (TiXmlElement *link_xml = group_xml->FirstChildElement("link"); link_xml;
         link_xml = link_xml->NextSiblingElement("link")) {
        const char *lname = link_xml->Attribute("name");
        if (!lname) {
            M4D_ERROR("No name for link in group " << gname);
            continue;
        }
        int i = object->getLinkIndex(lname);
        if (i < 0) {
            M4D_ERROR("Link '" << lname << "' specified in group '" << gname
                               << "' is not known to the URDF");
            continue;
        } else {
            links.push_back((size_t)i);
        }
    }

    for (TiXmlElement *joint_xml = group_xml->FirstChildElement("joint"); joint_xml;
         joint_xml = joint_xml->NextSiblingElement("joint")) {
        const char *jname = joint_xml->Attribute("name");
        if (!jname) {
            M4D_ERROR("No name for joint in group " << gname);
            continue;
        }
        int i = object->getJointIndex(jname);
        if (i < 0) {
            M4D_ERROR("Joint '" << jname << "' specified in group '" << gname
                                << "' is not known to the URDF")
        } else {
            joints.push_back((size_t)i);
        }
    }

    for (TiXmlElement *chain_xml = group_xml->FirstChildElement("chain"); chain_xml;
         chain_xml = chain_xml->NextSiblingElement("chain")) {
        int i = loadChain(object, chain_xml, gname);
        if (i >= 0) {
            chains.push_back((size_t)i);
        }
    }

    // TODO: Add nested group support

    if (!links.empty() || !joints.empty() || !chains.empty()) {
        object->groups.push_back(
            std::make_shared<mdl::Group>(mdl::Group{gname, joints, links, chains}));
        return (int)object->groups.size() - 1;
    } else {
        M4D_ERROR("Group '" << gname << "' is empty.");
        return -1;
    }
}

int SRDFImporter::Impl::loadChain(const mdl::SceneObjectPtr &object, TiXmlElement *chain_xml,
                                  std::string group_name)
{
    const char *bl_name = chain_xml->Attribute("base_link");
    const char *tl_name = chain_xml->Attribute("tip_link");
    M4D_DEBUG("Adding chain in " << group_name);
    if (!bl_name) {
        M4D_ERROR("Base link name of the chain in '" << group_name << "' is not defined");
        return -1;
    }
    if (!tl_name) {
        M4D_ERROR("Tip link name of the chain in '" << group_name << "' is not defined");
        return -1;
    }

    auto tip_link = object->getLinkIndex(tl_name);
    if (tip_link < 0) {
        M4D_ERROR("Tip link '" << tl_name << "' of chain in '" << group_name
                               << "' has not been found in object '" << object->getName() << "'");
        return -1;
    }

    auto ikfast_xml = chain_xml->FirstChildElement("ikfast");
    std::string ikfast;
    if (ikfast_xml) {
        ikfast = ikfast_xml->GetText();
    }

    std::vector<size_t> bijection;
    std::shared_ptr<mdl::Kinematic> kin =
        utils::kinematicSubChain(*object, bl_name, tl_name, bijection);
    assert(kin);
    kin->setName(group_name + "_chain_srdf");
    M4D_DEBUG("created chain " << std::quoted(kin->getName()));
    object->chains.push_back(std::make_shared<mdl::Chain>(
        object->getName(), group_name, tl_name,
        object->m_kinematic->getFrame(object->getLinkIndex(bl_name)), kin, bijection));
    object->chains.back()->ikfast = ikfast;
    return (int)object->chains.size() - 1;
} // namespace sem

} // namespace sem
} // namespace file
} // namespace move4d

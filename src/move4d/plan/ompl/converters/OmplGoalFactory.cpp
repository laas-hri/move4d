//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#include "move4d/plan/ompl/converters/OmplGoalFactory.h"
#include "move4d/plan/GoalList.h"
#include "move4d/plan/GoalSampler.h"
#include "move4d/plan/WorldStateGoal.h"
#include "move4d/plan/ompl/KinematicGraphStateSpace.h"
#include "move4d/plan/ompl/StateSpace.h"
#include "move4d/plan/ompl/converters/StateSpaceFactory.h"
#include "move4d/plan/ompl/goals/GoalList.h"
#include "move4d/plan/ompl/goals/GoalSpaceSampler.h"
#include "move4d/plan/ompl/goals/KinematicGraphGoalSampler.h"

#include <ompl/base/goals/GoalLazySamples.h>
#include <ompl/base/goals/GoalState.h>

namespace move4d
{
namespace plan
{
namespace ompl
{
namespace convert
{

OmplGoalFactory::OmplGoalFactory(::ompl::base::SpaceInformationPtr si) : _si(std::move(si))
{
    _wrapper = _si->getStateSpace()->as<StateSpace>()->getWrapper().lock().get();
}

::ompl::base::GoalPtr OmplGoalFactory::convert(const Goal &goal)
{
    if (dynamic_cast<const WorldStateGoal *>(&goal)) {
        auto g = std::make_shared<::ompl::base::GoalState>(_si);
        auto goal_ws = reinterpret_cast<const WorldStateGoal &>(goal).getGoalWorldState();
        g->setState(_wrapper->extractState(*goal_ws));
        return g;
    } else if (dynamic_cast<const GoalSampler *>(&goal)) {
        return std::make_shared<KinematicGraphGoalSampler>(
            reinterpret_cast<const GoalSampler &>(goal), _si);
    } else if (dynamic_cast<const plan::GoalSpaceSampler *>(&goal)) {
        auto goal_l = reinterpret_cast<const plan::GoalSpaceSampler *>(&goal);
        auto goal_space_wrapper =
            convert::SearchSpaceFactory().createForPlanning(*goal_l->getGoalSpace());
        return std::make_shared<ompl::GoalSpaceSampler>(
            _si, goal_space_wrapper, goal_l->getBijetion(), goal_l->getVertexBijection());
    } else if (dynamic_cast<const plan::GoalList *>(&goal)) {
        auto goal_l = reinterpret_cast<const plan::GoalList *>(&goal);
        auto res = std::make_shared<ompl::GoalList>(_si, std::vector<ompl::GoalList::Item>{});
        for (auto g : goal_l->getGoals()) {
            res->getGoals().push_back(
                std::dynamic_pointer_cast<::ompl::base::GoalSampleableRegion>(convert(*g)));
        }
        return res;
    } else {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " : unsupported goal type");
    }
}

} // namespace convert
} // namespace ompl
} // namespace plan
} // namespace move4d

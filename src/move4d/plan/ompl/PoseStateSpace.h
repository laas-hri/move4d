//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_PLAN_OMPL_POSESTATESPACE_H
#define MOVE4D_PLAN_OMPL_POSESTATESPACE_H

#include "move4d/plan/ompl/StateSpace.h"
#include <ompl/base/spaces/SE3StateSpace.h>

namespace move4d
{
namespace mdl
{
class SceneObject;
using SceneObjectPtr = std::shared_ptr<SceneObject>;
struct EndEffector;
using EndEffectorPtr = std::shared_ptr<EndEffector>;
struct Chain;
using ChainPtr = std::shared_ptr<Chain>;
} // namespace mdl
namespace plan
{
namespace ompl
{

class PoseStateSpace : public StateSpace
{
    MOVE4D_STATIC_LOGGER;

  public:
    class StateType : public StateSpace::StateType
    {
      public:
        ::ompl::base::SE3StateSpace::StateType **poses = nullptr;
        enum {
            IkComputed = 1 << 6,
            FkComputed = 1 << 7,
        };
        bool kinComputed() const { return this->flags & IkComputed && this->flags & FkComputed; }
    };
    struct PoseComponent {
        std::shared_ptr<::ompl::base::SE3StateSpace> stateSpace;
        const mdl::SceneObject *object;
        mdl::ChainPtr chain;
        /// indices of the kinematic group in StateType::values
        std::vector<size_t> bijection;
        /// index of the subspace corresponding to this chain
        size_t subspace;
        std::string kinematicSolver;
        enum class Method { FkOnly, IkOnly, ForeignIkTarget };
        Method method;
        size_t foreignIkTarget;

        PoseComponent(gsl::owner<::ompl::base::SE3StateSpace *> state_space,
                      const mdl::SceneObject *object, mdl::ChainPtr chain, size_t subspace_i,
                      std::vector<size_t> bijection, std::string kinematicSolver, Method method)
            : stateSpace(state_space), object(object), chain(std::move(chain)),
              bijection(std::move(bijection)), subspace(subspace_i),
              kinematicSolver(std::move(kinematicSolver)), method(method)
        {
        }
    };

    PoseStateSpace(const SearchSpace &searchSpace, mdl::WorldStatePtr initWs = {});

    void copyState(::ompl::base::State *destination,
                   const ::ompl::base::State *source) const override;
    double distance(const ::ompl::base::State *state1,
                    const ::ompl::base::State *state2) const override;
    void interpolate(const ::ompl::base::State *from, const ::ompl::base::State *to, double t,
                     ::ompl::base::State *state) const override;
    ::ompl::base::StateSamplerPtr allocDefaultStateSampler() const override;
    ::ompl::base::State *allocState() const override;
    void freeState(::ompl::base::State *state) const override;
    void printState(const ::ompl::base::State *state, std::ostream &out) const override;
    void readableFlag(int flags, std::ostream &out) const;

    virtual bool computeIK(StateType *state) const;
    virtual bool computeIK(size_t pose_i, StateType *state, const mdl::WorldState &ws,
                           const StateSpaceWrapperPtr &wrapper) const;
    virtual bool computeFK(StateType *state) const;
    virtual bool computeFK(size_t pose_i, StateType *state, const mdl::WorldState &ws,
                           const StateSpaceWrapperPtr &wrapper) const;
    /// computes FK, and if succeeded, IK. returns computeFK(state) && computeIK(state)
    virtual bool computeK(StateType *state) const;

    size_t getPosesCount() const;
    const PoseComponent &getPoseComponent(size_t i) const;

    void sanityChecks() const override;

  protected:
    size_t m_posesCount = 0;
    std::vector<PoseComponent> m_poses;

    // recurse
    void addPoseComponents(const SearchSpace &searchSpace);
    void addPoseComponents(const SearchSpace &searchSpace, size_t &firstDof);
    void allocStateMembers(StateSpace::StateType *state) const override;
};

/**
 * @brief The KinematicValidStateSampler class to sample poses with valid kinemtics
 */
class KinematicValidStateSampler : public DefaultStateSampler
{
    MOVE4D_STATIC_LOGGER;

  public:
    KinematicValidStateSampler(const PoseStateSpace *space);

    void sampleUniform(::ompl::base::State *state) override;
    void sampleUniformNear(::ompl::base::State *state, const ::ompl::base::State *near,
                           double distance) override;
    void sampleGaussian(::ompl::base::State *state, const ::ompl::base::State *mean,
                        double stdDev) override;

  protected:
    std::vector<::ompl::base::StateSamplerPtr> poseSamplers_;
    static size_t count_total_, count_success_;
};

class KinematicStateSampler : public DefaultStateSampler
{
    MOVE4D_STATIC_LOGGER;

  public:
    KinematicStateSampler(const PoseStateSpace *space);

    void sampleUniform(::ompl::base::State *state) override;
    void sampleUniformNear(::ompl::base::State *state, const ::ompl::base::State *near,
                           double distance) override;
    void sampleGaussian(::ompl::base::State *state, const ::ompl::base::State *mean,
                        double stdDev) override;

  protected:
    std::vector<::ompl::base::StateSamplerPtr> poseSamplers_;
};

} // namespace ompl
} // namespace plan
} // namespace move4d

#endif // MOVE4D_PLAN_OMPL_POSESTATESPACE_H

//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
// Created by gbuisan on 02/04/19.
//

#include "KinematicHelper.hpp"
#include "move4d/io/Logger.h"
#include "move4d/mdl/SceneObject.h"
#include "move4d/utils/try_clone.hpp"
#include <rl/mdl/Body.h>
#include <rl/mdl/Dynamic.h>
#include <rl/mdl/Fixed.h>
#include <rl/mdl/Joint.h>
#include <rl/mdl/Kinematic.h>
#include <rl/mdl/World.h>

#include <rl/mdl/Cylindrical.h>
#include <rl/mdl/Fixed.h>
#include <rl/mdl/Helical.h>
#include <rl/mdl/Prismatic.h>
#include <rl/mdl/Revolute.h>
#include <rl/mdl/SixDof.h>
#include <rl/mdl/Spherical.h>

namespace move4d
{
namespace utils
{
bool getParentTransform(rl::mdl::Kinematic *model, rl::mdl::Frame *frame, size_t &index)
{
    for (size_t i = 0; i < model->getTransforms(); ++i) {
        if (model->getTransform(i)->out == frame) {
            index = i;
            return true;
        }
    }
    return false;
}
bool getParentTransform(rl::mdl::Kinematic *model, size_t frame, size_t &index)
{
    return getParentTransform(model, model->getFrame(frame), index);
}
bool getParentFrame(rl::mdl::Kinematic *model, rl::mdl::Transform *transform, size_t &index)
{
    for (size_t i = 0; i < model->getFrames(); ++i) {
        if (model->getFrame(i)->getVertexDescriptor() == transform->in->getVertexDescriptor()) {
            index = i;
            return true;
        }
    }
    return false;
}
bool getParentFrame(rl::mdl::Kinematic *model, size_t transform_index, size_t &index)
{
    return getParentFrame(model, model->getTransform(transform_index), index);
}

bool getChildTransform(rl::mdl::Kinematic *model, rl::mdl::Frame *frame, size_t &index)
{
    for (size_t i = 0; i < model->getTransforms(); ++i) {
        if (model->getTransform(i)->in == frame) {
            index = i;
            return true;
        }
    }
    return false;
}

bool getParentFrameOfFrame(rl::mdl::Kinematic *model, size_t frame, size_t &index)
{
    size_t transform(SIZE_MAX);
    if (getParentTransform(model, frame, transform)) {
        return getParentFrame(model, transform, index);
    }
    return false;
}

bool isBodyOrGetPrevious(rl::mdl::Kinematic *model, size_t frame, size_t &bodyIndex,
                         size_t &frameIndex)
{
    if (dynamic_cast<rl::mdl::Body *>(model->getFrame(frame)) != nullptr) {
        for (size_t i = 0; i < model->getBodies(); ++i) {
            if (model->getBody(i) == model->getFrame(frame)) {
                bodyIndex = i;
                frameIndex = frame;
                return true;
            }
        }
    }
    return getPreviousBodyOfFrame(model, frame, bodyIndex, frameIndex);
}
bool getPreviousBodyOfFrame(rl::mdl::Kinematic *model, size_t frame, size_t &bodyIndex,
                            size_t &frameIndex)
{
    size_t frame1(frame), frame2(SIZE_MAX);
    while (getParentFrameOfFrame(model, frame1, frame2)) {
        if (dynamic_cast<rl::mdl::Body *>(model->getFrame(frame2)) != nullptr) {
            for (size_t i = 0; i < model->getBodies(); ++i) {
                if (model->getBody(i) == model->getFrame(frame2)) {
                    bodyIndex = i;
                    frameIndex = frame2;
                    return true;
                }
            }
        }
        frame1 = frame2;
    }
    return false;
}

rl::mdl::Joint *getDofJoint(rl::mdl::Kinematic *kin, std::size_t index, std::size_t &inDof)
{
    // TODO(Jules): uses only DofPosition here, is that OK?
    std::size_t jntFirstDof{0};
    std::size_t iJnt{0};
    ::rl::mdl::Joint *jnt = nullptr;
    while (jntFirstDof < kin->getDofPosition()) {
        jnt = kin->getJoint(iJnt);
        if (jntFirstDof + jnt->getDofPosition() > index) {
            inDof = index - jntFirstDof;
            return jnt;
        }
        jntFirstDof = jntFirstDof + jnt->getDofPosition();
        ++iJnt;
    }
    return nullptr;
}

std::unique_ptr<rl::mdl::Dynamic> kinematicSubChain(const mdl::SceneObject &object,
                                                    const std::string &base_link_name,
                                                    const std::string &tip_link_name,
                                                    std::vector<size_t> &bijection)
{
    INIT_MOVE4D_LOG("move4d.utils.kinematicsubchain");
    if (base_link_name.empty()) {
        M4D_ERROR("Base link name is not defined");
        return {};
    }
    if (tip_link_name.empty()) {
        M4D_ERROR("Tip link name is not defined");
        return {};
    }

    auto tip_link = object.getLinkIndex(tip_link_name);
    if (tip_link < 0) {
        M4D_ERROR("Tip link '" << tip_link_name << "' has not been found in object '"
                               << object.getName() << "'");
        return {};
    }

    auto kin = std::make_unique<rl::mdl::Dynamic>();
    size_t parentFrame, childFrame, transform;
    childFrame = (size_t)tip_link;
    ::rl::mdl::Frame *cf_ori = object.m_kinematic->getFrame(childFrame);
    ::rl::mdl::Frame *cf = nullptr;
    cf = try_clone<::rl::mdl::Body, ::rl::mdl::Frame>(cf_ori);
    assert(cf);
    kin->add(cf);
    if (base_link_name == tip_link_name) {
        // empty chain
        auto f = new ::rl::mdl::Frame(*object.m_kinematic->getFrame(tip_link));
        kin->add(f);
        auto world = new rl::mdl::World();
        kin->add(world);
        auto fixed = new rl::mdl::Fixed();
        kin->add(fixed, world, f);
        kin->update();
        return kin;
    }
    auto pf = cf;
    // loop from the tip link to the root link
    while (utils::getParentTransform(object.m_kinematic.get(), childFrame, transform) &&
           utils::getParentFrame(object.m_kinematic.get(), transform, parentFrame)) {
        cf = pf;
        // cloning the frame because rl::mdl::Kinematic takes ownership
        pf = new ::rl::mdl::Frame(*object.m_kinematic->getFrame(parentFrame));
        ::rl::mdl::Transform *t = object.m_kinematic->getTransform(transform);
        t = try_clone<::rl::mdl::SixDof, ::rl::mdl::Revolute, ::rl::mdl::Helical,
                      ::rl::mdl::Spherical, ::rl::mdl::Fixed, ::rl::mdl::Prismatic>(t);
        assert(t && "need to add types to the try_clone's");

        t->in = nullptr;
        t->out = nullptr;
        kin->add(pf);
        kin->add(t, pf, cf);

        M4D_DEBUG("Adding transform/joint from " << pf->getName() << " to " << cf->getName()
                                                 << " via " << t->getName() << " of type "
                                                 << typeid(*t).name());
        M4D_TRACE("it has " << (dynamic_cast<::rl::mdl::Joint *>(t)
                                    ? dynamic_cast<::rl::mdl::Joint *>(t)->getDofPosition()
                                    : 0)
                            << " position DoFs");
        if (object.m_kinematic->getFrame(parentFrame)->getName() == base_link_name) {
            auto world = new rl::mdl::World();
            // Add a fixed just after the world, and it is this fixed that must be updated during
            // IK.
            kin->add(world);
            auto fixed = new rl::mdl::Fixed();
            kin->add(fixed, world, pf);
            kin->update();

            for (size_t i = 0; i < kin->getJoints(); ++i) {
                M4D_TRACE("joint " << i << " : " << kin->getJoint(i)->getName() << " with "
                                   << kin->getJoint(i)->getDofPosition() << " position DoF");
            }

            // Searching for the bijection between chain DoFs and robot DoFs
            bijection.resize(kin->getDofPosition());
            for (size_t i = 0; i < kin->getDofPosition(); i++) {
                size_t in_dof;
                auto jnt = utils::getDofJoint(kin.get(), i, in_dof);
                bool found = false;
                for (size_t j = 0; j < object.m_kinematic->getDofPosition(); j++) {
                    size_t in_dof_robot;
                    auto jnt_robot = utils::getDofJoint(object.m_kinematic.get(), j, in_dof_robot);
                    if (jnt_robot && jnt_robot->getName() == jnt->getName() &&
                        in_dof == in_dof_robot) {
                        assert(!jnt->getName().empty());
                        found = true;
                        bijection[i] = j;
                        M4D_TRACE("subchain DoF " << i << " matches robot DoF " << j);
                        break;
                    }
                }
                if (!found) {
                    M4D_ERROR("Can't find a bijection for robot '" << object.getName() << "'");
                    return {};
                }
            }
            M4D_DEBUG("created chain of " << kin->getDofPosition() << " position DoFs and "
                                          << kin->getDof() << " DoF");
            return kin;
        }
        childFrame = parentFrame;
    }
    M4D_ERROR("Error while creating chain");
    return {};
}

} // namespace utils
} // namespace move4d

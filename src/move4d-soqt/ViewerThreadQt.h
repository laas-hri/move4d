//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef _MOVE4D_SOQT_VIEWERTHREADQT_H
#define _MOVE4D_SOQT_VIEWERTHREADQT_H

#include "move4d/mdl/WorldPositions.h"
#include "move4d/mdl/WorldState.h"
#include "move4d/view/ViewerInterface.h"

#include <QThread>

namespace move4d
{
namespace mdl
{
class WorldState;
class WorldPositions;

} // namespace mdl
namespace view
{
class ViewerSoQt;
using ViewerSoQtPtr = std::shared_ptr<ViewerSoQt>;

class ViewerThreadQt;
using ViewerThreadQtPtr = std::shared_ptr<ViewerThreadQt>;
class ViewerThreadQt : public QObject, public move4d::view::ViewerInterface
{
    Q_OBJECT
  public:
    ViewerThreadQt(QObject *parent = nullptr);

    static bool connect(ViewerThreadQt *emiter, QObject *receiver);

    // ViewerInterface interface
  public:
    void update(const move4d::mdl::WorldPositions &positions) override;
    void update(const move4d::mdl::WorldState &positions) override;
    void drawConfiguration(size_t robot_index, const move4d::math::Vector &q) override;
    void drawState(const move4d::mdl::State &state) override;
    void drawPose(const rl::math::Transform &pose) override;
    void drawStampedPose(const rl::math::Transform &pose, const rl::math::Transform &parent,
                         const std::string &name) override;
    void resetPoses() override;
  signals:
    void updateRequested(move4d::mdl::WorldPositions positions);
    void updateRequested(move4d::mdl::WorldState positions);
    void drawConfigurationRequested(const rl::math::Vector &q);
    void drawConfigurationRequested(unsigned int robot_index, const rl::math::Vector &q);
    void drawPoseRequested(const rl::math::Transform &pose);
    void drawStampedPoseRequested(const rl::math::Transform &pose,
                                  const rl::math::Transform &parent, const std::string &name);
    void resetPosesRequested();
    void drawConfigurationEdgeRequested(const rl::math::Vector &q0, const rl::math::Vector &q1,
                                        const bool &free);
    void drawConfigurationPathRequested(const rl::plan::VectorList &path);
    void drawConfigurationVertexRequested(const rl::math::Vector &q, const bool &free);
    void drawLineRequested(const rl::math::Vector &xyz0, const rl::math::Vector &xyz1);
    void drawPointRequested(const rl::math::Vector &xyz);
    void drawSphereRequested(const rl::math::Vector &center, const double &radius);
    void drawWorkRequested(const rl::math::Transform &t);
    void drawWorkEdgeRequested(const rl::math::Vector &q0, const rl::math::Vector &q1);
    void drawWorkPathRequested(const rl::plan::VectorList &path);
    void drawWorkVertexRequested(const rl::math::Vector &q);
    void resetRequested();
    void resetEdgesRequested();
    void resetLinesRequested();
    void resetPointsRequested();
    void resetSpheresRequested();
    void resetVerticesRequested();
    void showMessageRequested(const std::string &message);
    void saveImageRequested(const std::string &path);
    void viewAllRequested();
    void playPathRequested(const move4d::mdl::WorldState &ws_init,
                           const move4d::motion::GeometricPath &path);
    void toggleRecordingRequested(bool record);

    // Viewer interface
  public:
    void drawConfiguration(const rl::math::Vector &q) override;
    void drawConfigurationEdge(const rl::math::Vector &q0, const rl::math::Vector &q1,
                               const bool &free) override;
    void drawConfigurationPath(const rl::plan::VectorList &path) override;
    void drawConfigurationVertex(const rl::math::Vector &q, const bool &free) override;
    void drawLine(const rl::math::Vector &xyz0, const rl::math::Vector &xyz1) override;
    void drawPoint(const rl::math::Vector &xyz) override;
    void drawSphere(const rl::math::Vector &center, const rl::math::Real &radius) override;
    void drawWork(const rl::math::Transform &t) override;
    void drawWorkEdge(const rl::math::Vector &q0, const rl::math::Vector &q1) override;
    void drawWorkPath(const rl::plan::VectorList &path) override;
    void drawWorkVertex(const rl::math::Vector &q) override;
    void reset() override;
    void resetEdges() override;
    void resetLines() override;
    void resetPoints() override;
    void resetSpheres() override;
    void resetVertices() override;
    void showMessage(const std::string &message) override;
    void saveImage(const std::string &path) override;
    void viewAll();
    void playPath(const move4d::mdl::WorldState &ws_init,
                  const move4d::motion::GeometricPath &path) override;
    void toggleRecording(bool record) override;
};

class ThreadedViewer : public ViewerThreadQt
{
  public:
    ThreadedViewer(QWidget *parent = nullptr, const Qt::WindowFlags &f = 0);

    ViewerSoQt *getViewer() const;

  private:
    ViewerSoQt *m_viewer;
};
} // namespace view
} // namespace move4d
#endif // _MOVE4D_SOQT_VIEWERTHREADQT_H

//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#include "move4d/motion/GeometricPath.h"
#include "move4d/plan/SearchSpace.h"
#include "move4d/plan/ompl/PoseMotionValidator.h"
#include "move4d/plan/ompl/PoseStateSpace.h"
#include "move4d/plan/ompl/StateValidityChecker.h"
#include "move4d/plan/ompl/converters/StateSpaceFactory.h"
#include "move4d/plan/ompl/converters/converters.h"
#include "move4d/plan/ompl/magic.h"

#include <ompl/geometric/PathGeometric.h>

#include <boost/serialization/vector.hpp>

namespace move4d
{
namespace motion
{

GeometricPath::GeometricPath(plan::SearchSpacePtr search_space) : Path(std::move(search_space))
{
    _space_wrapper = plan::ompl::convert::SearchSpaceFactory().createForPlanning(*getSearchSpace());
}

bool GeometricPath::check(const mdl::WorldStatePtr &init_ws,
                          const mdl::CollisionManagerPtr &collision_manager) const
{
    // TODO(Jules) for now i'll use OMPL to compute move4d GeometricPath validity and interpolation
    auto m_spaceInformation =
        std::make_shared<::ompl::base::SpaceInformation>(_space_wrapper->getStateSpace());
    _space_wrapper->getStateSpace()->as<plan::ompl::StateSpace>()->setInitWorldState(init_ws);
    m_spaceInformation->setStateValidityChecker(std::make_shared<plan::ompl::StateValidityChecker>(
        collision_manager, init_ws, _space_wrapper, m_spaceInformation));
    m_spaceInformation->setStateValidityCheckingResolution(
        magic::unwanted::DEFAULT_SAMPLING_RESOLUTION);
    m_spaceInformation->setMotionValidator(
        std::make_shared<plan::ompl::PoseMotionValidator>(m_spaceInformation));
    assert(m_spaceInformation->getStateValidityChecker());
    m_spaceInformation->setup();

    auto state_1 = _space_wrapper->stateFromValues(_values[0]);

    if (!m_spaceInformation->isValid(state_1.get())) {
        return false;
    }
    for (size_t i = 0; i < _values.size() - 1; ++i) {
        auto state_2 = _space_wrapper->stateFromValues(_values[i + 1]);
        if (!m_spaceInformation->getMotionValidator()->checkMotion(state_1.get(), state_2.get())) {
            return false;
        }
        std::swap(state_1, state_2);
    }

    return true;
}

void GeometricPath::interpolate(const mdl::WorldStatePtr &init_ws)
{
    // TODO(Jules) for now i'll use OMPL to compute move4d GeometricPath validity and interpolation
    _space_wrapper->getStateSpace()->as<plan::ompl::StateSpace>()->setInitWorldState(init_ws);
    auto m_spaceInformation =
        std::make_shared<::ompl::base::SpaceInformation>(_space_wrapper->getStateSpace());
    m_spaceInformation->setStateValidityCheckingResolution(
        magic::unwanted::DEFAULT_SAMPLING_RESOLUTION);
    m_spaceInformation->setup();
    auto ompl_path =
        move4d::plan::ompl::converters::geometricPath(*this, *_space_wrapper, m_spaceInformation);
    ompl_path->interpolate();

    this->_values.clear();
    _values.reserve(ompl_path->getStateCount());
    for (const auto &x : ompl_path->getStates()) {
        this->_values.push_back(_space_wrapper->valuesFromState(x));
    }
}

void GeometricPath::interpolate(const mdl::WorldState &init_ws)
{
    auto ws_p = std::make_shared<mdl::WorldState>(init_ws);
    interpolate(ws_p);
}

void GeometricPath::append(math::Vector value) { _values.push_back(std::move(value)); }

} // namespace motion
} // namespace move4d

#ifndef NDEBUG
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
void test_move4d_motion_Geometric_path_serialize()
{
    std::stringstream ss;
    boost::archive::text_oarchive oa{ss};
    move4d::motion::GeometricPath path(std::make_shared<move4d::plan::SearchSpace>(
        move4d::plan::SearchSpace::Type::Other, "test_dummy_search_space"));
    Eigen::VectorXd v(4);
    v << 1., 2., 3., 4.;
    path.append(v);
    // v << 1., 2., 3., 5.;
    // path.append(v);

    oa << v;

    oa << path;
}
#endif

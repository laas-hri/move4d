//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//

#include "move4d/file/model/URDFImporter.h"
#include "move4d/mdl/Scene.h"
#include "move4d/mdl/SceneObject.h"
#include "move4d/view/Viewer.h"

#include <fstream>
#include <memory>
#include <rl/mdl/Body.h>
#include <rl/mdl/UrdfFactory.h>
#include <rl/plan/SimpleModel.h>
#include <rl/sg/Body.h>
#include <urdf_parser/urdf_parser.h>

#include "move4d/file/model/AssimpImporter.h"
#include "move4d/utils/filesystem_path.h"
#include "move4d/utils/model/GeometryCreator.h"

#if __has_include(<filesystem>)
#include <filesystem>
#elif __has_include(<experimental/filesystem>)
#include <experimental/filesystem>
namespace std
{
namespace filesystem = experimental::filesystem;
}
#else
#error need std c++ filesystem library (or experimental/filesystem)
#endif

#ifndef URDF_TYPEDEF_CLASS_POINTER
namespace urdf
{
using GeometrySharedPtr = ::boost::shared_ptr<Geometry>;
using MaterialSharedPtr = ::boost::shared_ptr<Material>;
} // namespace urdf
#endif

namespace move4d
{

namespace file
{

namespace model
{

class URDFImporter::PImpl
{
    MOVE4D_STATIC_LOGGER;

  public:
    rl::mdl::UrdfFactory rlUrdfFactory;
    char padding[7]{}; // TODO(Jules) this removes a warning, find a cleaner way?
    AssimpImporter assimpImport;

    bool isDae(const std::string &path) { return checkExtension(path, "dae", false); }
    void loadGeometry(const urdf::GeometrySharedPtr &geometry,
                      const std::filesystem::path &urdf_dir, rl::sg::Body *body,
                      const move4d::math::Transform &pos,
                      const std::optional<Eigen::Vector4f> &color = {})
    {
        if (geometry->type == urdf::Geometry::MESH) {
            auto mesh = reinterpret_cast<urdf::Mesh *>(geometry.get());
            math::Vector3 scale(mesh->scale.x, mesh->scale.y, mesh->scale.z);
            if (isDae(mesh->filename)) {
                scale *= 0.1; // TODO(Jules): applying scale 1/10 when loading a DAE file. Should
                              // check that
            }
            try {
                assimpImport.load(urdf_dir / mesh->filename, body, scale, pos, color);
            } catch (model::LoadException e) {
                // no-op
                // TODO(Jules): have a nice handling of missing/unreadable mesh files
                // e.g mark object to always be in collision, forbid planning in this
                // instance...
                M4D_ERROR("failed to load mesh " << mesh->filename << " for body "
                                                 << body->getName());
            }
        } else if (geometry->type == urdf::Geometry::SPHERE) {
            auto sphere = reinterpret_cast<urdf::Sphere *>(geometry.get());
            utils::model::addSphereToBody(sphere->radius, body, pos, color);
        } else if (geometry->type == urdf::Geometry::BOX) {
            auto box = reinterpret_cast<urdf::Box *>(geometry.get());
            auto size = box->dim;
            utils::model::addBoxToBody({size.x, size.y, size.z}, body, pos, color);
        } else if (geometry->type == urdf::Geometry::CYLINDER) {
            auto cylinder = reinterpret_cast<urdf::Cylinder *>(geometry.get());
            utils::model::addCylinderToBody(cylinder->radius, cylinder->length, body, pos, color);
        } else {
            // TODO(Jules): load other types of shapes
            std::cout << "should load a " << geometry->type << std::endl;
            assert(false);
        }
    }

    rl::sg::Body *getOrCreateBody(size_t i, rl::sg::Model &model)
    {
        if (model.getNumBodies() <= i) {
            return model.create();
        }
        return model.getBody(i);
    }

    void loadCollision(const urdf::ModelInterface &urdfMdl, const std::filesystem::path &urdf_dir,
                       rl::plan::Model &model, std::optional<Eigen::Vector4f> color = {})
    {
        // for each body of the kinematic chain, add the corresponding mesh, in the same order
        for (size_t i = 0; i < model.mdl->getBodies(); ++i) {
            for (auto lk : urdfMdl.links_) {
                if (lk.second->name == model.mdl->getBody(i)->getName()) {
                    if (lk.second) {
                        auto body = getOrCreateBody(i, *model.model);
                        body->setName(lk.second->name);
                        for (const auto &col :
                             lk.second->collision_array) { // requires urdfdom version >= 0.3.0
                            if (col->geometry) {
                                move4d::math::Transform pos;
                                urdf::Vector3 &trans = col->origin.position;
                                urdf::Rotation &rot = col->origin.rotation;
                                move4d::math::Quaternion q(rot.w, rot.x, rot.y, rot.z);
                                move4d::math::Translation t(trans.x, trans.y, trans.z);
                                pos = t * q;
                                this->loadGeometry(col->geometry, urdf_dir, body, pos, color);
                            } else {
                                model.model->create();
                            }
                        }
                    }
                }
            }
        }
    }
    void loadVisual(const urdf::ModelInterface &urdfMdl, const std::filesystem::path &urdf_dir,
                    rl::plan::Model &model)
    {
        // for each body of the kinematic chain, add the corresponding mesh, in the same order
        for (size_t i = 0; i < model.mdl->getBodies(); ++i) {
            for (auto lk : urdfMdl.links_) {
                if (lk.second->name == model.mdl->getBody(i)->getName()) {
                    if (lk.second) {
                        auto body = getOrCreateBody(i, *model.model);
                        for (const auto &vis :
                             lk.second->visual_array) { // requires urdfdom version >= 0.3.0
                            if (vis->geometry) {
                                move4d::math::Transform pos;
                                urdf::Vector3 &trans = vis->origin.position;
                                urdf::Rotation &rot = vis->origin.rotation;
                                move4d::math::Quaternion q(rot.w, rot.x, rot.y, rot.z);
                                move4d::math::Translation t(trans.x, trans.y, trans.z);
                                pos = t * q;
                                std::optional<Eigen::Vector4f> color;
                                urdf::MaterialSharedPtr urdfMaterial;
                                // checks that material => material_name (implies)
                                assert(!vis->material || !vis->material_name.empty());
                                if (!vis->material_name.empty()) {
                                    // material name is mandatory if a material is present
                                    auto searchMaterial =
                                        urdfMdl.materials_.find(vis->material_name);
                                    if (vis->material &&
                                        searchMaterial == urdfMdl.materials_.end()) {
                                        urdfMaterial = vis->material;
                                    } else {
                                        urdfMaterial = searchMaterial->second;
                                    }
                                }
                                if (urdfMaterial) {
                                    urdf::Color &c = urdfMaterial->color;
                                    color = Eigen::Vector4f(c.r, c.g, c.b, c.a);
                                }
                                this->loadGeometry(vis->geometry, urdf_dir, body, pos, color);
                            } else {
                                model.model->create();
                            }
                        }
                    }
                }
            }
        }
    }
};

INIT_MOVE4D_STATIC_LOGGER(URDFImporter::PImpl, "move4d.file.model.urdfimporter.pimpl");

URDFImporter::URDFImporter() : pimpl(new PImpl) {}

URDFImporter::~URDFImporter() { delete pimpl; }

move4d::mdl::SceneObjectPtr URDFImporter::load(const std::string &filepath,
                                               move4d::mdl::Scene *scene, const std::string &name,
                                               const move4d::Identifier &id)
{

    // init VRML //
    ::SoDB::init();
    // auto root = new ::SoVRMLGroup();

    std::ifstream file(filepath);
    std::ostringstream text;
    text << file.rdbuf();
    auto urdfMdl = urdf::parseURDF(text.str()); // to be ok with urdfdom versions < 0.4
    std::string l_name;
    if (name.empty()) {
        l_name = urdfMdl->name_;
    } else {
        l_name = name;
    }

    auto sceneObject =
        scene->create("Manipulable", l_name, id); // TODO(Jules): urdf/move4d object type
    auto model = sceneObject->model();

    auto kin = std::make_shared<rl::mdl::Dynamic>(); // NOLINT(cppcoreguidelines-owning-memory)
    model->mdl = kin.get();
    pimpl->rlUrdfFactory.load(filepath, model->mdl);

    auto urdf_dir = std::filesystem::path(filepath).remove_filename();

    pimpl->loadCollision(*urdfMdl, urdf_dir, *model);

    model->updateFrames();
    sceneObject->setKinematic(kin);
    sceneObject->updateModels();

    return sceneObject;
}

void URDFImporter::loadView(const std::string &filepath, view::Viewer *viewer,
                            const std::string &name, const Identifier &id, bool load_collisions)
{

    // init VRML //
    ::SoDB::init();
    // auto root = new ::SoVRMLGroup();

    std::ifstream file(filepath);
    std::ostringstream text;
    text << file.rdbuf();
    auto urdfMdl = urdf::parseURDF(text.str()); // to be ok with urdfdom versions < 0.4
    auto model = viewer->create();

    auto urdf_dir = std::filesystem::path(filepath).remove_filename();

    model->mdl = new rl::mdl::Dynamic(); // NOLINT(cppcoreguidelines-owning-memory)
    pimpl->rlUrdfFactory.load(filepath, model->mdl);

    if (load_collisions) {
        pimpl->loadCollision(*urdfMdl, urdf_dir, *model, Eigen::Vector4f(.5, .5, .5, .5));
    }
    pimpl->loadVisual(*urdfMdl, urdf_dir, *model);

    model->updateFrames();
    viewer->updateModels();
}

} // namespace model

} // namespace file

} // namespace move4d

//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//

#include "move4d/mdl/DegreeOfFreedomReference.h"
#include "move4d/mdl/State.h"

namespace move4d
{

namespace mdl
{

/**
 * creates the DoF with a reference to the given state
 */
DegreeOfFreedomReference::DegreeOfFreedomReference(const DegreeOfFreedomInfo &info,
                                                   State &state_ref)
    : DegreeOfFreedomInfo(info)
{
    m_value_ptr = &state_ref(index(), type());
}

move4d::Real DegreeOfFreedomReference::get() const { return *this->m_value_ptr; }

/**
 * Set the underlying value of the DoF. Performs no checks.
 */
void DegreeOfFreedomReference::set(move4d::Real value) { *m_value_ptr = value; }

} // namespace mdl

} // namespace move4d

//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef TRY_CLONE_INTERNAL_HPP
#define TRY_CLONE_INTERNAL_HPP

#include <type_traits>
#include <typeinfo>

template <class Base, std::enable_if_t<std::is_copy_constructible<Base>::value, int> = 0>
Base *_try_clone_internal(const Base *from)
{
    if (typeid(*from) == typeid(Base)) {
        return new Base(*from);
    }
    return nullptr;
}

template <class Base, std::enable_if_t<!std::is_copy_constructible<Base>::value, int> = 0>
Base *_try_clone_internal(const Base *from)
{
    return nullptr;
}

template <class Base, class NextType, class... Types> Base *_try_clone_internal(const Base *from)
{
    static_assert(std::is_copy_constructible<NextType>::value, "Type must be copy-constructible");
    static_assert(std::is_base_of<Base, NextType>::value, "Type must inherit from Base");
    if (typeid(*from) == typeid(NextType)) {
        return new NextType(reinterpret_cast<const NextType &>(*from));
    }

    return _try_clone_internal<Base, Types...>(from);
}

template <class Base> Base *_try_clone_explicit_internal(const Base *from) { return nullptr; }

template <class Base, class NextType, class... Types>
Base *_try_clone_explicit_internal(const Base *from)
{
    static_assert(std::is_copy_constructible<NextType>::value, "Type must be copy-constructible");
    static_assert(std::is_base_of<Base, NextType>::value, "Type must inherit from Base");
    if (typeid(*from) == typeid(NextType)) {
        return new NextType(reinterpret_cast<const NextType &>(*from));
    }

    return _try_clone_explicit_internal<Base, Types...>(from);
}

#endif // TRY_CLONE_INTERNAL_HPP

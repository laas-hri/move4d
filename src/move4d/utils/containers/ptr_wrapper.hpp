//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_UTILS_CONTAINERS_PTR_WRAPPER_HPP
#define MOVE4D_UTILS_CONTAINERS_PTR_WRAPPER_HPP

#include "gsl/pointers"
#include <memory>

/// a wrapper with value semantic around a std::unique_ptr<T>
template <class T> class ptr_wrapper
{
  public:
    ptr_wrapper() = default;
    /// clone the referenced object
    ptr_wrapper(const T &ref) : ptr(new_clone(&ref)) {}
    /// takes ownership of the pointer
    ptr_wrapper(gsl::owner<T *> p) : ptr(p) {}
    ptr_wrapper(std::unique_ptr<T> p) : ptr(std::move(p)) {}
    ptr_wrapper(const ptr_wrapper<T> &other)
        : ptr(other.ptr.get() ? new_clone(other.ptr.get()) : nullptr)
    {
    }
    ptr_wrapper(ptr_wrapper &&other) = default;
    ptr_wrapper &operator=(const ptr_wrapper<T> &other)
    {
        // ptr = std::unique_ptr<T>(new_clone(other.ptr.get()));
        *this = ptr_wrapper<T>(other);
        return *this;
    }
    ptr_wrapper &operator=(ptr_wrapper<T> &&other) = default;

    operator bool() const { return !!ptr; }

    bool operator==(const ptr_wrapper<T> &other) const { return *ptr == other.ptr; }

    const T *operator->() const { return ptr.get(); }
    T *operator->() { return ptr.get(); }
    const T &operator*() const { return *ptr; }
    T &operator*() { return *ptr; }

    typename std::unique_ptr<T>::pointer release() noexcept { return ptr.release(); }
    std::unique_ptr<T> release_ptr()
    {
        std::unique_ptr<T> p;
        ptr.swap(p);
        return p;
    }

  private:
    std::unique_ptr<T> ptr;
};

#endif // MOVE4D_UTILS_CONTAINERS_PTR_WRAPPER_HPP

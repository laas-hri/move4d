//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_MDL_COLLISIONMANAGER_H
#define MOVE4D_MDL_COLLISIONMANAGER_H

#include "move4d/common.h"
#include "move4d/mdl/Types.h"

namespace rl
{
namespace sg
{
class Model;
}
} // namespace rl
namespace rl
{
namespace sg
{
class Body;
}
} // namespace rl
namespace move4d
{
namespace mdl
{
class Scene;
}
} // namespace move4d
namespace rl
{
namespace sg
{
class SimpleScene;
}
} // namespace rl
namespace rl
{
namespace sg
{
class DistanceScene;
}
} // namespace rl
namespace rl
{
namespace sg
{
class DepthScene;
}
} // namespace rl
namespace rl
{
namespace sg
{
class RaycastScene;
}
} // namespace rl

namespace move4d
{

namespace mdl
{

/**
 * queries consider the whitelist of the scene
 */
// TODO(gbuisan) add getCollisionList and getClosestPoint
class CollisionManager
{
    MOVE4D_STATIC_LOGGER;

  public:
    typedef SceneObject &object_ref;

    typedef SceneBody &body_ref;

    /**
     * note: several pointer parameters can point to the same instance
     */
    CollisionManager(std::shared_ptr<Scene> scene,
                     std::shared_ptr<rl::sg::SimpleScene> collisionScene,
                     std::shared_ptr<rl::sg::DistanceScene> distanceScene = {},
                     std::shared_ptr<rl::sg::DepthScene> depthScene = {},
                     std::shared_ptr<rl::sg::RaycastScene> raycastScene = {});
    virtual ~CollisionManager() = default;

    void applyWorldState(const mdl::WorldState &ws);

    bool areColliding(object_ref a, object_ref b) const;

    bool areColliding(object_ref a, body_ref b) const;

    bool areColliding(body_ref a, body_ref b) const;

    std::vector<CollisionPair> collisionList() const;

    virtual bool isColliding() const;

    move4d::Real distance(object_ref a, object_ref b) const;

    move4d::Real distance(object_ref a, body_ref b) const;

    move4d::Real distance(body_ref a, body_ref b) const;

    move4d::Real depth(object_ref a, object_ref b) const;

    move4d::Real depth(object_ref a, body_ref b) const;

    move4d::Real depth(body_ref a, body_ref b) const;

    const std::shared_ptr<Scene> &getScene() const;

  protected:
    void updateFrames();

    std::shared_ptr<Scene> m_scene;

    std::shared_ptr<rl::sg::SimpleScene> m_collisionScene;

    std::shared_ptr<rl::sg::DistanceScene> m_distanceScene;

    std::shared_ptr<rl::sg::DepthScene> m_depthScene;

    std::shared_ptr<rl::sg::RaycastScene> m_raycastScene;
};

} // namespace mdl

} // namespace move4d
#endif

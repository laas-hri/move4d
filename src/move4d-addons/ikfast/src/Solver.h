//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef _ADDONS_IKFAST_SRC_SOLVER_H
#define _ADDONS_IKFAST_SRC_SOLVER_H

#include <move4d/io/Logger.h>
#include <move4d/mdl/Kinematic.h>
#include <move4d/mdl/SceneObject.h>
#include <move4d/mdl/kin/KinematicSolver.h>

namespace move4d
{
namespace kin
{
namespace ikfast
{

class IkFastSolver;
using IkFastSolverPtr = std::shared_ptr<IkFastSolver>;
using IkFastSolverFactory = SimpleFactory<IkFastSolverPtr()>;

/**
 * @brief The IkFastSolver class is the base class for implementing IK Fast solvers.
 *
 * IK fast solvers are specific to a kinematic chain. The correct interface to use them is via
 * the move4d::kin::ikfast::Solver class (wich is registered in the KinematicSolverFactory)
 */
class IkFastSolver : public KinematicSolver
{
    MOVE4D_STATIC_LOGGER;

  public:
    using KinematicSolver::computeForwardKinematic;
    using KinematicSolver::computeInverseKinematic;
    bool computeInverseKinematic(const mdl::Chain &chain, const math::Transform &base_pose,
                                 const math::Transform &end_pose, math::Vector &x_in_out) final;
    void computeInverseKinematic(const mdl::SceneObject &object, const mdl::Chain &chain,
                                 const math::Transform &pose, const mdl::State &state,
                                 const math::Vector &x,
                                 std::vector<std::pair<Real, math::Vector>> &solutions);
    void computeInverseKinematic(const mdl::Chain &chain, const math::Transform &base_pose,
                                 const math::Transform &end_pose, const math::Vector &x,
                                 std::vector<std::pair<Real, math::Vector>> &solutions);
    bool computeForwardKinematic(const mdl::Chain &chain, const math::Transform &base_pose,
                                 math::Transform &end_pose_out, const math::Vector &x_in) override;

    virtual int getNumFreeParameters() = 0;
    virtual int *getFreeParameters() = 0;

  protected:
    virtual void computeInverseKinematic(gsl::span<const Real> eepos, gsl::span<const Real> eerot,
                                         const math::Vector &x, gsl::span<const Real> pfree,
                                         std::vector<std::pair<Real, math::Vector>> &solutions) = 0;
    virtual void computeForwardKinematic(gsl::span<Real> eepos, gsl::span<Real> eerot,
                                         const math::Vector &x_in) = 0;
};

class WrongFreeJointException : public std::runtime_error
{
  public:
    WrongFreeJointException(const std::string &msg)
        : std::runtime_error(std::string("IkFast: wrong free joints ") + msg)
    {
    }
};

/**
 * @brief The Solver class is the interface to call specific IK fast plugin.
 *
 * @todo the ikfast plugin is searched by robot name + chain name. need to find a better way
 */
class Solver : public KinematicSolver
{
    MOVE4D_STATIC_LOGGER;

  public:
    using KinematicSolver::computeForwardKinematic;
    using KinematicSolver::computeInverseKinematic;
    bool computeInverseKinematic(const mdl::Chain &chain, const math::Transform &base_pose,
                                 const math::Transform &end_pose, math::Vector &x_in_out) override;
    void computeInverseKinematic(const mdl::Chain &chain, const math::Transform &base_pose,
                                 const math::Transform &end_pose, const math::Vector &x_in,
                                 std::vector<std::pair<Real, math::Vector>> &solutions);
    void computeInverseKinematic(const mdl::SceneObject &object, const mdl::Chain &chain,
                                 const math::Transform &pose, const mdl::State &state,
                                 const math::Vector &x,
                                 std::vector<std::pair<Real, math::Vector>> &solutions);
    bool computeForwardKinematic(const mdl::Chain &chain, const math::Transform &base_pose,
                                 math::Transform &end_pose_out, const math::Vector &x_in) override;
};

/**
 * @brief The RedundantSolver class shoots redundant degrees of freedom until a solution is found
 * for IK.
 *
 */
class RedundantSolver : public Solver
{
    MOVE4D_STATIC_LOGGER;

  public:
    using Solver::computeForwardKinematic;
    using Solver::computeInverseKinematic;
    bool computeInverseKinematic(const mdl::SceneObject &object, const mdl::Chain &chain,
                                 const math::Transform &pose, const mdl::State &state,
                                 math::Vector &x_in_out) override;
};

} // namespace ikfast
} // namespace kin
} // namespace move4d

#endif // _ADDONS_IKFAST_SRC_SOLVER_H

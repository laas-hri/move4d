//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#include "move4d/plan/ompl/goals/KinematicGraphGoalSampler.h"
#include "move4d/plan/GoalSampler.h"
#include "move4d/plan/ompl/KinematicGraphStateSpace.h"
#include "move4d/plan/ompl/converters/StateSpaceFactory.h"
#include "move4d/utils/io/print_to_stream.hpp"

namespace move4d
{
namespace plan
{
namespace ompl
{

INIT_MOVE4D_STATIC_LOGGER(KinematicGraphGoalSampler, "move4d.plan.ompl.kinematicgraphgoalsampler");

KinematicGraphGoalSampler::KinematicGraphGoalSampler(const GoalSampler &goal_sampler,
                                                     const ::ompl::base::SpaceInformationPtr &si)
    : ::ompl::base::GoalSampleableRegion(si)
{
    _sampler = si_->getStateSpace()->as<KinematicGraphStateSpace>()->allocStateSampler();
    auto &sampler = dynamic_cast<KinematicGraphStateSampler &>(*_sampler);
    for (auto p : goal_sampler.getSamplers()) {
        sampler.setSampler(p.first, p.second);
    }
}

double KinematicGraphGoalSampler::distanceGoal(const ::ompl::base::State *st) const { return 1.; }

void KinematicGraphGoalSampler::sampleGoal(::ompl::base::State *st) const
{
    _sampler->sampleUniform(st);
    M4D_TRACE("shot " << PRINT_TO_STREAM(si_->getStateSpace()->printState(st, oss)));
}

unsigned int KinematicGraphGoalSampler::maxSampleCount() const { return UINT_MAX; }

} // namespace ompl
} // namespace plan
} // namespace move4d

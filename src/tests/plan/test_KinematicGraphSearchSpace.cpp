//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#include "move4d/mdl/Movable.h"
#include "move4d/plan/KinematicGraphSearchSpace.h"
#include "move4d/plan/SO3Space.h"
#include "tests/GlobalFixtures.h"
#include "tests/plan/FixtureKinematicGraph.h"
#include <boost/test/unit_test.hpp>
#include <rl/mdl/Dynamic.h>

namespace move4d
{
namespace tests
{

BOOST_FIXTURE_TEST_SUITE(kinematic_wrapper, GlobalFixture)

BOOST_AUTO_TEST_CASE(construct)
{
    mdl::Movable *object = lwr_ff_base.get()->as<mdl::Movable>();
    auto part_base = std::make_shared<plan::SE2Space>(
        object, 0, move4d::plan::SE2Space::SE2Type::Holonomic, "base_holo");
    plan::KinematicSpaceWrapper space(part_base->cloneSearchSpace(), object, "root",
                                      "lwr_base_link");
    BOOST_CHECK_EQUAL(space.getDimension(), part_base->getDimension());
    BOOST_CHECK_EQUAL(space.getDimension(), 3);
    BOOST_CHECK_EQUAL(part_base->getDimension(), 3);
}

BOOST_AUTO_TEST_SUITE_END() // kinematic_wrapper

BOOST_FIXTURE_TEST_SUITE(kinematic_graph_search_space, FixtureKinematicGraph)

BOOST_AUTO_TEST_CASE(construct)
{
    BOOST_CHECK_EQUAL(kin_graph->getDimension(), 3 + 7);
    BOOST_CHECK_EQUAL(arm_part->getDimension(), 7);
    BOOST_CHECK_EQUAL(kin_base->getDimension(), 3);
}

BOOST_AUTO_TEST_CASE(sample, *boost::unit_test::expected_failures(100))
{
    mdl::WorldState ws(_scene.get());
    for (size_t i = 0; i < 100; ++i) {
        BOOST_CHECK_NO_THROW(kin_graph->shootUniform(ws));
        BOOST_CHECK(ws[lwr_ff_base->getId()].isInBounds());
    }
}

BOOST_AUTO_TEST_CASE(interpolate) { BOOST_CHECK(true); }

BOOST_AUTO_TEST_SUITE_END() // kinematic_graph_state_space

} // namespace tests

} // namespace move4d

#include "MySampler.h"
#include <QApplication>
#include <move4d-soqt/Application.h>
#include <move4d/common.h>
#include <move4d/file/m4df/M4DFDefinition.h>
#include <move4d/mdl/CollisionManager.h>
#include <move4d/mdl/CollisionScene.h>
#include <move4d/motion/GeometricPath.h>
#include <move4d/plan/GoalSampler.h>
#include <move4d/plan/KinematicGraphSearchSpace.h>
#include <move4d/plan/SearchSpace.h>
#include <move4d/plan/ompl/Planner.h>
#include <move4d/view/Viewer.h>
#include <ompl/geometric/PathGeometric.h>

class Application
{
  public:
    Application(int argc, char *argv[]);
    void start();

  protected:
    void run();
    std::unique_ptr<move4d::SoQt::Application> m_app;

  private:
    bool m_running;
};

Application::Application(int argc, char *argv[])
{

    auto cli_desc = move4d::SoQt::Application::defaultCliDescritpions();
    boost::program_options::variables_map cli_options;
    boost::program_options::store(boost::program_options::parse_command_line(argc, argv, cli_desc),
                                  cli_options);
    m_app = std::make_unique<move4d::SoQt::Application>(cli_options,
                                                        std::bind(&Application::run, this));
}

void Application::start()
{
    // starts the visualisation and call the function object passed in the
    // construction it returns when the user closes the window and the function is
    // finished
    this->m_app->start();
}

void Application::run()
{
    using namespace move4d;
    m_running = true;

    // the m4df file should contain the definition of a kinematic_graph search space called
    // pull_door. The m4df parser creates the search space but also keeps the builder it
    // used to make it. We will get it to change the definition of the m4df search space
    // to add a custom sampler
    auto builder =
        m_app->getM4DFDefinition()->getKinematicGraphBuilders().find("pull_door")->second;
    move4d::math::Transform base_to_arm_tf =
        math::Translation(0, 0, 1.) * math::Quaternion::Identity();
    builder->selectByName("robot_base")
        .withSampler(
            [base_to_arm_tf](std::shared_ptr<const move4d::plan::SearchSpace> ss) {
                return std::make_shared<move4d::SampleKukaBaseFromEef>(ss, base_to_arm_tf, 1., 0);
            },
            {"eef"});

    std::shared_ptr<plan::KinematicGraphSearchSpace> ss = builder->build();
    if (!ss) {
        return;
    }

    // we will also create another space from the same definition with constraints for the goal
    std::shared_ptr<plan::KinematicGraphSearchSpace> goal_ss;
    {
        auto builder_goal = builder;
        math::Vector3 lower{-M_PI / 2, 0., 0.}, upper{-M_PI / 2, 0., 0.};
        builder_goal->selectByName("door_door").withLimits(lower, upper);
        goal_ss = builder_goal->withName("pull_door_goal").build();
    }
    if (!goal_ss) {
        return;
    }

    // and similarly another for the start
    std::shared_ptr<plan::KinematicGraphSearchSpace> start_ss;
    {
        auto builder_start = builder;
        math::Vector3 lower{0., 0., 0.}, upper{0., 0., 0.};
        builder_start->selectByName("door_door").withLimits(lower, upper);
        start_ss = builder_start->withName("pull_door_goal").build();
    }
    if (!start_ss) {
        return;
    }

    mdl::WorldState init_ws(m_app->getScene().get());
    mdl::WorldState goal_ws = init_ws;

    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    do {
        start_ss->shootUniform(init_ws);
        m_app->getCollisionManager()->applyWorldState(init_ws);
    } while (m_app->getCollisionManager()->isColliding());
    m_app->getViewer()->update(init_ws);
    do {
        goal_ss->shootUniform(goal_ws);
        m_app->getCollisionManager()->applyWorldState(goal_ws);
    } while (m_app->getCollisionManager()->isColliding());

    std::this_thread::sleep_for(std::chrono::seconds(2));
    m_app->getViewer()->update(goal_ws);

    plan::ompl::Planner planner(ss->cloneSearchSpace(), m_app->getCollisionManager(),
                                m_app->getCliOptions()["planner"].as<std::string>());
    planner.setStart(init_ws.cloneWorldState());
    planner.setGoal(std::make_shared<plan::GoalSpaceSampler>(goal_ss, ss, 0.1));

    bool ok = planner.solve(60);

    if (ok) {
        std::cout << "solution found\n";
        auto path = planner.getPath();
        auto path_simplified = planner.getPath(true, 10.);

        path->interpolate(init_ws);
        path_simplified->interpolate(init_ws);
        while (M4D_WHILE_CONDITION(true)) {
            std::cout << "original\n";
            m_app->getViewer()->playPath(init_ws, *path);
            std::this_thread::sleep_for(std::chrono::milliseconds(500));
            std::cout << "simplified\n";
            m_app->getViewer()->playPath(init_ws, *path_simplified);
            std::this_thread::sleep_for(std::chrono::milliseconds(500));
        }
    } else {
        std::cout << "no solution found\n";
    }
}

int main(int argc, char *argv[])
{
    Application app(argc, argv);
    app.start();
    return 0;
}

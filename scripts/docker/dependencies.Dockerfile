ARG DISTRIB=ubuntu
ARG DISTRIB_VERSION=latest
FROM ${DISTRIB}:${DISTRIB_VERSION} AS builder

RUN apt-get update && apt-get -y upgrade && apt-get install -y \
    build-essential \
    git \
    curl \
    tar \
    cmake \
    libeigen3-dev \
    libboost-all-dev \
    libxml2-dev \
    pkgconf \
    liburdfdom-dev \
    libsdformat*-dev \
    libxslt1-dev \
    libcoin*-dev \
    libsoqt*-dev \
    libqt4-dev \
    libfcl-dev \
    libnlopt-dev \
    liblog4cxx-dev \
    libtinyxml2-dev \
    libbullet-dev


WORKDIR /build
RUN curl -L https://github.com/roboticslibrary/rl/archive/d96c8b4cfdbed8ad81015f1879c27d0a79d18688.tar.gz --output rl.tar.gz && \
    tar xzf rl.tar.gz && mv rl-d96c8b4cfdbed8ad81015f1879c27d0a79d18688 rl && \
    cd rl && \
    mkdir build && \
    cd build && \
    cmake .. && \
    make -j`nproc` install

RUN cd /build && \
    curl -L https://github.com/ompl/ompl/archive/1.4.1.tar.gz --output ompl.tar.gz && \
    tar xzf ompl.tar.gz && \
    cd ompl-1.4.1 && \
    mkdir build && \
    cd build && \
    cmake .. && \
    make -j`nproc` install

RUN cd /build && \
    curl -L https://github.com/roboptim/roboptim-core/archive/master.tar.gz --output roboptim-core.tar.gz && \
    tar xzf roboptim-core.tar.gz && \
    cd roboptim-core-master && \
    curl -L https://github.com/jrl-umi3218/jrl-cmakemodules/archive/bf10c6ca3565b96ab283edaa8f18b5b533e59da9.tar.gz --output cmake.tar.gz && \
    tar xzf cmake.tar.gz && mv jrl-cmakemodules-bf10c6ca3565b96ab283edaa8f18b5b533e59da9/* cmake/ && \
    mkdir build && \
    cd build && \
    cmake .. -DDISABLE_TESTS=ON && \
    make -j`nproc` install

RUN cd /build && \
    curl -L https://github.com/assimp/assimp/archive/v4.1.0.tar.gz --output assimp.tar.gz && \
    tar xzf assimp.tar.gz && \
    cd assimp-4.1.0 && \
    mkdir build && \
    cd build && \
    cmake .. && \
    make -j`nproc` install


RUN cd /build && \
    curl -L https://github.com/orocos/orocos_kinematics_dynamics/archive/v1.4.0.tar.gz --output orocos-kinematics.tar.gz && \
    tar xzf orocos-kinematics.tar.gz && \
    cd orocos_kinematics_dynamics-1.4.0/orocos_kdl && \
    mkdir build && \
    cd build && \
    cmake .. && \
    make -j`nproc` install
    

FROM ${DISTRIB}:${DISTRIB_VERSION}
RUN apt-get update && apt-get -y upgrade && apt-get install -y \
    build-essential \
    git \
    curl \
    tar \
    cmake \
    libeigen3-dev \
    libboost-all-dev \
    libxml2-dev \
    pkgconf \
    liburdfdom-dev \
    libsdformat*-dev \
    libxslt1-dev \
    libcoin*-dev \
    libsoqt*-dev \
    libqt4-dev \
    libfcl-dev \
    libnlopt-dev \
    liblog4cxx-dev \
    libbullet-dev

COPY --from=builder /usr /usr

ENV LD_LIBRARY_PATH=/usr/lib:/usr/local/lib


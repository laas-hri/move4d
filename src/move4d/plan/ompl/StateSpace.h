//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_PLAN_OMPL_STATESPACE_H
#define MOVE4D_PLAN_OMPL_STATESPACE_H

#include "move4d/common.h"
#include <ompl/base/StateSpace.h>

namespace move4d
{
namespace mdl
{
class WorldState;
using WorldStatePtr = std::shared_ptr<WorldState>;
} // namespace mdl
namespace plan
{
class SearchSpace;
using SearchSpacePtr = std::shared_ptr<SearchSpace>;
namespace ompl
{
class StateSpaceWrapper;
using StateSpaceWrapperPtr = std::shared_ptr<StateSpaceWrapper>;

class StateSpace : public ::ompl::base::StateSpace
{
    MOVE4D_STATIC_LOGGER;

  public:
    class StateType : public ::ompl::base::State
    {
      public:
        enum {
            ValidityComputed = 1,
            IsValid = 1 << 1,
            GoalDistanceKnown = 1 << 2,
            IsStart = 1 << 3,
            IsGoal = 1 << 4
        };
        StateType() = default;
        move4d::Real *values = nullptr;
        move4d::Real distance = 0.0;
        int flags = 0;

        bool validityComputed() const { return flags & ValidityComputed; }
        bool isValid() const { return flags & IsValid; }
        void setValidity(bool valid)
        {
            flags |= ValidityComputed;
            (valid ? flags |= IsValid : flags &= ~IsValid);
        }
    };

    StateSpace(const move4d::plan::SearchSpace &space, mdl::WorldStatePtr initWs = {});

    const std::shared_ptr<const mdl::WorldState> &getInitWorldState() const;
    void setInitWorldState(std::shared_ptr<const mdl::WorldState> initWorldState);

    std::weak_ptr<StateSpaceWrapper> getWrapper() const;
    void setWrapper(std::weak_ptr<StateSpaceWrapper> wrapper);

    // StateSpace interface
  public:
    unsigned int getDimension() const override;
    double getMaximumExtent() const override;
    double getMeasure() const override;
    void enforceBounds(::ompl::base::State *state) const override;
    bool satisfiesBounds(const ::ompl::base::State *state) const override;
    void copyState(::ompl::base::State *destination,
                   const ::ompl::base::State *source) const override;
    double distance(const ::ompl::base::State *state1,
                    const ::ompl::base::State *state2) const override;
    bool equalStates(const ::ompl::base::State *state1,
                     const ::ompl::base::State *state2) const override;
    void interpolate(const ::ompl::base::State *from, const ::ompl::base::State *to, double t,
                     ::ompl::base::State *state) const override;
    ::ompl::base::StateSamplerPtr allocDefaultStateSampler() const override;
    ::ompl::base::State *allocState() const override;
    void freeState(::ompl::base::State *state) const override;

    const std::vector<StateSpaceWrapperPtr> &getSubspaces() const;

    /**
     * create and return an ompl State that uses the memory of state if possible.
     * Otherwise, the substate is created but the values are not copied
     */
    gsl::owner<::ompl::base::State *> createStateForSubspace(const StateSpaceWrapperPtr &subspace,
                                                             const StateType *state,
                                                             size_t first_dof,
                                                             size_t &next_dof) const;
    /** create and return an ompl State that uses the memory of state if possible.
     * Otherwise, the substate is created and the values ARE copied in it
     */
    gsl::owner<::ompl::base::State *> convertStateForSubspace(const StateSpaceWrapperPtr &subspace,
                                                              const StateType *state,
                                                              size_t first_dof,
                                                              size_t &next_dof) const;
    void copyValuesFromSubStates(const StateType *state,
                                 const std::vector<::ompl::base::State *> &states) const;

    /** for subspaces where [create/convert]StateForSubspace cannot reuse the memory, copy the
     * values from the substate to the state. Otherwise, the data should actually be the same heap
     * block.
     * @warning does not check that the data that can be shared on the heap is actually.
     * @note No copy is done if the data is supposed to be shared
     */
    size_t copyValuesFromSubState(const StateType *state, const StateSpaceWrapperPtr &subspace,
                                  const ::ompl::base::State *substate, size_t first_dof) const;

    void freeSubspaceState(const StateSpaceWrapperPtr &subspace, ::ompl::base::State *state) const;

    void printState(const ::ompl::base::State *state, std::ostream &out) const override;

    double *getValueAddressAtIndex(::ompl::base::State *state, unsigned int index) const override;
    void copyToReals(std::vector<double> &reals, const ::ompl::base::State *source) const override;
    void copyFromReals(::ompl::base::State *destination,
                       const std::vector<double> &reals) const override;

    inline size_t getVariableCount() const { return m_variableCount; }

    void sanityChecks(double zero, double eps, unsigned int flags) const override;
    void sanityChecks() const override;

  protected:
    std::shared_ptr<const mdl::WorldState> m_initWorldState;
    size_t m_variableCount; // number of values in associated StateType::values
    std::vector<StateSpaceWrapperPtr> m_subspaces;
    std::weak_ptr<StateSpaceWrapper> m_wrapper = {}; // weak_ptr to break reference cycle

    // memory:
    size_t m_stateValuesSize; // size of a StateType::values (for memory manipulation)

    void addSubspaces(const SearchSpace &space);
    virtual void allocStateMembers(StateType *state) const;
};

class DefaultStateSampler : public ::ompl::base::StateSampler
{
  public:
    DefaultStateSampler(const StateSpace *space);
    void sampleUniform(::ompl::base::State *state) override;
    void sampleUniformNear(::ompl::base::State *state, const ::ompl::base::State *near,
                           double distance) override;
    void sampleGaussian(::ompl::base::State *state, const ::ompl::base::State *mean,
                        double stdDev) override;

  protected:
    std::vector<::ompl::base::StateSamplerPtr> subsamplers_;
};
} // namespace ompl
} // namespace plan
} // namespace move4d

#endif // MOVE4D_PLAN_OMPL_STATESPACE_H

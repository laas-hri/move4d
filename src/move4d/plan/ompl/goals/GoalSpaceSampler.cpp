//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#include "move4d/plan/ompl/goals/GoalSpaceSampler.h"
#include "move4d/3rdParty/prettyprint/prettyprint.hpp"
#include "move4d/plan/ompl/KinematicGraphStateSpace.h"
#include "move4d/plan/ompl/converters/StateSpaceFactory.h"
#include "move4d/utils/io/print_to_stream.hpp"

namespace move4d
{
namespace plan
{
namespace ompl
{

INIT_MOVE4D_STATIC_LOGGER(GoalSpaceSampler, "move4d.plan.ompl.goalspacesampler");

Hyperbox::Hyperbox(::ompl::base::SpaceInformationPtr si,
                   std::map<size_t, std::pair<Real, Real>> box_limits)
    : _si(std::move(si)), _limits(std::move(box_limits))
{
}

bool Hyperbox::isIn(const ::ompl::base::State *st) const
{
    auto &locs = _si->getStateSpace()->getValueLocations();
    for (auto &l : locs) {
        auto search = _limits.find(l.index);
        if (search != _limits.end()) {
            if (!(search->second.first <= *_si->getStateSpace()->getValueAddressAtLocation(st, l) &&
                  search->second.second >=
                      *_si->getStateSpace()->getValueAddressAtLocation(st, l))) {
                return false;
            }
        }
    }
    return true;
}

Real Hyperbox::distance(const ::ompl::base::State *st) const
{
    auto &locs = _si->getStateSpace()->getValueLocations();
    Real dist = 0.;
    for (auto &l : locs) {
        auto search = _limits.find(l.index);
        if (search != _limits.end()) {
            if (!(search->second.first <= *_si->getStateSpace()->getValueAddressAtLocation(st, l) &&
                  search->second.second >=
                      *_si->getStateSpace()->getValueAddressAtLocation(st, l))) {
                // compute distance on dimension only if not in box
                Real diff =
                    std::min(std::abs(search->second.first -
                                      *_si->getStateSpace()->getValueAddressAtLocation(st, l)),
                             std::abs(search->second.second -
                                      *_si->getStateSpace()->getValueAddressAtLocation(st, l)));
                dist += diff * diff;
            }
        }
    }
    return std::sqrt(dist);
}

GoalSpaceSampler::GoalSpaceSampler(::ompl::base::SpaceInformationPtr si,
                                   StateSpaceWrapperPtr goal_space_wrapper,
                                   std::vector<size_t> goal_to_space_bijection,
                                   std::vector<size_t> goal_to_space_poses_bijection)
    : ::ompl::base::GoalSampleableRegion(std::move(si)),
      _goal_space_wrapper(std::move(goal_space_wrapper)),
      _goal_space(std::dynamic_pointer_cast<KinematicGraphStateSpace>(
          _goal_space_wrapper->getStateSpace())),
      _sampler(_goal_space->allocStateSampler()),
      _goal_to_space_poses_bijection(std::move(goal_to_space_poses_bijection))
{
    assert(_goal_space);
    _goal_space->setup();
    _goal_to_space_bijection.assign(si_->getStateDimension(), 0);

    auto search_space = si_->getStateSpace()->as<KinematicGraphStateSpace>();
    _goal_space->setInitWorldState(search_space->getInitWorldState());
    for (size_t i = 0; i < goal_to_space_bijection.size(); ++i) {
        assert(_goal_space->getElements()[i].bijection.size() ==
               search_space->getElements()[goal_to_space_bijection[i]].bijection.size());
        for (size_t sub_i = 0; sub_i < _goal_space->getElements()[i].bijection.size(); ++sub_i) {
            _goal_to_space_bijection[_goal_space->getElements()[i].bijection[sub_i]] =
                search_space->getElements()[goal_to_space_bijection[i]].bijection[sub_i];
        }
    }
    M4D_INFO(_goal_to_space_bijection);
    //_goal_space->getElements()[0].
    assert(_goal_to_space_bijection.size() == si_->getStateDimension());
    assert(_goal_to_space_bijection.size() == _goal_space->getDimension());
    assert(_goal_space->getNumberOfElements() ==
           si_->getStateSpace()->as<KinematicGraphStateSpace>()->getNumberOfElements());
    assert(_goal_to_space_poses_bijection.size() == _goal_space->getNumberOfVertices());
}

double GoalSpaceSampler::distanceGoal(const ::ompl::base::State *st) const
{
    // TODO(Jules): distanceGoal
    // abort();
    return 1.;
}

void GoalSpaceSampler::sampleGoal(::ompl::base::State *st) const
{
    KinematicGraphStateSpace::StateType *sampled_state =
        _goal_space->allocState()->as<KinematicGraphStateSpace::StateType>();
    _sampler->sampleUniform(sampled_state);

    auto *state = st->as<KinematicGraphStateSpace::StateType>();
    for (size_t i = 0; i < _goal_to_space_bijection.size(); ++i) {
        state->values[_goal_to_space_bijection[i]] = sampled_state->values[i];
    }

    for (size_t i = 0; i < _goal_to_space_poses_bijection.size(); ++i) {
        _goal_space->getVerticesStateSpaces()[i]->copyState(
            state->poses[_goal_to_space_poses_bijection[i]], sampled_state->poses[i]);
    }
    state->flags = sampled_state->flags;
    M4D_TRACE("sampled goal:\n"
              << PRINT_TO_STREAM(_goal_space->printState(sampled_state, oss)) << "\n---\n"
              << "goal state:\n"
              << PRINT_TO_STREAM(si_->getStateSpace()->printState(state, oss)) << "\n---");
}

} // namespace ompl
} // namespace plan
} // namespace move4d

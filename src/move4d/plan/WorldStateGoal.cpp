//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//

#include "move4d/plan/WorldStateGoal.h"

#include <utility>

#include "move4d/mdl/WorldState.h"
#include <utility>

namespace move4d
{

namespace plan
{

WorldStateGoal::WorldStateGoal(std::shared_ptr<const mdl::WorldState> ws, Real distance_threshold)
    : Goal(1, 1), // TODO(Jules)
      m_goalWorldState(std::move(std::move(ws))), m_distance_threshold(distance_threshold)
{
}

void WorldStateGoal::setGoalWorldState(std::shared_ptr<const mdl::WorldState> &value)
{
    m_goalWorldState = std::move(value);
}

/**
 * heuristic distance to reach the goal
 */
move4d::Real WorldStateGoal::distance(const move4d::mdl::WorldState &ws) const
{
    return m_goalWorldState->distance(ws);
}

/**
 * is the goal reached?
 */
bool WorldStateGoal::check(const move4d::mdl::WorldState &ws) const
{
    return distance(ws) < m_distance_threshold;
}

void WorldStateGoal::impl_compute(WorldStateFunction::result_ref result,
                                  WorldStateFunction::const_argument_ref argument) const
{
    // TODO(Jules)
}

} // namespace plan

} // namespace move4d

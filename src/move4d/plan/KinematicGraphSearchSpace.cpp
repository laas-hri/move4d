//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#include "move4d/plan/KinematicGraphSearchSpace.h"
#include "move4d/mdl/Kinematic.h"
#include "move4d/mdl/Movable.h"
#include "move4d/plan/RobotSpace.h"
#include "move4d/plan/SO3Space.h"
#include "move4d/utils/KinematicHelper.hpp"
#include "move4d/utils/containers/iterators.h"
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/breadth_first_search.hpp>
#include <boost/graph/graphviz.hpp>
#include <boost/graph/strong_components.hpp>
#include <boost/graph/topological_sort.hpp>
#include <rl/mdl/Joint.h>

namespace move4d
{
namespace plan
{

const std::string KinematicGraphSearchSpaceBuilder::Fixed = "__m4d_kgssb_fixed_vertex__";

INIT_MOVE4D_STATIC_LOGGER(KinematicGraphSearchSpaceBuilder,
                          "move4d.plan.kinematicgraphsearchspace.builder");
INIT_MOVE4D_STATIC_LOGGER(KinematicGraphSearchSpace, "move4d.plan.kinematicgraphsearchspace");

// used in topo_order for the breadth_first_search
template <class Container> class BFSVisitor : public boost::default_bfs_visitor
{

  public:
    // v is where to append elements (vertex and edge descriptors) in the order of the bfs
    // only those elements belongin to the covering tree are added
    BFSVisitor(Container &v) : order(v) {}
    template <class Vertex, class Graph> void discover_vertex(Vertex v, const Graph &g) const
    {
        // the vertices are added to the order as they are visited
        order.push_back(g[v]);
    }
    template <class Edge, class Graph> void tree_edge(Edge e, const Graph &g) const
    {
        // only edges (spaces) marked as tree edges are added to the order.
        // tree edges are those belonging to the covering tree produced by the search
        order.push_back(g[e]);
    }

  private:
    Container &order;
};

std::unique_ptr<mdl::Chain> KinematicSpaceWrapper::_createChain(const SearchSpace &space,
                                                                const mdl::Movable &object,
                                                                const std::string &from_frame,
                                                                const std::string &to_frame)
{
    std::unique_ptr<mdl::Chain> chain_ptr(new mdl::Chain);
    auto &chain = *chain_ptr;
    chain.name = "auto_" + space.getName();
    chain.tipName = to_frame;
    size_t root_frame_index = object.getLinkIndex(from_frame);
    assert(root_frame_index < object.m_kinematic->getFrames());
    chain.rootFrame = object.m_kinematic->getFrame(root_frame_index);
    chain.kinematic = utils::kinematicSubChain(object, from_frame, to_frame, chain.bijection);

    return chain_ptr;
}

KinematicSpaceWrapper::KinematicSpaceWrapper(std::unique_ptr<SearchSpace> other,
                                             const mdl::Movable *object,
                                             const std::string &from_frame,
                                             const std::string &to_frame, const std::string &name)
    : RobotKinematicGroupSpace(
          object,
          /*TODO kinematicChain=*/_createChain(*other, *object, from_frame, to_frame),
          /*TODO ik_method=*/"default_forward", /*dofs=*/other->getDegreesOfFreedom(), name),
      _space(std::move(other))
{
    Expects(object);
    m_type = Type::KinematicGroup;
    // m_type = _space->getType();
}

const SearchSpace &KinematicSpaceWrapper::getSpace() const { return *_space; }

struct KinematicGraphSearchSpaceBuilder::PImpl {
    MOVE4D_STATIC_LOGGER;
    using Edge = KinematicGraphSearchSpace::Edge;
    using Vertex = KinematicGraphSearchSpace::Vertex;
    using Graph = boost::adjacency_list<boost::vecS, boost::vecS, boost::directedS, Vertex, Edge>;
    struct DependencyVertex {
        enum type_t { Edge, Vertex };
        DependencyVertex() { vd = {}; }
        DependencyVertex(std::string name, type_t type) : name(std::move(name)), type(type) {}
        std::string name;
        type_t type;
        union {
            Graph::vertex_descriptor vd;
            Graph::edge_descriptor ed;
        };
        static DependencyVertex make_vertex(Graph::vertex_descriptor vd, std::string name)
        {
            DependencyVertex x{std::move(name), Vertex};
            x.vd = vd;
            return x;
        }
        static DependencyVertex make_edge(Graph::edge_descriptor ed, std::string name)
        {
            DependencyVertex x{std::move(name), Edge};
            x.ed = ed;
            return x;
        }
    };
    using DependencyGraph =
        boost::adjacency_list<boost::setS, boost::vecS, boost::bidirectionalS, DependencyVertex>;

    // create a vertex of given name if it doesn't exist, and return the vertex_descriptor with that
    // name
    Graph::vertex_descriptor vertex(const std::string &name);

    void add_edge(Graph::vertex_descriptor u, Graph::vertex_descriptor v, Edge edge);
    void add_attachment(Graph::vertex_descriptor u, const SE3Space &se3space);

    /**
     * @brief get_sinks check if we depend on some unfixed poses
     * @return
     *
     * Sinks are the elements that will need to be sampled (uniformly) without apriory knowledge.
     * If a sampler is specified through withSampler and it takes some poses as arguments,
     * the corresponding space will not appear in the sink list, as it will be sampled
     * with a method provided by the user.
     */
    std::vector<DependencyVertex> get_sinks() const;
    bool is_sink(const DependencyGraph::vertex_descriptor &vd) const;
    /**
     * @brief topo_order the elements will need to be defined (sampled or computed) in the order
     * they are in the returned vector.
     * @return
     */
    std::vector<Edge> topo_order() const;

    Graph graph;
    std::unordered_map<std::string, Graph::vertex_descriptor> vertices;
    KinematicGraphSearchSpace::Attachments _attachments;
    std::vector<Graph::edge_descriptor> creation_order;
    Graph::edge_descriptor select;

    DependencyGraph dependency_graph;
    std::unordered_map<Graph::vertex_descriptor, DependencyGraph::vertex_descriptor>
        vertex_to_dependency_vertex;
    std::map<Graph::edge_descriptor, DependencyGraph::vertex_descriptor> edge_to_dependency_vertex;
};

INIT_MOVE4D_STATIC_LOGGER(KinematicGraphSearchSpaceBuilder::PImpl,
                          "move4d.plan.kinematicgraphsearchspace.pimpl");

KinematicGraphSearchSpaceBuilder::KinematicGraphSearchSpaceBuilder() : _pimpl(new PImpl()) {}

KinematicGraphSearchSpaceBuilder::~KinematicGraphSearchSpaceBuilder() {}

KinematicGraphSearchSpaceBuilder::KinematicGraphSearchSpaceBuilder(
    const KinematicGraphSearchSpaceBuilder &other)
    : _name(other._name), _pimpl(new PImpl)
{
    const auto &g = other._pimpl->graph;
    for (auto &ed : other._pimpl->creation_order) {
        auto &edge = g[ed];
        auto &source = g[boost::source(ed, g)];
        auto &target = g[boost::target(ed, g)];
        switch (edge.type) {
        case PImpl::Edge::Forward:
            addForwardChain(*edge.space->as<RobotKinematicGroupSpace>(), source, target);
            break;
        case PImpl::Edge::Inverse:
            addInverseChain(*edge.space->as<RobotKinematicGroupSpace>(), source, target);
            break;
        case PImpl::Edge::Fixed:
            addFixedTransform(source, target, edge.transform.get());
            break;
        }
        if (edge.sampler_allocator) {
            withSampler(edge.sampler_allocator, edge.sampler_args);
        }
    }
    for (auto &att : other._pimpl->_attachments) {
        for (auto &s : att.second) {
            attachSE3ToVertex(*s, att.first);
        }
    }
}

KinematicGraphSearchSpaceBuilder &
KinematicGraphSearchSpaceBuilder::addForwardChain(const RobotKinematicGroupSpace &chain,
                                                  std::string base_vertex, std::string tip_vertex)
{
    auto u = _pimpl->vertex(base_vertex);
    auto v = _pimpl->vertex(tip_vertex);
    _pimpl->add_edge(u, v, PImpl::Edge{PImpl::Edge::Forward, chain.cloneSearchSpace(), {}});
    return *this;
}

KinematicGraphSearchSpaceBuilder &
KinematicGraphSearchSpaceBuilder::addInverseChain(const RobotKinematicGroupSpace &chain,
                                                  std::string base_vertex, std::string tip_vertex)
{
    auto u = _pimpl->vertex(base_vertex);
    auto v = _pimpl->vertex(tip_vertex);
    _pimpl->add_edge(u, v, PImpl::Edge{PImpl::Edge::Inverse, chain.cloneSearchSpace(), {}});
    return *this;
}

KinematicGraphSearchSpaceBuilder &
KinematicGraphSearchSpaceBuilder::addFixedTransform(std::string from_vertex, std::string to_vertex,
                                                    math::Transform transform)
{
    auto u = _pimpl->vertex(from_vertex);
    auto v = _pimpl->vertex(to_vertex);
    PImpl::Edge e{PImpl::Edge::Fixed, {}, {}};
    e.transform = std::move(transform);
    e.transform_inverse = e.transform.get().inverse();
    _pimpl->add_edge(u, v, e);
    PImpl::Edge e_reverse{PImpl::Edge::Fixed, {}, {}};
    e_reverse.transform = e.transform_inverse.get();
    e_reverse.transform_inverse = e.transform.get();
    _pimpl->add_edge(v, u, e_reverse);
    return *this;
}

KinematicGraphSearchSpaceBuilder &
KinematicGraphSearchSpaceBuilder::attachSE3ToVertex(const SE3Space &se3space, std::string vertex)
{
    auto u = _pimpl->vertex(vertex);
    _pimpl->add_attachment(u, se3space);
    return *this;
}

KinematicGraphSearchSpaceBuilder &
KinematicGraphSearchSpaceBuilder::withSampler(SamplerAllocator sampler_allocator,
                                              std::vector<std::string> args)
{
    for (const auto &v : args) {
        boost::add_edge(_pimpl->edge_to_dependency_vertex[_pimpl->select],
                        _pimpl->vertex_to_dependency_vertex[_pimpl->vertex(v)],
                        _pimpl->dependency_graph);
    }
    _pimpl->graph[_pimpl->select].sampler_allocator = std::move(sampler_allocator);
    _pimpl->graph[_pimpl->select].sampler_args = std::move(args);

    return *this;
}

KinematicGraphSearchSpaceBuilder &
KinematicGraphSearchSpaceBuilder::withLimits(const math::Vector &low, const math::Vector &high)
{
    auto &dofs = _pimpl->graph[_pimpl->select].space->getDegreesOfFreedom();
    assert(dofs.size() == low.size());
    assert(dofs.size() == high.size());
    for (size_t i = 0; i < low.size(); ++i) {
        dofs[i].setMinimum(low[i]);
        dofs[i].setMaximum(high[i]);
    }
    return *this;
}

KinematicGraphSearchSpaceBuilder &KinematicGraphSearchSpaceBuilder::withName(std::string name)
{
    _name = name;
    return *this;
}

KinematicGraphSearchSpaceBuilder &
KinematicGraphSearchSpaceBuilder::selectByName(const std::string &space_name)
{
    PImpl::Graph::edge_descriptor select;
    unsigned int found_count = 0;
    for (auto &ed : _pimpl->creation_order) {
        if (_pimpl->graph[ed].space && _pimpl->graph[ed].space->getName() == space_name) {
            // matches
            ++found_count;
            select = ed;
        }
    }
    if (found_count == 1) {
        _pimpl->select = select;
    } else if (found_count == 0) {
        throw NoMatch(space_name);
    } else {
        throw MultipleMatch(space_name, found_count);
    }
    return *this;
}

KinematicGraphSearchSpaceBuilder &
KinematicGraphSearchSpaceBuilder::selectByEndNames(const std::string &base_vertex,
                                                   const std::string &tip_vertex)
{
    PImpl::Graph::edge_descriptor select;
    unsigned int found_count = 0;
    for (auto &ed : _pimpl->creation_order) {
        if (_pimpl->graph[boost::source(ed, _pimpl->graph)] == base_vertex &&
            _pimpl->graph[boost::target(ed, _pimpl->graph)] == tip_vertex) {
            // matches
            ++found_count;
            select = ed;
        }
    }
    if (found_count == 1) {
        _pimpl->select = select;
    } else if (found_count == 0) {
        throw NoMatch(base_vertex, tip_vertex);
    } else {
        throw MultipleMatch(base_vertex, tip_vertex, found_count);
    }
    return *this;
}

KinematicGraphSearchSpaceBuilder &
KinematicGraphSearchSpaceBuilder::resetSpace(const RobotKinematicGroupSpace &chain)
{
    _pimpl->graph[_pimpl->select].space = chain.cloneSearchSpace();
    return *this;
}

std::unique_ptr<KinematicGraphSearchSpace> KinematicGraphSearchSpaceBuilder::build() const
{
    {
        auto sinks = _pimpl->get_sinks();
        std::vector<std::string> sink_names(sinks.size());
        std::transform(sinks.begin(), sinks.end(), sink_names.begin(),
                       [&](PImpl::DependencyVertex &dv) { return dv.name; });

        // std::ofstream ofgv(_name + "_dependencies.dot");
        // boost::write_graphviz(
        //     ofgv, _pimpl->dependency_graph,
        //     [&](std::ostream &out, const PImpl::DependencyGraph::vertex_descriptor &v) {
        //         out << "\[label=\"" << _pimpl->dependency_graph[v].name << "\"]";
        //     });
        // ofgv.close();
        M4D_INFO("sinks = " << array_to_str(sink_names, ", "));
    }

    auto topo_order = _pimpl->topo_order();
    // std::vector<std::string> topo_names(topo_order.size());
    auto topo_names = iterators::make_transform_iterator(
        topo_order.begin(), topo_order.end(),
        // std::transform(topo_order.begin(), topo_order.end(), topo_names.begin(),
        [&](PImpl::Edge &edge) {
            return (edge.type != PImpl::Edge::Fixed ? edge.space->getName() : "fixed");
        });
    M4D_INFO("topo_order = " << cat_to_str(topo_names, ", "));

    return std::unique_ptr<KinematicGraphSearchSpace>(
        new KinematicGraphSearchSpace(topo_order, _pimpl->_attachments, _name));
}

const std::string &KinematicGraphSearchSpaceBuilder::getName() const { return _name; }

KinematicGraphSearchSpace::KinematicGraphSearchSpace(
    std::vector<Edge> order, KinematicGraphSearchSpace::Attachments attachments, std::string name)
    : SearchSpace(SearchSpace::Type::KinematicGraph, name), _order(std::move(order)),
      _attachments(std::move(attachments))
{
    std::vector<std::string> names(_order.size());
    std::transform(_order.begin(), _order.end(), names.begin(), [&](const Edge &edge) {
        auto &space = edge.space;
        return (space ? space->getName() : "fixed");
    });

    initDegreeOfFreedomList();
    initVerticesOrder();
    resetSamplers();
    M4D_INFO("order:\n\t" << array_to_str(names, "\n\t"));
    M4D_INFO("attachments: " << _attachments);
}

KinematicGraphSearchSpace::~KinematicGraphSearchSpace() {}

void KinematicGraphSearchSpace::shootUniform(mdl::WorldState &ws) const
{
    std::unordered_map<std::string, math::Transform> poses;
    poses[KinematicGraphSearchSpaceBuilder::Fixed] = math::Transform::Identity();

    bool ok = true;
    do {
        ok = true;
        for (size_t i = 0; ok && i < _order.size(); ++i) {
            ok = shootUniform(ws, poses, i);
        }
    } while (M4D_WHILE_CONDITION(!ok));
    for (const auto &attachments : _attachments) {
        M4D_DEBUG(attachments);
        auto pose = poses.find(attachments.first);
        if (pose == poses.end()) {
            throw KinematicGraphException("the pose of " + attachments.first +
                                          " is not known for applying to the attachments");
        }
        for (auto &space : attachments.second) {
            M4D_DEBUG(space->getName() << " attached to " << attachments.first << ":\n"
                                       << pose->second.matrix());
            space->setIn(pose->second, ws);
        }
    }
}

bool KinematicGraphSearchSpace::shootUniform(
    mdl::WorldState &ws, std::unordered_map<std::string, math::Transform> &poses,
    size_t edge_i) const
{
    const auto &edge = _order[edge_i];
    M4D_TRACE("shootUniform " << edge.space->getName());
    const std::string &target_name = edge.target;
    auto search = poses.find(target_name);
    if (search == poses.end() && edge.type == KinematicGraphSearchSpace::Edge::Inverse) {
        throw KinematicGraphException(
            "the pose of " + target_name + " is not known when solving the IK for " +
            edge.space->getName() + " in the KinematicGraphSearchSpace " + getName());
    } else {
        search = poses.insert({target_name, {}}).first;
    }
    auto &target_pose = search->second;
    const std::string &base_name = edge.source;
    search = poses.find(base_name);

    if (search == poses.end()) {
        throw KinematicGraphException("the pose of " + base_name +
                                      " is not known when sampling for " + edge.space->getName() +
                                      " in the KinematicGraphSearchSpace " + getName());
    }
    const auto &base_pose = search->second;
    if (edge.sampler_allocator) {
        M4D_TRACE("Custom sampler");

        std::vector<math::Transform> args(edge.sampler_args.size());
        std::transform(edge.sampler_args.begin(), edge.sampler_args.end(), args.begin(),
                       [&poses, &edge, this](const std::string &s) {
                           auto search = poses.find(s);
                           if (search == poses.end())
                               throw KinematicGraphException(
                                   "the pose of " + s + " is not known when using user sampler " +
                                   edge.space->getName() + " in the KinematicGraphSearchSpace " +
                                   this->getName());
                           return search->second;
                       });
        auto ws_view = views::DofsViewWorldstate(ws, *edge.space);
        sampler(edge_i).sampleUniform(ws_view, base_pose, target_pose, args);
        return true;
    } else if (edge.type == KinematicGraphSearchSpace::Edge::Forward) {
        M4D_TRACE("Forward");
        edge.space->as<RobotKinematicGroupSpace>()->RobotSpace::shootUniform(ws);
        return edge.space->as<RobotKinematicGroupSpace>()->computeFK(ws, target_pose);
    } else if (edge.type == KinematicGraphSearchSpace::Edge::Inverse) {
        M4D_TRACE("Inverse");
        return edge.space->as<RobotKinematicGroupSpace>()->computeIK(ws, target_pose);
    } else if (edge.type == KinematicGraphSearchSpace::Edge::Fixed) {
        M4D_TRACE("Fixed");
        target_pose = base_pose * edge.transform.get();
        return true;
    }
}

std::vector<std::pair<size_t, size_t>>
KinematicGraphSearchSpace::computeEdgeBijectionTo(const KinematicGraphSearchSpace &other) const
{
    std::vector<std::pair<size_t, size_t>> bijection;
    for (size_t i = 0; i < this->_order.size(); ++i) {
        const auto &self_edge = _order[i];
        for (size_t j = 0; j < other._order.size(); ++j) {
            const auto &other_edge = other._order[j];
            if (self_edge.source == other_edge.source && self_edge.target == other_edge.target) {
                // match
                bijection.push_back({i, j});
                break;
            }
        }
        if (bijection.size() != i + 1) {
            M4D_DEBUG("no match found for edge " << i);
        }
    }
    M4D_DEBUG("computed edge bijection between kinematic graphs: " << bijection);
    return bijection;
}

std::vector<std::pair<size_t, size_t>>
KinematicGraphSearchSpace::computeVertexBijectionTo(const KinematicGraphSearchSpace &other) const
{
    std::vector<std::pair<size_t, size_t>> bijection;
    for (size_t i = 0; i < this->_vertices_order.size(); ++i) {
        const auto &self_edge = _vertices_order[i];
        for (size_t j = 0; j < other._vertices_order.size(); ++j) {
            const auto &other_edge = other._vertices_order[j];
            if (self_edge == other_edge) {
                // match
                bijection.push_back({i, j});
                break;
            }
        }
        if (bijection.size() != i + 1) {
            M4D_DEBUG("no match found for vertex " << i);
        }
    }
    M4D_DEBUG("computed vertices bijection between kinematic graphs: " << bijection);
    return bijection;
}

BaseSampler &KinematicGraphSearchSpace::sampler(size_t edge_i)
{
    const Edge &edge = _order[edge_i];
    if (!edge.sampler_allocator) {
        throw KinematicGraphException("In KinematicGraphSearchSpace " + getName() +
                                      ", there is no sampler allocator for the part " +
                                      edge.space->getName());
    }
    auto search = _samplers.find(edge_i);
    if (search == _samplers.end()) {
        search = _samplers.insert({edge_i, edge.sampler_allocator(edge.space->cloneSearchSpace())})
                     .first;
    }
    return *search->second;
}

BaseSampler &KinematicGraphSearchSpace::sampler(size_t edge_i) const
{
    auto search = _samplers.find(edge_i);
    if (search == _samplers.end()) {
        throw KinematicGraphException("In KinematicGraphSearchSpace " + getName() +
                                      ", there is no sampler allocated for the part " +
                                      _order[edge_i].space->getName());
    }
    return *search->second;
}

void KinematicGraphSearchSpace::initDegreeOfFreedomList()
{
    for (const auto &e : *this) {
        if (e.type != Edge::Fixed) {
            for (auto &d : e.space->getDegreesOfFreedom()) {
                m_degreesOfFreedom.push_back(d);
            }
        }
    }
    for (const auto &atts : _attachments) {
        for (const auto &a : atts.second) {
            m_additionalDoFs.insert(m_additionalDoFs.end(), a->getDegreesOfFreedom().begin(),
                                    a->getDegreesOfFreedom().end());
        }
    }
}

void KinematicGraphSearchSpace::initVerticesOrder()
{
    for (const auto &e : _order) {
        auto search = std::find(_vertices_order.begin(), _vertices_order.end(), e.source);
        if (search == _vertices_order.end()) {
            _vertices_order.push_back(e.source);
        }
        search = std::find(_vertices_order.begin(), _vertices_order.end(), e.target);
        if (search == _vertices_order.end()) {
            _vertices_order.push_back(e.target);
        }
    }
}

void KinematicGraphSearchSpace::resetSamplers()
{
    _samplers.clear();
    for (size_t i = 0; i < _order.size(); ++i) {
        const auto &edge = _order[i];
        if (edge.sampler_allocator) {
            _samplers[i] = edge.sampler_allocator(edge.space->cloneSearchSpace());
        }
    }
}

KinematicGraphSearchSpaceBuilder::PImpl::Graph::vertex_descriptor
KinematicGraphSearchSpaceBuilder::PImpl::vertex(const std::string &name)
{
    auto s = vertices.find(name);
    if (s == vertices.end()) {
        auto vd = vertices.insert({name, boost::add_vertex(name, graph)}).first->second;
        vertex_to_dependency_vertex[vd] =
            boost::add_vertex(DependencyVertex::make_vertex(vd, name), dependency_graph);
        return vd;
    }
    return s->second;
}

void KinematicGraphSearchSpaceBuilder::PImpl::add_edge(
    KinematicGraphSearchSpaceBuilder::PImpl::Graph::vertex_descriptor u,
    KinematicGraphSearchSpaceBuilder::PImpl::Graph::vertex_descriptor v,
    KinematicGraphSearchSpaceBuilder::PImpl::Edge edge)
{
    edge.source = graph[u];
    edge.target = graph[v];
    creation_order.push_back(boost::add_edge(u, v, edge, graph).first);
    select = creation_order.back();
    if (edge.type == Edge::Inverse) {
        auto vd = boost::add_vertex(
            PImpl::DependencyVertex::make_edge(select, edge.space->getName()), dependency_graph);
        edge_to_dependency_vertex[select] = vd;
        // configuration depends on both base and eef poses
        boost::add_edge(vd, vertex_to_dependency_vertex[u], dependency_graph);
        boost::add_edge(vd, vertex_to_dependency_vertex[v], dependency_graph);
    } else if (edge.type == Edge::Fixed) {
        auto vd = boost::add_vertex(PImpl::DependencyVertex::make_edge(select, "fixed"),
                                    dependency_graph);
        edge_to_dependency_vertex[select] = vd;
        // each pose depends on the other and the fixed transform
        boost::add_edge(vertex_to_dependency_vertex[v], vd, dependency_graph);
        boost::add_edge(vd, vertex_to_dependency_vertex[u], dependency_graph);
    } else {
        auto vd = boost::add_vertex(
            PImpl::DependencyVertex::make_edge(select, edge.space->getName()), dependency_graph);
        edge_to_dependency_vertex[select] = vd;
        // tip pose depends on space config and base pose
        boost::add_edge(vertex_to_dependency_vertex[v], vd, dependency_graph);
        boost::add_edge(vertex_to_dependency_vertex[v], vertex_to_dependency_vertex[u],
                        dependency_graph);
    }
}

void KinematicGraphSearchSpaceBuilder::PImpl::add_attachment(
    KinematicGraphSearchSpaceBuilder::PImpl::Graph::vertex_descriptor u, const SE3Space &se3space)
{
    _attachments[graph[u]].emplace_back(
        static_cast<SE3Space *>(se3space.cloneSearchSpace().release()));
}

std::vector<KinematicGraphSearchSpaceBuilder::PImpl::DependencyVertex>
KinematicGraphSearchSpaceBuilder::PImpl::get_sinks() const
{
    auto range = boost::vertices(dependency_graph);
    std::vector<DependencyVertex> sinks;
    for (auto it = range.first; it != range.second; ++it) {
        if (is_sink(*it)) {
            sinks.push_back(dependency_graph[*it]);
        }
    }
    return sinks;
}

bool KinematicGraphSearchSpaceBuilder::PImpl::is_sink(
    const DependencyGraph::vertex_descriptor &vd) const
{
    auto e_range = boost::out_edges(vd, dependency_graph);
    return (e_range.first == e_range.second) &&
           dependency_graph[vd].name != KinematicGraphSearchSpaceBuilder::Fixed;
}

std::vector<KinematicGraphSearchSpaceBuilder::PImpl::Edge>
KinematicGraphSearchSpaceBuilder::PImpl::topo_order() const
{
    std::vector<DependencyGraph::vertex_descriptor> component(
        boost::num_vertices(dependency_graph));
    boost::strong_components(dependency_graph,
                             boost::make_iterator_property_map(
                                 component.begin(), get(boost::vertex_index, dependency_graph)));

    M4D_INFO("strong_components: " << component);

    std::map<size_t, std::vector<DependencyGraph::vertex_descriptor>> components;
    for (size_t i = 0; i < component.size(); ++i) {
        components[component[i]].push_back(i);
    }
    std::vector<DependencyGraph::vertex_descriptor> order;
    for (size_t i = 0; i < components.size(); ++i) {
        if (components[i].size() == 1) {
            order.push_back(components[i][0]);
        } else {
            // connected component has more than one element
            // must find an order for them
            // the first will be the one connected only to vertices already present in the 'order'
            // vector

            // the order will be taken from a breadth first search on the transpose graph
            // representing the component (SubGraph):
            using SubGraph = boost::adjacency_list<boost::setS, boost::vecS, boost::directedS,
                                                   DependencyGraph::vertex_descriptor,
                                                   DependencyGraph::vertex_descriptor>;
            SubGraph subgraph;
            bool found = false;
            DependencyGraph::vertex_descriptor first{};
            std::map<DependencyGraph::vertex_descriptor, SubGraph::vertex_descriptor>
                dep_to_sub_map;
            // add vertices to the subgraph
            for (auto &v : components[i]) {
                if (dependency_graph[v].type == DependencyVertex::Vertex) {
                    dep_to_sub_map[v] = boost::add_vertex(v, subgraph);
                }
            }
            // add edges to the subgraph
            for (auto &v : components[i]) {
                if (dependency_graph[v].type == DependencyVertex::Edge) {
                    auto out_edges = boost::out_edges(v, dependency_graph);
                    auto in_edges = boost::in_edges(v, dependency_graph);
                    for (auto e_out = out_edges.first; e_out != out_edges.second; ++e_out) {
                        // search if the target is in the component
                        auto search_target = std::find(components[i].begin(), components[i].end(),
                                                       boost::target(*e_out, dependency_graph));
                        if (search_target != components[i].end()) {

                            for (auto e_in = in_edges.first; e_in != in_edges.second; ++e_in) {
                                auto search_source =
                                    std::find(components[i].begin(), components[i].end(),
                                              boost::source(*e_in, dependency_graph));
                                if (search_source != components[i].end()) {
                                    // the main graph edge links two vertices of the component, add
                                    // it to the subgraph; but in reverse
                                    M4D_TRACE("add edge " << v << " to component subgraph "
                                                          << *search_target << " to "
                                                          << *search_source);
                                    boost::add_edge(dep_to_sub_map[*search_target],
                                                    dep_to_sub_map[*search_source], v, subgraph);
                                }
                            }
                        }
                    }
                }
            }

            for (auto &v : components[i]) {
                bool has_external_dependency = false;
                auto edges_range = boost::out_edges(v, dependency_graph);
                if (dependency_graph[v].name == KinematicGraphSearchSpaceBuilder::Fixed) {
                    // the vertex represents the fixed frame. It must be the root of the final
                    // dependency graph
                    has_external_dependency = true;
                } else {
                    for (auto e = edges_range.first; e != edges_range.second; ++e) {
                        // search for a dependency that is already in the 'order' vector
                        auto search = std::find(order.begin(), order.end(),
                                                boost::target(*e, dependency_graph));
                        if (search != order.end()) {
                            // if it has one, then this vertex can be next
                            has_external_dependency = true;
                            break;
                        }
                    }
                }
                if (has_external_dependency) {
                    found = true;
                    first = v;
                    break;
                }
            }
            if (found) {
                M4D_TRACE("first node of component = " << first);
                BFSVisitor<std::vector<DependencyGraph::vertex_descriptor>> vis(order);
                boost::breadth_first_search(subgraph, dep_to_sub_map[first], boost::visitor(vis));
            } else {
                M4D_ERROR("first vertex for connected component not found");
            }
        }
    }

    // std::vector<DependencyGraph::vertex_descriptor> order;
    // boost::topological_sort(dependency_graph, std::back_inserter(order));

    M4D_INFO("topo_order: " << order);

    std::vector<Edge> part_order;

    for (auto &x : order) {
        if (dependency_graph[x].type == DependencyVertex::Vertex) {
            M4D_INFO("- " << dependency_graph[x].name << " (pose) ");
            // part_order.emplace_back(dependency_graph[x].vd);
            auto out = boost::out_edges(x, dependency_graph);
            if (dependency_graph[x].name == Fixed) {
                M4D_INFO("  \tis fixed");
            } else if (out.first == out.second) {
                // no out edges, i.e. no dependencies, so need to shoot uniformly
                M4D_INFO("  \tneed to be uniformly shot");
                throw std::runtime_error(
                    std::string("a vertex (pose) must have dependencies. You may need to "
                                "specify add a space with custom sampler for fixing vertex \"") +
                    dependency_graph[x].name + "\"");
            } else {
                auto strs = iterators::make_transform_iterator(
                    out, [&](const DependencyGraph::edge_descriptor &x) -> std::string {
                        return dependency_graph[boost::target(x, dependency_graph)].name;
                    });
                M4D_INFO("  \tdepends on: " << cat_to_str(strs.first, strs.second, ", "));
                // part_order.back().is_noop = true;
            }
        } else {
            M4D_INFO("- " << dependency_graph[x].name << " (chain)");
            part_order.emplace_back(graph[dependency_graph[x].ed]);
            auto &edge = graph[dependency_graph[x].ed];
            auto out = boost::out_edges(x, dependency_graph);
            auto strs = iterators::make_transform_iterator(out, [&](const auto &x) {
                return dependency_graph[boost::target(x, dependency_graph)].name;
            });
            if (out.first != out.second) {
                M4D_INFO("  \tdepends on: " << cat_to_str(strs.first, strs.second, ", "));
            }
            if (edge.sampler_allocator) {
                M4D_INFO("  \thas custom sampler");
            }
            if (edge.type == KinematicGraphSearchSpace::Edge::Forward) {
                M4D_INFO("  \tneed to shoot configuration");
            } else if (edge.type == KinematicGraphSearchSpace::Edge::Inverse) {
                M4D_INFO("  \tneed to solve IK");
            }
        }
    }
    return part_order;
}

KinematicGraphSearchSpaceBuilder::NoMatch::NoMatch(const std::string &space_name)
    : Exception(std::string("No space matching the specified name \"") + space_name + "\"")
{
}

KinematicGraphSearchSpaceBuilder::NoMatch::NoMatch(const std::string &source_name,
                                                   const std::string &target_name)
    : Exception(std::string("No space matching the specified end names source=\"") + source_name +
                "\" target=\"" + target_name + "\"")
{
}

KinematicGraphSearchSpaceBuilder::MultipleMatch::MultipleMatch(const std::string &space_name,
                                                               unsigned int count)
    : Exception(std::string("Multiple spaces matching the specified name \"") + space_name +
                "\" (" + std::to_string(count) + " matches)")
{
}

KinematicGraphSearchSpaceBuilder::MultipleMatch::MultipleMatch(const std::string &source_name,
                                                               const std::string &target_name,
                                                               unsigned int count)
    : Exception(std::string("Multiple space matching the specified end names source=\"") +
                source_name + "\" target=\"" + target_name + "\" (" + std::to_string(count) +
                " matches)")
{
}

} // namespace plan
} // namespace move4d

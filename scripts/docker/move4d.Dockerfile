ARG DISTRIB=ubuntu
ARG DISTRIB_VERSION=latest
FROM registry.gitlab.com/laas-hri/move4d/dependencies/${DISTRIB}:${DISTRIB_VERSION} AS builder

WORKDIR /build
COPY . .
RUN mkdir build && cd build && cmake .. && make -j`nproc` install

FROM registry.gitlab.com/laas-hri/move4d/dependencies/${DISTRIB}:${DISTRIB_VERSION}
COPY --from=builder /usr /usr


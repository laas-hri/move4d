//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_TESTS_FIXTUREKINEMATICGRAPH_H
#define MOVE4D_TESTS_FIXTUREKINEMATICGRAPH_H

#include "tests/GlobalFixtures.h"

#include "move4d/mdl/Types.h"
#include "move4d/plan/Types.h"

namespace move4d
{
namespace tests
{

class FixtureKinematicGraph : public FixtureSRDF
{
  public:
    FixtureKinematicGraph();
    virtual ~FixtureKinematicGraph() = default;

  protected:
    mdl::Movable *object;
    std::shared_ptr<plan::SE2Space> part_base;
    std::shared_ptr<plan::KinematicSpaceWrapper> kin_base;
    std::shared_ptr<plan::RobotKinematicGroupSpace> arm_part;
    std::shared_ptr<plan::KinematicGraphSearchSpace> kin_graph;
};

} // namespace tests
} // namespace move4d

#endif // MOVE4D_TESTS_FIXTUREKINEMATICGRAPH_H

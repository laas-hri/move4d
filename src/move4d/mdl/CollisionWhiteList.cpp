//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
// Created by gbuisan on 20/12/18.
//

#include "CollisionWhiteList.h"
#include "move4d/mdl/CollisionManager.h"
#include "move4d/mdl/SceneObject.h"
#include "move4d/utils/KinematicHelper.hpp"
#include <rl/mdl/Body.h>
#include <rl/mdl/Compound.h>
#include <rl/mdl/Dynamic.h>
#include <rl/mdl/Fixed.h>
#include <rl/mdl/Joint.h>
#include <rl/plan/SimpleModel.h>

namespace move4d
{
namespace mdl
{

CollisionWhiteList::CollisionWhiteList(
    const std::unordered_map<move4d::Identifier, std::shared_ptr<SceneObject>> &objects)
{
    reset(objects);
}

CollisionWhiteList::CollisionWhiteList(const CollisionWhiteList &whiteList)
{
    m_whiteList = whiteList.m_whiteList;
}

CollisionWhiteList::CollisionWhiteList(const CollisionWhiteList::CollisionList &whitelist) {}

CollisionWhiteList::CollisionWhiteList(
    const std::unordered_map<move4d::Identifier, std::shared_ptr<SceneObject>> &objects,
    const CollisionWhiteList::CollisionList &blackList)
{
    m_whiteList.reserve(
        objects.size() -
        blackList.size()); // We at least need that much space (but more for sure...)
    std::unordered_map<move4d::Identifier, std::shared_ptr<SceneObject>> toAdd(objects);
    CollisionList bl(blackList);
    for (const auto &body1 : objects) {
        toAdd.erase(body1.first);
        for (const auto &body2 : toAdd) {
            for (size_t i = 0; i < body1.second->getNumberOfBodies(); i++) {
                for (size_t j = 0; j < body2.second->getNumberOfBodies(); j++) {
                    int toRemoveFromBl = -1;
                    for (int k = 0; k < bl.size(); k++) {
                        if ((bl[k].first().sceneObj == body1.first &&
                             bl[k].first().bodyIndex == i &&
                             bl[k].second().sceneObj == body2.first &&
                             bl[k].second().bodyIndex == j) ||
                            (bl[k].second().sceneObj == body1.first &&
                             bl[k].second().bodyIndex == i &&
                             bl[k].first().sceneObj == body2.first &&
                             bl[k].first().bodyIndex == j)) {
                            toRemoveFromBl = k;
                            break;
                        }
                    }
                    if (toRemoveFromBl != -1) {
                        bl.erase(bl.begin() + toRemoveFromBl);
                        continue;
                    }
                    m_whiteList.push_back(CollisionPair({body1.first, i}, {body2.first, j}));
                }
            }
        }
    }
}

void CollisionWhiteList::reset(
    const std::unordered_map<move4d::Identifier, std::shared_ptr<SceneObject>> &objects)
{
    m_whiteList.clear();
    m_whiteList.reserve(objects.size()); // We at least need that much space (but more for sure...)
    std::unordered_map<move4d::Identifier, std::shared_ptr<SceneObject>> toAdd(objects);
    for (const auto &body1 : objects) {
        toAdd.erase(body1.first);
        for (const auto &body2 : toAdd) {
            for (size_t i = 0; i < body1.second->getNumberOfBodies(); i++) {
                for (size_t j = 0; j < body2.second->getNumberOfBodies(); j++) {
                    m_whiteList.push_back(CollisionPair({body1.first, i}, {body2.first, j}));
                }
            }
        }

        // add self collisions
        for (size_t i = 0; i < body1.second->getNumberOfBodies(); ++i) {
            for (size_t j = i + 1; j < body1.second->getNumberOfBodies(); ++j) {
                m_whiteList.push_back(CollisionPair({body1.first, i}, {body1.first, j}));
            }
        }
        removeNeighbourBodies2(*body1.second);
    }
}

void CollisionWhiteList::disable(object_ref a, object_ref b) {}

void CollisionWhiteList::disable(body_ref a, body_ref b)
{
    auto search = std::find(m_whiteList.begin(), m_whiteList.end(), CollisionPair(a, b));
    if (search != m_whiteList.end()) {
        m_whiteList.erase(search);
    } else {
        std::cout << "collision pair to remove not found" << std::endl;
    }
}

void CollisionWhiteList::removeNeighbourBodies(CollisionWhiteList::object_ref obj)
{
    auto model = obj.model()->mdl;
    for (size_t i = 0; i < model->getTransforms(); ++i) {
        auto jnt = dynamic_cast<rl::mdl::Joint *>(model->getTransform(i));
        if (jnt == nullptr) {
            continue;
        }
        auto fIn = jnt->in;
        auto fOut = jnt->out;
        size_t iIn(SIZE_MAX), iOut(SIZE_MAX);
        size_t pframe;
        if (utils::getParentFrame(model, i, pframe)) {
            size_t pfixed;
            if (utils::getParentTransform(model, pframe, pfixed)) {
                size_t pjnt;
                if (utils::getParentFrame(model, pfixed, pjnt)) {
                    for (size_t j = 0; j < model->getBodies(); ++j) {
                        if (model->getBody(j) == model->getFrame(pjnt)) {
                            iIn = j;
                        }
                    }
                }
            }
        }
        for (size_t j = 0; j < model->getBodies(); j++) {
            if (model->getBody(j)->getVertexDescriptor() == fOut->getVertexDescriptor()) {
                iOut = j;
                break;
            }
        }
        if (iIn != iOut) {
            std::cout << "remove neighbours " << iIn << "-" << iOut << std::endl;
            std::cout << "remove neighbours " << model->getBody(iIn)->getName() << "-"
                      << model->getBody(iOut)->getName() << std::endl;
            SceneBody a{obj.getId(), iIn}, b{obj.getId(), iOut};
            this->disable(a, b);
        }
    }
}

void CollisionWhiteList::removeNeighbourBodies2(CollisionWhiteList::object_ref obj)
{
    auto model = obj.model()->mdl;
    for (size_t i = 0; i < model->getTransforms(); ++i) {
        std::cout << "frame " << i;
        if (dynamic_cast<::rl::mdl::Joint *>(model->getTransform(i))) {
            std::cout << " is joint "
                      << dynamic_cast<::rl::mdl::Joint *>(model->getTransform(i))->getName();
        }
        if (dynamic_cast<::rl::mdl::Fixed *>(model->getTransform(i))) {
            std::cout << " is fixed "
                      << dynamic_cast<::rl::mdl::Fixed *>(model->getTransform(i))->getName();
        }
        std::cout << "\n";
        // auto jnt=dynamic_cast<rl::mdl::Joint*>(model->getTransform(i));
        auto jnt = model->getTransform(i);
        // if(!jnt){ continue; }
        auto fIn = jnt->in;
        auto fOut = jnt->out;
        size_t iPrev(SIZE_MAX), iIn(SIZE_MAX), iOut(SIZE_MAX);
        size_t pframe;
        if (utils::getParentFrame(model, i, pframe)) {
            size_t pbodyframe(SIZE_MAX);
            if (utils::isBodyOrGetPrevious(model, pframe, iIn, pbodyframe)) {
                utils::getPreviousBodyOfFrame(model, pbodyframe, iPrev, pbodyframe);
            }
        }
        for (size_t j = 0; j < model->getBodies(); j++) {
            if (model->getBody(j)->getVertexDescriptor() == fOut->getVertexDescriptor()) {
                iOut = j;
                break;
            }
        }
        if (iOut < model->getBodies()) {
            SceneBody b{obj.getId(), iOut};
            if (iIn < model->getBodies()) {
                std::cout << "remove neighbours " << iIn << "-" << iOut << std::endl;
                std::cout << "remove neighbours " << model->getBody(iIn)->getName() << "-"
                          << model->getBody(iOut)->getName() << std::endl;
                SceneBody a{obj.getId(), iIn};
                this->disable(a, b);
            }
            if (iPrev < model->getBodies()) {
                std::cout << "remove neighbours2 " << iPrev << "-" << iOut << std::endl;
                std::cout << "remove neighbours2 " << model->getBody(iPrev)->getName() << "-"
                          << model->getBody(iOut)->getName() << std::endl;
                SceneBody prev{obj.getId(), iPrev};
                this->disable(prev, b);
            }
        }
    }
}

void CollisionWhiteList::removeSelfColliding(CollisionWhiteList::object_ref obj,
                                             CollisionManager &collisionManager)
{
    auto list = collisionManager.collisionList();
    for (auto p : list) {
        if (p.first().sceneObj == p.second().sceneObj && p.first().sceneObj == obj.getId()) {
            disable(p.first(), p.second());
        }
    }
}

std::ostream &operator<<(std::ostream &os, const CollisionWhiteList &whitelist)
{
    for (auto p : whitelist.m_whiteList) {
        os << p.first().sceneObj << " : " << p.first().bodyIndex << "\t\tX\t\t"
           << p.second().sceneObj << " : " << p.second().bodyIndex << "\n";
    }
    return os;
}

} // namespace mdl

} // namespace move4d

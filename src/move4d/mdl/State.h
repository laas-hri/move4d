//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_MDL_STATE_H
#define MOVE4D_MDL_STATE_H

#include "move4d/common.h"
#include "move4d/math/Vector.h"
#include "move4d/mdl/Types.h"
#include "rl/math/Vector.h"
#include <boost/serialization/access.hpp>
#include <vector>

namespace move4d
{
namespace mdl
{
class SceneObject;
}
} // namespace move4d

namespace move4d
{

namespace mdl
{

/**
 * represents the state of an object (dof values and derivatives)
 */
class State
{
  public:
    State() = delete;
    State(const State &) = default;
    State(State &&) = default;
    State &operator=(const State &) = default;
    State &operator=(State &&) = default;
    virtual ~State() = default;

    [[deprecated]] State(SceneObject *object, std::size_t nbDofPosition,
                         std::size_t nbDofDerivatives);
    State(SceneObject *object);

    inline std::size_t size(StateType::Type type = StateType::Position) const;

    /**
     * resize the fit the new number of dofs. Keep existing data
     */
    void resize(std::size_t nbDofPosition, std::size_t nbDofDerivatives);

    move4d::Real &operator()(std::size_t index, StateType::Type type);

    const move4d::Real &operator()(std::size_t index, StateType::Type type) const;

    inline const rl::math::Vector &operator()(StateType::Type type) const;

    inline rl::math::Vector &operator()(StateType::Type type);

    void setValues(std::size_t fromDof, const move4d::math::Transform &position);

    /**
     * the sum of square distance between each dof of this and other
     */
    move4d::Real distanceSquared(const State &other,
                                 StateType::Type type = StateType::Position) const;

    bool operator==(const State &other) const;

    bool isInBounds() const;

    /**
     * @brief reset all values.
     *
     * Set all values to 0, then clip position dofs to respect bounds and normalize (for
     * quaternions)
     */
    void reset();

  protected:
    SceneObject *m_object;

    /**
     * the value of the degrees of freedom and their derivatives
     */
    std::vector<move4d::math::Vector> m_value;

  private:
    friend class boost::serialization::access;
    template <typename Archive> void serialize(Archive &ar, const unsigned int version);

  public:
    inline SceneObject *getObject() const;
};

template <typename Archive> void State::serialize(Archive &ar, const unsigned int version)
{
    // clang-format off
    ar & m_value;
    // clang-format on
}
inline std::size_t State::size(StateType::Type type) const
{
    Expects(m_value.size() > type);
    return m_value[static_cast<size_t>(type)].size();
}

inline const rl::math::Vector &State::operator()(StateType::Type type) const
{
    Expects(this->m_value.size() > type);
    return this->m_value[type];
}

inline rl::math::Vector &State::operator()(StateType::Type type)
{
    Expects(this->m_value.size() > type);
    return this->m_value[type];
}

inline SceneObject *State::getObject() const { return m_object; }

} // namespace mdl

} // namespace move4d
#endif

//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_MDL_SCENEOBJECT_H
#define MOVE4D_MDL_SCENEOBJECT_H

#include "move4d/common.h"
#include "move4d/mdl/ObjectFactory.h"
#include "move4d/mdl/State.h"
#include "move4d/mdl/Types.h"
#include <memory>
#include <string>
#include <unordered_map>

namespace move4d
{
namespace mdl
{
class Scene;
class DegreeOfFreedomInfo;
} // namespace mdl
} // namespace move4d
namespace rl
{
namespace mdl
{
class Joint;
class Frame;
class Kinematic;
} // namespace mdl
namespace sg
{
class Body;
} // namespace sg
namespace plan
{
class SimpleModel;
} // namespace plan
} // namespace rl

namespace move4d
{

namespace mdl
{

struct EndEffector {
    std::string name;
    std::string parentFrame;
    // TODO(Jules): EndEffector group;
};
struct Chain {
  public:
    Chain() = default;
    Chain(std::string robot_class, std::string name, std::string tipName,
          ::rl::mdl::Frame *rootFrame, std::shared_ptr<::rl::mdl::Kinematic> kinematic,
          std::vector<size_t> bijection)
        : robot_class(std::move(robot_class)), name(std::move(name)), tipName(std::move(tipName)),
          rootFrame(rootFrame), kinematic(std::move(kinematic)), bijection(std::move(bijection))
    {
    }
    std::string robot_class;
    std::string name;
    std::string tipName;
    ::rl::mdl::Frame *rootFrame;
    std::shared_ptr<::rl::mdl::Kinematic> kinematic = {};
    std::vector<size_t> bijection;
    std::string ikfast; ///< name of the ifkast solver to use

    math::Vector configFromState(const State &state) const;
    math::Vector configFromWorldState(const WorldState &ws, Identifier object_id) const;
};
struct Group {
    std::string name;
    std::vector<size_t> joints; // Index in m_kinematic
    std::vector<size_t> links;  // Index in m_kinematic
    std::vector<size_t> chains; // Index in chains
};
using EndEffectorPtr = std::shared_ptr<EndEffector>;
using ChainPtr = std::shared_ptr<Chain>;

/**
 * The representation of an element of the scene, including its 3D model. This is the base class for
 * all types of objects
 */
class SceneObject
{
    MOVE4D_STATIC_LOGGER;

  public:
    SceneObject(const Scene &scene, std::shared_ptr<Model> bodies, std::shared_ptr<Kinematic> kin,
                std::string name, move4d::Identifier id, std::size_t index);

    SceneObject() = delete;

    SceneObject(const SceneObject &other);

    virtual ~SceneObject();

    template <typename T> inline const T *as() const { return dynamic_cast<const T *>(this); }
    template <typename T> inline T *as() { return dynamic_cast<T *>(this); }

    inline const std::string getName() const;

    void setName(std::string value);

    inline const move4d::Identifier getId() const;

    void setId(move4d::Identifier value);

    inline const std::shared_ptr<State> &getState() const;

    virtual void applyState(State state);

    virtual rl::mdl::Joint *getJointOfDof(std::size_t dof_index, std::size_t &index_in_joint) const;
    virtual size_t getJointIndexOfDof(std::size_t dof_index, std::size_t &index_in_joint) const;
    virtual size_t getJointFirstDof(std::size_t joint_index) const;

    std::size_t getNumberOfDof(StateType::Type type = StateType::Position) const;

    std::size_t getNumberOfBodies() const;

    ::rl::sg::Body *getBody(const std::string &name);

    ::rl::mdl::Frame *getLink(const std::string &name) const;

    int getLinkIndex(const std::string &name) const;

    ::rl::mdl::Joint *getJoint(const std::string &name) const;
    ::rl::mdl::Joint *getJoint(size_t i) const;
    size_t getJointNumber() const;

    int getJointIndex(const std::string &name) const;

    DegreeOfFreedomInfo getDegreeOfFreedomInfo(std::size_t index, StateType::Type type) const;

    std::size_t getSceneIndex() const;

    /**
     * minimal value of the indicated DoF
     */
    virtual move4d::Real getMinimum(std::size_t index, StateType::Type type) const;

    /**
     * maximal value of the indicated DoF
     */
    virtual move4d::Real getMaximum(std::size_t index, StateType::Type type) const;

    /**
     * does the degree of freedom wraps around (e.g. is the position of a continuous rotation DoF)
     *
     * it should never be true for something else than POSITION
     */
    virtual bool wrapAround(std::size_t index, StateType::Type type) const;

    move4d::Real distanceSquared(const State &q1, const State &q2,
                                 StateType::Type type = StateType::Position) const;

    inline const std::shared_ptr<rl::plan::SimpleModel> &model() const { return m_model; }

    /**
     * need to be called whenever models are modified.
     *
     * refreshes m_state to fit the number of dof
     */
    void updateModels();

    void setKinematic(std::shared_ptr<Kinematic> kin);

    /**
     * @brief computeInverseKinematic compute the inverse kinematic of an end effector group
     * @param end_effector is the index of the end effector object in this->endEffectors
     * @param pose is the desired pose of the end effector
     * @param state is the current state of the object to use as root
     * @param x_in_out this value is the configuration of the current state of the group, and will
     * be set to the solution of the IK
     * @return true if a solution is found (valid kinematic)
     */
    bool computeInverseKinematic(const Chain &chain, const math::Transform &pose,
                                 const mdl::State &state, math::Vector &x_in_out) const;
    bool computeForwardKinematic(const Chain &chain, math::Transform &pose_out,
                                 const mdl::State &state, const math::Vector &x_in) const;

    inline const std::size_t getIndex() const;

    std::shared_ptr<Model> m_bodies;

    std::shared_ptr<Kinematic> m_kinematic;
    std::shared_ptr<rl::plan::SimpleModel> m_model;

    std::vector<std::shared_ptr<EndEffector>> endEffectors;
    std::vector<std::shared_ptr<Chain>> chains;
    std::vector<std::shared_ptr<Group>> groups;

  protected:
    const Scene &m_scene;
    std::shared_ptr<State> m_state;
    /**
     * unique identifier of the object
     */
    move4d::Identifier m_id;

    /**
     * name of the object
     */
    std::string m_name;

    /**
     * index in the underlying scene representation
     */
    std::size_t m_index;

    StatePtr m_minimum;

    StatePtr m_maximum;
};

inline const std::string SceneObject::getName() const { return m_name; }

inline const move4d::Identifier SceneObject::getId() const { return m_id; }

[[deprecated]] inline const std::shared_ptr<State> &SceneObject::getState() const
{
    Expects(m_state->getObject() == this);
    return m_state;
}

inline const std::size_t SceneObject::getIndex() const { return m_index; }

} // namespace mdl

} // namespace move4d
#endif

//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#include "move4d/mdl/CollisionManager.h"
#include "move4d/mdl/CollisionWhiteList.h"
#include "move4d/mdl/DegreeOfFreedomInfo.h"
#include "move4d/mdl/Movable.h"
#include "move4d/mdl/Scene.h"
#include "move4d/mdl/SceneObject.h"
#include "move4d/mdl/Types.h"
#include "move4d/mdl/WorldState.h"

#include "move4d/plan/RobotPart.h"
#include "move4d/plan/RobotSpace.h"
#include "move4d/plan/SO3Space.h"
#include "move4d/plan/WorldStateGoal.h"
#include "move4d/plan/ompl/Planner.h"
#include "move4d/plan/ompl/converters/StateSpaceFactory.h"

#include "move4d/file/model/URDFImporter.h"

#include "move4d-soqt/Application.h"
#include "move4d-soqt/ViewerSoQt.h"
#include "move4d-soqt/ViewerThreadQt.h"
#include <Inventor/Qt/SoQt.h>

#include <rl/sg/so/Scene.h>

#include <QApplication>
#include <QVBoxLayout>

#include <chrono>
#include <fstream>
#include <functional>
#include <thread>

#include <ompl/base/Path.h>
#include <ompl/base/StateSpace.h>
#include <ompl/geometric/PathGeometric.h>

#include <gsl/gsl>

#include <boost/progress.hpp>

using namespace move4d;

class Application
{
  public:
    Application(int argc, char *argv[]);

    void run();

  protected:
    static Application *s_instance;
    void f();

    std::unique_ptr<move4d::SoQt::Application> m_app;
    move4d::mdl::ScenePtr m_scene;
    move4d::mdl::SceneObjectPtr m_robot;
    move4d::mdl::SceneObjectPtr m_robot2;
    QWidget *m_mainWidget;
    view::ViewerSoQt *m_viewer;
    view::ThreadedViewerPtr m_viewerIF;
    std::shared_ptr<move4d::plan::RobotPartLists> m_partList;

    size_t m_run_nb{1};
    bool m_showPath{false};

    Real m_z_desired{1.};
    Real m_z_tolerance{.01};
    float m_time_limit{1.};

    std::string m_planningPart;
    std::string m_plannerType;
};

Application *Application::s_instance = nullptr;

Application::Application(int argc, char *argv[])
{
    Application::s_instance = this;

    std::string engine{"undef"};
    std::vector<std::string> files;
    for (int a = 0; a < argc; ++a) {
        std::cout << "\"" << argv[a] << "\"" << std::endl;
    }
    std::string robotName, robotName2;
    for (int a = 0; a < argc; ++a) {
        if (strcmp(argv[a], "-n") == 0) {
            m_run_nb = std::strtol(argv[++a], nullptr, 10);
        } else if (strcmp(argv[a], "-e") == 0 || strcmp(argv[a], "--engine") == 0) {
            engine = argv[++a];
        } else if (strcmp(argv[a], "-a") == 0) {
            m_plannerType = std::string(argv[++a]);
        } else if (strcmp(argv[a], "-f") == 0) {
            files.emplace_back(argv[++a]);
        } else if (strcmp(argv[a], "-v") == 0) {
            m_showPath = true;
        } else if (strcmp(argv[a], "-t") == 0) {
            m_time_limit = std::strtod(argv[++a], nullptr);
        } else if (strcmp(argv[a], "-p") == 0) {
            m_planningPart = std::string(argv[++a]);
        } else if (strcmp(argv[a], "-r") == 0) {
            robotName = std::string(argv[++a]);
        } else if (strcmp(argv[a], "-r2") == 0) {
            robotName2 = std::string(argv[++a]);
        }
    }

    m_app = std::make_unique<move4d::SoQt::Application>(engine, std::bind(&Application::f, this));

    // the main scene for computation
    m_scene = m_app->getScene();

    //  for the interface (async)
    m_viewerIF = m_app->getViewer();
    // the scene for visualisation
    m_viewer = m_viewerIF->getViewer();

    m_partList = std::make_shared<move4d::plan::RobotPartLists>();

    file::model::URDFImporter importer;
    for (const auto &f : files) {
        // load URDF
        m_app->loadFile(f);
        // if (robotName.empty()) {
        //    robotName = f;
        //} else if (robotName2.empty()) {
        //    robotName2 = f;
        //}
    }
    for (const auto &o : *m_scene) {
        auto &r = o.second;
        if (!m_robot) {
            if (robotName.empty() || (!robotName.empty() && r->getName() == robotName)) {
                m_robot = r;
                std::cout << "robot = " << m_robot->getName() << std::endl;
            }
        }
        if (!m_robot2 && m_robot != r) {
            if (robotName2.empty() || (!robotName2.empty() && r->getName() == robotName2)) {
                m_robot2 = r;
                std::cout << "robot2 = " << m_robot2->getName() << std::endl;
            }
        }
        assert(!m_robot || m_robot2 != m_robot);

        if (r->getName().find("obstacle") == std::string::npos) {
            // Create Robot Parts
            std::cout << "create robot parts for " << r->getName() << std::endl;

            bool isFloatingBase{false};
            uint firstArmDof = 3;
            size_t inDof(0);
            r->getJointOfDof(6, inDof);
            if (inDof == 6) // first joint is a floating joint
            {
                isFloatingBase = true;
                firstArmDof = 7;
            } else {
                isFloatingBase = false;
                firstArmDof = 3;
            }

            // Base Rot only:
            //    auto partBaseRot = move4d::plan::RobotPart("baseRot",m_robot.get());
            //    partBaseRot.addSubspace(std::make_shared<move4d::plan::SO3Space>(*m_robot->as<const
            //    move4d::mdl::Movable>(),3));
            //    partBaseRot.addSubspace(std::make_shared<move4d::plan::SO2Space>(*m_robot->as<const
            //    move4d::mdl::Movable>(),2)); m_partList->add(partBaseRot);

            // Base:
            auto partBase = move4d::plan::RobotPart("base", r.get());
            if (isFloatingBase) {
                partBase.addSubspace(
                    move4d::plan::SE3Space(r->as<const move4d::mdl::Movable>(), 0));
            } else {
                partBase.addSubspace(
                    move4d::plan::SE2Space(r->as<const move4d::mdl::Movable>(), 0, 1, 2));
            }
            m_partList->add(partBase);

            // Base Reeds Shepp:
            auto partRS = move4d::plan::RobotPart("baseRS", r.get());
            if (isFloatingBase) {
                partRS.addSubspace(move4d::plan::SE2Space(r->as<const move4d::mdl::Movable>(), 0,
                                                          plan::SE2Space::SE2Type::ReedsShepp));
            } else {
                partRS.addSubspace(move4d::plan::SE2Space(r->as<const move4d::mdl::Movable>(), 0, 1,
                                                          2, plan::SE2Space::SE2Type::ReedsShepp));
            }
            m_partList->add(partRS);

            // Arm:
            auto part1 = move4d::plan::RobotPart("1", r.get());
            auto part2 = move4d::plan::RobotPart("2", r.get());
            Eigen::Matrix<bool, 1, -1> dofs1(r->getNumberOfDof()), dofs2(r->getNumberOfDof());
            uint i = firstArmDof;
            dofs1.setZero();
            dofs2.setZero();
            std::cout << "total number of dofs is " << dofs1.size() << std::endl;
            for (; i < firstArmDof + (dofs1.size() - firstArmDof) / 2; ++i) {
                dofs1[i] = true;
            }
            for (; i < dofs1.size(); ++i) {
                dofs2[i] = true;
            }

            part1.addSubspace(move4d::plan::RobotSpace(r->as<move4d::mdl::Movable>(), dofs1));
            part2.addSubspace(move4d::plan::RobotSpace(r->as<move4d::mdl::Movable>(), dofs2));
            m_partList->add(part1);
            m_partList->add(part2);

            auto partArm = move4d::plan::RobotPart("Arm", r.get());
            partArm.addSubspace(part1);
            partArm.addSubspace(part2);
            m_partList->add(partArm);

            auto partAll = move4d::plan::RobotPart("All", r.get());
            partAll.addSubspace(partBase);
            partAll.addSubspace(part1);
            partAll.addSubspace(part2);
            m_partList->add(partAll);

            auto partAllRS = move4d::plan::RobotPart("AllRS", r.get());
            partAllRS.addSubspace(partRS);
            partAllRS.addSubspace(part1);
            partAllRS.addSubspace(part2);
            m_partList->add(partAllRS);

            auto partNames = m_partList->get(r->getId());
            std::cout << "parts for robot " << r->getId() << "\n";
            for (const auto &s : partNames) {
                std::cout << s << "\n";
            }
            std::cout.flush();
        }
    }
    m_scene->resetCollisionWhiteList();
    for (const auto &r : *m_scene) {
        m_scene->getCollisionWhitelist()->removeSelfColliding(*r.second,
                                                              *m_app->getCollisionManager());
    }
    //    m_scene->getCollisionWhitelist()->disable(*m_robot,*m_robot2);
    std::cout << "whitelist\n" << *m_scene->getCollisionWhitelist() << std::endl;
}
void Application::run() { m_app->start(); }

void Application::f()
{
    using namespace std;
    using namespace move4d;

    this_thread::sleep_for(chrono::microseconds(1));

    auto space = make_shared<plan::RobotPart>(m_partList->get(m_robot->getId(), m_planningPart));
    std::ofstream dataFile("data.csv");
    mdl::WorldState startws;
    for (uint execCount = 0; execCount < m_run_nb; ++execCount) {
        std::cout << "shooting start" << std::endl;
        do {
            startws = mdl::WorldState(m_scene.get());
            space->shootUniform(startws);
            m_scene->applyWorldState(startws);
            if (m_showPath) {
                m_viewerIF->update(startws);
                this_thread::sleep_for(chrono::milliseconds(100));
            }
        } while (m_app->getCollisionManager()->isColliding());
        std::cout << "found start" << std::endl;
        this_thread::sleep_for(chrono::milliseconds(1000 * m_showPath));
        std::cout << "shooting goal..." << std::endl;
        mdl::WorldState goalws;
        do {
            goalws = mdl::WorldState(m_scene.get());
            space->shootUniform(goalws);
            m_scene->applyWorldState(goalws);
            if (m_showPath) {
                m_viewerIF->update(goalws);
                this_thread::sleep_for(chrono::milliseconds(100));
            }
        } while (m_app->getCollisionManager()->isColliding());
        std::cout << "found goal" << std::endl;
        this_thread::sleep_for(chrono::milliseconds(500 * m_showPath));
        auto goal = make_shared<plan::WorldStateGoal>(make_shared<mdl::WorldState>(goalws));
        plan::ompl::Planner planner(space, m_app->getCollisionManager(), m_plannerType);
        planner.setStart(std::make_shared<mdl::WorldState>(startws));
        planner.setGoal(goal);
        auto time_start = std::chrono::high_resolution_clock::now();
        bool ok = planner.solve(m_time_limit);
        auto time_end = std::chrono::high_resolution_clock::now();
        auto duration = time_end - time_start;
        dataFile << execCount << "," << duration.count() << "," << ok << std::endl;
        cout << "Planner status = " << ok << endl;
        if (ok) {
            auto space = planner.getSpaceWrapper();
            ompl::base::PathPtr path = planner.getPathOmpl();
            std::cout << "Found Solution" << std::endl;
            if (m_showPath) {
                path->print(std::cout);
                auto gpath = path->as<ompl::geometric::PathGeometric>();
                gpath->interpolate(500);
                mdl::WorldState ws(m_scene.get());
                boost::progress_display show_progress(500);
                for (size_t i = 0; M4D_WHILE_CONDITION(i < gpath->getStateCount()); ++i) {
                    space->applyState(gpath->getState(i), ws);
                    m_scene->applyWorldState(ws);
                    m_viewerIF->update(ws);
                    ++show_progress;
                    this_thread::sleep_for(std::chrono::milliseconds(1000 / 60));
                }
                std::cout << '\n';
            }
        }
    }
}

int main(int argc, char *argv[])
{
    Application app(argc, argv);
    app.run();
    return 0;
}

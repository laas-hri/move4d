//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_PLAN_OMPL_POSEMOTIONVALIDATOR_H
#define MOVE4D_PLAN_OMPL_POSEMOTIONVALIDATOR_H

#include "move4d/common.h"
#include <ompl/base/MotionValidator.h>

namespace move4d
{
namespace plan
{
namespace ompl
{

class PoseMotionValidator : public ::ompl::base::MotionValidator
{
    MOVE4D_STATIC_LOGGER;

  public:
    PoseMotionValidator(::ompl::base::SpaceInformation *si);
    PoseMotionValidator(const ::ompl::base::SpaceInformationPtr &si);
    bool checkMotion(const ::ompl::base::State *s1, const ::ompl::base::State *s2) const override;
    bool checkMotion(const ::ompl::base::State *s1, const ::ompl::base::State *s2,
                     std::pair<::ompl::base::State *, double> &lastValid) const override;

  protected:
    double maxJump_ = 1.e1; // TODO(Jules): magic value
};

} // namespace ompl
} // namespace plan
} // namespace move4d

#endif // MOVE4D_PLAN_OMPL_POSEMOTIONVALIDATOR_H

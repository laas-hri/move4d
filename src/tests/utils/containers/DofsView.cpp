//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#include <list> // needed by some versions of boost

#include <boost/mpl/list.hpp>
#include <boost/test/data/test_case.hpp>
#include <boost/test/unit_test.hpp>
#include <move4d/utils/containers/DofsView.h>

namespace move4d
{
typedef boost::mpl::list<std::vector<Real>, math::Vector> container_types;

BOOST_AUTO_TEST_SUITE(dofs_view)
template <class T> void test_containers(size_t size)
{
    using namespace views;
    // DofView v; // no default c-tor
    T container(size);
    container[0] = 0.;
    auto dvc = DofsViewContainer<T>(container);

    dvc[0].set(1.);

    BOOST_CHECK_EQUAL(dvc[0].get(), container[0]);
    BOOST_CHECK_EQUAL(dvc[0].get(), 1.);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(test1, T, container_types)
{
    // test_containers<T>(0);
    test_containers<T>(1);
    test_containers<T>(100);
}

BOOST_DATA_TEST_CASE(test2, boost::unit_test::data::make({1, 2, 3, 100, 1000}), size)
{
    test_containers<std::vector<Real>>(size);
}

BOOST_AUTO_TEST_SUITE_END()

} // namespace move4d

//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//

#include "move4d/mdl/DegreeOfFreedomInfo.h"
#include "move4d/mdl/SceneObject.h"
#include "move4d/mdl/WorldState.h"

namespace move4d
{

namespace mdl
{

/**
 * creates the DoF with a reference to the given state
 */
// TODO(Jules): may need to find another way to manage limit-less joints
// (min=DOUBLE_MIN,max=DOUBLE_MAX)
DegreeOfFreedomInfo::DegreeOfFreedomInfo(Identifier object_id, move4d::Real vmin, move4d::Real vmax,
                                         bool wrap, StateType::Type type, std::size_t index)
    : m_handle(std::make_shared<const DegreeOfFreedomHandle>(object_id, type, index)),
      m_vmin(std::max(vmin, -std::numeric_limits<move4d::Real>::max() / 2)),
      m_vmax(std::min(vmax, std::numeric_limits<move4d::Real>::max() / 2)), m_wrap(wrap)
{
}

DegreeOfFreedomInfo::DegreeOfFreedomInfo(std::shared_ptr<const DegreeOfFreedomHandle> handle,
                                         Real vmin, Real vmax, bool wrap)
    : m_handle(std::move(handle)),
      m_vmin(std::max(vmin, -std::numeric_limits<move4d::Real>::max() / 2)),
      m_vmax(std::min(vmax, std::numeric_limits<move4d::Real>::max() / 2)), m_wrap(wrap)
{
}

move4d::Real DegreeOfFreedomInfo::shootUniform(const move4d::Real &rand) const
{
    assert(0 <= rand && rand <= 1);
    move4d::Real x = this->getMinimum() + rand * (this->getMaximum() - this->getMinimum());
    assert(x >= this->getMinimum() && x <= this->getMaximum());
    return x;
}

move4d::Real DegreeOfFreedomInfo::shootGaussian(const move4d::Real &rand, const move4d::Real &mean,
                                                const move4d::Real &sigma) const
{
    Real v = mean + rand * sigma;
    this->clip(v);
    return v;
}

bool DegreeOfFreedomInfo::wrapAround() const { return m_wrap; }

void DegreeOfFreedomInfo::clip(move4d::Real &value) const
{
    if (wrapAround()) {
        Real range = std::abs(getMaximum() - getMinimum());
        while (value > getMaximum()) {
            value -= range;
        }
        while (value < getMinimum()) {
            value += range;
        }
    } else {
        value = std::min(getMaximum(), std::max(getMinimum(), value));
    }
}

void DegreeOfFreedomHandle::setIn(State &state, Real value) const
{
    assert(state.size(m_type) > m_index);
    state(m_index, m_type) = value;
}

void DegreeOfFreedomHandle::setIn(WorldState &ws, Real value) const
{
    assert(ws.getStates().find(m_object_id) != ws.getStates().end());
    setIn(ws[m_object_id], value);
}

Real DegreeOfFreedomHandle::getFrom(const State &state) const
{
    assert(state.size(m_type) > m_index);
    return state(m_index, m_type);
}

Real DegreeOfFreedomHandle::getFrom(const WorldState &ws) const
{
    assert(ws.getStates().find(m_object_id) != ws.getStates().end());
    return getFrom(ws[m_object_id]);
}

RotationInQuaternionDoF::RotationInQuaternionDoF(Identifier object_id, math::Vector3 axis,
                                                 std::size_t firstQuaternionDof)
    : DegreeOfFreedomHandle(object_id, StateType::Position, firstQuaternionDof + 3),
      m_axis(std::move(axis))
{
    m_axis.normalize();
    for (size_t i = 0; i < 3; ++i) {
        m_xyz.push_back(std::make_shared<const DegreeOfFreedomHandle>(object_id, type(),
                                                                      firstQuaternionDof + i));
    }
}

RotationInQuaternionDoF::RotationInQuaternionDoF(Identifier object_id, math::Vector3 axis,
                                                 std::size_t indexX, std::size_t indexY,
                                                 std::size_t indexZ, std::size_t indexW)
    : DegreeOfFreedomHandle(object_id, StateType::Position, indexW), m_axis(std::move(axis))
{
    m_axis.normalize();
    m_xyz.push_back(std::make_shared<const DegreeOfFreedomHandle>(object_id, type(), indexX));
    m_xyz.push_back(std::make_shared<const DegreeOfFreedomHandle>(object_id, type(), indexY));
    m_xyz.push_back(std::make_shared<const DegreeOfFreedomHandle>(object_id, type(), indexZ));
}
void RotationInQuaternionDoF::setIn(State &state, Real value) const
{
    assert(std::abs(m_axis.norm() - 1) < 1e-5); // axis is normalized
    double w, k;
    w = std::cos(value / 2);
    k = sin(value / 2);
    state(index(), type()) = w;
    for (size_t i = 0; i < 3; i++) {
        state(m_xyz[i]->index(), type()) = m_axis[i] * k;
    }
    assert(std::abs(math::Quaternion(w, m_axis[0] * k, m_axis[1] * k, m_axis[2] * k).norm() - 1) <
           1e-5);
}

Real RotationInQuaternionDoF::getFrom(const State &state) const
{
    assert(std::abs(m_axis.norm() - 1) < 1e-5); // axis is normalized
    if (std::abs(math::Vector3(m_xyz[0]->getFrom(state), m_xyz[1]->getFrom(state),
                               m_xyz[2]->getFrom(state))
                     .cross(m_axis)
                     .norm()) < 1e-5) {
        // quaternion axis and m_axis are colinear <=> cross-product = 0
        return 2 * std::acos(DegreeOfFreedomHandle::getFrom(state));
    }
    std::cout << "Warn: rotation around wrong axis, returning NaN" << std::endl;
    return std::numeric_limits<move4d::Real>::quiet_NaN();
}

} // namespace mdl

} // namespace move4d

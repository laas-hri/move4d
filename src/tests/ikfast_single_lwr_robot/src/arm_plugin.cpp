//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#include "ikfast.h"
#include <move4d-addons/ikfast/src/Solver.h>
#include <move4d/io/Logger.h>
#include <move4d/mdl/SceneObject.h>

#include <kdl/chain.hpp>
#include <rl/mdl/Dynamic.h>
#include <rl/mdl/Frame.h>

namespace move4d
{
namespace kin
{
namespace ikfast
{

class PluginSolver : public IkFastSolver
{
    MOVE4D_STATIC_LOGGER;

  public:
    int getNumFreeParameters() override;
    int *getFreeParameters() override;

  protected:
    void computeInverseKinematic(gsl::span<const Real> eepos, gsl::span<const Real> eerot,
                                 const math::Vector &x, gsl::span<const Real> pfree,
                                 std::vector<std::pair<Real, math::Vector>> &solutions) override;

    void computeForwardKinematic(gsl::span<Real> eepos, gsl::span<Real> eerot,
                                 const math::Vector &x_in) override;
};

INIT_MOVE4D_STATIC_LOGGER(PluginSolver, "move4d.kin.ikfast.solver");

FACTORY_REGISTER_FACTORY(IkFastSolverFactory, "single_lwr_robot_arm",
                         []() { return std::make_shared<PluginSolver>(); });

int PluginSolver::getNumFreeParameters() { return ::ikfast::GetNumFreeParameters(); }

int *PluginSolver::getFreeParameters() { return ::ikfast::GetFreeParameters(); }

void PluginSolver::computeInverseKinematic(gsl::span<const Real> eepos, gsl::span<const Real> eerot,
                                           const math::Vector &x, gsl::span<const Real> pfree,
                                           std::vector<std::pair<Real, math::Vector>> &solutions_q)
{
    ::ikfast::IkSolutionList<Real> solutions;
    Real _pos[3];
    Real _rot[9];
    gsl::span<Real> pos(_pos), rot(_rot);

    //::ikfast::GetNumFreeParameters();
    //::ikfast::GetFreeParameters();
    bool ok = ::ikfast::ComputeIk(eepos.data(), eerot.data(), pfree.data(), solutions);

    if (ok) {
        // M4D_TRACE("ikfast single_lwr_robot_arm found " << solutions.GetNumSolutions()
        //                                                << " solutions\n");
        math::Vector q(x);
        for (size_t i = 0; i < solutions.GetNumSolutions(); ++i) {
            auto &s = solutions.GetSolution(i);
            s.GetSolution(q.data(), nullptr);
            Real dist = (x - q).squaredNorm();
            solutions_q.emplace_back(dist, q);
            computeForwardKinematic(pos, rot, q);
            M4D_ASSERT_MSG(std::equal(eepos.begin(), eepos.end(), pos.begin(), pos.end(),
                                      [](Real a, Real b) { return std::abs(a - b) < 1e-8; }),
                           "error in check with FK");
            M4D_ASSERT_MSG(std::equal(eerot.begin(), eerot.end(), rot.begin(), rot.end(),
                                      [](Real a, Real b) { return std::abs(a - b) < 1e-8; }),
                           "error in check with FK");
            // M4D_TRACE("solution " << i);
            // M4D_TRACE("solutions dofs# : " << s.GetDOF());
            // M4D_TRACE("free dofs to set: " << (s.GetFree().size() ? APPEND_ARRAY(s.GetFree(), ",
            // ")
            //                                                       : "none"));
            // M4D_TRACE("solution: " << x.transpose());
            // M4D_TRACE("sq distance: " << dist);
        }
        // std::sort(solutions_q.begin(), solutions_q.end(),
        //           [](auto &a1, auto &a2) { return a1.first < a2.first; });
        // Ensures(solutions_q.front().first <= solutions_q.back().first);
    }
}

void PluginSolver::computeForwardKinematic(gsl::span<Real> eepos, gsl::span<Real> eerot,
                                           const math::Vector &x_in)
{
    ::ikfast::ComputeFk(x_in.data(), eepos.data(), eerot.data());
}

} // namespace ikfast

} // namespace kin

} // namespace move4d

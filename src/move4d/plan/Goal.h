//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_PLAN_GOAL_H
#define MOVE4D_PLAN_GOAL_H

#include "move4d/common.h"
#include "move4d/plan/Types.h"

namespace move4d
{

namespace mdl
{
class WorldState;
} // namespace mdl

namespace plan
{

class Goal : public WorldStateFunction
{
  public:
    /**
     * heuristic distance to reach the goal
     */
    virtual move4d::Real distance(const move4d::mdl::WorldState &ws) const = 0;

    /**
     * is the goal reached?
     */
    virtual bool check(const move4d::mdl::WorldState &ws) const = 0;

  protected:
    Goal(size_type input_size = 1, size_type output_size = 1, std::string name = {});

    virtual void impl_compute(result_ref result, const_argument_ref argument) const override = 0;
};

} // namespace plan

} // namespace move4d
#endif

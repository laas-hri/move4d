//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_PLAN_OMPL_SO3STATESPACEBOUNDED_H
#define MOVE4D_PLAN_OMPL_SO3STATESPACEBOUNDED_H

#include "move4d/io/Logger.h"
#include "move4d/math/Vector.h"
#include <ompl/base/spaces/SO3StateSpace.h>

namespace move4d
{
namespace plan
{
namespace ompl
{

class SO3StateSpaceBounded : public ::ompl::base::SO3StateSpace
{
    MOVE4D_STATIC_LOGGER;

  public:
    SO3StateSpaceBounded(std::vector<Real> reference_quaternion_xyzw, Real tolerance);
    SO3StateSpaceBounded(const math::Quaternion &reference, Real tolerance);

    double getMaximumExtent() const override;
    double getMeasure() const override;
    void enforceBounds(::ompl::base::State *state) const override;
    bool satisfiesBounds(const ::ompl::base::State *state) const override;
    ::ompl::base::StateSamplerPtr allocDefaultStateSampler() const override;

    class Sampler : public ::ompl::base::SO3StateSampler
    {
        MOVE4D_STATIC_LOGGER;

      public:
        Sampler(const SO3StateSpaceBounded *space);
        void sampleUniform(::ompl::base::State *state) override;
        void sampleUniformNear(::ompl::base::State *state, const ::ompl::base::State *near,
                               double distance) override;
        void sampleGaussian(::ompl::base::State *state, const ::ompl::base::State *mean,
                            double stdDev) override;
    };

    Real getTolerance() const;
    const StateType &getReference() const;

  private:
    StateType _reference;
    const Real _tolerance;
};

} // namespace ompl
} // namespace plan
} // namespace move4d

#endif // MOVE4D_PLAN_OMPL_SO3STATESPACEBOUNDED_H

//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//

#include "move4d/math/Vector.h"

namespace move4d
{

namespace math
{

move4d::Real utilities::m_epsilon = 1e-6;

Transform changeFrame(const Transform &frame_A, const Transform &pos_in_A, const Transform &frame_B)
{
    Eigen::Affine3d new_pos;
    Eigen::Affine3d abs_pos = frame_A * pos_in_A;
    new_pos = frame_B.inverse() * abs_pos;
    return new_pos;
}

Vector3 changeFramePoint(const Transform &frame_A, const Vector3 &point_in_A,
                         const Transform &frame_B)
{
    Vector3 new_pos;
    Vector3 abs_pos = frame_A * point_in_A;
    new_pos = frame_B.inverse() * abs_pos;
    return new_pos;
}

Vector3 changeFrameVector(const Transform &frame_A, const Vector3 &vector_in_A,
                          const Transform &frame_B)
{
    Vector3 new_pos;
    Vector3 abs_pos = frame_A.linear() * vector_in_A;
    new_pos = frame_B.linear().inverse() * abs_pos;
    return new_pos;
}

} // namespace math

} // namespace move4d

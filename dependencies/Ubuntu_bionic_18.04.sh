sudo apt-get install \
    build-essential \
    git \
    curl \
    tar \
    cmake \
    libeigen3-dev \
    libboost-all-dev \
    libxml2-dev \
    pkgconf \
    liburdfdom-dev \
    libsdformat*-dev \
    libxslt1-dev \
    libcoin*-dev \
    libsoqt*-dev \
    libqt4-dev \
    libfcl-dev \
    libnlopt-dev \
    liblog4cxx-dev \
    libtinyxml2-dev \
    libbullet-dev

//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_MDL_SCENE_H
#define MOVE4D_MDL_SCENE_H

#include "move4d/common.h"
#include "move4d/math/Vector.h"
#include "move4d/mdl/CollisionScene.h"
#include "move4d/mdl/Types.h"
#include <map>
#include <memory>
#include <string>
#include <unordered_map>

namespace move4d
{
namespace mdl
{
class SceneObject;
}
} // namespace move4d
namespace move4d
{
namespace mdl
{
class WorldState;
}
} // namespace move4d
namespace move4d
{
namespace mdl
{
class CollisionManager;
}
} // namespace move4d
namespace move4d
{
namespace mdl
{
class CollisionWhiteList;
}
} // namespace move4d

namespace move4d
{

namespace mdl
{

/**
 * Registers all the objects used for the planning (robots, humans, obstacles, manipulables)
 */
class Scene
{
    MOVE4D_STATIC_LOGGER;

  public:
    typedef std::unordered_map<move4d::Identifier, std::shared_ptr<SceneObject>> ObjectMap;

    typedef ObjectMap::iterator iterator;

    typedef ObjectMap::const_iterator const_iterator;

    Scene() = default;
    Scene(std::shared_ptr<CollisionScene> collisionScene);

    SceneObjectPtr create(const std::string &objecttype, const std::string &name = "",
                          const move4d::Identifier &id = Identifier{});

    void remove(const move4d::Identifier &id);

    move4d::Identifier createId(const std::string &name);

    SceneObjectPtr getObject(const move4d::Identifier &id);

    [[deprecated]] SceneObjectPtr getObjectByName(const std::string &name) const;
    std::vector<SceneObjectPtr> getObjectsByName(const std::string &name) const;

    inline iterator begin();

    inline const_iterator begin() const;

    inline iterator end();

    inline const_iterator end() const;

    /**
     * creates a new worldstate with copies of all the current states
     * @deprecated will not store currend world state anymore soon
     */
    [[deprecated]] std::shared_ptr<move4d::mdl::WorldState> getCurrentWorldState() const;

    void applyWorldState(const WorldState &ws);

    const std::shared_ptr<CollisionWhiteList> &getCollisionWhitelist();
    void resetCollisionWhiteList();

    /// limits of the environment (box)
    const std::pair<math::Vector3, math::Vector3> &getSpaceBounds() const;
    void setSpaceBounds(std::pair<math::Vector3, math::Vector3> space_bounds);

  protected:
    std::shared_ptr<CollisionScene> m_collisionScene;
    std::shared_ptr<CollisionWhiteList> m_collisionWhitelist;

    std::unordered_map<move4d::Identifier, std::shared_ptr<SceneObject>> m_objects;

    std::map<std::string, std::vector<SceneObjectPtr>> m_nameMap;

    std::pair<math::Vector3, math::Vector3> m_space_bounds = {math::Vector3::Zero(),
                                                              math::Vector3::Zero()};
};
inline Scene::iterator Scene::begin() { return m_objects.begin(); }

inline Scene::const_iterator Scene::begin() const { return m_objects.begin(); }

inline Scene::iterator Scene::end() { return m_objects.end(); }

inline Scene::const_iterator Scene::end() const { return m_objects.cend(); }

} // namespace mdl

} // namespace move4d
#endif

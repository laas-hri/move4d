//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_VIEWS_DOFSVIEW_H
#define MOVE4D_VIEWS_DOFSVIEW_H

#include "move4d/3rdParty/prettyprint/prettyprint.hpp"
#include "move4d/common.h"
#include "move4d/mdl/DegreeOfFreedomInfo.h"
#include "move4d/mdl/Types.h"
#include "move4d/plan/Types.h"

namespace move4d
{
namespace views
{

class DofViewBase
{
  public:
    virtual std::unique_ptr<DofViewBase> clone() const = 0;
    template <typename V> DofViewBase &operator=(V &v)
    {
        set(v);
        return *this;
    }
    virtual Real get() const = 0;
    virtual void set(const Real &x) = 0;
};

class DofView
{
  public:
    DofView(std::unique_ptr<DofViewBase> ptr) : _ptr(std::move(ptr)) {}
    DofView(const DofView &other) : _ptr(other._ptr->clone()) {}
    virtual Real get() const { return _ptr->get(); }
    virtual void set(const Real &x) { _ptr->set(x); }

    operator Real() { return get(); }
    DofView &operator=(const Real &x)
    {
        set(x);
        return *this;
    }

  private:
    std::unique_ptr<DofViewBase> _ptr;
};
template <typename V> Real operator*(const DofView &d, const V &v) { return d.get() * v; }
template <typename V> Real operator+(const DofView &d, const V &v) { return d.get() + v; }
template <typename V> Real operator/(const DofView &d, const V &v) { return d.get() / v; }
template <typename V> Real operator-(const DofView &d, const V &v) { return d.get() - v; }
template <typename V> Real operator*(const V &v, const DofView &d) { return v * d.get(); }
template <typename V> Real operator+(const V &v, const DofView &d) { return v + d.get(); }
template <typename V> Real operator/(const V &v, const DofView &d) { return v / d.get(); }
template <typename V> Real operator-(const V &v, const DofView &d) { return v - d.get(); }
// clang-format off
template <typename V> DofView &operator+=(DofView &d, const V &v) { d.set(d + v); return d; }
template <typename V> DofView &operator-=(DofView &d, const V &v) { d.set(d - v); return d; }
template <typename V> DofView &operator*=(DofView &d, const V &v) { d.set(d * v); return d; }
template <typename V> DofView &operator/=(DofView &d, const V &v) { d.set(d / v); return d; }
template <typename V> Real &operator+=(const V &v, DofView &d) { v = (v + d); return v; }
template <typename V> Real &operator-=(const V &v, DofView &d) { v = (v - d); return v; }
template <typename V> Real &operator*=(const V &v, DofView &d) { v = (v * d); return v; }
template <typename V> Real &operator/=(const V &v, DofView &d) { v = (v / d); return v; }
// clang-format on

class DofViewWorldState : public DofViewBase
{
  public:
    DofViewWorldState(const DofViewWorldState &) = default;
    DofViewWorldState(DofViewWorldState &&) = default;
    DofViewWorldState &operator=(const DofViewWorldState &other)
    {
        _ws = other._ws;
        _dof = other._dof;
        return *this;
    }
    DofViewWorldState &operator=(DofViewWorldState &&other)
    {
        _ws = other._ws;
        _dof = std::move(other._dof);
        return *this;
    }
    virtual ~DofViewWorldState() = default;

  protected:
    DofViewWorldState(mdl::WorldState &ws, mdl::DegreeOfFreedomInfo dof)
        : _ws(ws), _dof(std::move(dof))
    {
    }

  public:
    inline static DofView create(mdl::WorldState &ws, mdl::DegreeOfFreedomInfo dof)
    {
        return DofView(std::unique_ptr<DofViewBase>(new DofViewWorldState(ws, dof)));
    }
    Real get() const override { return _dof.getFrom(_ws); }
    void set(const Real &x) override { _dof.setIn(_ws, x); }

    std::unique_ptr<DofViewBase> clone() const override
    {
        return std::unique_ptr<DofViewBase>(new DofViewWorldState(*this));
    }

  private:
    mdl::WorldState &_ws;
    mdl::DegreeOfFreedomInfo _dof;
};

template <typename C> class DofViewContainer : public DofViewBase
{
  public:
    DofViewContainer(const DofViewContainer &) = default;
    DofViewContainer(DofViewContainer &&) = default;
    DofViewContainer &operator=(const DofViewContainer &) = default;
    DofViewContainer &operator=(DofViewContainer &&) = default;
    virtual ~DofViewContainer() = default;

  protected:
    DofViewContainer(C &container, size_t index) : _container(container), _index(index) {}

  public:
    inline static DofView create(C &container, size_t index)
    {
        return DofView(std::unique_ptr<DofViewBase>(new DofViewContainer<C>(container, index)));
    }
    Real get() const override { return _container[_index]; }
    void set(const Real &x) override { _container[_index] = x; }

    std::unique_ptr<DofViewBase> clone() const override
    {
        return std::unique_ptr<DofViewBase>(new DofViewContainer(*this));
    }

  private:
    C &_container;
    size_t _index;
};

class DofsView
{
  public:
    virtual DofView operator[](size_t i) = 0;
    virtual const DofView operator[](size_t i) const = 0;

    using iterator_base = std::iterator<std::output_iterator_tag, DofView>;
    class const_iterator : public iterator_base
    {
      public:
        const_iterator(const DofsView *view, size_t i) : view(view), i(i) {}
        const_iterator &operator++()
        {
            ++i;
            return *this;
        }
        const_iterator operator++(int)
        {
            const_iterator retval = *this;
            ++(*this);
            return retval;
        }
        value_type operator*() { return std::move((*view)[i]); }
        bool operator==(const const_iterator &other) { return view == other.view && i == other.i; }
        bool operator!=(const const_iterator &other) { return view != other.view || i != other.i; }

      protected:
        const DofsView *view;
        size_t i;
    };

    virtual const_iterator begin() const { return const_iterator(this, 0); }
    virtual const_iterator end() const { return const_iterator(this, size()); }

    virtual size_t size() const = 0;
};

class DofsViewWorldstate : public DofsView
{
  public:
    DofsViewWorldstate(mdl::WorldState &ws, const plan::SearchSpace &space);
    DofView operator[](size_t i) override;
    const DofView operator[](size_t i) const override;

    size_t size() const override;

  private:
    mdl::WorldState &_ws;
    const plan::SearchSpace &_space;
};

template <typename C> class DofsViewContainer : public DofsView
{
  public:
    /// for raw arrays (C=T[] or C=T*), use c++20 std::span<T> or gsl::span<T>
    DofsViewContainer(C &container) : _container(container) {}
    DofView operator[](size_t i) override { return DofViewContainer<C>::create(_container, i); }
    const DofView operator[](size_t i) const override
    {
        return DofViewContainer<C>::create(_container, i);
    }

    size_t size() const override { return _container.size(); }

  private:
    C &_container;
};

} // namespace views
} // namespace move4d

#endif // MOVE4D_VIEWS_DOFSVIEW_H

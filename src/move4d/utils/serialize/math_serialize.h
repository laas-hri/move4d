//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
/**
 * @file math_serialize.h
 * from https://stackoverflow.com/a/23407209/2883580
 */

// chkhdr: no-check

#ifndef MATH_SERIALIZE_H
#define MATH_SERIALIZE_H

friend class boost::serialization::access;
template <class Archive> void save(Archive &ar, const unsigned int version) const
{
    derived().eval();
    const Index rows = derived().rows(), cols = derived().cols();
    ar &rows;
    ar &cols;
    for (Index j = 0; j < cols; ++j)
        for (Index i = 0; i < rows; ++i)
            ar &derived().coeff(i, j);
}

template <class Archive> void load(Archive &ar, const unsigned int version)
{
    Index rows, cols;
    ar &rows;
    ar &cols;
    if (rows != derived().rows() || cols != derived().cols())
        derived().resize(rows, cols);
    ar &boost::serialization::make_array(derived().data(), derived().size());
}

template <class Archive> void serialize(Archive &ar, const unsigned int file_version)
{
    boost::serialization::split_member(ar, *this, file_version);
}

#endif // MATH_SERIALIZE_H

//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_PLAN_OMPL_CONVERTERS_CONVERTERS_H
#define MOVE4D_PLAN_OMPL_CONVERTERS_CONVERTERS_H

#include "move4d/math/Vector.h"
#include "move4d/motion/Types.h"
#include <ompl/base/spaces/SE3StateSpace.h>

namespace ompl
{
namespace base
{
class SpaceInformation;
using SpaceInformationPtr = std::shared_ptr<SpaceInformation>;

} // namespace base
namespace geometric
{
class PathGeometric;
} // namespace geometric
} // namespace ompl

namespace move4d
{
namespace plan
{
namespace ompl
{

class StateSpaceWrapper;
using StateSpaceWrapperPtr = std::shared_ptr<StateSpaceWrapper>;

namespace converters
{

math::Transform toTransform(const ::ompl::base::SE3StateSpace::StateType *se3);
/// se3state must be allocated
void toState(const math::Transform &transform, ::ompl::base::SE3StateSpace::StateType *se3state);

std::unique_ptr<::ompl::geometric::PathGeometric>
geometricPath(const move4d::motion::GeometricPath &path_in, const StateSpaceWrapper &wrapper,
              const ::ompl::base::SpaceInformationPtr &si);

} // namespace converters
} // namespace ompl
} // namespace plan
} // namespace move4d

#endif // MOVE4D_PLAN_OMPL_CONVERTERS_CONVERTERS_H

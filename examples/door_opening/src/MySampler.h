#ifndef _MOVE4D_EXAMPLES_DOOR_OPENING_SRC_SAMPLER_SAMPLEKUKABASEFROMEEF_H_
#define _MOVE4D_EXAMPLES_DOOR_OPENING_SRC_SAMPLER_SAMPLEKUKABASEFROMEEF_H_

#include <move4d/plan/KinematicGraphSearchSpace.h>
#include <move4d/plan/ompl/PoseStateSpace.h>

namespace move4d
{

class SampleKukaBaseFromEef : public plan::BaseSampler
{
    MOVE4D_STATIC_LOGGER;

  public:
    SampleKukaBaseFromEef(std::shared_ptr<const plan::SearchSpace> space,
                          math::Transform base_to_arm_tf, Real arm_reach, size_t first_dof);
    void sampleUniform(views::DofsView &conf_in_out, const math::Transform &base_pose,
                       math::Transform &end_pose_out,
                       const std::vector<math::Transform> &input_poses) override;
    void sampleUniformNear(views::DofsView &conf_in_out, const math::Transform &base_pose,
                           math::Transform &end_pose_out,
                           const std::vector<math::Transform> &input_poses, views::DofsView &near,
                           Real distance) override;
    void sampleGaussian(views::DofsView &conf_in_out, const math::Transform &base_pose,
                        math::Transform &end_pose_out,
                        const std::vector<math::Transform> &input_poses, views::DofsView &mean,
                        Real stdev) override;

    static plan::SamplerAllocator make_allocator(math::Transform base_to_arm_tf, Real arm_reach);

  private:
    Real arm_reach_, inner_radius_;
    math::Transform base_to_arm_tf_;
    size_t first_dof;
};

} // namespace move4d

#endif // _MOVE4D_EXAMPLES_DOOR_OPENING_SRC_SAMPLER_SAMPLEKUKABASEFROMEEF_H_

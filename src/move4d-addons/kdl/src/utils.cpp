//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#include "utils.h"
#include "converters.h"
#include "move4d/view/ViewerInterface.h"

#include <kdl/chain.hpp>
#include <kdl/chainfksolverpos_recursive.hpp>

namespace move4d
{
namespace kin
{
namespace kdl
{
namespace utils
{

void displayJoint(const KDL::Segment &segment, const math::Transform &segment_pose,
                  bool display_tip)
{
    // math::Vector3 axis;
    // axis.x() = segment.getJoint().JointAxis().x();
    // axis.y() = segment.getJoint().JointAxis().y();
    // axis.z() = segment.getJoint().JointAxis().z();
    math::Vector3 origin;
    origin.x() = segment.getJoint().JointOrigin().x();
    origin.y() = segment.getJoint().JointOrigin().y();
    origin.z() = segment.getJoint().JointOrigin().z();

    // TODO(Jules): compute the orientation of the joint frame correctly (using axis)
    math::Transform joint_pose = math::Transform::Identity();
    joint_pose.translation() = origin;
    joint_pose = math::changeFrame(segment_pose, joint_pose, math::Transform::Identity());
    view::ViewerInstance::instance().get()->drawStampedPose(joint_pose, segment_pose,
                                                            segment.getJoint().getName() + "__j");

    if (display_tip) {
        // TODO(Jules): erroneous kdl tip frame display due to erroneous joint orientation (above)
        auto tip_pose = joint_pose * converters::toTransform(segment.getFrameToTip());
        view::ViewerInstance::instance().get()->drawStampedPose(tip_pose, joint_pose, ">");
    }
}
void displayChainFrames(const KDL::Chain &chain, const KDL::JntArray &q,
                        const math::Transform &base_pose)
{
    KDL::ChainFkSolverPos_recursive solver(chain);
    std::vector<KDL::Frame> poses(chain.getNrOfSegments());
    int res = solver.JntToCart(q, poses);
    bool display_tip = false; // TODO(Jules): disabled because of erroneous computation of tip frame
    if (!poses.empty()) {
        auto segment_pose = base_pose;
        // math::changeFrame(base_pose, converters::toTransform(poses[0]),
        // math::Transform::Identity());
        view::ViewerInstance::instance().get()->drawStampedPose(
            segment_pose, segment_pose, chain.getSegment(0).getName() + "_k_");
        displayJoint(chain.getSegment(0), segment_pose, display_tip);
    }
    for (size_t i = 1; i < poses.size(); ++i) {
        auto segment_pose = math::changeFrame(base_pose, converters::toTransform(poses[i - 1]),
                                              math::Transform::Identity());
        view::ViewerInstance::instance().get()->drawStampedPose(
            segment_pose,
            math::changeFrame(base_pose, converters::toTransform(poses[i - 1]),
                              math::Transform::Identity()),
            chain.getSegment(i).getName() + "_k_");
        displayJoint(chain.getSegment(i), segment_pose, display_tip);
    }
}

} // namespace utils
} // namespace kdl
} // namespace kin
} // namespace move4d

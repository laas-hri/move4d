//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_MDL_DEGREEOFFREEDOMINTERFACE_H
#define MOVE4D_MDL_DEGREEOFFREEDOMINTERFACE_H

#include "move4d/common.h"
#include "move4d/mdl/DegreeOfFreedomInfo.h"

namespace move4d
{

namespace mdl
{

/**
 * Represents one degree of freedom, that is one scalar value with its bounds
 */
class DegreeOfFreedomInterface : virtual public DegreeOfFreedomInfo
{
  public:
    virtual move4d::Real get() const = 0;

    virtual void set(move4d::Real value) = 0;
};

} // namespace mdl

} // namespace move4d
#endif

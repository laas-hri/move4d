//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_PLAN_OMPL_CONVERTERS_PLANNERFACTORY_H
#define MOVE4D_PLAN_OMPL_CONVERTERS_PLANNERFACTORY_H

/// \file

#include "move4d/Factory.h"
#include <memory>

namespace ompl
{
namespace base
{
class SpaceInformation;
using SpaceInformationPtr = std::shared_ptr<SpaceInformation>;
class Planner;
using PlannerPtr = std::shared_ptr<Planner>;
} // namespace base
} // namespace ompl

namespace move4d
{
namespace plan
{
namespace ompl
{
namespace convert
{

/**
  Available planner types:

   - RRT
   - RRTConnect
   - LazyRRT
   - TRRT
   - EST
   - SBL
   - KPIECE
   - BKPIECE
   - LBKPIECE
   - RRTstar
   - PRM
   - PRMstar
   - FMT
   - BFMT
   - PDST
   - STRIDE
   - BiTRRT
   - LBTRRT
   - BiEST
   - ProjEST
   - LazyPRM
   - LazyPRMstar
   - SPARS
   - SPARStwo

    available OMPL planner types can be querried with Factory::getTypes():
    \code
    PlannerFactory::instance()->getTypes()
    \endcode
 */
using PlannerFactory =
    ::move4d::SimpleFactory<::ompl::base::PlannerPtr(const ::ompl::base::SpaceInformationPtr &)>;

} // namespace convert
} // namespace ompl
} // namespace plan
} // namespace move4d

#endif // MOVE4D_PLAN_OMPL_CONVERTERS_PLANNERFACTORY_H

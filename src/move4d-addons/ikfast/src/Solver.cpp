//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#include "Solver.h"
#include "ikfast.h"

#include <dlfcn.h>
#include <move4d/mdl/DegreeOfFreedomInfo.h>
#include <move4d/mdl/SceneObject.h>
#include <move4d/utils/random.h>
#include <rl/mdl/Dynamic.h>

namespace move4d
{
namespace kin
{
namespace ikfast
{

INIT_MOVE4D_STATIC_LOGGER(Solver, "move4d.kin.ikfast.solver");
INIT_MOVE4D_STATIC_LOGGER(IkFastSolver, "move4d.kin.ikfast.ikfastsolver");

FACTORY_REGISTER_FACTORY(KinematicSolverFactory, "ikfast",
                         []() { return std::make_shared<Solver>(); });

bool Solver::computeInverseKinematic(const mdl::Chain &chain, const math::Transform &base_pose,
                                     const math::Transform &end_pose, math::Vector &x_in_out)
{
    IkFastSolverPtr solver = IkFastSolverFactory::instance().create(
        (chain.ikfast.empty() ? chain.robot_class + "_" + chain.name : chain.ikfast));
    return solver->computeInverseKinematic(chain, base_pose, end_pose, x_in_out);
}

void Solver::computeInverseKinematic(const mdl::Chain &chain, const math::Transform &base_pose,
                                     const math::Transform &end_pose, const math::Vector &x_in,
                                     std::vector<std::pair<Real, math::Vector>> &solutions)
{
    IkFastSolverPtr solver =
        IkFastSolverFactory::instance().create(chain.robot_class + "_" + chain.name);
    solver->computeInverseKinematic(chain, base_pose, end_pose, x_in, solutions);
}

void Solver::computeInverseKinematic(const mdl::SceneObject &object, const mdl::Chain &chain,
                                     const math::Transform &pose, const mdl::State &state,
                                     const math::Vector &x,
                                     std::vector<std::pair<Real, math::Vector>> &solutions)
{
    IkFastSolverPtr solver = IkFastSolverFactory::instance().create(
        (chain.ikfast.empty() ? chain.robot_class + "_" + chain.name : chain.ikfast));
    solver->computeInverseKinematic(object, chain, pose, state, x, solutions);
}

bool Solver::computeForwardKinematic(const mdl::Chain &chain, const math::Transform &base_pose,
                                     math::Transform &end_pose_out, const math::Vector &x_in)
{

    IkFastSolverPtr solver = IkFastSolverFactory::instance().create(
        (chain.ikfast.empty() ? chain.robot_class + "_" + chain.name : chain.ikfast));
    return solver->computeForwardKinematic(chain, base_pose, end_pose_out, x_in);
}

INIT_MOVE4D_STATIC_LOGGER(RedundantSolver, "move4d.kin.ikfast.solver.redundant");

FACTORY_REGISTER_FACTORY(KinematicSolverFactory, "ikfast_redundant",
                         []() { return std::make_shared<RedundantSolver>(); });

bool RedundantSolver::computeInverseKinematic(const mdl::SceneObject &object,
                                              const mdl::Chain &chain, const math::Transform &pose,
                                              const mdl::State &state, math::Vector &x_in_out)
{
    bool ok;
    size_t count{};
    IkFastSolverPtr solver = IkFastSolverFactory::instance().create(
        (chain.ikfast.empty() ? chain.robot_class + "_" + chain.name : chain.ikfast));
    do {

        for (size_t i = 0; i < solver->getNumFreeParameters(); ++i) {
            auto dof = object.getDegreeOfFreedomInfo(
                chain.bijection[solver->getFreeParameters()[i]], mdl::StateType::Position);
            x_in_out[solver->getFreeParameters()[i]] = dof.shootGaussian(
                move4d::random::instance().uniform(),
                state(chain.bijection[solver->getFreeParameters()[i]], mdl::StateType::Position),
                dof.getMaximum() - dof.getMinimum());
        }
        ok = Solver::computeInverseKinematic(object, chain, pose, state, x_in_out);
    } while (M4D_WHILE_CONDITION(!ok && count++ < 100));
    // TODO(Jules): max iter number is magic variable
    return ok;
}

bool IkFastSolver::computeForwardKinematic(const mdl::Chain &chain,
                                           const math::Transform &base_pose,
                                           math::Transform &end_pose_out, const math::Vector &x_in)
{
    math::Transform local_pose;

    double eepos[3], eerot[9];

    computeForwardKinematic(eepos, eerot, x_in);

    for (uint i = 0; i < 3; ++i) {
        local_pose.translation()[i] = eepos[i];
    }
    for (uint k = 0; k < 9; ++k) {
        // assume same aligment
        // TODO(Jules): check aligment is same
        // local_pose.linear().data()[k] = eerot[k];
    }
    local_pose(0, 0) = eerot[0];
    local_pose(0, 1) = eerot[1];
    local_pose(0, 2) = eerot[2];
    local_pose(1, 0) = eerot[3];
    local_pose(1, 1) = eerot[4];
    local_pose(1, 2) = eerot[5];
    local_pose(2, 0) = eerot[6];
    local_pose(2, 1) = eerot[7];
    local_pose(2, 2) = eerot[8];

    end_pose_out = math::changeFrame(base_pose, local_pose, math::Transform::Identity());
    return true;
}

void move4d::kin::ikfast::IkFastSolver::computeInverseKinematic(
    const mdl::SceneObject &object, const mdl::Chain &chain, const math::Transform &pose,
    const mdl::State &state, const math::Vector &x,
    std::vector<std::pair<Real, math::Vector>> &solutions)
{
    object.m_kinematic->setPosition(state(mdl::StateType::Position));
    object.m_kinematic->forwardPosition();
    const math::Transform base_pose = chain.rootFrame->x.transform();

    M4D_TRACE("input state = " << state(mdl::StateType::Position).transpose());

    computeInverseKinematic(chain, base_pose, pose, x, solutions);
    M4D_TRACE("ikfast found " << solutions.size() << " solutions");
}

void IkFastSolver::computeInverseKinematic(const mdl::Chain &chain,
                                           const math::Transform &base_pose,
                                           const math::Transform &end_pose, const math::Vector &x,
                                           std::vector<std::pair<Real, math::Vector>> &solutions)
{
    math::Transform local_pose =
        math::changeFrame(math::Transform::Identity(), end_pose, base_pose);

    double eepos[3], eerot[9], pfree[getNumFreeParameters()];
    for (uint i = 0; i < 3; ++i) {
        eepos[i] = local_pose.translation()[i];
    }
    eerot[0] = local_pose(0, 0);
    eerot[1] = local_pose(0, 1);
    eerot[2] = local_pose(0, 2);
    eerot[3] = local_pose(1, 0);
    eerot[4] = local_pose(1, 1);
    eerot[5] = local_pose(1, 2);
    eerot[6] = local_pose(2, 0);
    eerot[7] = local_pose(2, 1);
    eerot[8] = local_pose(2, 2);

    M4D_TRACE("base_pose = \n" << base_pose.matrix());
    M4D_TRACE("ikfast ee local pose =\n" << local_pose.matrix());

    for (size_t i = 0; i < getNumFreeParameters(); ++i) {
        pfree[i] = x[getFreeParameters()[i]];
    }

    computeInverseKinematic(eepos, eerot, x, gsl::span<Real>(pfree, getNumFreeParameters()),
                            solutions);
    M4D_TRACE("ikfast found " << solutions.size() << " solutions");
}

bool IkFastSolver::computeInverseKinematic(const mdl::Chain &chain,
                                           const math::Transform &base_pose,
                                           const math::Transform &end_pose, math::Vector &x_in_out)
{
    std::vector<std::pair<Real, math::Vector>> solutions;
    computeInverseKinematic(chain, base_pose, end_pose, x_in_out, solutions);
    if (solutions.size()) {
        // TODO(Jules): select the proper IkFast Solution
        x_in_out = solutions.front().second;
        return true;
    }
    return false;
}

} // namespace ikfast
} // namespace kin
} // namespace move4d

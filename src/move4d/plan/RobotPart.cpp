//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#include "RobotPart.h"
#include "move4d/mdl/SceneObject.h"

namespace move4d
{
namespace plan
{

RobotPart::RobotPart(const std::string &name, mdl::SceneObject *object)
    : CompoundSearchSpace(name), m_robotId(object->getId())
{
}

void RobotPartLists::add(RobotPart part) { m_list[part.getRobotId()][part.getName()] = part; }

const RobotPart &RobotPartLists::get(const Identifier &robotId, const std::string &partName) const
{
    auto search1 = m_list.find(robotId);
    if (search1 != m_list.end()) {
        auto search2 = search1->second.find(partName);
        if (search2 != search1->second.end()) {
            return search2->second;
        }
    }
    throw std::out_of_range("no part named " + partName + " for robot " + robotId);
}

std::vector<std::string> RobotPartLists::get(const Identifier &robotId) const
{
    std::vector<std::string> names;
    auto search = m_list.find(robotId);
    if (search == m_list.end()) {
        return names;
    }

    names.reserve(search->second.size());
    for (const auto &p : search->second) {
        names.push_back(p.first);
    }
    return names;
}

} // namespace plan
} // namespace move4d

//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#include "move4d/plan/ompl/goals/GoalList.h"
#include "move4d/utils/random.h"
#include <numeric>

namespace move4d
{
namespace plan
{
namespace ompl
{

double GoalList::distanceGoal(const ::ompl::base::State *st) const
{
    double min_dist = std::numeric_limits<double>::max();
    for (auto &g : _goals) {
        min_dist = std::min(min_dist, g->distanceGoal(st));
    }
    return min_dist;
}

void GoalList::sampleGoal(::ompl::base::State *st) const
{
    float total = 0;
    for (auto &g : _goals) {
        total += g->maxSampleCount();
    }
    float dice = move4d::random::instance().uniform(0., total);
    float counter = 0;
    for (auto &g : _goals) {
        counter += g->maxSampleCount();
        if (dice < counter) {
            g->sampleGoal(st);
            return;
        }
    }
}

unsigned int GoalList::maxSampleCount() const
{
    unsigned int count = 0;
    for (auto &g : _goals) {
        if (g->maxSampleCount() == UINT_MAX)
            return UINT_MAX;
        unsigned int c = count + g->maxSampleCount();
        if (c < count) // overflow
            return UINT_MAX;
        count = c;
    }
    return count;
}

} // namespace ompl
} // namespace plan
} // namespace move4d

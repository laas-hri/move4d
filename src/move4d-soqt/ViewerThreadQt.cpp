//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#include "move4d-soqt/ViewerThreadQt.h"
#include "move4d-soqt/ViewerSoQt.h"
#include "move4d/motion/GeometricPath.h"
#include "move4d/plan/SearchSpace.h"
#include "move4d/utils/config/MagicConstants.h"
#include <thread>

#include <QMetaType>

#include "move4d/mdl/Scene.h"

Q_DECLARE_METATYPE(move4d::mdl::Scene);
Q_DECLARE_METATYPE(move4d::mdl::WorldPositions);
Q_DECLARE_METATYPE(move4d::mdl::WorldState);
Q_DECLARE_METATYPE(move4d::motion::GeometricPath);
Q_DECLARE_METATYPE(rl::math::Transform);
Q_DECLARE_METATYPE(rl::math::Vector);
Q_DECLARE_METATYPE(rl::plan::VectorList);
Q_DECLARE_METATYPE(std::string);

namespace move4d
{
namespace view
{

ViewerThreadQt::ViewerThreadQt(QObject *parent) : QObject(parent) {}

bool ViewerThreadQt::connect(ViewerThreadQt *emiter, QObject *receiver)
{
    qRegisterMetaType<std::string>();
    qRegisterMetaType<move4d::mdl::WorldState>();
    qRegisterMetaType<move4d::mdl::WorldPositions>();
    qRegisterMetaType<move4d::mdl::Scene>();
    qRegisterMetaType<move4d::motion::GeometricPath>();
    qRegisterMetaType<rl::math::Vector>();
    qRegisterMetaType<rl::math::Transform>();
    qRegisterMetaType<rl::math::Transform>();
    qRegisterMetaType<rl::plan::VectorList>();
    bool ok = true;
    ok =
        ok && QObject::connect(emiter, SIGNAL(updateRequested(move4d::mdl::WorldState)), receiver,
                               SLOT(update(const move4d::mdl::WorldState &)), Qt::QueuedConnection);
    ok = ok &&
         QObject::connect(emiter, SIGNAL(updateRequested(move4d::mdl::WorldPositions)), receiver,
                          SLOT(update(const move4d::mdl::WorldPositions &)), Qt::QueuedConnection);
    ok = ok &&
         QObject::connect(emiter, SIGNAL(drawConfigurationRequested(rl::math::Vector)), receiver,
                          SLOT(drawConfiguration(rl::math::Vector)), Qt::QueuedConnection);
    ok = ok && QObject::connect(emiter,
                                SIGNAL(drawConfigurationRequested(unsigned int, rl::math::Vector)),
                                receiver, SLOT(drawConfiguration(unsigned int, rl::math::Vector)),
                                Qt::QueuedConnection);
    ok = ok && QObject::connect(emiter, SIGNAL(drawPoseRequested(rl::math::Transform)), receiver,
                                SLOT(drawPose(rl::math::Transform)), Qt::QueuedConnection);
    ok =
        ok &&
        QObject::connect(
            emiter,
            SIGNAL(drawStampedPoseRequested(rl::math::Transform, rl::math::Transform, std::string)),
            receiver, SLOT(drawStampedPose(rl::math::Transform, rl::math::Transform, std::string)),
            Qt::QueuedConnection);
    ok = ok && QObject::connect(emiter, SIGNAL(resetPosesRequested()), receiver, SLOT(resetPoses()),
                                Qt::QueuedConnection);
    ok = ok && QObject::connect(
                   emiter,
                   SIGNAL(drawConfigurationEdgeRequested(rl::math::Vector, rl::math::Vector, bool)),
                   receiver, SLOT(drawConfigurationEdge(rl::math::Vector, rl::math::Vector, bool)),
                   Qt::QueuedConnection);
    ok = ok && QObject::connect(
                   emiter, SIGNAL(drawConfigurationPathRequested(rl::plan::VectorList)), receiver,
                   SLOT(drawConfigurationPath(rl::plan::VectorList)), Qt::QueuedConnection);
    ok = ok &&
         QObject::connect(emiter, SIGNAL(drawConfigurationVertexRequested(rl::math::Vector, bool)),
                          receiver, SLOT(drawConfigurationVertex(rl::math::Vector, bool)),
                          Qt::QueuedConnection);
    ok = ok && QObject::connect(
                   emiter, SIGNAL(drawLineRequested(rl::math::Vector, rl::math::Vector)), receiver,
                   SLOT(drawLine(rl::math::Vector, rl::math::Vector)), Qt::QueuedConnection);
    ok = ok && QObject::connect(emiter, SIGNAL(drawPointRequested(rl::math::Vector)), receiver,
                                SLOT(drawPoint(rl::math::Vector)), Qt::QueuedConnection);
    ok = ok &&
         QObject::connect(emiter, SIGNAL(drawSphereRequested(rl::math::Vector, double)), receiver,
                          SLOT(drawSphere(rl::math::Vector, double)), Qt::QueuedConnection);
    ok = ok && QObject::connect(emiter, SIGNAL(drawWorkRequested(rl::math::Transform)), receiver,
                                SLOT(drawWork(rl::math::Transform)), Qt::QueuedConnection);
    ok = ok &&
         QObject::connect(emiter, SIGNAL(drawWorkEdgeRequested(rl::math::Vector, rl::math::Vector)),
                          receiver, SLOT(drawWorkEdge(rl::math::Vector, rl::math::Vector)),
                          Qt::QueuedConnection);
    ok = ok &&
         QObject::connect(emiter, SIGNAL(drawWorkPathRequested(rl::plan::VectorList)), receiver,
                          SLOT(drawWorkPath(rl::plan::VectorList)), Qt::QueuedConnection);
    ok = ok && QObject::connect(emiter, SIGNAL(drawWorkVertexRequested(rl::math::Vector)), receiver,
                                SLOT(drawWorkVertex(rl::math::Vector)), Qt::QueuedConnection);
    ok = ok && QObject::connect(emiter, SIGNAL(resetRequested()), receiver, SLOT(reset()),
                                Qt::QueuedConnection);
    ok = ok && QObject::connect(emiter, SIGNAL(resetEdgesRequested()), receiver, SLOT(resetEdges()),
                                Qt::QueuedConnection);
    ok = ok && QObject::connect(emiter, SIGNAL(resetLinesRequested()), receiver, SLOT(resetLines()),
                                Qt::QueuedConnection);
    ok = ok && QObject::connect(emiter, SIGNAL(resetPointsRequested()), receiver,
                                SLOT(resetPoints()), Qt::QueuedConnection);
    ok = ok && QObject::connect(emiter, SIGNAL(resetSpheresRequested()), receiver,
                                SLOT(resetSpheres()), Qt::QueuedConnection);
    ok = ok && QObject::connect(emiter, SIGNAL(resetVerticesRequested()), receiver,
                                SLOT(resetVertices()), Qt::QueuedConnection);
    ok = ok && QObject::connect(emiter, SIGNAL(showMessageRequested(std::string)), receiver,
                                SLOT(showMessage(std::string)), Qt::QueuedConnection);
    ok = ok && QObject::connect(emiter, SIGNAL(saveImageRequested(std::string)), receiver,
                                SLOT(saveImage(std::string)), Qt::QueuedConnection);
    ok = ok && QObject::connect(emiter, SIGNAL(viewAllRequested()), receiver, SLOT(viewAll()),
                                Qt::QueuedConnection);
    ok =
        ok && QObject::connect(
                  emiter,
                  SIGNAL(playPathRequested(move4d::mdl::WorldState, move4d::motion::GeometricPath)),
                  receiver, SLOT(playPath(move4d::mdl::WorldState, move4d::motion::GeometricPath)),
                  Qt::QueuedConnection);

    ok = ok && QObject::connect(emiter, SIGNAL(toggleRecordingRequested(bool)), receiver,
                                SLOT(toggleRecording(bool)), Qt::QueuedConnection);

    return ok;
}

void ViewerThreadQt::update(const mdl::WorldPositions &positions)
{
    emit(updateRequested(positions));
}

void ViewerThreadQt::update(const mdl::WorldState &positions) { emit(updateRequested(positions)); }

void ViewerThreadQt::drawConfiguration(size_t robot_index, const math::Vector &q)
{
    emit(drawConfigurationRequested(robot_index, q));
}

void ViewerThreadQt::drawState(const mdl::State & /*state*/)
{
    // TODO(Jules)
    throw std::runtime_error("not implemented");
}

void ViewerThreadQt::drawPose(const math::Transform &pose) { emit drawPoseRequested(pose); }

void ViewerThreadQt::drawStampedPose(const rl::math::Transform &pose,
                                     const rl::math::Transform &parent, const std::string &name)
{
    emit drawStampedPoseRequested(pose, parent, name);
}

void ViewerThreadQt::resetPoses() { emit(resetPosesRequested()); }

ThreadedViewer::ThreadedViewer(QWidget *parent, const Qt::WindowFlags &f)
    : ViewerThreadQt(parent), m_viewer(new ViewerSoQt(parent, f))
{
    if (!ViewerThreadQt::connect(this, m_viewer)) {
        throw std::runtime_error("failed to connect viewer interface to asynchronous viewer");
    }
}

ViewerSoQt *ThreadedViewer::getViewer() const { return m_viewer; }

void ViewerThreadQt::drawConfiguration(const rl::math::Vector & /*q*/)
{ // TODO(Jules): missing implementation
}

void ViewerThreadQt::drawConfigurationEdge(const rl::math::Vector & /*q0*/,
                                           const rl::math::Vector & /*q1*/, const bool & /*free*/)
{ // TODO(Jules): missing implementation
}

void ViewerThreadQt::drawConfigurationPath(const rl::plan::VectorList & /*path*/)
{ // TODO(Jules): missing implementation
}

void ViewerThreadQt::drawConfigurationVertex(const rl::math::Vector & /*q*/, const bool & /*free*/)
{ // TODO(Jules): missing implementation
}

void ViewerThreadQt::drawLine(const rl::math::Vector & /*xyz0*/, const rl::math::Vector & /*xyz1*/)
{ // TODO(Jules): missing implementation
}

void ViewerThreadQt::drawPoint(const rl::math::Vector & /*xyz*/)
{ // TODO(Jules): missing implementation
}

void ViewerThreadQt::drawSphere(const rl::math::Vector & /*center*/,
                                const rl::math::Real & /*radius*/)
{ // TODO(Jules): missing implementation
}

void ViewerThreadQt::drawWork(const rl::math::Transform & /*t*/)
{ // TODO(Jules): missing implementation
}

void ViewerThreadQt::drawWorkEdge(const rl::math::Vector & /*q0*/, const rl::math::Vector & /*q1*/)
{ // TODO(Jules): missing implementation
}

void ViewerThreadQt::drawWorkPath(const rl::plan::VectorList & /*path*/)
{ // TODO(Jules): missing implementation
}

void ViewerThreadQt::drawWorkVertex(const rl::math::Vector & /*q*/)
{ // TODO(Jules): missing implementation
}

void ViewerThreadQt::reset()
{ // TODO(Jules): missing implementation
}

void ViewerThreadQt::resetEdges()
{ // TODO(Jules): missing implementation
}

void ViewerThreadQt::resetLines()
{ // TODO(Jules): missing implementation
}

void ViewerThreadQt::resetPoints()
{ // TODO(Jules): missing implementation
}

void ViewerThreadQt::resetSpheres()
{ // TODO(Jules): missing implementation
}

void ViewerThreadQt::resetVertices()
{ // TODO(Jules): missing implementation
}

void ViewerThreadQt::showMessage(const std::string & /*message*/)
{ // TODO(Jules): missing implementation
}

void ViewerThreadQt::saveImage(const std::string &path) { emit(saveImageRequested(path)); }

void ViewerThreadQt::viewAll() { emit(viewAllRequested()); }

void ViewerThreadQt::playPath(const mdl::WorldState &ws_init, const motion::GeometricPath &path)
{
    mdl::WorldState ws(ws_init);
    Real fps = move4d::magic::view::PlayMotionDefaultFPS;
    std::chrono::steady_clock::time_point next_tp;
    for (auto &x : path.values()) {
        if (isStopping())
            break;
        auto next_tp = std::chrono::steady_clock::now() +
                       std::chrono::milliseconds(size_t(std::round(1000. / fps)));
        path.getSearchSpace()->setIn(x, ws);
        update(ws);
        std::this_thread::sleep_until(next_tp);
    }
}

void ViewerThreadQt::toggleRecording(bool record) { emit(toggleRecordingRequested(record)); }

} // namespace view
} // namespace move4d

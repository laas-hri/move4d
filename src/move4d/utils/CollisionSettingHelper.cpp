//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#include "CollisionSettingHelper.h"
#include "move4d/mdl/CollisionManager.h"
#include "move4d/mdl/Kinematic.h"
#include "move4d/mdl/Model.h"
#include "move4d/mdl/Scene.h"
#include "move4d/mdl/SceneObject.h"
#include "move4d/utils/random.h"
#include <rl/mdl/Body.h>
#include <rl/plan/SimpleModel.h>

namespace move4d
{
namespace utils
{

CollisionSettingHelper::CollisionSettingHelper(mdl::CollisionManager *collisionManager)
    : m_collisionManager(collisionManager)
{
}

void CollisionSettingHelper::detectRercurringCollisions(size_t tests)
{
    auto &mgr = m_collisionManager;
    std::map<mdl::CollisionPair, size_t> counts;
    for (size_t i = 0; i < tests; ++i) {
        for (const auto &idrob : *m_collisionManager->getScene()) {
            auto robot = idrob.second;
            auto mdl = robot->model();
            move4d::math::Vector rand(mdl->getDof());
            auto &generator = move4d::random::instance();
            for (uint i = 0; i < mdl->getDof(); ++i) {
                rand(i) = generator.uniform();
            }
            move4d::math::Vector pos = mdl->generatePositionUniform(rand);
            for (long i = 0; i < pos.size(); ++i) {
                pos[i] = (pos[i] != std::numeric_limits<move4d::Real>::infinity() ? pos[i] : 0);
            }
            auto state = mdl::State(robot.get());
            state(mdl::StateType::Position) = pos;
            robot->applyState(state);
        }
        auto list = mgr->collisionList();
        for (const auto &p : list) {
            ++counts[p];
        }
    }
    std::vector<std::pair<size_t, mdl::CollisionPair>> sorted;
    sorted.reserve(counts.size());
    for (auto p : counts) {
        sorted.emplace_back(p.second, p.first);
    }
    std::sort(sorted.begin(), sorted.end());

    for (const auto &x : sorted) {
        if (x.first > tests / 2) {
            auto p = x.second;
            std::cout << m_collisionManager->getScene()->getObject(p.first().sceneObj)->getName()
                      << "."
                      << m_collisionManager->getScene()
                             ->getObject(p.first().sceneObj)
                             ->model()
                             ->mdl->getBody(p.first().bodyIndex)
                             ->getName()
                      << "  X  "
                      << m_collisionManager->getScene()->getObject(p.second().sceneObj)->getName()
                      << "."
                      << m_collisionManager->getScene()
                             ->getObject(p.second().sceneObj)
                             ->model()
                             ->mdl->getBody(p.second().bodyIndex)
                             ->getName()
                      << "\t\t: " << float(x.first) / tests * 100.f << "%"
                      << "\n";
        }
    }
}

} // namespace utils
} // namespace move4d

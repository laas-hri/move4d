#ifndef MOVE4D_FILE_M4DF_M4DFDEFINITION_H
#define MOVE4D_FILE_M4DF_M4DFDEFINITION_H

#include "move4d/common.h"
#include "move4d/mdl/Types.h"
#include "move4d/plan/Types.h"
#include "move4d/utils/containers/ptr_wrapper.hpp"

namespace move4d
{
namespace view
{
class Viewer;
using ViewerPtr = std::shared_ptr<Viewer>;
} // namespace view
namespace file
{
namespace m4df
{

class M4DFDefinition
{

    MOVE4D_STATIC_LOGGER;

  public:
    explicit M4DFDefinition(mdl::ScenePtr scene, view::Viewer *viewer = nullptr);
    virtual ~M4DFDefinition();
    const mdl::ScenePtr &getScene() const { return _scene; }
    view::Viewer *getViewer() const { return _viewer; }
    ptr_wrapper<plan::SearchSpace> searchSpace(const std::string &name) const;

    const std::map<std::string, ptr_wrapper<plan::KinematicGraphSearchSpaceBuilder>> &
    getKinematicGraphBuilders() const;

    M4DFDefinition &operator+=(const M4DFDefinition &other);

  private:
    friend class M4DFParser;
    void addSearchSpace(const std::string &name, std::unique_ptr<plan::SearchSpace> &&space);
    std::map<std::string, ptr_wrapper<plan::SearchSpace>> _search_spaces;
    std::map<std::string, ptr_wrapper<plan::KinematicGraphSearchSpaceBuilder>>
        _kinematic_graph_builders;
    mdl::ScenePtr _scene;
    view::Viewer *_viewer;
};

} // namespace m4df
} // namespace file
} // namespace move4d

#endif // MOVE4D_FILE_M4DF_M4DFDEFINITION_H

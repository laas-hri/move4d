//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//

#include "move4d/view/Viewer.h"
#include "Viewer.h"

#include "move4d/mdl/Scene.h"
#include "move4d/mdl/SceneObject.h"
#include "move4d/mdl/State.h"
#include "move4d/mdl/WorldPositions.h"
#include "move4d/mdl/WorldState.h"
#include "rl/plan/Model.h"
#include "rl/sg/Scene.h"
#include <utility>

namespace move4d
{

namespace view
{

const std::shared_ptr<rl::plan::Model> &Viewer::create()
{
    m_models.emplace_back(std::make_shared<rl::plan::Model>());
    const auto &model = m_models.back();
    model->model = m_scene->create();
    model->kin = nullptr;
    model->mdl = nullptr;
    return model;
}

void Viewer::update(const move4d::mdl::WorldPositions &positions)
{
    // TODO(Jules)
}

void Viewer::update(const move4d::mdl::WorldState &ws)
{
    for (const auto &x : ws) {
        ::std::size_t index = x.second.getObject()->getIndex();
        m_models[index]->setPosition(x.second(move4d::mdl::StateType::Type::Position));
        m_models[index]->updateFrames();
    }
    this->paint();
}

void Viewer::drawConfiguration(std::size_t robot_index, const move4d::math::Vector &q)
{
    auto model = m_models[robot_index];
    model->setPosition(q);
    model->updateFrames();
    this->paint();
}

void Viewer::drawState(const move4d::mdl::State &state)
{
    auto model = m_models[state.getObject()->getIndex()];
    model->setPosition(state(move4d::mdl::StateType::Type::Position));
    model->updateFrames();
    this->paint();
}

void Viewer::setScene(std::shared_ptr<rl::sg::Scene> scene) { m_scene = std::move(scene); }

void Viewer::add(std::shared_ptr<rl::plan::Model> model)
{
    m_models.emplace_back(std::move(model));
}

} // namespace view

} // namespace move4d

void move4d::view::Viewer::drawConfiguration(const rl::math::Vector &q) { drawConfiguration(0, q); }

void move4d::view::Viewer::drawConfigurationEdge(const rl::math::Vector &q0,
                                                 const rl::math::Vector &q1, const bool &free)
{
    std::cout << "edge " << q0.transpose() << " -- " << q1.transpose() << " : " << free
              << std::endl;
}

void move4d::view::Viewer::drawConfigurationPath(const rl::plan::VectorList &path) {}

void move4d::view::Viewer::drawConfigurationVertex(const rl::math::Vector &q, const bool &free) {}

void move4d::view::Viewer::drawLine(const rl::math::Vector &xyz0, const rl::math::Vector &xyz1) {}

void move4d::view::Viewer::drawPoint(const rl::math::Vector &xyz) {}

void move4d::view::Viewer::drawSphere(const rl::math::Vector &center, const rl::math::Real &radius)
{
}

void move4d::view::Viewer::drawWork(const rl::math::Transform &t) {}

void move4d::view::Viewer::drawWorkEdge(const rl::math::Vector &q0, const rl::math::Vector &q1) {}

void move4d::view::Viewer::drawWorkPath(const rl::plan::VectorList &path) {}

void move4d::view::Viewer::drawWorkVertex(const rl::math::Vector &q) {}

void move4d::view::Viewer::reset() {}

void move4d::view::Viewer::resetEdges() {}

void move4d::view::Viewer::resetLines() {}

void move4d::view::Viewer::resetPoints() {}

void move4d::view::Viewer::resetSpheres() {}

void move4d::view::Viewer::resetVertices() {}

void move4d::view::Viewer::showMessage(const std::string &message) {}

//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_PLAN_OMPL_KINEMATICGRAPHGOALSAMPLER_H
#define MOVE4D_PLAN_OMPL_KINEMATICGRAPHGOALSAMPLER_H

#include "move4d/plan/Types.h"
#include "move4d/plan/ompl/Types.h"
#include <ompl/base/goals/GoalLazySamples.h>

namespace move4d
{
namespace plan
{
namespace ompl
{

class KinematicGraphGoalSampler : public ::ompl::base::GoalSampleableRegion
{
    MOVE4D_STATIC_LOGGER;

  public:
    KinematicGraphGoalSampler() = delete;
    KinematicGraphGoalSampler(const GoalSampler &goal_sampler,
                              const ::ompl::base::SpaceInformationPtr &si);

    double distanceGoal(const ::ompl::base::State *st) const override;
    void sampleGoal(::ompl::base::State *st) const override;
    unsigned int maxSampleCount() const override;

  private:
    ::ompl::base::StateSamplerPtr _sampler;
};

} // namespace ompl
} // namespace plan
} // namespace move4d

#endif // MOVE4D_PLAN_OMPL_KINEMATICGRAPHGOALSAMPLER_H

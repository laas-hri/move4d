//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#include <rl/math/Matrix.h> // must be first to load eigen quaternion base plugins

#include "move4d/plan/RobotSpace.h"
#include "move4d/plan/SO3Space.h"

#include "move4d/mdl/DegreeOfFreedomInfo.h"
#include "move4d/mdl/Movable.h"
#include "move4d/mdl/State.h"
#include "move4d/mdl/WorldState.h"
#include "move4d/utils/random.h"

namespace move4d
{
namespace plan
{

SO3Space::SO3Space(const mdl::Movable *robot, std::size_t firstQuaternionDof, std::string name)
    : SearchSpace(Type::SO3, std::move(name))
{
    initDegreesOfFreedom(robot, firstQuaternionDof);
}

SO3Space::SO3Space(const mdl::Movable *robot, std::size_t firstQuaternionDof,
                   const math::Quaternion &reference, Real tolerance, std::string name)
    : SearchSpace(Type::SO3, std::move(name)), _reference(reference), _tolerance(tolerance)
{
    initDegreesOfFreedom(robot, firstQuaternionDof);
}

void SO3Space::initDegreesOfFreedom(const mdl::Movable *robot, std::size_t firstQuaternionDof)
{
    for (unsigned char i = 0; i < 4; ++i) {
        m_degreesOfFreedom.push_back(
            robot->getDegreeOfFreedomInfo(i + firstQuaternionDof, mdl::StateType::Position));
    }
}

Real SO3Space::getTolerance() const { return _tolerance; }

const math::Quaternion &SO3Space::getReference() const { return _reference; }

void move4d::plan::SO3Space::shootUniform(mdl::State &state, std::size_t firstQuaternionDof)
{
    auto &generator = ::move4d::random::instance();
    state(mdl::StateType::Position).block(firstQuaternionDof, 0, 4, 1) =
        math::Quaternion::Random(
            math::Vector3{generator.uniform(), generator.uniform(), generator.uniform()})
            .coeffs();
}

void SO3Space::setBounds(math::Quaternion reference, Real tolerance)
{
    _reference = std::move(reference);
    _tolerance = tolerance;
}

bool SO3Space::isBounded() const { return _tolerance < M_PI; }

void SO3Space::shootUniform(mdl::WorldState &ws) const
{
    shootUniform(ws[m_degreesOfFreedom[0].getObjectId()], m_degreesOfFreedom[0].index());
}

SE3Space::SE3Space(const mdl::Movable *robot, std::size_t firstFloatingJointDof, std::string name)
    : SearchSpace(Type::SE3, std::move(name)), m_robot(robot)
{
    initDegreesOfFreedom(firstFloatingJointDof);
}

SE3Space::SE3Space(const mdl::Movable *robot, std::size_t firstFloatingJointDof,
                   const math::Quaternion &reference, Real tolerance, std::string name)
    : SearchSpace(Type::SE3, std::move(name)), m_robot(robot), _reference(reference),
      _tolerance(tolerance)
{
    initDegreesOfFreedom(firstFloatingJointDof);
}

void SE3Space::shootUniform(mdl::WorldState &ws) const
{
    auto &state = ws[m_degreesOfFreedom[0].getObjectId()];
    auto &generator = ::move4d::random::instance();
    for (size_t i = 0; i < 3; ++i) {
        auto dof = m_degreesOfFreedom[i];
        dof.setIn(state, dof.shootUniform(generator.uniform()));
    }
    SO3Space::shootUniform(state, m_degreesOfFreedom[3].index());
}

std::pair<RobotSpacePtr, SO3SpacePtr> SE3Space::split() const
{
    auto vec = std::make_shared<RobotSpace>(
        m_robot,
        std::vector<mdl::DegreeOfFreedomInfo>{getDegreesOfFreedom()[0], getDegreesOfFreedom()[1],
                                              getDegreesOfFreedom()[2]},
        getName() + "_vec");
    return {vec, std::make_shared<SO3Space>(m_robot, m_degreesOfFreedom[3].index(), _reference,
                                            _tolerance, getName() + "_so3")};
}

void SE3Space::setBounds(const math::Vector3 &min, const math::Vector3 &max)
{
    for (uint i = 0; i < 3; ++i) {
        m_degreesOfFreedom[i].setMinimum(min[i]);
        m_degreesOfFreedom[i].setMaximum(max[i]);
    }
}

void SE3Space::setBounds(math::Quaternion reference, Real tolerance)
{
    _reference = std::move(reference);
    _tolerance = tolerance;
}

void SE3Space::setIn(const math::Transform &t, mdl::WorldState &ws)
{
    auto &state = ws[m_degreesOfFreedom[0].getObjectId()];
    // translation:
    for (size_t i = 0; i < 3; ++i) {
        m_degreesOfFreedom[i].setIn(state, t.translation()[i]);
    }
    // rotation:
    const math::Quaternion q(t.rotation());
    for (size_t i = 0; i < 4; ++i) {
        m_degreesOfFreedom[i + 3].setIn(state, q.coeffs()[i]); // coeffs=(x,y,z,w)
    }
}

void SE3Space::initDegreesOfFreedom(std::size_t firstFloatingJointDof)
{
    for (unsigned char i = 0; i < 7; ++i) {
        m_degreesOfFreedom.push_back(
            m_robot->getDegreeOfFreedomInfo(i + firstFloatingJointDof, mdl::StateType::Position));
    }
}

SO2Space::SO2Space(const mdl::Movable *robot, mdl::DegreeOfFreedomInfo dof, std::string name)
    : SearchSpace(Type::SO2, std::move(name))
{
    m_degreesOfFreedom.push_back(std::move(dof));
}

SE2Space::SE2Space(const mdl::Movable *robot, std::size_t firstFloatingJointDof, SE2Type type,
                   std::string name)
    : SearchSpace(Type(type), std::move(name))
{
    // trans x:
    m_degreesOfFreedom.push_back(
        robot->getDegreeOfFreedomInfo(0 + firstFloatingJointDof, mdl::StateType::Position));
    // trans y:
    m_degreesOfFreedom.push_back(
        robot->getDegreeOfFreedomInfo(1 + firstFloatingJointDof, mdl::StateType::Position));
    // rot angle (w)
    m_degreesOfFreedom.push_back(mdl::RotationInQuaternionDoF(robot->getId(),
                                                              math::Vector3::UnitZ(),
                                                              firstFloatingJointDof + 3)
                                     .make_info());

    Ensures((m_type == Type::SE2 && type == SE2Type::Holonomic) ||
            (m_type == Type::Dubins && type == SE2Type::Dubins) ||
            (m_type == Type::ReedsShepp && type == SE2Type::ReedsShepp));
}

SE2Space::SE2Space(const mdl::Movable *robot, std::size_t dofTransX, std::size_t dofTransY,
                   std::size_t dofRotAngle, SE2Type type, std::string name)
    : SearchSpace(Type(type), std::move(name)), m_robot(robot)
{
    // Expects the rotation dof to be in a revolute joint (not a quaternion)
    Expects(
        std::abs(
            (robot->getDegreeOfFreedomInfo(dofRotAngle, mdl::StateType::Position).getMaximum() -
             robot->getDegreeOfFreedomInfo(dofRotAngle, mdl::StateType::Position).getMinimum()) -
            2 * M_PI) < 1e-5);
    Expects(robot->getDegreeOfFreedomInfo(dofRotAngle, mdl::StateType::Position).wrapAround());
    // trans x:
    m_degreesOfFreedom.push_back(
        robot->getDegreeOfFreedomInfo(dofTransX, mdl::StateType::Position));
    // trans y:
    m_degreesOfFreedom.push_back(
        robot->getDegreeOfFreedomInfo(dofTransY, mdl::StateType::Position));
    // rot angle (Rz)
    m_degreesOfFreedom.push_back(
        robot->getDegreeOfFreedomInfo(dofRotAngle, mdl::StateType::Position));

    Ensures((m_type == Type::SE2 && type == SE2Type::Holonomic) ||
            (m_type == Type::Dubins && type == SE2Type::Dubins) ||
            (m_type == Type::ReedsShepp && type == SE2Type::ReedsShepp));
}

void SE2Space::setBounds(const math::Vector2 &min, const math::Vector2 &max)
{
    for (uint i = 0; i < 2; ++i) {
        m_degreesOfFreedom[i].setMinimum(min[i]);
        m_degreesOfFreedom[i].setMaximum(max[i]);
    }
}

std::pair<RobotSpacePtr, SO2SpacePtr> SE2Space::split() const
{
    // TODO: this depends on if it is a freeflyer joint, or ...
    auto vec = std::make_shared<RobotSpace>(
        m_robot,
        std::vector<mdl::DegreeOfFreedomInfo>{m_degreesOfFreedom[0], m_degreesOfFreedom[1]},
        getName() + "_vec");
    return {vec, std::make_shared<SO2Space>(m_robot, m_degreesOfFreedom[2], getName() + "_so2")};
}
} // namespace plan
} // namespace move4d

clang-format pre-commit hook
============================

Installation
------------

Copy git-clang-format somewhere in your ```$PATH``` and make it executable

Copy (or add the content of) pre-commit to the project .git/hooks/pre-commit

Set some git config values:
```
git config clangFormat.binary  /path/to/clang-format-executable
git config clangFormat.style   file
```

You can make the first one ```--global```


Usage
-----

Now, when you run ```git commit```, it first checks that your staged changes respect the format specified in the .clang-format file. If not, the commit is aborted and you are prompted to run ```git clang-format```. Don't forget to stage those new changes before retrying commit (otherwise it will complain again, and tell you to stage your changes, or run ```git clang-format```)

//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef PATH_H
#define PATH_H

#include "move4d/plan/Types.h"

namespace move4d
{
namespace motion
{

/**
 * @brief Path abstract base class
 *
 */
class Path
{
  public:
    // default c-tor d-tor
    Path() = default; // TODO a path needs to know its space (interpolation methods...)
    Path(const Path &) = default;
    Path(Path &&) = default;
    Path &operator=(const Path &) = default;
    Path &operator=(Path &&) = default;
    virtual ~Path() = default;

    Path(plan::SearchSpacePtr searchSpace);

    const plan::SearchSpacePtr &getSearchSpace() const;

    /**
     * @brief check
     * @param init_ws the test needs to know it's context
     * @param collision_manager to perform collision tests
     * @return true if the state is valid
     *
     * \e init_ws values are used only if they are not in the searchSpace associated to this path.
     * i.e. it is not requiered that init_ws matches the begining of the path, hence a
     * reimplementation must not assume that init_ws is the begining of the path, and so must not
     * check init_ws for validity
     */
    virtual bool check(const mdl::WorldStatePtr &init_ws,
                       const mdl::CollisionManagerPtr &collision_manager) const = 0;

  private:
    plan::SearchSpacePtr m_searchSpace;
};

} // namespace motion
} // namespace move4d

#endif // PATH_H

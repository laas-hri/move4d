//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#include "move4d/mdl/kin/DefaultForwardKinematicSolver.h"
#include "move4d/mdl/SceneObject.h"
#include <rl/mdl/Dynamic.h>
#include <rl/mdl/Joint.h>

namespace move4d
{
namespace kin
{

FACTORY_REGISTER_FACTORY(KinematicSolverFactory, "default_forward",
                         []() { return std::make_shared<DefaultForwardKinematicSolver>(); });
bool DefaultForwardKinematicSolver::computeInverseKinematic(const mdl::SceneObject &object,
                                                            const mdl::Chain &chain,
                                                            const math::Transform &pose,
                                                            const mdl::State &state,
                                                            math::Vector &x_in_out)
{
    assert(false);
    abort();
}

bool DefaultForwardKinematicSolver::computeInverseKinematic(const mdl::Chain &chain,
                                                            const math::Transform &base_pose,
                                                            const math::Transform &end_pose,
                                                            math::Vector &x_in_out)
{
    assert(false);
    abort();
}

bool DefaultForwardKinematicSolver::computeForwardKinematic(const mdl::SceneObject &object,
                                                            const mdl::Chain &chain,
                                                            const mdl::State &state,
                                                            const math::Vector x_in,
                                                            math::Transform &pose_out)
{
    mdl::State s = state;
    for (size_t i = 0; i < chain.bijection.size(); ++i) {
        s(chain.bijection[i], mdl::StateType::Position) = x_in[i];
    }
    object.m_kinematic->setPosition(state(mdl::StateType::Position));
    object.m_kinematic->forwardPosition();
    pose_out = object.getLink(chain.tipName)->x.transform();
    return true;
}

bool DefaultForwardKinematicSolver::computeForwardKinematic(const mdl::Chain &chain,
                                                            const math::Transform &base_pose,
                                                            math::Transform &end_pose_out,
                                                            const math::Vector &x_in)
{

    ::rl::mdl::Frame *f = nullptr;
    for (size_t i = 0; i < chain.kinematic->getFrames(); ++i) {
        if (chain.kinematic->getFrame(i)->getName() == chain.tipName) {
            f = chain.kinematic->getFrame(i);
            break;
        }
    }
    if (!f) {
        assert(false);
        return false;
    }
    chain.kinematic->setPosition(x_in);
    chain.kinematic->forwardPosition();
    end_pose_out = f->x.transform();
    return true;
}

} // namespace kin
} // namespace move4d

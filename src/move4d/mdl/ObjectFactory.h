//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_MDL_OBJECTFACTORY_H
#define MOVE4D_MDL_OBJECTFACTORY_H

#include "move4d/Factory.h"
#include "move4d/common.h"
#include "move4d/mdl/Types.h"
#include <string>

namespace move4d
{

namespace mdl
{

using ObjectFactory =
    SimpleFactory<SceneObjectPtr(const Scene &, std::shared_ptr<Model>, std::shared_ptr<Kinematic>,
                                 const std::string &, const Identifier &, std::size_t)>;

} // namespace mdl

} // namespace move4d
#endif

//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_IO_READABLE_TIME_H
#define MOVE4D_IO_READABLE_TIME_H

#include <assert.h>
#include <chrono>
#include <ctime>
#include <string>

namespace move4d
{
namespace io
{
namespace time
{

std::string timeStampToHReadble(std::chrono::system_clock::time_point time);

std::string timestampHReadable()
{
    auto tnow = std::chrono::system_clock::now();
    assert(std::chrono::system_clock::period::den > 1.);
    return timeStampToHReadble(tnow);
}

std::string timeStampToHReadble(std::chrono::system_clock::time_point time)
{
    struct tm *dt;
    char buffer[30];
    std::time_t rawtime = std::chrono::system_clock::to_time_t(time);
    dt = localtime(&rawtime);
    strftime(buffer, sizeof(buffer), "%Y-%m-%d-%H:%M:%S", dt);
    auto time_in_sec = std::chrono::time_point_cast<std::chrono::seconds>(time);
    auto floored_time =
        std::chrono::time_point_cast<std::chrono::system_clock::duration>(time_in_sec);
    auto remainder_ms =
        std::chrono::duration_cast<std::chrono::milliseconds>(time - floored_time).count();
    char remainder_str[4];
    assert(remainder_ms < 1000);
    snprintf(remainder_str, 4, "%03lu", remainder_ms);
    return std::string(buffer) + "." + remainder_str;
}

} // namespace time

} // namespace io

} // namespace move4d

#endif // MOVE4D_IO_READABLE_TIME_H

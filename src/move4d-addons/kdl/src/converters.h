//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef _ADDONS_KDL_SRC_CONVERTERS_H
#define _ADDONS_KDL_SRC_CONVERTERS_H

#include "move4d/common.h"
#include "move4d/math/Vector.h"
#include <kdl/frames.hpp>

namespace move4d
{
namespace kin
{
namespace kdl
{
namespace converters
{

inline move4d::math::Transform toTransform(const KDL::Frame &in);
inline KDL::Frame fromTransform(const math::Transform &in);

move4d::math::Transform toTransform(const KDL::Frame &in)
{
    math::Transform out;
    for (size_t i = 0; i < 4; ++i) {
        for (size_t j = 0; j < 4; ++j) {
            out(i, j) = in(i, j);
        }
    }
    return out;
}

KDL::Frame fromTransform(const math::Transform &in)
{
    KDL::Rotation rot(in(0, 0), in(0, 1), in(0, 2), //
                      in(1, 0), in(1, 1), in(1, 2), //
                      in(2, 0), in(2, 1), in(2, 2));
    return {rot, KDL::Vector(in(0, 3), in(1, 3), in(2, 3))};
}

} // namespace converters
} // namespace kdl
} // namespace kin
} // namespace move4d

#endif // _ADDONS_KDL_SRC_CONVERTERS_H

//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#include "move4d/plan/ompl/SearchSpaceOmpl.h"
#include "move4d/mdl/WorldState.h"
#include "move4d/plan/ompl/KinematicGraphStateSpace.h"
#include "move4d/plan/ompl/converters/StateSpaceFactory.h"

namespace move4d
{
namespace plan
{
namespace ompl
{

SearchSpaceOmpl::SearchSpaceOmpl(const SearchSpace &other) : SearchSpace(other)
{
    convert::SearchSpaceFactory factory;
    _wrapper = factory.createForPlanning(other);

    //_wrapper = std::make_shared<StateSpaceWrapper>(other,
    // std::make_shared<PoseStateSpace>(other));
    //_wrapper->getStateSpace()->as<StateSpace>()->setWrapper(_wrapper);

    _wrapper->getStateSpace()->setup();
}

void SearchSpaceOmpl::shootUniform(mdl::WorldState &ws) const
{
    Expects(dynamic_cast<KinematicGraphStateSpace *>(_wrapper->getStateSpace().get()));
    _wrapper->getStateSpace()->as<KinematicGraphStateSpace>()->setInitWorldState(
        std::make_shared<mdl::WorldState>(ws));
    // auto sampler = std::make_shared<KinematicStateSampler>(
    //     _wrapper->getStateSpace().get()->as<PoseStateSpace>());
    auto sampler = _wrapper->getStateSpace()->allocStateSampler();
    auto state_ompl = _wrapper->extractState(ws);
    sampler->sampleUniform(state_ompl.get());
    _wrapper->applyState(state_ompl.get(), ws);
}

const StateSpaceWrapperPtr &SearchSpaceOmpl::getWrapper() const { return _wrapper; }

void SearchSpaceOmpl::sanityChecks(const mdl::WorldState &ws) const
{
    Expects(dynamic_cast<KinematicGraphStateSpace *>(_wrapper->getStateSpace().get()));
    _wrapper->getStateSpace()->as<KinematicGraphStateSpace>()->setInitWorldState(
        std::make_shared<mdl::WorldState>(ws));
    _wrapper->getStateSpace()->sanityChecks();

    mdl::WorldState ws2 = ws;
    assert(ws2 == ws);
    auto state = _wrapper->extractState(ws);
    auto state2 = _wrapper->extractState(ws);
    auto sampler = _wrapper->getStateSpace()->allocStateSampler();
    sampler->sampleUniform(state2.get());
    _wrapper->applyState(state2.get(), ws2);
    assert(ws != ws2);
    _wrapper->applyState(state.get(), ws2);
    assert(ws == ws2);
}

} // namespace ompl
} // namespace plan
} // namespace move4d

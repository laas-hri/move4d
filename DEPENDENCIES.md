# list of dependencies:
 * urdfdom >= 0.3.0
 * sdformat (TODO == 4.0.0)
 * rl (robotics-library) >= 0.7.0
 * ompl (TODO == 1.5.0)
 * Eigen3
 * boost >= 1.58 (>=1.59 for unit tests)
 * qt4
 * xml2
 * Coin
 * SoQt
 * bullet, fcl, pqp,... (optional, used through RL, see RL documentation)
 * log4cxx
 * nlopt





# ubuntu 16.04 (Xenial) packages:
## compilation dependencies:
    build-essential \
    git \
    curl \
    tar \
    cmake \
    libeigen3-dev \
    libboost-all-dev \
    libxml2-dev \
    pkgconf \
    liburdfdom-dev \
    libsdformat*-dev \
    libxslt1-dev \
    libcoin*-dev \
    libsoqt*-dev \
    libqt4-dev \
    libfcl-dev \
    libnlopt-dev \
    liblog4cxx-dev \
    libbullet-dev

## runtime dependencies:
TODO (probably the above without the <-dev>)

# ubuntu 14.04 packages:

# ubuntu 14.04 not in official repositories:
urdfdom >= 0.3.0

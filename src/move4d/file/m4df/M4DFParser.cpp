#include "move4d/file/m4df/M4DFParser.h"
#include "move4d/file/find_file.h"
#include "move4d/file/m4df/M4DFDefinition.h"
#include "move4d/file/model/URDFImporter.h"
#include "move4d/file/sem/SRDFImporter.h"
#include "move4d/mdl/Movable.h"
#include "move4d/mdl/Scene.h"
#include "move4d/plan/KinematicGraphSearchSpace.h"
#include "move4d/plan/SO3Space.h"
#include "move4d/utils/filesystem.h"
#include <fstream>
#include <sstream>
#include <tinyxml2.h>

using namespace tinyxml2;
namespace move4d
{
namespace file
{

namespace m4df
{

INIT_MOVE4D_STATIC_LOGGER(M4DFParser, "move4d.file.m4df.parser");

void parseLimits(const XMLElement &limits, math::Vector &low, math::Vector &high);

M4DFParser::M4DFParser(mdl::ScenePtr scene, view::Viewer *viewer)
    : _scene(std::move(scene)), _viewer(viewer)
{
}

std::unique_ptr<M4DFDefinition> M4DFParser::loadFile(const std::string &filepath)
{
    try {
        tinyxml2::XMLDocument doc;
        doc.LoadFile(filepath.c_str());
        std::filesystem::path p(filepath);
        p = std::filesystem::absolute(p);
        p.remove_filename();
        return loadXml(doc, p);
    } catch (const M4DFException &e) {
        throw e.setFile(filepath);
    }
}

std::unique_ptr<M4DFDefinition> M4DFParser::loadString(const std::string &xml_string,
                                                       const std::string &path)
{
    tinyxml2::XMLDocument doc;
    doc.Parse(xml_string.c_str());
    return loadXml(doc, path);
}

std::unique_ptr<M4DFDefinition> M4DFParser::loadXml(const XMLDocument &doc, const std::string &path)
{
    if (doc.Error()) {
        throw M4DFException(doc.ErrorName());
    }
    _path = path;
    auto root = doc.RootElement();
    assert(root->NextSiblingElement() == nullptr);
    return loadXml(*root, path);
}

void M4DFParser::loadObject(const XMLElement &xml_element, M4DFDefinition &definition)
{
    auto id_c = xml_element.Attribute("id");
    auto uri = xml_element.FirstChildElement("uri");
    if (uri) {
        std::filesystem::path path(uri->GetText());
        if (path.extension() == ".urdf") {
            path = findFile(path, {_path});
            M4D_DEBUG("load URDF " << path);
            model::URDFImporter importer;
            std::string id = (id_c ? std::string(id_c) : "");
            importer.load(path, definition.getScene().get(), "", id);
            if (definition.getViewer()) {
                importer.loadView(path, definition.getViewer(), "", id, _load_collision_in_view);
            }
        } else {
            throw M4DFException("expected urdf (*.urdf) file in m4df object uri element");
        }
    }
}

std::unique_ptr<M4DFDefinition> M4DFParser::loadXml(const tinyxml2::XMLElement &xml_element,
                                                    const std::string &path)
{

    if (std::string("m4df") != xml_element.Name()) {
        throw("M4DF root element must be a <m4df> element");
    }
    auto definition = std::make_unique<M4DFDefinition>(_scene, _viewer);
    _path = path;
    const XMLElement *scene_element = xml_element.FirstChildElement("scene");
    // if (scene_element && definition && definition->getScene()) {
    //     M4D_ERROR("the M4DF definition contains a scene, but a scene was provided to the parser."
    //               " Aborting");
    //     throw M4DFException(
    //         "the M4DF definition contains a scene, but a scene was provided to the parser.");
    // }
    if (scene_element) {
        if (!definition->getScene()) {
            abort(); // TODO: get collision engine type and build it
            definition = std::make_unique<M4DFDefinition>(std::make_shared<mdl::Scene>());
        }

        auto limits = scene_element->FirstChildElement("limits");
        if (limits) {
            math::Vector lower, upper;
            parseLimits(*limits, lower, upper);
            definition->getScene()->setSpaceBounds({lower, upper});
        }

        assert(scene_element->NextSiblingElement("scene") ==
               nullptr); // there should be only one scene element
    }
    if (!definition) {
        M4D_ERROR("the M4DF definition does not contain any scene, and no scene was provided to "
                  "the parser");
        throw M4DFException("the M4DF definition does not contain any scene, and no scene was "
                            "provided to the parser");
    }

    M4D_DEBUG("searching for object elements");
    const XMLElement *element = xml_element.FirstChildElement("object");
    while (element) {
        loadObject(*element, *definition);
        element = element->NextSiblingElement("object");
    }

    element = xml_element.FirstChildElement("srdf");
    while (element) {
        sem::SRDFImporter importer;
        auto uri = element->FirstChildElement("uri");
        if (uri) {
            importer.loadFile(findFile(uri->GetText(), {_path}), definition->getScene());
        }
        element = element->NextSiblingElement("srdf");
    }

    M4D_DEBUG("searching for search_space elements");
    size_t counter = 0;
    element = xml_element.FirstChildElement("search_space");
    while (element) {
        ++counter;
        auto ss = parseSearchSpace(*element, *definition);
        definition->_search_spaces[ss->getName()] = std::move(ss);

        element = element->NextSiblingElement("search_space");
    }
    M4D_DEBUG("found " << counter << " search_space elements");

    return std::move(definition);
}

void parseChain(const XMLElement &chain_element, std::string &robot_id, std::string &chain_name,
                std::string &solver)
{
    static M4DFException err(
        "M4DF chain element must have the following attributes: robot, name, solver");
    auto s = chain_element.Attribute("robot");
    if (s)
        robot_id = s;
    else
        throw err;
    s = chain_element.Attribute("name");
    if (s)
        chain_name = s;
    else
        throw err;
    s = chain_element.Attribute("solver");
    if (s)
        solver = s;
    else
        throw err;
}

void parseKinematicGraphChainAttributes(const XMLElement &element, std::string &from,
                                        std::string &to)
{

    auto from_c = element.Attribute("from");
    auto to_c = element.Attribute("to");
    auto name_conv = [](const char *v) {
        if (strcmp(v, "_fixed_") == 0 || strcmp(v, "_origin_") == 0)
            return plan::KinematicGraphSearchSpaceBuilder::Fixed;
        return std::string(v);
    };
    if (from_c && to_c) {
        from = name_conv(from_c);
        to = name_conv(to_c);
    } else {
        throw M4DFException("element must contain a \"from\" and \"to\" attribute.");
    }
}

math::Vector parseVector(const char *s)
{
    INIT_MOVE4D_LOG("move4d.file.m4df.parser.vector");
    std::istringstream ss;
    ss.str(s);
    std::vector<Real> v;
    Real x;
    // read the first value
    ss >> x;
    while ((ss.rdstate() & std::stringstream::failbit) == 0) {
        v.push_back(x);
        // read the next value
        ss >> x;
    }
    // create a Map to the std::vector and *copy* the values into the result vector
    math::Vector res = Eigen::Map<math::Vector, Eigen::Unaligned>(v.data(), v.size());
    M4D_DEBUG("parseVector: " << s << " => " << res);
    return res;
}
math::Transform parseTransform(const XMLElement &element)
{
    math::Translation t;
    math::Quaternion q;
    const char *xyz = element.Attribute("xyz");
    if (xyz) {
        t.vector() = parseVector(xyz);
    }
    const char *rpy = element.Attribute("rpy");
    if (rpy) {
        Real roll, pitch, yaw;
        std::istringstream ss(rpy);
        ss >> roll;
        ss >> pitch;
        ss >> yaw;
        q = math::utilities::quaternionFromRPY(roll, pitch, yaw);
    } else {
        q.setIdentity();
    }
    return t * q;
}

void parseLimits(const XMLElement &limits, math::Vector &low, math::Vector &high)
{
    auto fixed = limits.FirstChildElement("fixed");
    if (fixed) {
        low = parseVector(fixed->GetText());
        high = low;
        return;
    }
    auto low_xml = limits.FirstChildElement("lower");
    auto upper_xml = limits.FirstChildElement("upper");
    if (low_xml && upper_xml) {
        low = parseVector(low_xml->GetText());
        high = parseVector(upper_xml->GetText());
        if (low.size() != high.size()) {
            throw M4DFException("<lower> and <upper> in <limits> differ in dimension");
        }
        return;
    }

    throw M4DFException("<limits> must contain an element <fixed> or both <upper> and <lower>");
}

std::unique_ptr<plan::RobotKinematicGroupSpace>
M4DFParser::parseKinematicGraphChain(const XMLElement &part_element, M4DFDefinition &definition)
{
    auto chain_element = part_element.FirstChildElement("chain");
    std::string robot_id, chain_name, ik_method;
    if (!chain_element) {
        throw M4DFException("invalid M4DF file, expected a chain");
    }
    parseChain(*chain_element, robot_id, chain_name, ik_method);
    auto obj = definition.getScene()->getObject(robot_id);
    if (!obj) {
        throw M4DFException("a M4DF chain specifies the robot ID " + robot_id +
                            " that is unknown to the scene");
    }
    auto robot = obj->as<mdl::Movable>();
    assert(robot);

    return plan::RobotKinematicGroupSpace::create(robot, chain_name, ik_method);
}

void M4DFParser::parserRobotJoint(const tinyxml2::XMLElement &element, mdl::SceneObjectPtr &robot,
                                  size_t &joint_index, M4DFDefinition &definition)
{
    if (!element.FirstChildElement("robot")) {
        throw M4DFException("expected <robot> element");
    }
    if (!element.FirstChildElement("joint")) {
        throw M4DFException("expected <joint> element");
    }
    robot = definition.getScene()->getObject(element.FirstChildElement("robot")->GetText());
    const char *joint_name = element.FirstChildElement("joint")->GetText();
    joint_index = robot->getJointIndex(joint_name);
    if (joint_index == -1u)
        throw M4DFException(std::string("no joint named \"") + joint_name + "\" in robot " +
                            robot->getId());
}

bool M4DFParser::getLoadCollisionInView() const { return _load_collision_in_view; }

void M4DFParser::setLoadCollisionInView(bool load_collision_in_view)
{
    _load_collision_in_view = load_collision_in_view;
}

ptr_wrapper<plan::SearchSpace> M4DFParser::parseSearchSpace(const XMLElement &element,
                                                            M4DFDefinition &definition)
{

    auto name = element.Attribute("name");
    if (!name) {
        throw M4DFException("<search_space> must have a \"name\" attribute");
    }

    std::string type = element.Attribute("type");
    if (element.Attribute("type", "kinematic_graph")) {
        auto builder = parseKinematicGraphBuilder(element, definition);
        auto ss = builder->build();
        definition._kinematic_graph_builders[name] = std::move(builder);
        M4D_DEBUG("parsed kinematic graph search space with name " << name);
        return ptr_wrapper<plan::SearchSpace>(std::move(ss));
    } else if (type == "se2" || type == "reeds_shepp" || type == "dubins") {
        mdl::SceneObjectPtr robot;
        size_t joint_index;
        parserRobotJoint(element, robot, joint_index, definition);
        size_t dof = robot->getJointFirstDof(joint_index);
        M4D_DEBUG("parsed SE2 space with name " << name << " for robot " << robot->getId()
                                                << " starting at dof " << dof);
        using SE2Type = plan::SE2Space::SE2Type;
        SE2Type se2type;
        if (type == "se2")
            se2type = SE2Type::Holonomic;
        else if (type == "reeds_shepp")
            se2type = SE2Type::ReedsShepp;
        else if (type == "dubins")
            se2type = SE2Type::Dubins;
        auto ss = ptr_wrapper<plan::SearchSpace>(
            new plan::SE2Space(robot->as<mdl::Movable>(), dof, se2type, name));
        auto limits = element.FirstChildElement("limits");
        if (limits) {
            math::Vector lower, upper;
            parseLimits(*limits, lower, upper);
            if (lower.size() != 2)
                throw M4DFException("limits for SE2 space must have 2 values");
            ss->as<plan::SE2Space>()->setBounds(lower, upper);
        }
        return ss;
    } else {
        M4D_WARN("search_space element with unsupported type: " << element.Attribute("type"));
        throw M4DFException(std::string() + "search_space element with unsupported type: " +
                            element.Attribute("type"));
        return {};
    }
}

std::unique_ptr<plan::KinematicGraphSearchSpaceBuilder>
M4DFParser::parseKinematicGraphBuilder(const XMLElement &kg_element, M4DFDefinition &definition)
{
    auto builder_ptr = std::make_unique<plan::KinematicGraphSearchSpaceBuilder>();
    auto &builder = *builder_ptr;

    const char *name = kg_element.Attribute("name");
    if (name) {
        builder.withName(name);
    } else {
        throw M4DFException("<search_space> must have a \"name\" attribute");
    }
    if (definition._search_spaces.find(name) != definition._search_spaces.end()) {
        throw M4DFException("<search_space> name must be unique");
    }
    auto element = kg_element.FirstChildElement("forward");
    while (element) {
        std::string from, to;
        std::unique_ptr<plan::RobotKinematicGroupSpace> kgroup;
        parseKinematicGraphChainAttributes(*element, from, to);
        if (element->FirstChildElement("chain")) {
            kgroup = parseKinematicGraphChain(*element, definition);
        } else if (element->FirstChildElement("kinematic_wrapper")) {
            auto kin_wrap_xml = element->FirstChildElement("kinematic_wrapper");
            auto ss = parseSearchSpace(*kin_wrap_xml->FirstChildElement("search_space"), definition)
                          .release_ptr();
            auto ss_name = ss->getName();
            M4D_DEBUG("parsed space for kinematic_wrapper with name " << ss->getName());
            kgroup =
                std::unique_ptr<plan::RobotKinematicGroupSpace>(new plan::KinematicSpaceWrapper(
                    std::move(ss),
                    definition.getScene()
                        ->getObject(kin_wrap_xml->Attribute("robot"))
                        ->as<mdl::Movable>(),
                    kin_wrap_xml->Attribute("from"), kin_wrap_xml->Attribute("to"), ss_name));
        }
        builder.addForwardChain(*kgroup, from, to);
        auto limits = element->FirstChildElement("limits");
        if (limits) {
            math::Vector low, high;
            parseLimits(*limits, low, high);
            M4D_DEBUG("setting limits " << low.transpose() << " < " << high.transpose());
            builder.withLimits(low, high);
        }
        element = element->NextSiblingElement("forward");
    }
    element = kg_element.FirstChildElement("inverse");
    while (element) {
        std::string from, to;
        parseKinematicGraphChainAttributes(*element, from, to);
        auto kgroup = parseKinematicGraphChain(*element, definition);
        builder.addInverseChain(*kgroup, from, to);
        element = element->NextSiblingElement("inverse");
    }
    element = kg_element.FirstChildElement("fixed");
    while (element) {
        std::string from, to;
        parseKinematicGraphChainAttributes(*element, from, to);
        auto tf_element = element->FirstChildElement("transform");
        math::Transform tf;
        if (tf_element) {
            tf = parseTransform(*tf_element);
        } else {
            tf.setIdentity();
        }
        M4D_DEBUG("transform between " << from << " and " << to << " is\n" << tf.matrix());
        builder.addFixedTransform(from, to, tf);
        element = element->NextSiblingElement("fixed");
    }
    return builder_ptr;
}

} // namespace m4df

} // namespace file
} // namespace move4d

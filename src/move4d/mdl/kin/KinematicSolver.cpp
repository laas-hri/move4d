//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#include "move4d/mdl/kin/KinematicSolver.h"
#include "move4d/mdl/Kinematic.h"
#include "move4d/mdl/SceneObject.h"

bool move4d::kin::KinematicSolver::computeInverseKinematic(const move4d::mdl::SceneObject &object,
                                                           const move4d::mdl::Chain &chain,
                                                           const move4d::math::Transform &pose,
                                                           const move4d::mdl::State &state,
                                                           move4d::math::Vector &x_in_out)
{
    object.m_kinematic->setPosition(state(mdl::StateType::Position));
    object.m_kinematic->forwardPosition();
    const math::Transform base_pose = chain.rootFrame->x.transform();

    return computeInverseKinematic(chain, base_pose, pose, x_in_out);
}

bool move4d::kin::KinematicSolver::computeForwardKinematic(const move4d::mdl::SceneObject &object,
                                                           const move4d::mdl::Chain &chain,
                                                           const move4d::mdl::State &state,
                                                           const move4d::math::Vector x_in,
                                                           move4d::math::Transform &pose_out)
{
    object.m_kinematic->setPosition(state(mdl::StateType::Position));
    object.m_kinematic->forwardPosition();
    const math::Transform base_pose = chain.rootFrame->x.transform();

    return computeForwardKinematic(chain, base_pose, pose_out, x_in);
}

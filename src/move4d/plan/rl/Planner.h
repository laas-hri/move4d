//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_PLAN_RL_PLANNER_H
#define MOVE4D_PLAN_RL_PLANNER_H

#include "move4d/plan/Planner.h"
#include <rl/plan/VectorList.h>
#include <vector>

namespace rl
{
namespace plan
{
class Planner;
class NearestNeighbors;
class Sampler;
class Viewer;
} // namespace plan
} // namespace rl

namespace move4d
{
namespace plan
{
class WorldStateGoal;
class RobotSpace;
namespace rl
{

class Planner : public move4d::plan::Planner
{
  public:
    Planner(std::shared_ptr<move4d::plan::RobotSpace> space,
            mdl::CollisionManagerPtr collisionManager);
    virtual bool solve(float timeLimit) override;

    ::rl::plan::VectorList getPath() const;

    void setViewer(::rl::plan::Viewer *viewer);

  protected:
    ::rl::plan::Planner *m_planner;
    std::vector<std::shared_ptr<::rl::plan::NearestNeighbors>> m_nearestNeighbors;
    std::shared_ptr<::rl::plan::Sampler> m_sampler;
};

} // namespace rl
} // namespace plan
} // namespace move4d

#endif // MOVE4D_PLAN_RL_PLANNER_H

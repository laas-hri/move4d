//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//

#include "move4d/plan/SearchSpace.h"
#include "move4d/mdl/DegreeOfFreedomInterface.h"
#include "move4d/mdl/SceneObject.h"
#include "move4d/mdl/WorldState.h"
#include "move4d/utils/random.h"
#include <iomanip> //std::quoted

namespace move4d
{

namespace plan
{
INIT_MOVE4D_STATIC_LOGGER(SearchSpace, "move4d.plan.searchspace");

SearchSpace::SearchSpace(Type type, std::string name) : m_type(type), m_name(std::move(name))
{
    M4D_TRACE("SearchSpace::SearchSpace type=" << typeString(m_type)
                                               << ", name=" << std::quoted(m_name));
}

SearchSpace::Type SearchSpace::getType() const { return m_type; }

const std::string &SearchSpace::typeString(Type type)
{
    static std::vector<std::string> names = {
        "Undef",      "Compound",       "RealVector", "SO2", "SO3",
        "SE2",        "ReedsShepp",     "Dubins",     "SE3", "KinematicGroup",
        "ClosedLoop", "KinematicGraph", "Other",      "Size"};
    return names[(size_t)type];
}

void SearchSpace::shootUniform(mdl::WorldState &ws) const
{
    for (auto dof : m_degreesOfFreedom) {
        dof.setIn(ws, dof.shootUniform(move4d::random::instance().uniform()));
    }
}

std::string SearchSpace::toString(const mdl::WorldState &ws) const
{
    std::ostringstream oss;
    oss << "move4d WorldState [";
    for (const auto &d : m_degreesOfFreedom) {
        oss << d.getFrom(ws) << " ";
    }
    oss << "]";
    return oss.str();
}

void SearchSpace::setIn(const math::Vector &values, mdl::WorldState &ws)
{
    assert(values.size() == getDimension() ||
           values.size() == getDimension() + m_additionalDoFs.size());
    if (values.size() != getDimension() &&
        values.size() != getDimension() + m_additionalDoFs.size()) {
        throw std::out_of_range(
            "dimension mismatch in SearchSpace::setIn(math::Vector, mdl::WorldState)");
    }
    for (size_t i = 0; i < getDimension(); ++i) {
        m_degreesOfFreedom[i].setIn(ws, values[i]);
    }
    if (values.size() >= getDimension() + m_additionalDoFs.size()) {
        size_t offset = getDimension();
        for (size_t i = 0; i < m_additionalDoFs.size(); ++i) {
            m_additionalDoFs[i].setIn(ws, values[offset + i]);
        }
    }
}

math::Vector SearchSpace::getFrom(const mdl::WorldState &ws)
{
    math::Vector values(getDimension() + m_additionalDoFs.size());
    for (size_t i = 0; i < getDimension(); ++i) {
        values[i] = m_degreesOfFreedom[i].getFrom(ws);
    }
    size_t offset = getDimension();
    for (size_t i = 0; i < m_additionalDoFs.size(); ++i) {
        values[offset + i] = m_additionalDoFs[i].getFrom(ws);
    }
    return values;
}

void SearchSpace::setStateSamplerAllocator(ompl::base::StateSamplerAllocator allocator)
{
    m_state_sampler_allocator = std::move(allocator);
}

const ompl::base::StateSamplerAllocator &SearchSpace::getStateSamplerAllocator() const
{
    return m_state_sampler_allocator;
}

std::vector<size_t> SearchSpace::bijection() const
{
    std::vector<size_t> bijec(getDimension());
    std::transform(m_degreesOfFreedom.begin(), m_degreesOfFreedom.end(), bijec.begin(),
                   [this](const auto &dof) { return dof.index(); });
    return bijec;
}

CompoundSearchSpace::CompoundSearchSpace(std::string name)
    : SearchSpace(Type::Compound, std::move(name))
{
}

void CompoundSearchSpace::addSubspace(gsl::owner<SearchSpace *> &subspace)
{
    for (const auto &dof : subspace->getDegreesOfFreedom()) {
        // TODO(Jules): check CompoundStateSpace::addSubspace: duplicate DoF?
        m_degreesOfFreedom.push_back(dof);
    }
    m_subspaces.push_back(subspace);
    subspace = nullptr;
}

void CompoundSearchSpace::addSubspace(const SearchSpace &subspace)
{
    gsl::owner<SearchSpace *> ptr = new_clone(&subspace);
    addSubspace(ptr);
}

void CompoundSearchSpace::shootUniform(mdl::WorldState &ws) const
{
    for (const auto &subspace : m_subspaces) {
        subspace.shootUniform(ws);
    }
}

void KinematicSearchSpace::setPoseBounds(const std::pair<math::Vector3, math::Vector3> &bounds)
{
    setPoseBounds(bounds.first, bounds.second);
}

} // namespace plan

} // namespace move4d

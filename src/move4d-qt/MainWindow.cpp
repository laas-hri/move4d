//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#include "MainWindow.h"
#include "move4d-soqt/Application.h"
#include "ui_MainWindow.h"

#include <QCloseEvent>
#include <QFileDialog>

namespace move4d
{
namespace qt
{

MainWindow::MainWindow(move4d::SoQt::Application &app, QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow), _app(app)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow() { delete ui; }

void MainWindow::on_actionLoad_file_triggered()
{
    auto filename = QFileDialog::getOpenFileName(
        this, tr("Open File"), "",
        tr("All files readable by move4d (*.urdf *.sdf *.srdf);;Robot Descriptions (*urdf "
           "*sdf);;Robot Semantic Descriptions (*srdf)"));
    if (filename.isNull() || filename.isEmpty()) {
        return;
    }
    _app.loadFile(filename.toStdString());
}

void MainWindow::on_actionQuit_triggered() { this->close(); }
void MainWindow::closeEvent(QCloseEvent *) { move4d::Stop("main window closed"); }

} // namespace qt

} // namespace move4d

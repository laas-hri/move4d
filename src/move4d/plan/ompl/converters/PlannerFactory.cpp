//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#include "move4d/plan/ompl/converters/PlannerFactory.h"
#include <ompl/geometric/planners/est/BiEST.h>
#include <ompl/geometric/planners/est/EST.h>
#include <ompl/geometric/planners/est/ProjEST.h>
#include <ompl/geometric/planners/fmt/BFMT.h>
#include <ompl/geometric/planners/fmt/FMT.h>
#include <ompl/geometric/planners/kpiece/BKPIECE1.h>
#include <ompl/geometric/planners/kpiece/KPIECE1.h>
#include <ompl/geometric/planners/kpiece/LBKPIECE1.h>
#include <ompl/geometric/planners/pdst/PDST.h>
#include <ompl/geometric/planners/prm/LazyPRM.h>
#include <ompl/geometric/planners/prm/LazyPRMstar.h>
#include <ompl/geometric/planners/prm/PRM.h>
#include <ompl/geometric/planners/prm/PRMstar.h>
#include <ompl/geometric/planners/prm/SPARS.h>
#include <ompl/geometric/planners/prm/SPARStwo.h>
#include <ompl/geometric/planners/rrt/BiTRRT.h>
#include <ompl/geometric/planners/rrt/LBTRRT.h>
#include <ompl/geometric/planners/rrt/LazyRRT.h>
#include <ompl/geometric/planners/rrt/RRT.h>
#include <ompl/geometric/planners/rrt/RRTConnect.h>
#include <ompl/geometric/planners/rrt/RRTstar.h>
#include <ompl/geometric/planners/rrt/TRRT.h>
#include <ompl/geometric/planners/rrt/pRRT.h>
#include <ompl/geometric/planners/sbl/SBL.h>
#include <ompl/geometric/planners/sbl/pSBL.h>
#include <ompl/geometric/planners/stride/STRIDE.h>

namespace move4d
{
namespace plan
{
namespace ompl
{
namespace convert
{

namespace
{
struct ProxyExecRegisterOmplPlannerFactory {
    ProxyExecRegisterOmplPlannerFactory() noexcept;
    template <class T> inline void registerPlanner(const std::string &name)
    {
        PlannerFactory::instance().registerCreator(
            name,
            [](const ::ompl::base::SpaceInformationPtr &si) { return std::make_shared<T>(si); });
    }
};
ProxyExecRegisterOmplPlannerFactory s_proxyExecRegisterOmplPlannerFactory;

ProxyExecRegisterOmplPlannerFactory::ProxyExecRegisterOmplPlannerFactory() noexcept
{
    try {
        registerPlanner<::ompl::geometric::RRT>("RRT");
        registerPlanner<::ompl::geometric::RRTConnect>("RRTConnect");
        registerPlanner<::ompl::geometric::LazyRRT>("LazyRRT");
        registerPlanner<::ompl::geometric::TRRT>("TRRT");
        registerPlanner<::ompl::geometric::EST>("EST");
        registerPlanner<::ompl::geometric::SBL>("SBL");
        registerPlanner<::ompl::geometric::KPIECE1>("KPIECE");
        registerPlanner<::ompl::geometric::BKPIECE1>("BKPIECE");
        registerPlanner<::ompl::geometric::LBKPIECE1>("LBKPIECE");
        registerPlanner<::ompl::geometric::RRTstar>("RRTstar");
        registerPlanner<::ompl::geometric::PRM>("PRM");
        registerPlanner<::ompl::geometric::PRMstar>("PRMstar");
        registerPlanner<::ompl::geometric::FMT>("FMT");
        registerPlanner<::ompl::geometric::BFMT>("BFMT");
        registerPlanner<::ompl::geometric::PDST>("PDST");
        registerPlanner<::ompl::geometric::STRIDE>("STRIDE");
        registerPlanner<::ompl::geometric::BiTRRT>("BiTRRT");
        registerPlanner<::ompl::geometric::LBTRRT>("LBTRRT");
        registerPlanner<::ompl::geometric::BiEST>("BiEST");
        registerPlanner<::ompl::geometric::ProjEST>("ProjEST");
        registerPlanner<::ompl::geometric::LazyPRM>("LazyPRM");
        registerPlanner<::ompl::geometric::LazyPRMstar>("LazyPRMstar");
        registerPlanner<::ompl::geometric::SPARS>("SPARS");
        registerPlanner<::ompl::geometric::SPARStwo>("SPARStwo");
    } catch (std::exception &e) {
        move4d::FastStop(std::string("could not initialize factories for ompl planners: ") +
                         e.what());
    } catch (...) {
        move4d::FastStop("could not initialize factories for ompl planners: ");
    }
}
} // namespace

} // namespace convert
} // namespace ompl
} // namespace plan
} // namespace move4d

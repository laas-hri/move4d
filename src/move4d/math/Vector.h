//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_MATH_VECTOR_H
#define MOVE4D_MATH_VECTOR_H

#include "move4d/math/Eigen.h"

#include "move4d/common.h"
#include "rl/math/Vector.h"

namespace move4d
{

/**
 * @namespace move4d::math
 * @brief defines mathematical structures and basic algorithms
 */
namespace math
{

/// dynamic real vector
using Vector = rl::math::Vector;
using Vector2 = rl::math::Vector2;
using Vector3 = rl::math::Vector3;
using Vector3f = Eigen::Vector3f;
using Vector4f = Eigen::Vector4f;
typedef ::Eigen::Transform<move4d::Real, 3, Eigen::Affine> Transform;
using AngleAxis = Eigen::AngleAxis<Real>;
/**
 * @brief Quaternion
 * @note move4d, Eigen and OMPL store quaternion as x,y,z,w. But Eigen (and thus move4d's)
 * quaternions are built with Quaternion(w,x,y,z)
 */
typedef Eigen::Quaternion<move4d::Real> Quaternion;
typedef ::Eigen::Translation<move4d::Real, 3> Translation;

/**
 * @brief changeFrame
 * @param frame_A position of the frame of origin
 * @param pos_in_A position to transform expressed in the frame A
 * @param frame_B position of the target frame
 *
 * frame_A and frame_B must be expressed in the same frame (e.g. absolute world frame)
 *
 * @return the position expressed in the frame B
 */
Transform changeFrame(const Transform &frame_A, const Transform &pos_in_A,
                      const Transform &frame_B);
Vector3 changeFramePoint(const Transform &frame_A, const Vector3 &point_in_A,
                         const Transform &frame_B);
Vector3 changeFrameVector(const Transform &frame_A, const Vector3 &vector_in_A,
                          const Transform &frame_B);

struct utilities {
    static inline bool equals(move4d::Real a, move4d::Real b);

  protected:
    static move4d::Real m_epsilon;

  public:
    static inline Quaternion rotationFromQuaternionXYZ(move4d::Real x, move4d::Real y,
                                                       move4d::Real z);
    static inline Quaternion quaternionFromRPY(Real roll, Real pitch, Real yaw);
};
inline bool utilities::equals(move4d::Real a, move4d::Real b)
{
    return ::std::abs(a - b) < m_epsilon;
}

inline Quaternion utilities::rotationFromQuaternionXYZ(move4d::Real x, move4d::Real y,
                                                       move4d::Real z)
{
    Real w = std::sqrt(1 - x * x - y * y - z * z);
    return Quaternion(w, x, y, z);
}

Quaternion utilities::quaternionFromRPY(Real roll, Real pitch, Real yaw)
{
    using AA = AngleAxis;
    Quaternion q = AA(yaw, math::Vector3::UnitZ()) * AA(pitch, math::Vector3::UnitY()) *
                   AA(roll, math::Vector3::UnitX());
    assert(std::abs(q.squaredNorm() - 1) < m_epsilon);
    return q;
}

} // namespace math

} // namespace move4d
#endif

CMAKE_MINIMUM_REQUIRED( VERSION 3.5 )
PROJECT(move4d
    VERSION 0.2.1.0
    LANGUAGES CXX)

option(BUILD_TESTING "Build unit tests" OFF)
option(BUILD_EXAMPLES "Build examples" OFF)
set(MOVE4D_LOG_LEVEL "TRACE" CACHE STRING "Disable compilation of log messages bellow that level (OFF(=all disabled), ERROR, INFO, TRACE(=all enabled))")
include(CTest)

# Offer the user the choice of overriding the installation directories
set(INSTALL_LIB_DIR lib CACHE PATH "Installation directory for libraries")
set(INSTALL_BIN_DIR bin CACHE PATH "Installation directory for executables")
set(INSTALL_INCLUDE_DIR include/move4d CACHE PATH
  "Installation directory for header files")
if(WIN32 AND NOT CYGWIN)
  set(DEF_INSTALL_CMAKE_DIR CMake)
else()
  set(DEF_INSTALL_CMAKE_DIR lib/cmake/${PROJECT_NAME})
endif()
set(INSTALL_CMAKE_DIR ${DEF_INSTALL_CMAKE_DIR} CACHE PATH
  "Installation directory for CMake files")



set (CMAKE_CXX_FLAGS "-std=c++17 -Wall ${CMAKE_CXX_FLAGS}")
list(APPEND CMAKE_CONFIGURATION_TYPES "Fast")
set(CMAKE_CXX_FLAGS_FAST "-Ofast -DNDEBUG")

#--------------- OPTIONS -------------------------------------------------
SET(MOVE4D_REAL_TYPE double CACHE STRING "The type used for floating-point computation (double or float)")
SET(CMAKE_EXPORT_COMPILE_COMMANDS ON)

find_package (PkgConfig REQUIRED)
list(APPEND CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/scripts/cmake/modules)

pkg_check_modules(roboptim REQUIRED roboptim-core)
link_directories(${roboptim_LIBRARY_DIRS})

find_package(rl REQUIRED)

#find_package(Eigen3 REQUIRED NO_MODULE)
set(INCLUDE_DIRS ${INCLUDE_DIRS} ${EIGEN3_INCLUDE_DIRS})

find_package(Coin3D REQUIRED)
message("coin3d libs :${COIN3D_LIBRARIES} -- includes: ${COIN3D_INCLUDE_DIRS}")

find_package(urdfdom REQUIRED) #requires version 0.3.0 at least

find_package(assimp 4.0 REQUIRED)

find_package(ompl REQUIRED)
find_package(Boost REQUIRED COMPONENTS serialization)

find_package(SDFormat REQUIRED)
link_directories(${SDFormat_LIBRARY_DIRS})

find_package(orocos_kdl 1.4.0 REQUIRED )

find_package(LOG4CXX REQUIRED)

pkg_check_modules(tinyxml2 REQUIRED tinyxml2>=2.2)

#------------- INCLUDED 3RD PARTY LIBRARIES -------------------------------------
set(GSL_CXX_STANDARD "17" CACHE STRING "")
add_subdirectory(3rdParty/GSL)

add_subdirectory(src/move4d/3rdParty)

#------------- DOXYGEN ----------------------------------------------------------

find_package(Doxygen)
if(DOXYGEN_FOUND)

    set(DOXYFILE_IN  ${CMAKE_SOURCE_DIR}/docs/Doxyfile.in)
    set(DOXYFILE_OUT ${CMAKE_CURRENT_BINARY_DIR}/docs/Doxyfile)
    configure_file(${DOXYFILE_IN} ${DOXYFILE_OUT} @ONLY)
    add_custom_target(doc
        COMMAND ${DOXYGEN_EXECUTABLE} ${DOXYFILE_OUT}
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
        COMMENT "Generating API Documentation with Doxygen"
        VERBATIM
        )

    set(DOXYFILE_COLL_IN  ${CMAKE_SOURCE_DIR}/docs/DoxyfileCollaboration.in)
    set(DOXYFILE_COLL_OUT ${CMAKE_CURRENT_BINARY_DIR}/docs/DoxyfileCollaboration)
    configure_file(${DOXYFILE_COLL_IN} ${DOXYFILE_COLL_OUT} @ONLY)
    add_custom_target(doc_collaboration
        COMMAND ${DOXYGEN_EXECUTABLE} ${DOXYFILE_COLL_OUT}
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
        COMMENT "Generating API Documentation with Doxygen. WARNING: output is modified to display collaboration better (remove some smart pointers and so...)"
        VERBATIM
        )
else(DOXYGEN_FOUND)
    message("Doxygen need to be installed to generate the doxygen documentation")
endif(DOXYGEN_FOUND)


#load source file list
set(MOVE4D_SRCS)
include(src/move4d/CMakeLists.txt)


#--------------- ADDONS ------------------------------------------------------------
set(ADDON_TARGETS)
option(BUILD_MOVE4D_KDL "build move4d kdl addon" ON)
if(${BUILD_MOVE4D_KDL})
    add_subdirectory(src/move4d-addons/kdl)
endif(${BUILD_MOVE4D_KDL})
option(BUILD_MOVE4D_TRAC_IK "build move4d trac_ik addon" ON)
if(${BUILD_MOVE4D_KDL})
    add_subdirectory(src/move4d-addons/trac_ik)
endif(${BUILD_MOVE4D_KDL})
option(BUILD_MOVE4D_IKFAST "build move4d openrave ikfast addon" ON)
if(${BUILD_MOVE4D_IKFAST})
    add_subdirectory(src/move4d-addons/ikfast)
endif(${BUILD_MOVE4D_IKFAST})

add_library(${PROJECT_NAME} SHARED ${MOVE4D_SRCS})
target_link_libraries(${PROJECT_NAME}
    PUBLIC
    ${RL_LIBRARIES}
    ${roboptim_LIBRARIES}
    ${urdfdom_LIBRARIES}
    ${ASSIMP_LIBRARIES}
    ${OMPL_LIBRARIES}
    ${SDFormat_LIBRARIES}
    Boost::serialization
    orocos-kdl
    GSL
    ${LOG4CXX_LIBRARIES}
    ${tinyxml2_LIBRARIES}
    stdc++fs
    )
target_include_directories(${PROJECT_NAME}
    PUBLIC
    $<BUILD_INTERFACE:${CMAKE_SOURCE_DIR}/src>
    $<INSTALL_INTERFACE:include/move4d>
    ${roboptim_INCLUDE_DIRS}
    ${LOG4CXX_INCLUDE_DIRS}
    PRIVATE
    ${urdfdom_INCLUDE_DIRS}
    ${ASSIMP_INCLUDE_DIRS}
    ${OMPL_INCLUDE_DIRS}
    ${SDFormat_INCLUDE_DIRS}
    )

target_compile_definitions(${PROJECT_NAME} PUBLIC MOVE4D_REAL_TYPE=${MOVE4D_REAL_TYPE})
if(MOVE4D_LOG_LEVEL STREQUAL "INFO")
    target_compile_definitions(${PROJECT_NAME} PRIVATE MOVE4D_LOG_INFO_ONLY)
elseif(MOVE4D_LOG_LEVEL STREQUAL "ERROR")
    target_compile_definitions(${PROJECT_NAME} PRIVATE MOVE4D_LOG_ERROR_ONLY)
elseif(MOVE4D_LOG_LEVEL STREQUAL "OFF")
    target_compile_definitions(${PROJECT_NAME} PRIVATE MOVE4D_LOG_OFF)
elseif(MOVE4D_LOG_LEVEL STREQUAL "TRACE")
else()
    message(SEND_ERROR "Option MOVE4D_LOG_LEVEL set to an unsupported value: ${MOVE4D_LOG_LEVEL}")
endif()
#target_compile_features(${PROJECT_NAME} PUBLIC cxx_defaulted_functions cxx_override cxx_range_for)

# add_executable(${PROJECT_NAME}-test ${CMAKE_SOURCE_DIR}/src/move4d-test/Main.cpp)
# target_include_directories(${PROJECT_NAME}-test
#     PUBLIC
#     $<BUILD_INTERFACE:${CMAKE_SOURCE_DIR}/src>
#     $<INSTALL_INTERFACE:include/move4d>
#     ${COIN3D_INCLUDE_DIRS}
#     )
# target_link_libraries(${PROJECT_NAME}-test ${PROJECT_NAME} GSL)

find_package(OpenGL REQUIRED)
find_package(Qt4 REQUIRED QtGui QtOpenGL)
find_library(SOQT4_LIB SoQt4 REQUIRED)
set(CMAKE_AUTOMOC ON)

set(UIS ${CMAKE_SOURCE_DIR}/src/move4d-qt/MainWindow.ui)
QT4_WRAP_UI( UI_HEADERS ${UIS} )
add_library(${PROJECT_NAME}-soqt SHARED ${CMAKE_SOURCE_DIR}/src/move4d-soqt/ViewerSoQt.cpp
    ${CMAKE_SOURCE_DIR}/src/move4d-soqt/ViewerThreadQt.cpp
    ${CMAKE_SOURCE_DIR}/src/move4d-soqt/Application.cpp
    ${CMAKE_SOURCE_DIR}/src/move4d-qt/MainWindow.cpp
    ${UI_HEADERS}
    )
target_link_libraries(${PROJECT_NAME}-soqt
    ${SOQT4_LIB}
    ${PROJECT_NAME}
    Qt4::QtGui
    Qt4::QtOpenGL
    ${OPENGL_LIBRARIES}
    Boost::program_options
    )
target_include_directories(${PROJECT_NAME}-soqt
    PUBLIC
    $<BUILD_INTERFACE:${CMAKE_SOURCE_DIR}/src>
    $<INSTALL_INTERFACE:include/move4d>
    ${COIN3D_INCLUDE_DIRS}
    PRIVATE
    $<BUILD_INTERFACE:${CMAKE_BINARY_DIR}>
    )

find_package(Boost REQUIRED COMPONENTS program_options)

if(BUILD_TESTING)
    find_package(Boost 1.59 REQUIRED COMPONENTS unit_test_framework)
    if(${Boost_MAJOR_VERSION} EQUAL 1 AND ${Boost_MINOR_VERSION} LESS 59)
        # Boost 1.59 is requested for tests only (dataset tests). Disabling tests remove the dependency (usefull for older distros)
        message(SEND_ERROR "Boost version too old, try disabling BUILD_TESTS option")
    endif()
endif(BUILD_TESTING)

if(BUILD_EXAMPLES)
    add_subdirectory(examples)
endif(BUILD_EXAMPLES)

add_executable(${PROJECT_NAME}-soqtview ${CMAKE_SOURCE_DIR}/src/move4d-soqt/main.cpp)
target_link_libraries(${PROJECT_NAME}-soqtview ${PROJECT_NAME}-soqt Boost::program_options)

#add_executable(${PROJECT_NAME}-plan ${CMAKE_SOURCE_DIR}/src/move4d-test/Plan.cpp)
#target_link_libraries(${PROJECT_NAME}-plan ${PROJECT_NAME}-soqt)

#add_executable(${PROJECT_NAME}-ompl ${CMAKE_SOURCE_DIR}/src/move4d-test/OmplTest.cpp)
#target_link_libraries(${PROJECT_NAME}-ompl ${PROJECT_NAME}-soqt)

add_executable(${PROJECT_NAME}-parts ${CMAKE_SOURCE_DIR}/src/move4d-test/RobotPartOmpl.cpp)
target_link_libraries(${PROJECT_NAME}-parts ${PROJECT_NAME}-soqt)

add_executable(${PROJECT_NAME}-srdf ${CMAKE_SOURCE_DIR}/src/move4d-test/SrdfIK.cpp)
target_link_libraries(${PROJECT_NAME}-srdf ${PROJECT_NAME}-soqt)

#add_executable(${PROJECT_NAME}-ik ${CMAKE_SOURCE_DIR}/src/move4d-test/ConstraintOmpl.cpp)
#target_link_libraries(${PROJECT_NAME}-ik ${PROJECT_NAME}-soqt)

#add_executable(${PROJECT_NAME}-urdf ${CMAKE_SOURCE_DIR}/src/move4d-test/LoadUrdf.cpp)
#target_link_libraries(${PROJECT_NAME}-urdf ${PROJECT_NAME} GSL Qt4::QtGui ${SOQT4_LIB})

add_executable(${PROJECT_NAME}-sdf ${CMAKE_SOURCE_DIR}/src/move4d-test/LoadSdf.cpp)
target_link_libraries(${PROJECT_NAME}-sdf  ${PROJECT_NAME} ${PROJECT_NAME}-soqt GSL Qt4::QtGui ${SOQT4_LIB})

# unit test target
if(BUILD_TESTING)
    #sub project move4d-unit-tests
    #executable runing unit tests
    add_subdirectory(src/tests)

    # register unit tests to ctest
    add_test(NAME unit-tests COMMAND ${PROJECT_NAME}-unit-tests --log_level=test_suite --report_level=short)
    set_tests_properties(unit-tests PROPERTIES
        ENVIRONMENT
        "MOVE4D_HOME=${CMAKE_BINARY_DIR};MOVE4D_PLUGIN_DIR=${CMAKE_BINARY_DIR}/lib/move4d/plugins"
        )

    #FIXTURES for move4d-unit-tests
    add_test(NAME setupAssets COMMAND cmake -E create_symlink ${CMAKE_SOURCE_DIR}/src/tests/assets ${CMAKE_BINARY_DIR}/assets)
    add_test(NAME cleanUpAssets COMMAND cmake -E remove ${CMAKE_BINARY_DIR}/assets)
    if(CMAKE_VERSION VERSION_LESS "3.7")
        #test fixture unsupported, hack with DEPENDS property
        set_tests_properties(unit-tests PROPERTIES DEPENDS setupAssets)
        set_tests_properties(cleanUpAssets PROPERTIES DEPENDS unit-tests)
    else()
        #test fixtures supported since 3.7
        set_tests_properties(setupAssets PROPERTIES FIXTURES_SETUP Assets)
        set_tests_properties(cleanUpAssets PROPERTIES FIXTURES_CLEANUP Assets)
        set_tests_properties(unit-tests PROPERTIES FIXTURES_REQUIRED Assets)
    endif()


    # CHKHDR (checks headers are self-contained)
    file(GLOB_RECURSE CHKHDR_MOVE4D_HEADERS ${CMAKE_SOURCE_DIR}/src/move4d/*.h*)
    add_test(NAME chkhdr COMMAND ${CMAKE_CURRENT_SOURCE_DIR}/scripts/check_headers/chkhdr ${CHKHDR_MOVE4D_HEADERS})

endif(BUILD_TESTING)



# INSTALLATION

install(
    DIRECTORY ${CMAKE_SOURCE_DIR}/src/
    DESTINATION include/move4d
    FILES_MATCHING PATTERN "*.h" PATTERN "*.hpp"
)

install(TARGETS
    ${PROJECT_NAME}
    ${PROJECT_NAME}-soqt
    ${PROJECT_NAME}-parts
    ${PROJECT_NAME}-soqtview
    EXPORT ${PROJECT_NAME}-targets
    LIBRARY DESTINATION lib
    RUNTIME DESTINATION bin
    )
install(EXPORT ${PROJECT_NAME}-targets DESTINATION lib/cmake/${PROJECT_NAME})

file(RELATIVE_PATH REL_INCLUDE_DIR "${CMAKE_INSTALL_PREFIX}/${INSTALL_CMAKE_DIR}"
   "${CMAKE_INSTALL_PREFIX}/${INSTALL_INCLUDE_DIR}")

set(CONF_INCLUDE_DIRS "\${MOVE4D_CMAKE_DIR}/${REL_INCLUDE_DIR}")
configure_file(scripts/cmake/move4d-config.cmake.in
    "${PROJECT_BINARY_DIR}/move4d-config.cmake" @ONLY)

include(CMakePackageConfigHelpers)
write_basic_package_version_file(${PROJECT_BINARY_DIR}/move4d-config-version.cmake
    VERSION ${PROJECT_VERSION}
    COMPATIBILITY SameMajorVersion)

install(FILES
    ${PROJECT_BINARY_DIR}/move4d-config.cmake
    ${PROJECT_BINARY_DIR}/move4d-config-version.cmake
    DESTINATION ${INSTALL_CMAKE_DIR})

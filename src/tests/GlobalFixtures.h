//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_TEST_GLOBALFIXTURES_H
#define MOVE4D_TEST_GLOBALFIXTURES_H

#include <list> // needed by some versions of boost

#include <move4d/mdl/Scene.h>
#include <move4d/mdl/SceneObject.h>

namespace move4d
{
namespace tests
{

class GlobalFixture
{
  public:
    GlobalFixture();
    virtual ~GlobalFixture() = default;

  protected: // accessible from the test cases
    mdl::ScenePtr _scene;
    mdl::CollisionManagerPtr _collisionManager;

    mdl::SceneObjectPtr lwr_ff_base;

  private: // not accessible from the test cases
};

class FixtureSRDF : public GlobalFixture
{
  public:
    FixtureSRDF();
    virtual ~FixtureSRDF() = default;

  protected:
    mdl::ChainPtr arm_chain, arm_6_chain;
    mdl::EndEffectorPtr eef;
};

} // namespace tests
} // namespace move4d

#endif // MOVE4D_TEST_GLOBALFIXTURES_H

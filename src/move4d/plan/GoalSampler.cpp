//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#include "move4d/plan/GoalSampler.h"
#include "move4d/plan/KinematicGraphSearchSpace.h"

namespace move4d
{
namespace plan
{

GoalSampler::GoalSampler(std::map<size_t, BaseSamplerPtr> samplers)
    : Goal(1, 1), // TODO(Jules)
      _samplers(std::move(samplers))
{
}

const std::map<size_t, BaseSamplerPtr> &GoalSampler::getSamplers() const { return _samplers; }

void GoalSampler::setSamplers(std::map<size_t, BaseSamplerPtr> samplers)
{
    _samplers = std::move(samplers);
}

Real GoalSampler::distance(const mdl::WorldState &ws) const { abort(); }

bool GoalSampler::check(const mdl::WorldState &ws) const { abort(); }

void GoalSampler::impl_compute(result_ref result, const_argument_ref argument) const { abort(); }

GoalSpaceSampler::GoalSpaceSampler(KinematicGraphSearchSpacePtr goal_sampling_space,
                                   KinematicGraphSearchSpacePtr search_space, Real threshold)
    : Goal(1, 1), // TODO(Jules)
      _sampling_space(std::move(goal_sampling_space)), _search_space(std::move(search_space)),
      _threshold(threshold)
{
    {
        auto mapping = _sampling_space->computeEdgeBijectionTo(*_search_space);
        _bijection_sampling_to_search_space.assign(mapping.size(), 0);
        for (auto &p : mapping) {
            assert(p.first < mapping.size());
            _bijection_sampling_to_search_space[p.first] = p.second;
        }
    }
    {
        auto mapping = _sampling_space->computeVertexBijectionTo(*_search_space);
        _bijection_vertices_sampling_to_search_space.assign(mapping.size(), 0);
        for (auto &p : mapping) {
            assert(p.first < mapping.size());
            _bijection_vertices_sampling_to_search_space[p.first] = p.second;
        }
    }
}

Real GoalSpaceSampler::distance(const mdl::WorldState &ws) const
{
    Real dist = 0;
    abort();
    return dist;
}

bool GoalSpaceSampler::check(const mdl::WorldState &ws) const { return distance(ws) < _threshold; }

void GoalSpaceSampler::getSample(mdl::WorldState &ws) const
{
    // ws = *_world_state;
    _sampling_space->shootUniform(ws);
}

void GoalSpaceSampler::impl_compute(result_ref result, const_argument_ref argument) const
{
    result[0] = distance(argument);
}

SamplerAllocator FixSampler::make_allocator(math::Vector conf, math::Transform end_pose)
{
    return [conf, end_pose](std::shared_ptr<const SearchSpace> space) {
        return std::make_shared<FixSampler>(std::move(space), conf, end_pose);
    };
}

} // namespace plan
} // namespace move4d

//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#include "move4d/mdl/Movable.h"
#include "move4d/plan/SO3Space.h"
#include "tests/GlobalFixtures.h"
#include <boost/test/unit_test.hpp>
#include <rl/mdl/Dynamic.h>

namespace move4d
{
namespace tests
{

BOOST_FIXTURE_TEST_CASE(import_srdf, FixtureSRDF)
{
    BOOST_REQUIRE(eef);
    BOOST_REQUIRE(arm_chain);
    BOOST_REQUIRE(arm_6_chain);
    BOOST_CHECK(eef->name == "wrist");
    BOOST_CHECK(arm_chain->name == "arm");
    BOOST_CHECK(arm_6_chain->name == "arm6");
    BOOST_CHECK_EQUAL(arm_chain->bijection.size(), 7);

    auto robot = lwr_ff_base->as<mdl::Movable>();
    BOOST_CHECK_EQUAL(robot->chains.size(), 2);
    BOOST_CHECK_EQUAL(robot->endEffectors.size(), 1);
}

} // namespace tests

} // namespace move4d

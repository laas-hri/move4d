//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_MDL_DEGREEOFFREEDOMREFERENCE_H
#define MOVE4D_MDL_DEGREEOFFREEDOMREFERENCE_H

#include "move4d/common.h"
#include "move4d/mdl/DegreeOfFreedomInfo.h"
#include "move4d/mdl/DegreeOfFreedomInterface.h"

namespace move4d
{
namespace mdl
{
class State;
}
} // namespace move4d

namespace move4d
{

namespace mdl
{

/**
 * Stores a reference to the value of the degree of freedom.
 *
 * \warning When using this, you need to ensure the underlying state is not deleted.
 */
class DegreeOfFreedomReference : public DegreeOfFreedomInterface, virtual public DegreeOfFreedomInfo
{
  public:
    /**
     * creates the DoF with a reference to the given state
     */
    DegreeOfFreedomReference(const DegreeOfFreedomInfo &info, State &state_ref);

    virtual move4d::Real get() const override;

  protected:
    move4d::Real *m_value_ptr = nullptr;

  public:
    /**
     * Set the underlying value of the DoF. Performs no checks.
     */
    virtual void set(move4d::Real value) override;
};

} // namespace mdl

} // namespace move4d
#endif

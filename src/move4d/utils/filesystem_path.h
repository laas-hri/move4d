//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//

#ifndef MOVE4D_UTILS_FILESYSTEM_PATH_H
#define MOVE4D_UTILS_FILESYSTEM_PATH_H

#include <string>

void toupper(std::string &str)
{
    for (char &c : str) {
        c = toupper(c);
    }
}

/**
 * @brief checkExtension
 * @param path
 * @param extension any string you want to check if path ends with
 * @return
 */
bool checkExtension(const std::string &path, const std::string &extension,
                    bool caseSensitive = true)
{
    std::string path_extension = path.substr(path.size() - extension.size());
    if (caseSensitive) {
        if (extension == path_extension) {
            return true;
        }
    } else {
        std::string extUp = extension;
        std::string pathExtUp = path_extension;
        toupper(extUp);
        toupper(pathExtUp);
        if (extUp == pathExtUp) {
            return true;
        }
    }
    return false;
}

#endif // MOVE4D_UTILS_FILESYSTEM_PATH_H

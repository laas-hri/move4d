//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//


#include "move4d/mdl/CollisionScene.h"

#if __has_include(<rl/sg/bullet/Scene.h>)
#include <rl/sg/bullet/Scene.h>
FACTORY_REGISTER_FACTORY(move4d::mdl::CollisionSceneFactory, "bullet",
                         std::bind([]() { return std::make_shared<rl::sg::bullet::Scene>(); }))
#endif

#if __has_include(<rl/sg/ode/Scene.h>)
#include <rl/sg/ode/Scene.h>
FACTORY_REGISTER_FACTORY(move4d::mdl::CollisionSceneFactory, "ode",
                         std::bind([]() { return std::make_shared<rl::sg::ode::Scene>(); }))
#endif

#if __has_include(<rl/sg/pqp/Scene.h>)
#include <rl/sg/pqp/Scene.h>
FACTORY_REGISTER_FACTORY(move4d::mdl::CollisionSceneFactory, "pqp",
                         std::bind([]() { return std::make_shared<rl::sg::pqp::Scene>(); }))
#endif

#if __has_include(<rl/sg/fcl/Scene.h>)
#include <rl/sg/fcl/Scene.h>
FACTORY_REGISTER_FACTORY(move4d::mdl::CollisionSceneFactory, "fcl",
                         std::bind([]() { return std::make_shared<rl::sg::fcl::Scene>(); }))
#endif

#if __has_include(<rl/sg/solid/Scene.h>)
#include <rl/sg/solid/Scene.h>
FACTORY_REGISTER_FACTORY(move4d::mdl::CollisionSceneFactory, "solid",
                         std::bind([]() { return std::make_shared<rl::sg::solid::Scene>(); }))
#endif

//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_PLAN_TYPES_H
#define MOVE4D_PLAN_TYPES_H

#include "move4d/math/Vector.h"
#include "move4d/mdl/WorldState.h"
#include "move4d/plan/GenericFunctionTraitsWorldState.h"
#include <memory>
#include <roboptim/core/differentiable-function.hh>
#include <roboptim/core/function.hh>
#include <roboptim/core/twice-differentiable-function.hh>

namespace move4d
{

namespace plan
{

class SearchSpace;
using SearchSpacePtr = std::shared_ptr<SearchSpace>;
class CompoundSearchSpace;
using CompoundSearchSpacePtr = std::shared_ptr<CompoundSearchSpace>;
class SE2Space;
class SE3Space;
class SO2Space;
class SO3Space;
class RobotSpace;
using RobotSpacePtr = std::shared_ptr<RobotSpace>;
class RobotKinematicGroupSpace;
using RobotKinematicGroupSpacePtr = std::shared_ptr<RobotKinematicGroupSpace>;
class KinematicGraphSearchSpace;
using KinematicGraphSearchSpacePtr = std::shared_ptr<KinematicGraphSearchSpace>;
class KinematicSpaceWrapper;
using KinematicSpaceWrapperPtr = std::shared_ptr<KinematicSpaceWrapper>;
class KinematicGraphSearchSpaceBuilder;
class BaseSampler;
using BaseSamplerPtr = std::shared_ptr<BaseSampler>;
using SamplerAllocator = std::function<BaseSamplerPtr(std::shared_ptr<const SearchSpace>)>;
class Goal;
using GoalPtr = std::shared_ptr<Goal>;
class GoalSampler;
using GoalSamplerPtr = std::shared_ptr<GoalSampler>;

using WorldStateDifferentiableFunction =
    roboptim::GenericDifferentiableFunction<move4d::mdl::WorldState>;
using WorldStateFunction = roboptim::GenericFunction<move4d::mdl::WorldState>;
typedef roboptim::GenericTwiceDifferentiableFunction<move4d::mdl::WorldState>
    WorldStateTwiceDifferentiableFunction;
typedef std::shared_ptr<SearchSpace> SearchSpacePtr;

} // namespace plan

} // namespace move4d
#endif

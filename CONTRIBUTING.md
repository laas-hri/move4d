https://github.com/isocpp/CppCoreGuidelines
https://www.fluentcpp.com/2017/09/08/make-polymorphic-copy-modern-cpp/

## Coding Style

### clang-format 

#### Setting-up pre-commit hook

Follow instructions in scripts/clang-format/README.md

#### Using git clang-format

Follow some instructions in scripts/clang-format/README.md

#### Usgin clang-format executable

Use clang-format to format your code *before commiting*.
Sample command:

   clang-format -i -style=file src/**/*.cpp src/**/*.h

Ideally, configure your IDE to run clang-format automatically, e.g. everytime you save a file.

##### In QtCreator

Tools > Options > Beautifier > General:

- tick "enable auto format on file save"
- Choose Tool: "ClangFormat"
- Restrict to MIME types: "text/x-c++src; text/x-c++hdr"
- optinally tick "restrict to files contained in the current project" (recommended)

In the same window in the tab ClangFormat, fill appropiatelly the "Clang Format command", and under
"options", tick and set "use predefined style" to "File", and "fallback style" to "None"

##### In CLion

TODO

### clang-tidy

Use clang-tidy with the file provided in src/.clang-tidy

TODO(Jules): enforce this at compile time

## Thread-safety

always use mutable locks


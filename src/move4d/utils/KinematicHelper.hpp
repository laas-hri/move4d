//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
// Created by gbuisan on 02/04/19.
//

#ifndef _MOVE4D_SRC_MOVE4D_UTILS_KINEMATICHELPER_HPP
#define _MOVE4D_SRC_MOVE4D_UTILS_KINEMATICHELPER_HPP

#include "move4d/mdl/Types.h"
namespace rl
{
namespace mdl
{
class Kinematic;
class Joint;
class Transform;
class Frame;
} // namespace mdl
} // namespace rl

/**
 * @file
 * @brief helpers to perform searches in an RL kinematic chain.
 */

namespace move4d
{
namespace utils
{
bool getParentTransform(rl::mdl::Kinematic *model, rl::mdl::Frame *frame, size_t &index);
bool getParentTransform(rl::mdl::Kinematic *model, size_t frame, size_t &index);
bool getParentFrame(rl::mdl::Kinematic *model, rl::mdl::Transform *transform, size_t &index);
bool getParentFrame(rl::mdl::Kinematic *model, size_t transform_index, size_t &index);
bool getParentFrameOfFrame(rl::mdl::Kinematic *model, size_t frame, size_t &index);
/**
 * @brief isBodyOrGetPrevious like getPreviousBodyOfFrame but include frame in the search
 * @param model
 * @param frame
 * @param[out] bodyIndex
 * @param[out] frameIndex
 * @return true if a body is found
 *
 * in any cases, the output parameters are set. If frame itself is a body, then frame==frameIndex.
 *
 * @see getPreviousBodyOfFrame
 */
bool isBodyOrGetPrevious(rl::mdl::Kinematic *model, size_t frame, size_t &bodyIndex,
                         size_t &frameIndex);
/**
 * @brief getPreviousBodyOfFrame searches for the first frame before frame that is a body
 * @param model
 * @param frame frame index to start with, it is not checked if it is a body
 * @param[out] bodyIndex index to get the body with model->getBody()
 * @param[out] frameIndex of the frame (model->getFrame()
 * @return true if a body is found
 *
 * @see isBodyOrGetPrevious to include frame in the search
 *
 */
bool getPreviousBodyOfFrame(rl::mdl::Kinematic *model, size_t frame, size_t &bodyIndex,
                            size_t &frameIndex);
bool getChildTransform(rl::mdl::Kinematic *model, rl::mdl::Frame *frame, size_t &index);
rl::mdl::Joint *getDofJoint(rl::mdl::Kinematic *kin, std::size_t index, std::size_t &inDof);

std::unique_ptr<rl::mdl::Dynamic> kinematicSubChain(const mdl::SceneObject &object,
                                                    const std::string &base_link_name,
                                                    const std::string &tip_link_name,
                                                    std::vector<size_t> &bijection);
} // namespace utils
} // namespace move4d
#endif // _MOVE4D_SRC_MOVE4D_UTILS_KINEMATICHELPER_HPP

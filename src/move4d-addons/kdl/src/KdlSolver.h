//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef _ADDONS_KDL_SRC_KDLSOLVER_H
#define _ADDONS_KDL_SRC_KDLSOLVER_H

#include "move4d/mdl/kin/KinematicSolver.h"

namespace move4d
{
namespace kin
{
namespace kdl
{

class KdlSolver : public KinematicSolver
{
    MOVE4D_STATIC_LOGGER;

  public:
    bool computeInverseKinematic(const mdl::Chain &chain, const math::Transform &base_pose,
                                 const math::Transform &end_pose, math::Vector &x_in_out) override;

    bool computeForwardKinematic(const mdl::Chain &chain, const math::Transform &base_pose,
                                 math::Transform &end_pose_out, const math::Vector &x_in) override;
};

} // namespace kdl
} // namespace kin
} // namespace move4d

#endif // _ADDONS_KDL_SRC_KDLSOLVER_H

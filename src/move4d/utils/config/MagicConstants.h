//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MAGICCONSTANTS_H
#define MAGICCONSTANTS_H

#include "move4d/common.h"

namespace move4d
{

/**
 * @brief Some constants required for computation.
 */
namespace magic
{

/**
 * @brief constants that shouldn't be constants, try to limit their use as they will probably be
 * removed
 */
namespace unwanted
{

} // namespace unwanted

namespace view
{

static const Real PlayMotionDefaultFPS = 30;

} // namespace view

} // namespace magic

} // namespace move4d

#endif // MAGICCONSTANTS_H

//
// Move4D
// Copyright (C) 2019 LAAS-CNRS
//
// This file is part of Move4D.
//
// Move4D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Move4D is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Move4D.  If not, see <https://www.gnu.org/licenses/>.
//
#ifndef MOVE4D_PLAN_OMPL_GOALLIST_H
#define MOVE4D_PLAN_OMPL_GOALLIST_H

#include <ompl/base/goals/GoalSampleableRegion.h>

namespace move4d
{
namespace plan
{
namespace ompl
{

class GoalList : public ::ompl::base::GoalSampleableRegion
{
  public:
    using Item = std::shared_ptr<const ::ompl::base::GoalSampleableRegion>;

    GoalList(const ::ompl::base::SpaceInformationPtr &si, std::vector<Item> goals)
        : ::ompl::base::GoalSampleableRegion(si), _goals(std::move(goals))
    {
    }

    const std::vector<Item> &getGoals() const { return _goals; }
    std::vector<Item> &getGoals() { return _goals; }
    void setGoals(std::vector<Item> goals) { _goals = std::move(goals); }

    double distanceGoal(const ::ompl::base::State *st) const override;
    void sampleGoal(::ompl::base::State *st) const override;
    unsigned int maxSampleCount() const override;

  private:
    void
    setupItems(const std::vector<std::shared_ptr<const ::ompl::base::GoalSampleableRegion>> &goals);
    std::vector<Item> _goals;
};

} // namespace ompl
} // namespace plan
} // namespace move4d

#endif // MOVE4D_PLAN_OMPL_GOALLIST_H
